Timestamp GlobalTime(s) Dose(Mrad)
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'TEMPSENS_1': 957}
34 {'TEMPSENS_1': 953}
36 {'TEMPSENS_1': 951}
38 {'TEMPSENS_1': 955}
40 {'TEMPSENS_1': 951}
42 {'TEMPSENS_1': 952}
44 {'TEMPSENS_1': 954}
46 {'TEMPSENS_1': 949}
48 {'TEMPSENS_1': 949}
50 {'TEMPSENS_1': 948}
52 {'TEMPSENS_1': 956}
54 {'TEMPSENS_1': 946}
56 {'TEMPSENS_1': 951}
58 {'TEMPSENS_1': 944}
60 {'TEMPSENS_1': 950}
62 {'TEMPSENS_1': 954}
33 {'TEMPSENS_1': 1392}
35 {'TEMPSENS_1': 1392}
37 {'TEMPSENS_1': 1392}
39 {'TEMPSENS_1': 1392}
41 {'TEMPSENS_1': 1392}
43 {'TEMPSENS_1': 1393}
45 {'TEMPSENS_1': 1393}
47 {'TEMPSENS_1': 1393}
49 {'TEMPSENS_1': 1392}
51 {'TEMPSENS_1': 1393}
53 {'TEMPSENS_1': 1392}
55 {'TEMPSENS_1': 1392}
57 {'TEMPSENS_1': 1391}
59 {'TEMPSENS_1': 1393}
61 {'TEMPSENS_1': 1392}
63 {'TEMPSENS_1': 1392}
32 {'TEMPSENS_2': 939}
34 {'TEMPSENS_2': 932}
36 {'TEMPSENS_2': 930}
38 {'TEMPSENS_2': 936}
40 {'TEMPSENS_2': 928}
42 {'TEMPSENS_2': 933}
44 {'TEMPSENS_2': 930}
46 {'TEMPSENS_2': 935}
48 {'TEMPSENS_2': 928}
50 {'TEMPSENS_2': 934}
52 {'TEMPSENS_2': 933}
54 {'TEMPSENS_2': 936}
56 {'TEMPSENS_2': 928}
58 {'TEMPSENS_2': 935}
60 {'TEMPSENS_2': 931}
62 {'TEMPSENS_2': 933}
33 {'TEMPSENS_2': 1374}
35 {'TEMPSENS_2': 1375}
37 {'TEMPSENS_2': 1374}
39 {'TEMPSENS_2': 1375}
41 {'TEMPSENS_2': 1375}
43 {'TEMPSENS_2': 1374}
45 {'TEMPSENS_2': 1375}
47 {'TEMPSENS_2': 1374}
49 {'TEMPSENS_2': 1375}
51 {'TEMPSENS_2': 1375}
53 {'TEMPSENS_2': 1375}
55 {'TEMPSENS_2': 1374}
57 {'TEMPSENS_2': 1376}
59 {'TEMPSENS_2': 1375}
61 {'TEMPSENS_2': 1374}
63 {'TEMPSENS_2': 1375}
32 {'TEMPSENS_3': 916}
34 {'TEMPSENS_3': 913}
36 {'TEMPSENS_3': 911}
38 {'TEMPSENS_3': 912}
40 {'TEMPSENS_3': 915}
42 {'TEMPSENS_3': 912}
44 {'TEMPSENS_3': 910}
46 {'TEMPSENS_3': 910}
48 {'TEMPSENS_3': 911}
50 {'TEMPSENS_3': 916}
52 {'TEMPSENS_3': 916}
54 {'TEMPSENS_3': 913}
56 {'TEMPSENS_3': 911}
58 {'TEMPSENS_3': 911}
60 {'TEMPSENS_3': 912}
62 {'TEMPSENS_3': 912}
33 {'TEMPSENS_3': 1357}
35 {'TEMPSENS_3': 1357}
37 {'TEMPSENS_3': 1358}
39 {'TEMPSENS_3': 1356}
41 {'TEMPSENS_3': 1357}
43 {'TEMPSENS_3': 1357}
45 {'TEMPSENS_3': 1357}
47 {'TEMPSENS_3': 1357}
49 {'TEMPSENS_3': 1358}
51 {'TEMPSENS_3': 1357}
53 {'TEMPSENS_3': 1357}
55 {'TEMPSENS_3': 1356}
57 {'TEMPSENS_3': 1357}
59 {'TEMPSENS_3': 1357}
61 {'TEMPSENS_3': 1357}
63 {'TEMPSENS_3': 1357}
32 {'TEMPSENS_4': 913}
34 {'TEMPSENS_4': 908}
36 {'TEMPSENS_4': 915}
38 {'TEMPSENS_4': 914}
40 {'TEMPSENS_4': 916}
42 {'TEMPSENS_4': 916}
44 {'TEMPSENS_4': 910}
46 {'TEMPSENS_4': 913}
48 {'TEMPSENS_4': 911}
50 {'TEMPSENS_4': 915}
52 {'TEMPSENS_4': 908}
54 {'TEMPSENS_4': 911}
56 {'TEMPSENS_4': 914}
58 {'TEMPSENS_4': 913}
60 {'TEMPSENS_4': 914}
62 {'TEMPSENS_4': 910}
33 {'TEMPSENS_4': 1357}
35 {'TEMPSENS_4': 1356}
37 {'TEMPSENS_4': 1357}
39 {'TEMPSENS_4': 1356}
41 {'TEMPSENS_4': 1356}
43 {'TEMPSENS_4': 1356}
45 {'TEMPSENS_4': 1357}
47 {'TEMPSENS_4': 1357}
49 {'TEMPSENS_4': 1356}
51 {'TEMPSENS_4': 1356}
53 {'TEMPSENS_4': 1357}
55 {'TEMPSENS_4': 1356}
57 {'TEMPSENS_4': 1356}
59 {'TEMPSENS_4': 1357}
61 {'TEMPSENS_4': 1356}
63 {'TEMPSENS_4': 1357}
32 {'RADSENS_1': 2558}
34 {'RADSENS_1': 2565}
36 {'RADSENS_1': 2562}
38 {'RADSENS_1': 2562}
40 {'RADSENS_1': 2561}
42 {'RADSENS_1': 2560}
44 {'RADSENS_1': 2556}
46 {'RADSENS_1': 2558}
48 {'RADSENS_1': 2561}
50 {'RADSENS_1': 2560}
52 {'RADSENS_1': 2562}
54 {'RADSENS_1': 2558}
56 {'RADSENS_1': 2560}
58 {'RADSENS_1': 2561}
60 {'RADSENS_1': 2564}
62 {'RADSENS_1': 2565}
33 {'RADSENS_1': 2914}
35 {'RADSENS_1': 2915}
37 {'RADSENS_1': 2914}
39 {'RADSENS_1': 2915}
41 {'RADSENS_1': 2914}
43 {'RADSENS_1': 2916}
45 {'RADSENS_1': 2916}
47 {'RADSENS_1': 2915}
49 {'RADSENS_1': 2914}
51 {'RADSENS_1': 2914}
53 {'RADSENS_1': 2916}
55 {'RADSENS_1': 2915}
57 {'RADSENS_1': 2915}
59 {'RADSENS_1': 2915}
61 {'RADSENS_1': 2915}
63 {'RADSENS_1': 2915}
32 {'RADSENS_2': 2541}
34 {'RADSENS_2': 2542}
36 {'RADSENS_2': 2544}
38 {'RADSENS_2': 2547}
40 {'RADSENS_2': 2548}
42 {'RADSENS_2': 2543}
44 {'RADSENS_2': 2544}
46 {'RADSENS_2': 2547}
48 {'RADSENS_2': 2544}
50 {'RADSENS_2': 2549}
52 {'RADSENS_2': 2545}
54 {'RADSENS_2': 2540}
56 {'RADSENS_2': 2547}
58 {'RADSENS_2': 2547}
60 {'RADSENS_2': 2542}
62 {'RADSENS_2': 2544}
33 {'RADSENS_2': 2901}
35 {'RADSENS_2': 2900}
37 {'RADSENS_2': 2900}
39 {'RADSENS_2': 2900}
41 {'RADSENS_2': 2899}
43 {'RADSENS_2': 2901}
45 {'RADSENS_2': 2900}
47 {'RADSENS_2': 2900}
49 {'RADSENS_2': 2900}
51 {'RADSENS_2': 2899}
53 {'RADSENS_2': 2900}
55 {'RADSENS_2': 2899}
57 {'RADSENS_2': 2899}
59 {'RADSENS_2': 2899}
61 {'RADSENS_2': 2900}
63 {'RADSENS_2': 2899}
32 {'RADSENS_3': 2528}
34 {'RADSENS_3': 2524}
36 {'RADSENS_3': 2528}
38 {'RADSENS_3': 2526}
40 {'RADSENS_3': 2522}
42 {'RADSENS_3': 2521}
44 {'RADSENS_3': 2525}
46 {'RADSENS_3': 2527}
48 {'RADSENS_3': 2524}
50 {'RADSENS_3': 2524}
52 {'RADSENS_3': 2528}
54 {'RADSENS_3': 2530}
56 {'RADSENS_3': 2525}
58 {'RADSENS_3': 2526}
60 {'RADSENS_3': 2528}
62 {'RADSENS_3': 2527}
33 {'RADSENS_3': 2883}
35 {'RADSENS_3': 2884}
37 {'RADSENS_3': 2883}
39 {'RADSENS_3': 2883}
41 {'RADSENS_3': 2884}
43 {'RADSENS_3': 2884}
45 {'RADSENS_3': 2883}
47 {'RADSENS_3': 2885}
49 {'RADSENS_3': 2883}
51 {'RADSENS_3': 2884}
53 {'RADSENS_3': 2884}
55 {'RADSENS_3': 2883}
57 {'RADSENS_3': 2883}
59 {'RADSENS_3': 2883}
61 {'RADSENS_3': 2882}
63 {'RADSENS_3': 2884}
32 {'RADSENS_4': 2521}
34 {'RADSENS_4': 2521}
36 {'RADSENS_4': 2528}
38 {'RADSENS_4': 2525}
40 {'RADSENS_4': 2518}
42 {'RADSENS_4': 2519}
44 {'RADSENS_4': 2518}
46 {'RADSENS_4': 2524}
48 {'RADSENS_4': 2521}
50 {'RADSENS_4': 2521}
52 {'RADSENS_4': 2522}
54 {'RADSENS_4': 2521}
56 {'RADSENS_4': 2519}
58 {'RADSENS_4': 2524}
60 {'RADSENS_4': 2520}
62 {'RADSENS_4': 2519}
33 {'RADSENS_4': 2877}
35 {'RADSENS_4': 2878}
37 {'RADSENS_4': 2877}
39 {'RADSENS_4': 2877}
41 {'RADSENS_4': 2878}
43 {'RADSENS_4': 2877}
45 {'RADSENS_4': 2878}
47 {'RADSENS_4': 2877}
49 {'RADSENS_4': 2878}
51 {'RADSENS_4': 2877}
53 {'RADSENS_4': 2878}
55 {'RADSENS_4': 2878}
57 {'RADSENS_4': 2877}
59 {'RADSENS_4': 2877}
61 {'RADSENS_4': 2877}
63 {'RADSENS_4': 2877}
