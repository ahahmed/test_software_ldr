Timestamp GlobalTime(s) Dose(Mrad)
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'TEMPSENS_1': 953}
34 {'TEMPSENS_1': 950}
36 {'TEMPSENS_1': 947}
38 {'TEMPSENS_1': 950}
40 {'TEMPSENS_1': 948}
42 {'TEMPSENS_1': 948}
44 {'TEMPSENS_1': 951}
46 {'TEMPSENS_1': 945}
48 {'TEMPSENS_1': 946}
50 {'TEMPSENS_1': 944}
52 {'TEMPSENS_1': 952}
54 {'TEMPSENS_1': 942}
56 {'TEMPSENS_1': 948}
58 {'TEMPSENS_1': 941}
60 {'TEMPSENS_1': 946}
62 {'TEMPSENS_1': 951}
33 {'TEMPSENS_1': 1390}
35 {'TEMPSENS_1': 1390}
37 {'TEMPSENS_1': 1389}
39 {'TEMPSENS_1': 1390}
41 {'TEMPSENS_1': 1390}
43 {'TEMPSENS_1': 1390}
45 {'TEMPSENS_1': 1390}
47 {'TEMPSENS_1': 1390}
49 {'TEMPSENS_1': 1390}
51 {'TEMPSENS_1': 1390}
53 {'TEMPSENS_1': 1389}
55 {'TEMPSENS_1': 1391}
57 {'TEMPSENS_1': 1389}
59 {'TEMPSENS_1': 1390}
61 {'TEMPSENS_1': 1390}
63 {'TEMPSENS_1': 1389}
32 {'TEMPSENS_2': 935}
34 {'TEMPSENS_2': 928}
36 {'TEMPSENS_2': 926}
38 {'TEMPSENS_2': 932}
40 {'TEMPSENS_2': 923}
42 {'TEMPSENS_2': 930}
44 {'TEMPSENS_2': 927}
46 {'TEMPSENS_2': 931}
48 {'TEMPSENS_2': 925}
50 {'TEMPSENS_2': 930}
52 {'TEMPSENS_2': 931}
54 {'TEMPSENS_2': 933}
56 {'TEMPSENS_2': 925}
58 {'TEMPSENS_2': 931}
60 {'TEMPSENS_2': 928}
62 {'TEMPSENS_2': 930}
33 {'TEMPSENS_2': 1371}
35 {'TEMPSENS_2': 1372}
37 {'TEMPSENS_2': 1373}
39 {'TEMPSENS_2': 1372}
41 {'TEMPSENS_2': 1372}
43 {'TEMPSENS_2': 1373}
45 {'TEMPSENS_2': 1373}
47 {'TEMPSENS_2': 1372}
49 {'TEMPSENS_2': 1373}
51 {'TEMPSENS_2': 1371}
53 {'TEMPSENS_2': 1372}
55 {'TEMPSENS_2': 1372}
57 {'TEMPSENS_2': 1373}
59 {'TEMPSENS_2': 1372}
61 {'TEMPSENS_2': 1372}
63 {'TEMPSENS_2': 1372}
32 {'TEMPSENS_3': 912}
34 {'TEMPSENS_3': 909}
36 {'TEMPSENS_3': 907}
38 {'TEMPSENS_3': 908}
40 {'TEMPSENS_3': 911}
42 {'TEMPSENS_3': 908}
44 {'TEMPSENS_3': 906}
46 {'TEMPSENS_3': 908}
48 {'TEMPSENS_3': 908}
50 {'TEMPSENS_3': 912}
52 {'TEMPSENS_3': 913}
54 {'TEMPSENS_3': 909}
56 {'TEMPSENS_3': 908}
58 {'TEMPSENS_3': 907}
60 {'TEMPSENS_3': 907}
62 {'TEMPSENS_3': 909}
33 {'TEMPSENS_3': 1354}
35 {'TEMPSENS_3': 1355}
37 {'TEMPSENS_3': 1355}
39 {'TEMPSENS_3': 1355}
41 {'TEMPSENS_3': 1354}
43 {'TEMPSENS_3': 1355}
45 {'TEMPSENS_3': 1354}
47 {'TEMPSENS_3': 1355}
49 {'TEMPSENS_3': 1354}
51 {'TEMPSENS_3': 1353}
53 {'TEMPSENS_3': 1353}
55 {'TEMPSENS_3': 1354}
57 {'TEMPSENS_3': 1354}
59 {'TEMPSENS_3': 1353}
61 {'TEMPSENS_3': 1354}
63 {'TEMPSENS_3': 1354}
32 {'TEMPSENS_4': 910}
34 {'TEMPSENS_4': 906}
36 {'TEMPSENS_4': 912}
38 {'TEMPSENS_4': 912}
40 {'TEMPSENS_4': 913}
42 {'TEMPSENS_4': 912}
44 {'TEMPSENS_4': 907}
46 {'TEMPSENS_4': 910}
48 {'TEMPSENS_4': 907}
50 {'TEMPSENS_4': 911}
52 {'TEMPSENS_4': 906}
54 {'TEMPSENS_4': 908}
56 {'TEMPSENS_4': 910}
58 {'TEMPSENS_4': 911}
60 {'TEMPSENS_4': 911}
62 {'TEMPSENS_4': 908}
33 {'TEMPSENS_4': 1354}
35 {'TEMPSENS_4': 1354}
37 {'TEMPSENS_4': 1353}
39 {'TEMPSENS_4': 1354}
41 {'TEMPSENS_4': 1353}
43 {'TEMPSENS_4': 1354}
45 {'TEMPSENS_4': 1354}
47 {'TEMPSENS_4': 1354}
49 {'TEMPSENS_4': 1354}
51 {'TEMPSENS_4': 1354}
53 {'TEMPSENS_4': 1354}
55 {'TEMPSENS_4': 1355}
57 {'TEMPSENS_4': 1354}
59 {'TEMPSENS_4': 1354}
61 {'TEMPSENS_4': 1355}
63 {'TEMPSENS_4': 1354}
32 {'RADSENS_1': 2553}
34 {'RADSENS_1': 2559}
36 {'RADSENS_1': 2557}
38 {'RADSENS_1': 2556}
40 {'RADSENS_1': 2557}
42 {'RADSENS_1': 2555}
44 {'RADSENS_1': 2551}
46 {'RADSENS_1': 2554}
48 {'RADSENS_1': 2555}
50 {'RADSENS_1': 2554}
52 {'RADSENS_1': 2557}
54 {'RADSENS_1': 2552}
56 {'RADSENS_1': 2555}
58 {'RADSENS_1': 2557}
60 {'RADSENS_1': 2557}
62 {'RADSENS_1': 2563}
33 {'RADSENS_1': 2911}
35 {'RADSENS_1': 2910}
37 {'RADSENS_1': 2910}
39 {'RADSENS_1': 2911}
41 {'RADSENS_1': 2911}
43 {'RADSENS_1': 2912}
45 {'RADSENS_1': 2910}
47 {'RADSENS_1': 2910}
49 {'RADSENS_1': 2910}
51 {'RADSENS_1': 2910}
53 {'RADSENS_1': 2910}
55 {'RADSENS_1': 2910}
57 {'RADSENS_1': 2911}
59 {'RADSENS_1': 2910}
61 {'RADSENS_1': 2911}
63 {'RADSENS_1': 2910}
32 {'RADSENS_2': 2535}
34 {'RADSENS_2': 2537}
36 {'RADSENS_2': 2538}
38 {'RADSENS_2': 2541}
40 {'RADSENS_2': 2543}
42 {'RADSENS_2': 2536}
44 {'RADSENS_2': 2538}
46 {'RADSENS_2': 2541}
48 {'RADSENS_2': 2540}
50 {'RADSENS_2': 2544}
52 {'RADSENS_2': 2539}
54 {'RADSENS_2': 2535}
56 {'RADSENS_2': 2542}
58 {'RADSENS_2': 2541}
60 {'RADSENS_2': 2538}
62 {'RADSENS_2': 2540}
33 {'RADSENS_2': 2896}
35 {'RADSENS_2': 2895}
37 {'RADSENS_2': 2895}
39 {'RADSENS_2': 2894}
41 {'RADSENS_2': 2894}
43 {'RADSENS_2': 2895}
45 {'RADSENS_2': 2894}
47 {'RADSENS_2': 2894}
49 {'RADSENS_2': 2896}
51 {'RADSENS_2': 2894}
53 {'RADSENS_2': 2895}
55 {'RADSENS_2': 2896}
57 {'RADSENS_2': 2895}
59 {'RADSENS_2': 2896}
61 {'RADSENS_2': 2895}
63 {'RADSENS_2': 2895}
32 {'RADSENS_3': 2523}
34 {'RADSENS_3': 2520}
36 {'RADSENS_3': 2523}
38 {'RADSENS_3': 2522}
40 {'RADSENS_3': 2517}
42 {'RADSENS_3': 2517}
44 {'RADSENS_3': 2520}
46 {'RADSENS_3': 2522}
48 {'RADSENS_3': 2519}
50 {'RADSENS_3': 2519}
52 {'RADSENS_3': 2523}
54 {'RADSENS_3': 2524}
56 {'RADSENS_3': 2519}
58 {'RADSENS_3': 2521}
60 {'RADSENS_3': 2524}
62 {'RADSENS_3': 2520}
33 {'RADSENS_3': 2878}
35 {'RADSENS_3': 2878}
37 {'RADSENS_3': 2879}
39 {'RADSENS_3': 2879}
41 {'RADSENS_3': 2879}
43 {'RADSENS_3': 2879}
45 {'RADSENS_3': 2878}
47 {'RADSENS_3': 2880}
49 {'RADSENS_3': 2879}
51 {'RADSENS_3': 2879}
53 {'RADSENS_3': 2878}
55 {'RADSENS_3': 2878}
57 {'RADSENS_3': 2879}
59 {'RADSENS_3': 2879}
61 {'RADSENS_3': 2879}
63 {'RADSENS_3': 2878}
32 {'RADSENS_4': 2514}
34 {'RADSENS_4': 2516}
36 {'RADSENS_4': 2522}
38 {'RADSENS_4': 2519}
40 {'RADSENS_4': 2512}
42 {'RADSENS_4': 2515}
44 {'RADSENS_4': 2514}
46 {'RADSENS_4': 2519}
48 {'RADSENS_4': 2516}
50 {'RADSENS_4': 2515}
52 {'RADSENS_4': 2516}
54 {'RADSENS_4': 2514}
56 {'RADSENS_4': 2512}
58 {'RADSENS_4': 2519}
60 {'RADSENS_4': 2515}
62 {'RADSENS_4': 2513}
33 {'RADSENS_4': 2873}
35 {'RADSENS_4': 2873}
37 {'RADSENS_4': 2872}
39 {'RADSENS_4': 2872}
41 {'RADSENS_4': 2873}
43 {'RADSENS_4': 2872}
45 {'RADSENS_4': 2874}
47 {'RADSENS_4': 2873}
49 {'RADSENS_4': 2874}
51 {'RADSENS_4': 2874}
53 {'RADSENS_4': 2874}
55 {'RADSENS_4': 2874}
57 {'RADSENS_4': 2874}
59 {'RADSENS_4': 2874}
61 {'RADSENS_4': 2874}
63 {'RADSENS_4': 2873}
