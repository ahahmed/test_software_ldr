# ------------------------------------------------------------
# BDAQ53: Simple hardware test
# Basic DAQ hardware and chip configuration
#
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import unittest
import os
import time
import numpy as np
import logging
import yaml
from basil.dut import Dut
from bdaq53.rd53a import RD53A
from basil.HL.GPAC import GpioPca9554
import bdaq53
from bdaq53.needle_card import needle_card

activelanes_tx = 1
activelanes_rx = activelanes_tx

timeout = 10

loglevel = logging.getLogger('RD53A').getEffectiveLevel()



#@unittest.skip('Needs to be rewritten')
class DAQ_HW_Test(unittest.TestCase):

    def setUp(self):
        proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        with open(proj_dir + '/bdaq53/bdaq53.yaml', 'r') as f:
            cnfg = yaml.load(f)

        # Initialization
        self.chip = RD53A(cnfg)
        self.chip.init()
        self.chip.init_communication()


    def test(self):
        # Needle card tests
        conf = {'name' : 'GpioPca9554', 'type' : 'GpioPca9554', 'interface': 'intf', 'base_addr': 0x40}
        self.needle_card = needle_card(self.chip['i2c'], conf)
        self.needle_card.init()

        while True:
            self.needle_card.adc_mux('GND')
            self.needle_card.read_gpio_expander('MUX_SEL')
            print 'ADC mux: GND'
            time.sleep(3)
            self.needle_card.adc_mux('NC')
            self.needle_card.read_gpio_expander('MUX_SEL')
            print 'ADC mux: NC'
            time.sleep(3)


    def tearDown(self):
        self.chip.close()


if __name__ == '__main__':
    unittest.main()
