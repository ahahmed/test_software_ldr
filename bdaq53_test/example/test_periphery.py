#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Test if your periphery (e.g. SCC powersupply) is set up correctly
'''

import os
import time
import logging

import basil

from bdaq53.periphery import BDAQ53Periphery


settings = {
    'VINA': 1.7,
    'VIND': 1.7,
    
    'VINA_cur_lim': 0.7,
    'VIND_cur_lim': 0.7,
    
    'sensor_bias'  : -0
    }
        
if __name__ == '__main__':
    periphery = BDAQ53Periphery()
    
    periphery.power_off_sensor_bias()
    time.sleep(1)
    periphery.power_off_SCC()
    time.sleep(1)
    periphery.power_off_BDAQ()
    
    time.sleep(5)
    
    periphery.power_on_BDAQ()
    time.sleep(1)
    periphery.power_on_SCC(**settings)
    time.sleep(5)
    periphery.power_on_sensor_bias(**settings)
