#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

from basil.HL.RegisterHardwareLayer import RegisterHardwareLayer
import logging

loglevel = logging.getLogger('RD53A').getEffectiveLevel()


class aurora_rx(RegisterHardwareLayer):
    '''
    '''

    _registers = {'RESET':      {'descr': {'addr': 0, 'size': 8, 'properties': ['writeonly']}},
                  'VERSION':    {'descr': {'addr': 0, 'size': 8, 'properties': ['ro']}},

                  'EN':                 {'descr': {'addr': 2, 'size': 1, 'offset': 0}},
                  'RX_READY':           {'descr': {'addr': 2, 'size': 1, 'offset': 1, 'properties': ['readonly']}},
                  'RX_LANE_UP':         {'descr': {'addr': 2, 'size': 1, 'offset': 2, 'properties': ['readonly']}},
                  'PLL_LOCKED':         {'descr': {'addr': 2, 'size': 1, 'offset': 3, 'properties': ['readonly']}},
                  'RX_HARD_ERROR':      {'descr': {'addr': 2, 'size': 1, 'offset': 4, 'properties': ['readonly']}},
                  'RX_SOFT_ERROR':      {'descr': {'addr': 2, 'size': 1, 'offset': 5, 'properties': ['readonly']}},
                  'MGT_REF_SEL':        {'descr': {'addr': 2, 'size': 1, 'offset': 6}},
                  'USER_K_FILTER_EN' :  {'descr': {'addr': 2, 'size': 1, 'offset': 7}},

                  'LOST_COUNT':         {'descr': {'addr': 3, 'size': 8, 'properties': ['ro']}},

                  'USER_K_FILTER_MASK_1': {'descr': {'addr': 4, 'size': 8}},
                  'USER_K_FILTER_MASK_2': {'descr': {'addr': 5, 'size': 8}},
                  'USER_K_FILTER_MASK_3': {'descr': {'addr': 6, 'size': 8}},

                  'RESET_COUNTERS':     {'descr': {'addr': 7, 'size': 1, 'offset': 0}},
                  'RESET_LOGIC':        {'descr': {'addr': 7, 'size': 1, 'offset': 1}},
                  'SI570_IS_CONFIGURED':{'descr': {'addr': 7, 'size': 1, 'offset': 2}},
                  'GTX_TX_MODE':        {'descr': {'addr': 7, 'size': 1, 'offset': 3}},

                  'FRAME_COUNTER':      {'descr': {'addr': 8, 'size': 32}},
                  'SOFT_ERROR_COUNTER': {'descr': {'addr':12, 'size': 8}},
                  'HARD_ERROR_COUNTER': {'descr': {'addr':13, 'size': 8}}
                  }
    _require_version = "==2"

    def __init__(self, intf, conf):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(loglevel)

        super(aurora_rx, self).__init__(intf, conf)

    def reset(self):
        '''Soft reset the module'''
        self.RESET = 0

    def reset_counters(self):
        '''Resets the soft/hard error counters'''
        self.RESET_COUNTERS = True;
        self.RESET_COUNTERS = False;

    def reset_logic(self):
        '''Resets only the logic and the FIFOs'''
        self.RESET_LOGIC = True;
        self.RESET_LOGIC = False;

    def set_en(self, value):
        self.EN = value

    def get_en(self):
        return self.EN

    def get_rx_ready(self):
        '''Aurora link established'''
        return self.RX_READY

    def get_pll_locked(self):
        '''Aurora PLL locked'''
        return self.PLL_LOCKED

    def get_lost_count(self):
        '''Lost data due to RX FIFO overflow'''
        return self.LOST_COUNT

    def set_mgt_ref(self, value):
        '''Controls the clock multiplexer chip, which is used for providing the Aurora RX reference clock'''
        if value == "int":
            self.logger.info('MGT: Switching to on-board (Si570) oscillator')
            self.MGT_REF_SEL = 1
        elif value == "ext":
            self.logger.info('MGT: Switching to external (SMA) clock source')
            self.MGT_REF_SEL = 0

    def get_mgt_ref(self):
        value = self.MGT_REF_SEL
        if value == 0:
            return 'EXT (SMA)'
        elif value == 1:
            return 'INT (Si570)'

    def get_SOFT_ERROR_COUNTER(self):
        '''Aurora soft errors'''
        return self.SOFT_ERROR_COUNTER

    def get_SOFT_ERROR_COUNTER(self):
        '''Aurora hard errors'''
        return self.HARD_ERROR_COUNTER

    def set_USER_K_FILTER_MASK(self, mask, value):
        ''' Define up to 3 user_k patterns, which are rejected by the FPGA '''
        if mask == 1:
            self.USER_K_FILTER_MASK_1 = value
        elif mask == 2:
            self.USER_K_FILTER_MASK_2 = value
        elif mask == 3:
            self.USER_K_FILTER_MASK_3 = value
        else:
            self.logger.error("USER_K_FILTER_MASK: Parameters: mask_number[1,2], value[byte]")

    def get_USER_K_FILTER_MASK(self):
        return self.USER_K_FILTER_MASK

    def set_USER_K_FILTER_EN(self, value):
        self.USER_K_FILTER_EN = value

    def get_USER_K_FILTER_EN(self):
        return self.USER_K_FILTER_EN

    def get_frame_count(self):
        return self.FRAME_COUNTER

    def set_Si570_is_configured(self, value):
        '''Emulates a "is_configured" register for the Si570 reference clock chip'''
        self.SI570_IS_CONFIGURED = value

    def get_Si570_is_configured(self):
        return self.SI570_IS_CONFIGURED

    def set_GTX_TX_MODE(self, value):
        '''
            Set operation mode for the GTX transmitter
            - CMD:     Command data is sent via GTX
            - CLK640:  640 MHz clock output
            Modes can be changed at any time after get_pll_locked==1
        '''
        if value == 'CMD':
            self.GTX_TX_MODE = 0
            self.logger.debug("GTX TX MODE: Command data")
        elif value == 'CLK640':
            self.GTX_TX_MODE = 1
            self.logger.debug("GTX TX MODE: 640 MHz clock")
        else:
            self.logger.error("GTX TX MODE: parameter out of range")
