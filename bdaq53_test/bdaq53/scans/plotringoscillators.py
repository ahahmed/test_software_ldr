import time
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import os
import glob
import datetime
#from mon1 import monitor_datalogger_power_supply
from matplotlib.pyplot import figure
import basil
import visa
from operator import itemgetter
import ast
import sys, termios, tty, os, time
import csv
import tables as tb
from bitarray import bitarray
from datetime import datetime
from bdaq53.fifo_readout import FifoReadout
import logging
import matplotlib.pyplot as plt


def list_folders(directory_ringoscillators):
	dir_array = os.listdir(directory_ringoscillators)
	return dir_array

def get_elements_line(x):
	array = []
	for i in range(len(x.split(' '))):
		a = x.split(' ')[i]
		if a != '':
			array.append(a)
	return array


def get_ring_vector(dir_array,directory_ringoscillators):
	r0 = []
	r1 = []
	r2 = []
	r3 = []
	r4 = []
	r5 = []
	r6 = []
	r7 = []
	t = []
	idcount = []
	temperature = []
	global_pulse = 9 #value for global pulse equal to 409.6 
	j = 0
	for directory in dir_array:
		idcount.append(j)
		j = j+1
		dir_textfile = directory_ringoscillators + '/' + directory+'/output_data.dat'
		f = open(dir_textfile, 'r')
		lines = f.readlines()
		for i,x in enumerate(lines):
			if i == 1:				
				t.append('%.2f'%(float(x.split(' ')[2])))
				temperature.append('%.1f'%(float(x.split(' ')[0])))
			elif i == global_pulse:
				counts = get_elements_line(x)			
				r0.append(float(counts[1]))
				r1.append(float(counts[2]))
				r2.append(float(counts[3]))
				r3.append(float(counts[4]))
				r4.append(float(counts[5]))
				r5.append(float(counts[6]))
				r6.append(float(counts[7]))
				r7.append(float(counts[8]))
				
			else:
				a = 0
		f.close()
	ring = [r0,r1,r2,r3,r4,r5,r6,r7]
	return ring, t, temperature, idcount

def plot_ro(ring, t, temperature,directory_ringoscillators,directory_plots,idcount):
	line_color = ['#ffcc00','#ff6633','#ff0066','#cc33ff', '#3333ff', '#0099ff', '#33cc66', '#99cc33','#11cc55']
	legend = ['R0','R1','R2','R3','R4','R5','R6','R7']
	figure(figsize=(15, 8))
	for i in range(len(ring)):
			plt.scatter(idcount,ring[i], label = legend[i],s=0.5, color = line_color[i])


	
	plt.legend(loc=4)	
	plt.title('Ring Oscillators counts')
	plt.xlabel('time (s)')
	plt.ylabel('Counts')
	
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")	
	plt.savefig(directory_plots+'/'+timestamp+'_ringOscillators_counts_idcount.png', dpi=1000)
	plt.scatter(idcount,temperature, label = 'Temperature', s=0.5,color = line_color[8])
	plt.savefig(directory_plots+'/'+timestamp+'_ringOscillators_counts_andTemp_idcount.png')

	plt.scatter(t,idcount, label = 'time', s=0.5,color = line_color[8])
	plt.savefig(directory_plots+'/'+timestamp+'_ringOscillators_time.png')
	

if __name__ == "__main__":

    directory = '00_TEST_INFO'	
    if not os.path.exists(directory):
	os.makedirs(directory)	

    directory_plots = '00_TEST_INFO/ring_oscillator_plots'		
    if not os.path.exists(directory_plots):
	os.makedirs(directory_plots)


    directory_ringoscillators = '00_TEST_INFO_rad/ring_oscillators'
    dir_array = list_folders(directory_ringoscillators)
    ring, t, temperature,idcount = get_ring_vector(dir_array,directory_ringoscillators)
    plot_ro(ring, t, temperature,directory_ringoscillators,directory_plots,idcount)





