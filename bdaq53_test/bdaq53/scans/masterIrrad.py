import time
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import os
import glob
import datetime
from bdaq53.fifo_readout import FifoReadout
import logging
import sys, select
from luismi_scans.glasgow import radDoses
import datetime
import calendar

#TODO go to rad_doses.py and define dose, steps and starting dose


def input_logfile(directory_textfile, text_input):
	log = open (directory_startstop_log , "a")
	log.write(time.strftime("%d_%m_%Y_%H_%M_%S") + ' ' + text_input + ' '  + '\n') 
	log.close()

if __name__ == '__main__':
	globalTime = time.time()
	directory = '00_TEST_INFO'	
	if not os.path.exists(directory):
    		os.makedirs(directory)	


	directory = '00_TEST_INFO/radiation_doses'	
	if not os.path.exists(directory):
    		os.makedirs(directory)
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")

	directory_rad_log = directory + '/' + timestamp +"_radiation_doses_log.txt"
	out = open (directory_rad_log , "a")
	out.write('Dose(Mrad) Time_stamp time_intervals')  

	directory_startstop_log = directory + '/' + timestamp +"_radiation_startstop_log.txt"
	out_startstop_log = open (directory_startstop_log , "a")
	out_startstop_log.write('Time_stamp Action' + '\n')  
	out_startstop_log.close()

	#start the xrays
	#Keeping the Shutter out during irradiation:
	os.system('python LeadShutterOut.py')
	input_logfile(directory_startstop_log, 'STARTING XRAYS')
	
	print("STARTING xrays...")
	t0 = time.time()
	out.write('Dose(Mrad) Time_stamp time_rradiated_last_inerval(s) GlobalTime(s) taken when the dose is reached before full charact' + '\n') 
	
	out.write('0 ' + time.strftime("%d_%m_%Y_%H_%M_%S") + '  ' + str(time.time()-t0) + ' ' + str( time.time()-globalTime)+ '\n')
	time_intervals, dose_steps_mrad = radDoses.times_tests()
	out.close()

	while(time_intervals):
		t1 = time.time()
		t_loop = t1 - t0
		if(t_loop < time_intervals[0]):
			dose =  dose_steps_mrad[0]
			print("WHILE IRRADIATION MODE ")
			os.system('cp default_chip_whileIrradiation.yaml default_chip.yaml')
			os.system("python whileIrradiation.py "+ str(globalTime) + ' ' + str(dose))
			print("Time until next FULL CHARACTERIZATION: " +  str(time_intervals[0]-t_loop) + '(s)')
			print("It will be reached in ")
			print radDoses.time_after_sec(time_intervals[0]-t_loop)
			print("And the dose reached will be: "+ str(dose_steps_mrad[0])+' Mrad')

		
		else:
			
			#stop the xrays
			print('Stopping xrays....')
			
			os.system('python LeadShutterIn.py')
			input_logfile(directory_startstop_log, 'STOPPING XRAYS')
			
			dose =  dose_steps_mrad[0]
			out = open (directory_rad_log , "a")
			out.write(str(dose) + ' ' +  time.strftime("%d_%m_%Y_%H_%M_%S") + '  ' + str(time.time()-t0) +  ' ' + str( time.time()-globalTime)+'\n')
			out.close()
			#Launching the chip characterization, sending as a parameter the dose reached:
			print("STARTING FULL CHARACTERIZATION ")
			os.system("python ChipFullCharacterization.py " + str(globalTime) + ' ' + str(dose))
			#os.system("python ChipFullCharacterization_test.py " + str(globalTime) + ' ' + str(dose))




			#Re-calculating time vector and calculating date of next measurement:	
			del dose_steps_mrad[0]
			del time_intervals[0]

			if(time_intervals):
				print "Last time interval vector after removal in seconds is is: ", time_intervals
				print "Next measurement in ", time_intervals[0]/60, "minutes"
				now = datetime.datetime.now()
				print "Time right now is: ", now 
				now_plus_time_interval = now + datetime.timedelta(minutes = time_intervals[0]/60)
				print "Next measurement will be done in: ", now_plus_time_interval 
				print "******************************************************************************************"
				print "__________________________________________________________________________________________"
				#re-start the xrays
				print('Re-starting xrays....')
				os.system('python LeadShutterOut.py')
				input_logfile(directory_startstop_log, 'STARTING XRAYS')
				t0 = time.time()
			else:
				print "END OF THE RADIATION CAMPAIGN"
				while(True):
					#TODO here launch the script for keeping the chip running during annealing
					print("WHILE ANNEALING MODE ")
					os.system("python annealing.py ")
					time.sleep(2)
				 


	
	
'''dose = starting_dose +dose_rate_Mrad_h*(time.time()-t)/3600'''







