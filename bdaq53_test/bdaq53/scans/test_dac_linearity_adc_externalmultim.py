#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic test scans over the values of the selected DAC and
    measures the resulting analog value with the chip's internal ADC.
'''

import tables as tb
from tqdm import tqdm

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting

import socket
import sys
import time
import re
import os
import matplotlib.pyplot as plt

local_configuration = {
    # Scan parameters
    'DAC': 'VCAL_HIGH',
    'type': 'U',
    'value_start': 0,
    'value_stop': 4096,
    'value_step': 100}


class DacTable(tb.IsDescription):
    scan_param_id = tb.UInt32Col(pos=0)
    dac = tb.UInt32Col(pos=1)
    voltage = tb.Float32Col(pos=2)


class DACLinTest(ScanBase):
    scan_id = "dac_linearity_test"

    def configure(self, **kwargs):
        self.chip.set_dacs(**kwargs)
        self.chip.enable_monitor_filter()
        self.chip.enable_monitor_data()


    def plot_dac_lin_multim(self,directory_textfile,directory_notextfile, dac_vector):
	f =  open(directory_textfile, 'r')
	lines = f.readlines()
	multimeter = []
	dac_linearity = []
	dose = []
	offset = 4 #When the real data starts, the multimeter values
	for i,x in enumerate(lines):
		if i>0:
			multimeter = []
			w=i-1
			dose.append(x.split(' ')[3])
			temperature = '%.2f'%(float(x.split(' ')[1]))
			legend = "DAC linearity, Dose: " + str(dose[w]) + ' Mrad, T='+ str(temperature) + '(C)'
			for j in range(offset, offset + len(dac_vector)):
				#print 'Next value multimeter: ', x.split(' ')[j]
				#raw_input('wait')
				multimeter.append(float(x.split(' ')[j]))
				
			plt.figure(w, figsize = (10,5))

			plt.scatter(dac_vector, multimeter, label = legend)
			plt.legend(loc=4)
			plt.title('DAC linearity with multimeter, '+str(dose[w]) + ' Mrad')
			plt.ylabel('Voltage multimeter (mV)')
			plt.xlabel('VCAL_HIGH (LSB)')
			plt.savefig(directory_notextfile+'/'+str(dose[w])+'Mrad_DAC_linearity_withMultim.png')
			plt.close()

			
	f.close()
	
	

    def plot_dac_lin_multim_all(self,directory_textfile,directory_notextfile, dac_vector):
	line_color = ['#ffcc00','#ff6633','#ff0066','#cc33ff', '#3333ff', '#0099ff', '#33cc66', '#99cc33', '#ffcc22','#ff6677','#ff0099','#cc3355', '#333355', '#009922', '#33cc11', '#99cc88', '#ffcc77','#ff6600','#ff0088','#5533ff', '#ff33ff', '#6699ff', '#77cc66', '#00cc33', '#ffcc99','#ff6633','#770066','#cc33ff', '#2233ff', '#8899ff', '#99cc66', '#66cc33', '#998877','#887766','#776655','#667755','#665544','#554433','#332211'];
	f =  open(directory_textfile, 'r')
	lines = f.readlines()
	multimeter = []
	dac_linearity = []
	dose = []
	#ax = []
	all_multimeter = []
	legend = []
	offset = 4 #When the real data starts, the multimeter values
	plt.figure(8000, figsize = (10,8))
	for i,x in enumerate(lines):
		if i>0:
			multimeter = []
			w=i-1
			dose.append(x.split(' ')[3])
			temperature = '%.2f'%(float(x.split(' ')[1]))
			legend.append("DAC linearity, Dose: " + str(dose[w]) + ' Mrad, T='+ str(temperature) + '(C)')
			for j in range(offset, offset + len(dac_vector)):
				#print 'Next value multimeter: ', x.split(' ')[j]
				#raw_input('wait')
				multimeter.append(float(x.split(' ')[j]))
			all_multimeter.append(multimeter)	
	#print legend
	#fig, ax = plt.subplots(figsize=(10, 5)))
	#print all_multimeter
	#raw_input('press enter') 
	for i in range(len(all_multimeter)):
			plt.scatter(dac_vector, all_multimeter[i], label = legend[i], color = line_color[i])
			#plt.show()
			#raw_input('pressbutton')
	plt.legend(loc=4)
	plt.title('DAC linearity with multimeter')
	plt.ylabel('Voltage multimeter (mV)')
	plt.xlabel('VCAL_HIGH (LSB)')
	plt.savefig(directory_notextfile+'/'+'AllDoses_Mrad_DAC_linearity_withMultim.png')

			
	f.close()

    def scan(self, **kwargs):
        values = range(kwargs.get('value_start'), kwargs.get('value_stop'), kwargs.get('value_step'))
	legend = ' '
	dac_vector = []
	for i in values:
		legend = legend + str(i) + ' '
		dac_vector.append(i)
	
        address = kwargs.get('DAC')
	print address
	typ = kwargs.get('type')

	globalTime = sys.argv[1]
	dose = sys.argv[2]

	#Multimeter communication:

	# Create a TCP/IP socket
	sockXY = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sockC = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	# connecting to CurrentMeasurer.vi
	server_address = ('194.36.1.188', 6346)
	print >> sys.stderr, 'connecting to %s port %s' % server_address
	sockC.connect(server_address)
	time.sleep(1)

	##### USER INPUT =====
	current_measure_command = 'km'
	message = current_measure_command


	directory = '00_TEST_INFO'
	if not os.path.exists(directory):
    		os.makedirs(directory)
	
	directory = directory + '/dac_linearity_multimeter'
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	if not os.path.exists(directory):
    		os.makedirs(directory)
		out = open( directory + '/'+"ACCUMULATIVE_DAC_linearity_withMultimeter.txt", "a")		
		tobewritten = "%Timestamp  T_FMC(C) GlobalTime(s)  DoseRange(Mrad) " + legend + "\n"		
		out.write(tobewritten) 
		out.close()
	directory_textfile = directory + '/'+"ACCUMULATIVE_DAC_linearity_withMultimeter.txt"
	directory_notextfile = directory




        self.logger.info('Starting scan...')

        self.dac_data_table = self.h5_file.create_table(self.h5_file.root, name='dac_data',
                                                        title='dac_data', description=DacTable)
        row = self.dac_data_table.row

	T_FMC = self.chip._measure_temperature_ntc_CERNFMC()
	tobewritten =  str(timestamp) + " " + str("%.4f" %T_FMC) + " " + str(time.time()-float(globalTime))+ ' ' + dose + ' ' 

        for scan_param_id, value in tqdm(enumerate(values), total=len(values), unit=' Values'):
            with self.readout(scan_param_id=scan_param_id):
                self.chip.write_register(register=address, data=value, write=True)#here the DAC value is written
		print address		
                self.chip.get_ADC(typ, address)

		#measuremnt with multimeter:
		sockC.sendall(message)
		msgReceivedC = sockC.recv(1024)
		end = time.time()
		_c = [float(x) for x in re.findall("-?\d+.?\d*(?:[Ee]-\d+)?", msgReceivedC)]
		multimeter = str(_c[0])		
		
		tobewritten = tobewritten + str(multimeter) + ' '



                try:
                    row['voltage'] = self.periphery.get_multimeter_voltage()
                except:
                    pass
                row['scan_param_id'] = scan_param_id
                row['dac'] = value
                row.append()
                self.dac_data_table.flush()
	out = open( directory_textfile, "a")	
	out.write(tobewritten +  "\n")
	out.close()


	self.plot_dac_lin_multim(directory_textfile,directory_notextfile, dac_vector)
   	self.plot_dac_lin_multim_all(directory_textfile,directory_notextfile, dac_vector)


        self.logger.success('Scan finished')

    def analyze(self, create_pdf=True):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5') as a:
            a.analyze_adc_data()

        if create_pdf:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.logger.info('Creating selected plots...')
                p.create_parameter_page()
                p.create_dac_linearity_plot()


if __name__ == "__main__":
    scan = DACLinTest()
    scan.start(**local_configuration)
    scan.close()
