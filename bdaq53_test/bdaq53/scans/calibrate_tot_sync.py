#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
   Creates the ToT calibration per pixel using the charge injection cicuit.
'''

from tqdm import tqdm
import tables as tb
import numpy as np

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting
from bdaq53.analysis import analysis_utils as au

local_configuration = {
    # Scan parameters
    'start_column': 0,
    'stop_column': 128,
    'start_row': 0,
    'stop_row': 192,
    'maskfile': None,
    'mask_diff': False,

    'VCAL_MED': 500,
    'VCAL_HIGH_start': 500,
    'VCAL_HIGH_stop': 4096,
    'VCAL_HIGH_step': 100
}


def get_rms_from_2d_hist(counts, bin_positions, result):
    ''' Compute the standard deviation along the last axis of a 2d histogram
    by repeating elements of (bin_positions) a number of (counts) times.

    Parameters
    ----------
        counts: Array containing data to be averaged
        bin_positions: array_like associated with the values in counts.
        result: array like
            Filled with result
    '''

    for i in tqdm(np.arange(0, counts.shape[0]), unit=' Pixels', unit_scale=True):  # pixel loop
        for j in np.arange(0, counts.shape[1]):  # parameter loop
            if np.any(counts[i][j]):
                result[i][j] = np.std(np.repeat(bin_positions, counts[i][j]))
            else:
                result[i][j] = np.NaN


def hist_2d_mean(counts, bin_positions, axis=0, param_count=0):
    ''' Mean along axis 2d histogram of two data samples.
        Parameters

        ----------
        counts : Array containing data to be histogramed
        bin_positions: array_like associated with the values in counts.
        axis: None or int
        param_count : binning values
    '''

    mean = au.get_mean_from_histogram(counts, bin_positions, axis)
    x_bins = np.arange(param_count)
    x = np.tile(x_bins, mean[:].shape[0])
    y = mean[:].ravel()
    histogram = np.histogram2d(x, y, bins=(param_count, counts.shape[2]), range=((x_bins.min(), len(x_bins)), (0, counts.shape[2])))[0]
    return histogram


class TotCalibration(ScanBase):
    scan_id = "tot_calibration"

    def configure(self, **kwargs):
        super(TotCalibration, self).configure(**kwargs)
        self.chip.set_dacs(**kwargs)

    def scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192, mask_step=192 + 24, VCAL_MED=500, VCAL_HIGH_start=500, VCAL_HIGH_stop=4000, VCAL_HIGH_step=100, n_injections=100, **kwargs):
        mask_data = self.prepare_injection_masks(start_column, stop_column, start_row, stop_row, mask_step)
        values = range(VCAL_HIGH_start, VCAL_HIGH_stop, VCAL_HIGH_step)

        self.logger.info('Starting scan...')
        pbar = tqdm(total=len(mask_data) * len(values), unit=' Mask steps')
        for scan_param_id, value in enumerate(values):
            with self.readout(scan_param_id=scan_param_id):
                self.chip.setup_analog_injection(vcal_high=value, vcal_med=VCAL_MED)
                for mask in mask_data:
                    self.chip.write_command(mask['command'])
                    self.chip.inject_analog_single(repetitions=n_injections, send_ecr=mask['flavor'] == 0)
                    pbar.update(1)
        pbar.close()
        self.logger.info('Scan finished')

    def analyze(self):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', store_hits=True) as a:
            a.analyze_data()
            values = np.arange(a.run_config['VCAL_HIGH_start'], a.run_config['VCAL_HIGH_stop'], a.run_config['VCAL_HIGH_step'])

        # Create col, row, tot histograms from hits
        with tb.open_file(self.output_filename + '_interpreted.h5', "r+") as io_file:
            shape_3d = (400 * 192, values.shape[0], 16)
            hist_3d = np.zeros(shape=shape_3d, dtype=np.uint8)

            # Loop over all words in the actual raw data file in chunks
            self.logger.info('Histogramming hits...')
            for i in tqdm(range(0, io_file.root.Hits.shape[0], a.chunk_size), unit=' Hits'):
                hits = io_file.root.Hits[i:i + a.chunk_size]
                channel = hits["col"] + 400 * hits["row"]
                hist_3d += au.hist_3d_index(x=channel, y=hits["scan_param_id"], z=hits["tot"], shape=shape_3d)

            # Create pixel, scan par id, tot hist
            io_file.create_carray(io_file.root,
                                  name='HistTotCal',
                                  title='Tot calibration histogram with channel, scan par id and tot',
                                  obj=hist_3d,
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))
            io_file.create_carray(io_file.root,
                                  name='HistTotCalMean',
                                  title='Mean tot calibration histogram',
                                  obj=au.get_mean_from_histogram(hist_3d, bin_positions=np.arange(16), axis=2),
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))

            result = np.zeros(shape=(400 * 192, values.shape[0]))
            self.logger.info('Calculate RMS per pixel...')
            get_rms_from_2d_hist(hist_3d, np.arange(16), result)
            io_file.create_carray(io_file.root,
                                  name='HistTotCalStd',
                                  title='Std tot calibration histogram',
                                  obj=result,
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))
            io_file.create_carray(io_file.root,
                                  name='TOThist',
                                  title='2d histogram for TOT values',
                                  obj=hist_2d_mean(hist_3d, bin_positions=np.arange(16), axis=2, param_count=values.shape[0]),
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))

        with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
            p.create_standard_plots()


if __name__ == "__main__":
    scan = TotCalibration()
    scan.start(**local_configuration)
    scan.analyze()
    scan.close()
