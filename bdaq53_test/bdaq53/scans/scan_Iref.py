#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic scan injects a specified charge into
    enabled pixels to test the analog front-end.
'''

from tqdm import tqdm

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting



local_configuration = {    
    # Scan parameters
    'start_column' : 0,
    'stop_column'  : 1,
    'start_row'    : 0,
    'stop_row'     : 1,
    'maskfile'     : 'auto',
    'mask_diff'    : False
    

    }


class AnalogScan(ScanBase):
    scan_id = "iref_scan"
    
    
    def configure(self, VCAL_MED=1000, VCAL_HIGH=4000, **kwargs):
        super(AnalogScan, self).configure(**kwargs)
        self.chip.setup_analog_injection(vcal_high=VCAL_HIGH, vcal_med=VCAL_MED)


    def scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192, mask_step=192+24, n_injections=100, **kwargs):

        raw_input("press enter to select iref in the output mux")
 	self.chip.write_register(register='MONITOR_SELECT', data=0b10000000000000, write=True)
        raw_input("press enter to continue")  

        
        


if __name__ == "__main__":
    scan = AnalogScan()
    scan.start(**local_configuration)
    #scan.analyze()
