'''
Created on 14 September 2018

@author: Dima Maneuski
'''

import socket
import time
 
server = socket.socket()
host = socket.gethostname()
port = 9091
server_address = (host, port)
server.bind(server_address)
print "# Wait for client connection..."
server.listen(5)
while True:
    client, address = server.accept()
    print 'Got connection from', address
    #time.sleep(1)
    msgReceived = client.recv(1024)
    print "Client sent me: " + msgReceived
    client.send(time.asctime() + " :: SERVER :: Hello Client")
    msgReceived = client.recv(1024)    
    print "Client sent me: " + msgReceived
    
    
    client.send(time.asctime() + " :: SERVER :: I acknowledge you did your job")
    
    msgReceived = client.recv(1024)
    print "Client sent me: " + msgReceived
    if (msgReceived == "CMD::SHUTDOWN"):
        client.close()
        break
    client.close()
