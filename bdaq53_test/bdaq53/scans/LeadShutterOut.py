'''
Created on 17 Sep 2018

@author: maneuski
@summary: This script moves the Standa X stage such that the lead brick mounted on it goes in and out of the beam.
based on GLADD-X project automation.XYScanExample.

Works in conjunction with Standa_v.1.16.vi Labview VI. 

'''

import socket
import sys
import time
import logging
# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
server_address = ('194.36.1.188', 6342)
print >>sys.stderr, 'connecting to %s port %s' % server_address
sock.connect(server_address)
time.sleep(2)

try:
    
    x_end = -50000
    print 'SHUTTER GOES OUT AND STOPS THE BEAM'    
    x_position = x_end
    message = 'pos stx ' + str(x_position)
    print "Sending message: " + message
    sock.sendall(message)
    msgReceived = sock.recv(1024)
    print "Received message: " + msgReceived      

finally:
    print >>sys.stderr, 'closing socket'
    sock.close()
