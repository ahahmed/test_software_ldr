import time
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import os
import glob
import datetime
#from mon1 import monitor_datalogger_power_supply
import basil
import visa
from operator import itemgetter
import ast
import sys, termios, tty, os, time
import csv
import tables as tb
from bitarray import bitarray
from datetime import datetime
from bdaq53.fifo_readout import FifoReadout
import logging
import matplotlib.pyplot as plt
import scipy
from scipy import stats


def plot_dac_lin_multim_(directory_textfile,directory_notextfile, dac_vector):
	f =  open(directory_textfile, 'r')
	lines = f.readlines()
	multimeter = []
	dac_linearity = []
	dose = []
	offset = 4 #When the real data starts, the multimeter values, in the data file
	
	for i,x in enumerate(lines):
		if i>0:
			multimeter = []
			w=i-1
			dose.append(x.split(' ')[3])
			temperature = '%.2f'%(float(x.split(' ')[1]))
			legend = "DAC linearity, Dose: " + str(dose[w]) + ' Mrad, T='+ str(temperature) + '(C)'
			for j in range(offset, offset + len(dac_vector)):
				#print 'Next value multimeter: ', x.split(' ')[j]
				#raw_input('wait')
				multimeter.append(float(x.split(' ')[j]))
				
			fig, ax = plt.subplots(figsize=(10, 5))
			dac_linearity.append( ax.scatter(dac_vector, multimeter, label = legend))
			first_legend = plt.legend(handles=[dac_linearity[w]], loc=4)
			ax.set_title('DAC linearity with multimeter, '+str(dose[w]) + ' Mrad')
			ax.set_ylabel('Voltage multimeter (mV)')
			ax.set_xlabel('VCAL_HIGH (LSB)')
			plt.savefig(directory_notextfile+'/'+str(dose[w])+'Mrad_DAC_linearity_withMultim.png')

			
	f.close()
	
	plt.close(fig)


def plot_dac_lin_multim(directory_textfile,directory_notextfile, dac_vector):
	f =  open(directory_textfile, 'r')
	lines = f.readlines()
	multimeter = []
	dac_linearity = []
	dose = []
	offset = 4 #When the real data starts, the multimeter values, in the text file
	for i,x in enumerate(lines):
		if i>0:
			multimeter = []
			w=i-1
			dose.append(x.split(' ')[3])
			temperature = '%.2f'%(float(x.split(' ')[1]))
			legend = "DAC linearity, Dose: " + str(dose[w]) + ' Mrad, T='+ str(temperature) + '(C)'
			for j in range(offset, offset + len(dac_vector)):
				#print 'Next value multimeter: ', x.split(' ')[j]
				#raw_input('wait')
				multimeter.append(float(x.split(' ')[j]))
				
			plt.figure(w, figsize = (10,5))

			plt.scatter(dac_vector, multimeter, label = legend)
			plt.legend(loc=4)
			plt.title('DAC linearity with external DVM, '+str(dose[w]) + ' Mrad')
			plt.ylabel('External DVM (V)')
			plt.xlabel('VCAL_HIGH (LSB)')
			plt.savefig(directory_notextfile+'/'+str(dose[w])+'Mrad_DAC_linearity_withMultim.png')
			plt.close()

			
	f.close()
	
	

def plot_dac_lin_multim_all(directory_textfile,directory_notextfile, dac_vector):
	line_color = ['#ffcc00','#ff6633','#ff0066','#cc33ff', '#3333ff', '#0099ff', '#33cc66', '#99cc33', '#ffcc22','#ff6677','#ff0099','#cc3355', '#333355', '#009922', '#33cc11', '#99cc88', '#ffcc77','#ff6600','#ff0088','#5533ff', '#ff33ff', '#6699ff', '#77cc66', '#00cc33', '#ffcc99','#ff6633','#770066','#cc33ff', '#2233ff', '#8899ff', '#99cc66', '#66cc33', '#998877','#887766','#776655','#667755','#665544','#554433','#332211'];
	f =  open(directory_textfile, 'r')
	lines = f.readlines()
	multimeter = []
	dac_linearity = []
	dose = []
	#ax = []
	all_multimeter = []
	legend = []
	offset = 4 #When the real data starts, the multimeter values
	plt.figure(8000, figsize = (10,10))
	for i,x in enumerate(lines):
		if i>0:
			multimeter = []
			w=i-1
			dose.append(x.split(' ')[3])
			temperature = '%.1f'%(float(x.split(' ')[1]))			
			
			for j in range(offset, offset + len(dac_vector)):
				#print 'Next value multimeter: ', x.split(' ')[j]
				#raw_input('wait')
				multimeter.append(float(x.split(' ')[j]))
			slope, intercept, r_value, p_value, std_err = stats.linregress(dac_vector,multimeter)
			legend.append(str(dose[w]) + ' Mrad, T='+ str(temperature) + '(C), f(x)=('+ str('%.2f'%(slope*10000))+' x + ' + str('%.2f'%(intercept*10000)) + ')*10$^-$$^4$, r = ' + str(('%.7f'%r_value)))
			all_multimeter.append(multimeter)	
	#print legend
	#fig, ax = plt.subplots(figsize=(10, 5)))
	#print all_multimeter
	#raw_input('press enter') 
	#ax = fig.add_axes([0.1, 0.1, 0.6, 0.75])
	for i in range(len(all_multimeter)):
			plt.scatter(dac_vector, all_multimeter[i], s = 15, label = legend[i], color = line_color[i])
			#plt.show()
			#raw_input('pressbutton')
	#plt.legend(loc=4)

	lgnd = plt.legend(bbox_to_anchor=(1.005, 1), loc=2,prop={'size': 13})#legend(loc=7 , prop={'size': 13})

	for handle in lgnd.legendHandles:
    		handle.set_sizes([9.0])
	plt.title('DAC linearity with external DVM, '+str(dose[w]) + ' Mrad', fontsize=20)
	plt.ylabel('External DVM (V)', fontsize=20)
	plt.xlabel('VCAL_HIGH (LSB)', fontsize=20)
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0)
	axes.set_ylim(bottom =0)
	plt.savefig(directory_notextfile+'/'+'AllDoses_Mrad_DAC_linearity_withMultim.png', dpi=1000,  bbox_inches='tight')
	#plt.show()

			
	f.close()

def plot_dac_lin_multim_all_last(directory_textfile,directory_notextfile, dac_vector):
	line_color = ['#ffcc00','#ff6633','#ff0066','#cc33ff', '#3333ff', '#0099ff', '#33cc66', '#99cc33', '#ffcc22','#ff6677','#ff0099','#cc3355', '#333355', '#009922', '#33cc11', '#99cc88', '#ffcc77','#ff6600','#ff0088','#5533ff', '#ff33ff', '#6699ff', '#77cc66', '#00cc33', '#ffcc99','#ff6633','#770066','#cc33ff', '#2233ff', '#8899ff', '#99cc66', '#66cc33', '#998877','#887766','#776655','#667755','#665544','#554433','#332211'];
	f =  open(directory_textfile, 'r')
	lines = f.readlines()
	multimeter = []
	dac_linearity = []
	dose = []
	#ax = []
	all_multimeter = []
	legend = []
	offset = 4 #When the real data starts, the multimeter values
	plt.figure(32, figsize = (10,10))
	for i,x in enumerate(lines):
		if i>0:
			multimeter = []
			w=i-1
			dose.append(x.split(' ')[3])
			temperature = '%.1f'%(float(x.split(' ')[1]))			
			
			for j in range(offset, offset + len(dac_vector)):
				#print 'Next value multimeter: ', x.split(' ')[j]
				#raw_input('wait')
				multimeter = (float(x.split(' ')[j]))
			#slope, intercept, r_value, p_value, std_err = stats.linregress(dac_vector,multimeter)
			legend.append(str(dose[w]) + ' Mrad, T='+ str(temperature) + '(C)' )
			all_multimeter.append(multimeter)	
	#print legend
	#fig, ax = plt.subplots(figsize=(10, 5)))
	#print all_multimeter
	#raw_input('press enter') 
	#ax = fig.add_axes([0.1, 0.1, 0.6, 0.75])
	dac_vector_last = 4000
	for i in range(len(all_multimeter)):
			plt.scatter(dac_vector_last, all_multimeter[i], s = 30, label = legend[i], color = line_color[i])
			#plt.show()
			#raw_input('pressbutton')
	#plt.legend(loc=4)

	lgnd = plt.legend(bbox_to_anchor=(1.005, 1), loc=2,prop={'size': 13})#legend(loc=7 , prop={'size': 13})

	for handle in lgnd.legendHandles:
    		handle.set_sizes([9.0])
	plt.title('DAC linearity with external DVM, '+str(dose[w]) + ' Mrad', fontsize=20)
	plt.ylabel('External DVM (V)', fontsize=20)
	plt.xlabel('VCAL_HIGH (LSB)', fontsize=20)
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=3995, right = 4005)
	axes.set_ylim(bottom =0.92, top = 0.96)
	plt.savefig(directory_notextfile+'/'+'AllDoses_Mrad_DAC_linearity_withMultim_lastpoint.png', dpi=1000,  bbox_inches='tight')
	#plt.show()

			
	f.close()


def plot_dac_lin_multim_all_last(directory_textfile,directory_notextfile, dac_vector):
	line_color = ['#ffcc00','#ff6633','#ff0066','#cc33ff', '#3333ff', '#0099ff', '#33cc66', '#99cc33', '#ffcc22','#ff6677','#ff0099','#cc3355', '#333355', '#009922', '#33cc11', '#99cc88', '#ffcc77','#ff6600','#ff0088','#5533ff', '#ff33ff', '#6699ff', '#77cc66', '#00cc33', '#ffcc99','#ff6633','#770066','#cc33ff', '#2233ff', '#8899ff', '#99cc66', '#66cc33', '#998877','#887766','#776655','#667755','#665544','#554433','#332211'];
	f =  open(directory_textfile, 'r')
	lines = f.readlines()
	multimeter = []
	dac_linearity = []
	dose = []
	#ax = []
	all_multimeter = []
	legend = []
	offset = 4 #When the real data starts, the multimeter values
	plt.figure(88, figsize = (10,10))
	for i,x in enumerate(lines):
		if i>0:
			multimeter = []
			w=i-1
			dose.append(x.split(' ')[3])
			temperature = '%.1f'%(float(x.split(' ')[1]))			
			
			for j in range(offset, offset + len(dac_vector)):
				#print 'Next value multimeter: ', x.split(' ')[j]
				#raw_input('wait')
				multimeter = (float(x.split(' ')[j]))
			#slope, intercept, r_value, p_value, std_err = stats.linregress(dac_vector,multimeter)
			legend.append(str(dose[w]) + ' Mrad, T='+ str(temperature) + '(C)' )
			all_multimeter.append(multimeter)	
	#print legend
	#fig, ax = plt.subplots(figsize=(10, 5)))
	#print all_multimeter
	#raw_input('press enter') 
	#ax = fig.add_axes([0.1, 0.1, 0.6, 0.75])
	
	axes = plt.gca()
	dose[0] = float(0.9)#To be able to plot 0Mrad in log scale
	for i in range(len(all_multimeter)):
			axes.scatter(float(dose[i]), all_multimeter[i], label = legend[i], color = line_color[i])
			#plt.show()
			#raw_input('pressbutton')
	#plt.legend(loc=4)

	lgnd = plt.legend(bbox_to_anchor=(1.005, 1), loc=2,prop={'size': 13})#legend(loc=7 , prop={'size': 13})

	for handle in lgnd.legendHandles:
    		handle.set_sizes([9.0])
	
	plt.title('DAC linearity with external DVM up to '+str(dose[w]) + ' Mrad', fontsize=20)
	plt.ylabel('External DVM (V)', fontsize=20)
	plt.xlabel('Dose (Mrad)', fontsize=20)
	plt.grid(True)
	
	axes.set_xlim(left=0.8)
	axes.set_xscale('log')
	axes.set_ylim(bottom =0.925, top = 0.95)
	plt.savefig(directory_notextfile+'/'+'AllDoses_Mrad_DAC_linearity_withMultim_lastpoint4000_dose.png', dpi=1000,  bbox_inches='tight')
	#plt.show()

			
	f.close()



if __name__ == "__main__":
    directory = '00_TEST_INFO_rad'
    directory_notextfile = directory + '/dac_linearity_multimeter'
    
    directory_textfile = directory_notextfile + '/'+"ACCUMULATIVE_DAC_linearity_withMultimeter.txt"
    dac_vector = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2100, 2200, 2300, 2400, 2500, 2600, 2700, 2800, 2900, 3000, 3100, 3200, 3300, 3400, 3500, 3600, 3700, 3800, 3900, 4000]
    #plot_dac_lin_multim(directory_textfile,directory_notextfile, dac_vector)
    plot_dac_lin_multim_all(directory_textfile,directory_notextfile, dac_vector)
    plot_dac_lin_multim_all_last(directory_textfile,directory_notextfile, dac_vector)

