Timestamp GlobalTime(s) Dose(Mrad)
02_10_2018_00_25_58 1538146232.06 150
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2157}
34 {'ADCbandgap': 2157}
36 {'ADCbandgap': 2158}
38 {'ADCbandgap': 2158}
40 {'ADCbandgap': 2154}
42 {'ADCbandgap': 2157}
44 {'ADCbandgap': 2154}
46 {'ADCbandgap': 2160}
48 {'ADCbandgap': 2151}
50 {'ADCbandgap': 2156}
52 {'ADCbandgap': 2151}
54 {'ADCbandgap': 2155}
56 {'ADCbandgap': 2156}
58 {'ADCbandgap': 2156}
60 {'ADCbandgap': 2156}
62 {'ADCbandgap': 2155}
33 {'ADCbandgap': 2160}
35 {'ADCbandgap': 2158}
37 {'ADCbandgap': 2156}
39 {'ADCbandgap': 2158}
41 {'ADCbandgap': 2155}
43 {'ADCbandgap': 2159}
45 {'ADCbandgap': 2158}
47 {'ADCbandgap': 2153}
49 {'ADCbandgap': 2156}
51 {'ADCbandgap': 2154}
53 {'ADCbandgap': 2156}
55 {'ADCbandgap': 2158}
57 {'ADCbandgap': 2159}
59 {'ADCbandgap': 2156}
61 {'ADCbandgap': 2160}
63 {'ADCbandgap': 2158}
32 {'TEMPSENS_1': 1283}
34 {'TEMPSENS_1': 1280}
36 {'TEMPSENS_1': 1271}
38 {'TEMPSENS_1': 1278}
40 {'TEMPSENS_1': 1267}
42 {'TEMPSENS_1': 1278}
44 {'TEMPSENS_1': 1280}
46 {'TEMPSENS_1': 1268}
48 {'TEMPSENS_1': 1279}
50 {'TEMPSENS_1': 1279}
52 {'TEMPSENS_1': 1284}
54 {'TEMPSENS_1': 1275}
56 {'TEMPSENS_1': 1279}
58 {'TEMPSENS_1': 1279}
60 {'TEMPSENS_1': 1277}
62 {'TEMPSENS_1': 1274}
33 {'TEMPSENS_1': 1624}
35 {'TEMPSENS_1': 1620}
37 {'TEMPSENS_1': 1619}
39 {'TEMPSENS_1': 1621}
41 {'TEMPSENS_1': 1622}
43 {'TEMPSENS_1': 1622}
45 {'TEMPSENS_1': 1621}
47 {'TEMPSENS_1': 1622}
49 {'TEMPSENS_1': 1623}
51 {'TEMPSENS_1': 1623}
53 {'TEMPSENS_1': 1619}
55 {'TEMPSENS_1': 1619}
57 {'TEMPSENS_1': 1618}
59 {'TEMPSENS_1': 1622}
61 {'TEMPSENS_1': 1622}
63 {'TEMPSENS_1': 1624}
32 {'TEMPSENS_2': 1264}
34 {'TEMPSENS_2': 1273}
36 {'TEMPSENS_2': 1270}
38 {'TEMPSENS_2': 1267}
40 {'TEMPSENS_2': 1272}
42 {'TEMPSENS_2': 1259}
44 {'TEMPSENS_2': 1274}
46 {'TEMPSENS_2': 1267}
48 {'TEMPSENS_2': 1267}
50 {'TEMPSENS_2': 1275}
52 {'TEMPSENS_2': 1271}
54 {'TEMPSENS_2': 1257}
56 {'TEMPSENS_2': 1272}
58 {'TEMPSENS_2': 1276}
60 {'TEMPSENS_2': 1279}
62 {'TEMPSENS_2': 1274}
33 {'TEMPSENS_2': 1616}
35 {'TEMPSENS_2': 1611}
37 {'TEMPSENS_2': 1616}
39 {'TEMPSENS_2': 1616}
41 {'TEMPSENS_2': 1613}
43 {'TEMPSENS_2': 1617}
45 {'TEMPSENS_2': 1615}
47 {'TEMPSENS_2': 1614}
49 {'TEMPSENS_2': 1617}
51 {'TEMPSENS_2': 1615}
53 {'TEMPSENS_2': 1615}
55 {'TEMPSENS_2': 1615}
57 {'TEMPSENS_2': 1616}
59 {'TEMPSENS_2': 1616}
61 {'TEMPSENS_2': 1614}
63 {'TEMPSENS_2': 1615}
32 {'TEMPSENS_3': 1272}
34 {'TEMPSENS_3': 1278}
36 {'TEMPSENS_3': 1267}
38 {'TEMPSENS_3': 1263}
40 {'TEMPSENS_3': 1273}
42 {'TEMPSENS_3': 1272}
44 {'TEMPSENS_3': 1267}
46 {'TEMPSENS_3': 1266}
48 {'TEMPSENS_3': 1271}
50 {'TEMPSENS_3': 1260}
52 {'TEMPSENS_3': 1278}
54 {'TEMPSENS_3': 1277}
56 {'TEMPSENS_3': 1271}
58 {'TEMPSENS_3': 1274}
60 {'TEMPSENS_3': 1273}
62 {'TEMPSENS_3': 1268}
33 {'TEMPSENS_3': 1620}
35 {'TEMPSENS_3': 1613}
37 {'TEMPSENS_3': 1618}
39 {'TEMPSENS_3': 1617}
41 {'TEMPSENS_3': 1620}
43 {'TEMPSENS_3': 1613}
45 {'TEMPSENS_3': 1616}
47 {'TEMPSENS_3': 1620}
49 {'TEMPSENS_3': 1616}
51 {'TEMPSENS_3': 1616}
53 {'TEMPSENS_3': 1617}
55 {'TEMPSENS_3': 1615}
57 {'TEMPSENS_3': 1615}
59 {'TEMPSENS_3': 1614}
61 {'TEMPSENS_3': 1617}
63 {'TEMPSENS_3': 1617}
32 {'TEMPSENS_4': 1266}
34 {'TEMPSENS_4': 1265}
36 {'TEMPSENS_4': 1257}
38 {'TEMPSENS_4': 1260}
40 {'TEMPSENS_4': 1263}
42 {'TEMPSENS_4': 1249}
44 {'TEMPSENS_4': 1253}
46 {'TEMPSENS_4': 1262}
48 {'TEMPSENS_4': 1259}
50 {'TEMPSENS_4': 1261}
52 {'TEMPSENS_4': 1257}
54 {'TEMPSENS_4': 1265}
56 {'TEMPSENS_4': 1267}
58 {'TEMPSENS_4': 1256}
60 {'TEMPSENS_4': 1259}
62 {'TEMPSENS_4': 1259}
33 {'TEMPSENS_4': 1607}
35 {'TEMPSENS_4': 1607}
37 {'TEMPSENS_4': 1603}
39 {'TEMPSENS_4': 1603}
41 {'TEMPSENS_4': 1601}
43 {'TEMPSENS_4': 1603}
45 {'TEMPSENS_4': 1607}
47 {'TEMPSENS_4': 1604}
49 {'TEMPSENS_4': 1605}
51 {'TEMPSENS_4': 1606}
53 {'TEMPSENS_4': 1610}
55 {'TEMPSENS_4': 1604}
57 {'TEMPSENS_4': 1603}
59 {'TEMPSENS_4': 1603}
61 {'TEMPSENS_4': 1608}
63 {'TEMPSENS_4': 1603}
32 {'RADSENS_1': 3040}
34 {'RADSENS_1': 3042}
36 {'RADSENS_1': 3046}
38 {'RADSENS_1': 3047}
40 {'RADSENS_1': 3040}
42 {'RADSENS_1': 3043}
44 {'RADSENS_1': 3042}
46 {'RADSENS_1': 3047}
48 {'RADSENS_1': 3049}
50 {'RADSENS_1': 3031}
52 {'RADSENS_1': 3043}
54 {'RADSENS_1': 3041}
56 {'RADSENS_1': 3035}
58 {'RADSENS_1': 3046}
60 {'RADSENS_1': 3043}
62 {'RADSENS_1': 3033}
33 {'RADSENS_1': 3376}
35 {'RADSENS_1': 3372}
37 {'RADSENS_1': 3376}
39 {'RADSENS_1': 3374}
41 {'RADSENS_1': 3372}
43 {'RADSENS_1': 3367}
45 {'RADSENS_1': 3367}
47 {'RADSENS_1': 3372}
49 {'RADSENS_1': 3359}
51 {'RADSENS_1': 3374}
53 {'RADSENS_1': 3372}
55 {'RADSENS_1': 3362}
57 {'RADSENS_1': 3376}
59 {'RADSENS_1': 3367}
61 {'RADSENS_1': 3376}
63 {'RADSENS_1': 3378}
32 {'RADSENS_2': 2953}
34 {'RADSENS_2': 2943}
36 {'RADSENS_2': 2935}
38 {'RADSENS_2': 2952}
40 {'RADSENS_2': 2941}
42 {'RADSENS_2': 2943}
44 {'RADSENS_2': 2945}
46 {'RADSENS_2': 2946}
48 {'RADSENS_2': 2937}
50 {'RADSENS_2': 2942}
52 {'RADSENS_2': 2956}
54 {'RADSENS_2': 2930}
56 {'RADSENS_2': 2958}
58 {'RADSENS_2': 2956}
60 {'RADSENS_2': 2932}
62 {'RADSENS_2': 2943}
33 {'RADSENS_2': 3332}
35 {'RADSENS_2': 3336}
37 {'RADSENS_2': 3336}
39 {'RADSENS_2': 3326}
41 {'RADSENS_2': 3331}
43 {'RADSENS_2': 3327}
45 {'RADSENS_2': 3335}
47 {'RADSENS_2': 3334}
49 {'RADSENS_2': 3334}
51 {'RADSENS_2': 3334}
53 {'RADSENS_2': 3332}
55 {'RADSENS_2': 3341}
57 {'RADSENS_2': 3329}
59 {'RADSENS_2': 3321}
61 {'RADSENS_2': 3327}
63 {'RADSENS_2': 3336}
32 {'RADSENS_3': 2920}
34 {'RADSENS_3': 2921}
36 {'RADSENS_3': 2930}
38 {'RADSENS_3': 2926}
40 {'RADSENS_3': 2936}
42 {'RADSENS_3': 2920}
44 {'RADSENS_3': 2916}
46 {'RADSENS_3': 2927}
48 {'RADSENS_3': 2924}
50 {'RADSENS_3': 2912}
52 {'RADSENS_3': 2928}
54 {'RADSENS_3': 2919}
56 {'RADSENS_3': 2910}
58 {'RADSENS_3': 2911}
60 {'RADSENS_3': 2925}
62 {'RADSENS_3': 2911}
33 {'RADSENS_3': 3321}
35 {'RADSENS_3': 3321}
37 {'RADSENS_3': 3316}
39 {'RADSENS_3': 3322}
41 {'RADSENS_3': 3317}
43 {'RADSENS_3': 3318}
45 {'RADSENS_3': 3318}
47 {'RADSENS_3': 3318}
49 {'RADSENS_3': 3313}
51 {'RADSENS_3': 3323}
53 {'RADSENS_3': 3328}
55 {'RADSENS_3': 3324}
57 {'RADSENS_3': 3311}
59 {'RADSENS_3': 3311}
61 {'RADSENS_3': 3325}
63 {'RADSENS_3': 3311}
32 {'RADSENS_4': 2915}
34 {'RADSENS_4': 2926}
36 {'RADSENS_4': 2931}
38 {'RADSENS_4': 2924}
40 {'RADSENS_4': 2918}
42 {'RADSENS_4': 2925}
44 {'RADSENS_4': 2923}
46 {'RADSENS_4': 2928}
48 {'RADSENS_4': 2918}
50 {'RADSENS_4': 2913}
52 {'RADSENS_4': 2919}
54 {'RADSENS_4': 2909}
56 {'RADSENS_4': 2919}
58 {'RADSENS_4': 2912}
60 {'RADSENS_4': 2924}
62 {'RADSENS_4': 2913}
33 {'RADSENS_4': 3313}
35 {'RADSENS_4': 3315}
37 {'RADSENS_4': 3317}
39 {'RADSENS_4': 3320}
41 {'RADSENS_4': 3315}
43 {'RADSENS_4': 3320}
45 {'RADSENS_4': 3315}
47 {'RADSENS_4': 3315}
49 {'RADSENS_4': 3320}
51 {'RADSENS_4': 3311}
53 {'RADSENS_4': 3315}
55 {'RADSENS_4': 3319}
57 {'RADSENS_4': 3310}
59 {'RADSENS_4': 3319}
61 {'RADSENS_4': 3311}
63 {'RADSENS_4': 3320}
