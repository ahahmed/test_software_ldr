Timestamp GlobalTime(s) Dose(Mrad)
02_10_2018_09_35_16 1538146232.06 150
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2151}
34 {'ADCbandgap': 2158}
36 {'ADCbandgap': 2158}
38 {'ADCbandgap': 2156}
40 {'ADCbandgap': 2157}
42 {'ADCbandgap': 2160}
44 {'ADCbandgap': 2155}
46 {'ADCbandgap': 2160}
48 {'ADCbandgap': 2153}
50 {'ADCbandgap': 2155}
52 {'ADCbandgap': 2155}
54 {'ADCbandgap': 2158}
56 {'ADCbandgap': 2153}
58 {'ADCbandgap': 2158}
60 {'ADCbandgap': 2161}
62 {'ADCbandgap': 2157}
33 {'ADCbandgap': 2158}
35 {'ADCbandgap': 2156}
37 {'ADCbandgap': 2158}
39 {'ADCbandgap': 2157}
41 {'ADCbandgap': 2159}
43 {'ADCbandgap': 2158}
45 {'ADCbandgap': 2155}
47 {'ADCbandgap': 2159}
49 {'ADCbandgap': 2155}
51 {'ADCbandgap': 2160}
53 {'ADCbandgap': 2160}
55 {'ADCbandgap': 2160}
57 {'ADCbandgap': 2157}
59 {'ADCbandgap': 2158}
61 {'ADCbandgap': 2160}
63 {'ADCbandgap': 2158}
32 {'TEMPSENS_1': 1273}
34 {'TEMPSENS_1': 1270}
36 {'TEMPSENS_1': 1263}
38 {'TEMPSENS_1': 1272}
40 {'TEMPSENS_1': 1265}
42 {'TEMPSENS_1': 1275}
44 {'TEMPSENS_1': 1267}
46 {'TEMPSENS_1': 1265}
48 {'TEMPSENS_1': 1270}
50 {'TEMPSENS_1': 1275}
52 {'TEMPSENS_1': 1279}
54 {'TEMPSENS_1': 1271}
56 {'TEMPSENS_1': 1271}
58 {'TEMPSENS_1': 1270}
60 {'TEMPSENS_1': 1273}
62 {'TEMPSENS_1': 1267}
33 {'TEMPSENS_1': 1614}
35 {'TEMPSENS_1': 1615}
37 {'TEMPSENS_1': 1619}
39 {'TEMPSENS_1': 1615}
41 {'TEMPSENS_1': 1619}
43 {'TEMPSENS_1': 1618}
45 {'TEMPSENS_1': 1616}
47 {'TEMPSENS_1': 1620}
49 {'TEMPSENS_1': 1618}
51 {'TEMPSENS_1': 1620}
53 {'TEMPSENS_1': 1616}
55 {'TEMPSENS_1': 1615}
57 {'TEMPSENS_1': 1616}
59 {'TEMPSENS_1': 1620}
61 {'TEMPSENS_1': 1617}
63 {'TEMPSENS_1': 1616}
32 {'TEMPSENS_2': 1264}
34 {'TEMPSENS_2': 1269}
36 {'TEMPSENS_2': 1262}
38 {'TEMPSENS_2': 1266}
40 {'TEMPSENS_2': 1271}
42 {'TEMPSENS_2': 1264}
44 {'TEMPSENS_2': 1271}
46 {'TEMPSENS_2': 1265}
48 {'TEMPSENS_2': 1263}
50 {'TEMPSENS_2': 1267}
52 {'TEMPSENS_2': 1268}
54 {'TEMPSENS_2': 1260}
56 {'TEMPSENS_2': 1271}
58 {'TEMPSENS_2': 1277}
60 {'TEMPSENS_2': 1275}
62 {'TEMPSENS_2': 1271}
33 {'TEMPSENS_2': 1614}
35 {'TEMPSENS_2': 1613}
37 {'TEMPSENS_2': 1616}
39 {'TEMPSENS_2': 1611}
41 {'TEMPSENS_2': 1607}
43 {'TEMPSENS_2': 1616}
45 {'TEMPSENS_2': 1609}
47 {'TEMPSENS_2': 1613}
49 {'TEMPSENS_2': 1616}
51 {'TEMPSENS_2': 1615}
53 {'TEMPSENS_2': 1616}
55 {'TEMPSENS_2': 1611}
57 {'TEMPSENS_2': 1615}
59 {'TEMPSENS_2': 1616}
61 {'TEMPSENS_2': 1617}
63 {'TEMPSENS_2': 1611}
32 {'TEMPSENS_3': 1268}
34 {'TEMPSENS_3': 1273}
36 {'TEMPSENS_3': 1265}
38 {'TEMPSENS_3': 1260}
40 {'TEMPSENS_3': 1264}
42 {'TEMPSENS_3': 1269}
44 {'TEMPSENS_3': 1276}
46 {'TEMPSENS_3': 1263}
48 {'TEMPSENS_3': 1272}
50 {'TEMPSENS_3': 1262}
52 {'TEMPSENS_3': 1278}
54 {'TEMPSENS_3': 1270}
56 {'TEMPSENS_3': 1272}
58 {'TEMPSENS_3': 1269}
60 {'TEMPSENS_3': 1273}
62 {'TEMPSENS_3': 1270}
33 {'TEMPSENS_3': 1617}
35 {'TEMPSENS_3': 1616}
37 {'TEMPSENS_3': 1615}
39 {'TEMPSENS_3': 1616}
41 {'TEMPSENS_3': 1616}
43 {'TEMPSENS_3': 1613}
45 {'TEMPSENS_3': 1614}
47 {'TEMPSENS_3': 1620}
49 {'TEMPSENS_3': 1616}
51 {'TEMPSENS_3': 1615}
53 {'TEMPSENS_3': 1612}
55 {'TEMPSENS_3': 1616}
57 {'TEMPSENS_3': 1614}
59 {'TEMPSENS_3': 1612}
61 {'TEMPSENS_3': 1613}
63 {'TEMPSENS_3': 1615}
32 {'TEMPSENS_4': 1262}
34 {'TEMPSENS_4': 1255}
36 {'TEMPSENS_4': 1258}
38 {'TEMPSENS_4': 1260}
40 {'TEMPSENS_4': 1260}
42 {'TEMPSENS_4': 1250}
44 {'TEMPSENS_4': 1255}
46 {'TEMPSENS_4': 1255}
48 {'TEMPSENS_4': 1254}
50 {'TEMPSENS_4': 1255}
52 {'TEMPSENS_4': 1249}
54 {'TEMPSENS_4': 1264}
56 {'TEMPSENS_4': 1271}
58 {'TEMPSENS_4': 1247}
60 {'TEMPSENS_4': 1255}
62 {'TEMPSENS_4': 1258}
33 {'TEMPSENS_4': 1596}
35 {'TEMPSENS_4': 1603}
37 {'TEMPSENS_4': 1599}
39 {'TEMPSENS_4': 1603}
41 {'TEMPSENS_4': 1602}
43 {'TEMPSENS_4': 1604}
45 {'TEMPSENS_4': 1603}
47 {'TEMPSENS_4': 1602}
49 {'TEMPSENS_4': 1599}
51 {'TEMPSENS_4': 1600}
53 {'TEMPSENS_4': 1601}
55 {'TEMPSENS_4': 1598}
57 {'TEMPSENS_4': 1604}
59 {'TEMPSENS_4': 1599}
61 {'TEMPSENS_4': 1600}
63 {'TEMPSENS_4': 1603}
32 {'RADSENS_1': 3020}
34 {'RADSENS_1': 3021}
36 {'RADSENS_1': 3023}
38 {'RADSENS_1': 3015}
40 {'RADSENS_1': 3006}
42 {'RADSENS_1': 3016}
44 {'RADSENS_1': 3023}
46 {'RADSENS_1': 3029}
48 {'RADSENS_1': 3027}
50 {'RADSENS_1': 3016}
52 {'RADSENS_1': 3020}
54 {'RADSENS_1': 3007}
56 {'RADSENS_1': 3023}
58 {'RADSENS_1': 3021}
60 {'RADSENS_1': 3023}
62 {'RADSENS_1': 3024}
33 {'RADSENS_1': 3355}
35 {'RADSENS_1': 3361}
37 {'RADSENS_1': 3356}
39 {'RADSENS_1': 3352}
41 {'RADSENS_1': 3358}
43 {'RADSENS_1': 3353}
45 {'RADSENS_1': 3364}
47 {'RADSENS_1': 3358}
49 {'RADSENS_1': 3351}
51 {'RADSENS_1': 3357}
53 {'RADSENS_1': 3352}
55 {'RADSENS_1': 3357}
57 {'RADSENS_1': 3361}
59 {'RADSENS_1': 3355}
61 {'RADSENS_1': 3359}
63 {'RADSENS_1': 3358}
32 {'RADSENS_2': 2934}
34 {'RADSENS_2': 2931}
36 {'RADSENS_2': 2926}
38 {'RADSENS_2': 2941}
40 {'RADSENS_2': 2926}
42 {'RADSENS_2': 2944}
44 {'RADSENS_2': 2933}
46 {'RADSENS_2': 2927}
48 {'RADSENS_2': 2913}
50 {'RADSENS_2': 2918}
52 {'RADSENS_2': 2945}
54 {'RADSENS_2': 2919}
56 {'RADSENS_2': 2943}
58 {'RADSENS_2': 2934}
60 {'RADSENS_2': 2928}
62 {'RADSENS_2': 2936}
33 {'RADSENS_2': 3324}
35 {'RADSENS_2': 3311}
37 {'RADSENS_2': 3315}
39 {'RADSENS_2': 3324}
41 {'RADSENS_2': 3328}
43 {'RADSENS_2': 3324}
45 {'RADSENS_2': 3324}
47 {'RADSENS_2': 3319}
49 {'RADSENS_2': 3328}
51 {'RADSENS_2': 3328}
53 {'RADSENS_2': 3324}
55 {'RADSENS_2': 3326}
57 {'RADSENS_2': 3321}
59 {'RADSENS_2': 3319}
61 {'RADSENS_2': 3320}
63 {'RADSENS_2': 3319}
32 {'RADSENS_3': 2912}
34 {'RADSENS_3': 2904}
36 {'RADSENS_3': 2915}
38 {'RADSENS_3': 2908}
40 {'RADSENS_3': 2912}
42 {'RADSENS_3': 2910}
44 {'RADSENS_3': 2906}
46 {'RADSENS_3': 2918}
48 {'RADSENS_3': 2899}
50 {'RADSENS_3': 2901}
52 {'RADSENS_3': 2912}
54 {'RADSENS_3': 2907}
56 {'RADSENS_3': 2903}
58 {'RADSENS_3': 2912}
60 {'RADSENS_3': 2909}
62 {'RADSENS_3': 2905}
33 {'RADSENS_3': 3303}
35 {'RADSENS_3': 3308}
37 {'RADSENS_3': 3303}
39 {'RADSENS_3': 3305}
41 {'RADSENS_3': 3308}
43 {'RADSENS_3': 3304}
45 {'RADSENS_3': 3308}
47 {'RADSENS_3': 3303}
49 {'RADSENS_3': 3312}
51 {'RADSENS_3': 3307}
53 {'RADSENS_3': 3303}
55 {'RADSENS_3': 3302}
57 {'RADSENS_3': 3314}
59 {'RADSENS_3': 3307}
61 {'RADSENS_3': 3299}
63 {'RADSENS_3': 3308}
32 {'RADSENS_4': 2910}
34 {'RADSENS_4': 2917}
36 {'RADSENS_4': 2912}
38 {'RADSENS_4': 2902}
40 {'RADSENS_4': 2910}
42 {'RADSENS_4': 2903}
44 {'RADSENS_4': 2914}
46 {'RADSENS_4': 2915}
48 {'RADSENS_4': 2901}
50 {'RADSENS_4': 2899}
52 {'RADSENS_4': 2905}
54 {'RADSENS_4': 2899}
56 {'RADSENS_4': 2895}
58 {'RADSENS_4': 2890}
60 {'RADSENS_4': 2905}
62 {'RADSENS_4': 2912}
33 {'RADSENS_4': 3304}
35 {'RADSENS_4': 3303}
37 {'RADSENS_4': 3307}
39 {'RADSENS_4': 3308}
41 {'RADSENS_4': 3295}
43 {'RADSENS_4': 3308}
45 {'RADSENS_4': 3305}
47 {'RADSENS_4': 3306}
49 {'RADSENS_4': 3308}
51 {'RADSENS_4': 3309}
53 {'RADSENS_4': 3303}
55 {'RADSENS_4': 3305}
57 {'RADSENS_4': 3299}
59 {'RADSENS_4': 3307}
61 {'RADSENS_4': 3310}
63 {'RADSENS_4': 3306}
