Timestamp GlobalTime(s) Dose(Mrad)
30_09_2018_13_57_13 1538146232.06 50
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2155}
34 {'ADCbandgap': 2151}
36 {'ADCbandgap': 2153}
38 {'ADCbandgap': 2159}
40 {'ADCbandgap': 2151}
42 {'ADCbandgap': 2151}
44 {'ADCbandgap': 2154}
46 {'ADCbandgap': 2155}
48 {'ADCbandgap': 2154}
50 {'ADCbandgap': 2155}
52 {'ADCbandgap': 2154}
54 {'ADCbandgap': 2155}
56 {'ADCbandgap': 2151}
58 {'ADCbandgap': 2155}
60 {'ADCbandgap': 2155}
62 {'ADCbandgap': 2154}
33 {'ADCbandgap': 2153}
35 {'ADCbandgap': 2156}
37 {'ADCbandgap': 2155}
39 {'ADCbandgap': 2151}
41 {'ADCbandgap': 2156}
43 {'ADCbandgap': 2154}
45 {'ADCbandgap': 2156}
47 {'ADCbandgap': 2153}
49 {'ADCbandgap': 2155}
51 {'ADCbandgap': 2155}
53 {'ADCbandgap': 2153}
55 {'ADCbandgap': 2156}
57 {'ADCbandgap': 2160}
59 {'ADCbandgap': 2158}
61 {'ADCbandgap': 2151}
63 {'ADCbandgap': 2156}
32 {'TEMPSENS_1': 1288}
34 {'TEMPSENS_1': 1291}
36 {'TEMPSENS_1': 1273}
38 {'TEMPSENS_1': 1279}
40 {'TEMPSENS_1': 1280}
42 {'TEMPSENS_1': 1279}
44 {'TEMPSENS_1': 1280}
46 {'TEMPSENS_1': 1280}
48 {'TEMPSENS_1': 1294}
50 {'TEMPSENS_1': 1292}
52 {'TEMPSENS_1': 1292}
54 {'TEMPSENS_1': 1288}
56 {'TEMPSENS_1': 1286}
58 {'TEMPSENS_1': 1278}
60 {'TEMPSENS_1': 1285}
62 {'TEMPSENS_1': 1284}
33 {'TEMPSENS_1': 1627}
35 {'TEMPSENS_1': 1631}
37 {'TEMPSENS_1': 1624}
39 {'TEMPSENS_1': 1628}
41 {'TEMPSENS_1': 1630}
43 {'TEMPSENS_1': 1628}
45 {'TEMPSENS_1': 1627}
47 {'TEMPSENS_1': 1628}
49 {'TEMPSENS_1': 1632}
51 {'TEMPSENS_1': 1630}
53 {'TEMPSENS_1': 1623}
55 {'TEMPSENS_1': 1630}
57 {'TEMPSENS_1': 1628}
59 {'TEMPSENS_1': 1628}
61 {'TEMPSENS_1': 1629}
63 {'TEMPSENS_1': 1627}
32 {'TEMPSENS_2': 1263}
34 {'TEMPSENS_2': 1275}
36 {'TEMPSENS_2': 1272}
38 {'TEMPSENS_2': 1274}
40 {'TEMPSENS_2': 1271}
42 {'TEMPSENS_2': 1270}
44 {'TEMPSENS_2': 1274}
46 {'TEMPSENS_2': 1265}
48 {'TEMPSENS_2': 1272}
50 {'TEMPSENS_2': 1280}
52 {'TEMPSENS_2': 1278}
54 {'TEMPSENS_2': 1263}
56 {'TEMPSENS_2': 1276}
58 {'TEMPSENS_2': 1284}
60 {'TEMPSENS_2': 1275}
62 {'TEMPSENS_2': 1271}
33 {'TEMPSENS_2': 1624}
35 {'TEMPSENS_2': 1617}
37 {'TEMPSENS_2': 1619}
39 {'TEMPSENS_2': 1617}
41 {'TEMPSENS_2': 1619}
43 {'TEMPSENS_2': 1618}
45 {'TEMPSENS_2': 1622}
47 {'TEMPSENS_2': 1619}
49 {'TEMPSENS_2': 1624}
51 {'TEMPSENS_2': 1619}
53 {'TEMPSENS_2': 1618}
55 {'TEMPSENS_2': 1622}
57 {'TEMPSENS_2': 1624}
59 {'TEMPSENS_2': 1615}
61 {'TEMPSENS_2': 1617}
63 {'TEMPSENS_2': 1617}
32 {'TEMPSENS_3': 1271}
34 {'TEMPSENS_3': 1280}
36 {'TEMPSENS_3': 1268}
38 {'TEMPSENS_3': 1263}
40 {'TEMPSENS_3': 1270}
42 {'TEMPSENS_3': 1275}
44 {'TEMPSENS_3': 1273}
46 {'TEMPSENS_3': 1272}
48 {'TEMPSENS_3': 1271}
50 {'TEMPSENS_3': 1262}
52 {'TEMPSENS_3': 1279}
54 {'TEMPSENS_3': 1276}
56 {'TEMPSENS_3': 1275}
58 {'TEMPSENS_3': 1276}
60 {'TEMPSENS_3': 1274}
62 {'TEMPSENS_3': 1275}
33 {'TEMPSENS_3': 1618}
35 {'TEMPSENS_3': 1620}
37 {'TEMPSENS_3': 1622}
39 {'TEMPSENS_3': 1621}
41 {'TEMPSENS_3': 1615}
43 {'TEMPSENS_3': 1615}
45 {'TEMPSENS_3': 1622}
47 {'TEMPSENS_3': 1622}
49 {'TEMPSENS_3': 1615}
51 {'TEMPSENS_3': 1615}
53 {'TEMPSENS_3': 1615}
55 {'TEMPSENS_3': 1620}
57 {'TEMPSENS_3': 1618}
59 {'TEMPSENS_3': 1615}
61 {'TEMPSENS_3': 1619}
63 {'TEMPSENS_3': 1618}
32 {'TEMPSENS_4': 1268}
34 {'TEMPSENS_4': 1265}
36 {'TEMPSENS_4': 1263}
38 {'TEMPSENS_4': 1267}
40 {'TEMPSENS_4': 1269}
42 {'TEMPSENS_4': 1259}
44 {'TEMPSENS_4': 1268}
46 {'TEMPSENS_4': 1268}
48 {'TEMPSENS_4': 1265}
50 {'TEMPSENS_4': 1265}
52 {'TEMPSENS_4': 1260}
54 {'TEMPSENS_4': 1273}
56 {'TEMPSENS_4': 1271}
58 {'TEMPSENS_4': 1254}
60 {'TEMPSENS_4': 1263}
62 {'TEMPSENS_4': 1271}
33 {'TEMPSENS_4': 1610}
35 {'TEMPSENS_4': 1607}
37 {'TEMPSENS_4': 1612}
39 {'TEMPSENS_4': 1616}
41 {'TEMPSENS_4': 1608}
43 {'TEMPSENS_4': 1609}
45 {'TEMPSENS_4': 1612}
47 {'TEMPSENS_4': 1609}
49 {'TEMPSENS_4': 1612}
51 {'TEMPSENS_4': 1607}
53 {'TEMPSENS_4': 1616}
55 {'TEMPSENS_4': 1612}
57 {'TEMPSENS_4': 1609}
59 {'TEMPSENS_4': 1614}
61 {'TEMPSENS_4': 1612}
63 {'TEMPSENS_4': 1610}
32 {'RADSENS_1': 3086}
34 {'RADSENS_1': 3087}
36 {'RADSENS_1': 3103}
38 {'RADSENS_1': 3107}
40 {'RADSENS_1': 3086}
42 {'RADSENS_1': 3092}
44 {'RADSENS_1': 3095}
46 {'RADSENS_1': 3102}
48 {'RADSENS_1': 3098}
50 {'RADSENS_1': 3088}
52 {'RADSENS_1': 3096}
54 {'RADSENS_1': 3091}
56 {'RADSENS_1': 3095}
58 {'RADSENS_1': 3091}
60 {'RADSENS_1': 3096}
62 {'RADSENS_1': 3104}
33 {'RADSENS_1': 3398}
35 {'RADSENS_1': 3404}
37 {'RADSENS_1': 3402}
39 {'RADSENS_1': 3400}
41 {'RADSENS_1': 3408}
43 {'RADSENS_1': 3395}
45 {'RADSENS_1': 3400}
47 {'RADSENS_1': 3393}
49 {'RADSENS_1': 3401}
51 {'RADSENS_1': 3404}
53 {'RADSENS_1': 3402}
55 {'RADSENS_1': 3394}
57 {'RADSENS_1': 3401}
59 {'RADSENS_1': 3400}
61 {'RADSENS_1': 3395}
63 {'RADSENS_1': 3400}
32 {'RADSENS_2': 2993}
34 {'RADSENS_2': 2996}
36 {'RADSENS_2': 2992}
38 {'RADSENS_2': 2995}
40 {'RADSENS_2': 2995}
42 {'RADSENS_2': 2993}
44 {'RADSENS_2': 2977}
46 {'RADSENS_2': 2984}
48 {'RADSENS_2': 2987}
50 {'RADSENS_2': 2988}
52 {'RADSENS_2': 2990}
54 {'RADSENS_2': 2979}
56 {'RADSENS_2': 2999}
58 {'RADSENS_2': 2988}
60 {'RADSENS_2': 2979}
62 {'RADSENS_2': 3005}
33 {'RADSENS_2': 3347}
35 {'RADSENS_2': 3360}
37 {'RADSENS_2': 3360}
39 {'RADSENS_2': 3364}
41 {'RADSENS_2': 3356}
43 {'RADSENS_2': 3361}
45 {'RADSENS_2': 3361}
47 {'RADSENS_2': 3364}
49 {'RADSENS_2': 3362}
51 {'RADSENS_2': 3358}
53 {'RADSENS_2': 3368}
55 {'RADSENS_2': 3362}
57 {'RADSENS_2': 3360}
59 {'RADSENS_2': 3357}
61 {'RADSENS_2': 3368}
63 {'RADSENS_2': 3359}
32 {'RADSENS_3': 2951}
34 {'RADSENS_3': 2952}
36 {'RADSENS_3': 2961}
38 {'RADSENS_3': 2952}
40 {'RADSENS_3': 2964}
42 {'RADSENS_3': 2960}
44 {'RADSENS_3': 2954}
46 {'RADSENS_3': 2950}
48 {'RADSENS_3': 2943}
50 {'RADSENS_3': 2960}
52 {'RADSENS_3': 2951}
54 {'RADSENS_3': 2948}
56 {'RADSENS_3': 2949}
58 {'RADSENS_3': 2957}
60 {'RADSENS_3': 2965}
62 {'RADSENS_3': 2956}
33 {'RADSENS_3': 3340}
35 {'RADSENS_3': 3335}
37 {'RADSENS_3': 3344}
39 {'RADSENS_3': 3344}
41 {'RADSENS_3': 3340}
43 {'RADSENS_3': 3337}
45 {'RADSENS_3': 3337}
47 {'RADSENS_3': 3338}
49 {'RADSENS_3': 3340}
51 {'RADSENS_3': 3340}
53 {'RADSENS_3': 3339}
55 {'RADSENS_3': 3342}
57 {'RADSENS_3': 3345}
59 {'RADSENS_3': 3335}
61 {'RADSENS_3': 3340}
63 {'RADSENS_3': 3327}
32 {'RADSENS_4': 2960}
34 {'RADSENS_4': 2956}
36 {'RADSENS_4': 2956}
38 {'RADSENS_4': 2961}
40 {'RADSENS_4': 2961}
42 {'RADSENS_4': 2959}
44 {'RADSENS_4': 2960}
46 {'RADSENS_4': 2960}
48 {'RADSENS_4': 2950}
50 {'RADSENS_4': 2956}
52 {'RADSENS_4': 2966}
54 {'RADSENS_4': 2946}
56 {'RADSENS_4': 2960}
58 {'RADSENS_4': 2952}
60 {'RADSENS_4': 2959}
62 {'RADSENS_4': 2958}
33 {'RADSENS_4': 3344}
35 {'RADSENS_4': 3344}
37 {'RADSENS_4': 3333}
39 {'RADSENS_4': 3339}
41 {'RADSENS_4': 3344}
43 {'RADSENS_4': 3335}
45 {'RADSENS_4': 3340}
47 {'RADSENS_4': 3344}
49 {'RADSENS_4': 3345}
51 {'RADSENS_4': 3347}
53 {'RADSENS_4': 3331}
55 {'RADSENS_4': 3348}
57 {'RADSENS_4': 3335}
59 {'RADSENS_4': 3350}
61 {'RADSENS_4': 3335}
63 {'RADSENS_4': 3335}
