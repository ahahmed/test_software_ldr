Timestamp GlobalTime(s) Dose(Mrad)
01_10_2018_01_37_55 1538146232.06 70
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2155}
34 {'ADCbandgap': 2153}
36 {'ADCbandgap': 2158}
38 {'ADCbandgap': 2156}
40 {'ADCbandgap': 2155}
42 {'ADCbandgap': 2154}
44 {'ADCbandgap': 2155}
46 {'ADCbandgap': 2156}
48 {'ADCbandgap': 2155}
50 {'ADCbandgap': 2158}
52 {'ADCbandgap': 2153}
54 {'ADCbandgap': 2155}
56 {'ADCbandgap': 2158}
58 {'ADCbandgap': 2155}
60 {'ADCbandgap': 2151}
62 {'ADCbandgap': 2156}
33 {'ADCbandgap': 2155}
35 {'ADCbandgap': 2156}
37 {'ADCbandgap': 2154}
39 {'ADCbandgap': 2155}
41 {'ADCbandgap': 2160}
43 {'ADCbandgap': 2160}
45 {'ADCbandgap': 2154}
47 {'ADCbandgap': 2156}
49 {'ADCbandgap': 2157}
51 {'ADCbandgap': 2155}
53 {'ADCbandgap': 2160}
55 {'ADCbandgap': 2157}
57 {'ADCbandgap': 2155}
59 {'ADCbandgap': 2153}
61 {'ADCbandgap': 2153}
63 {'ADCbandgap': 2160}
32 {'TEMPSENS_1': 1285}
34 {'TEMPSENS_1': 1282}
36 {'TEMPSENS_1': 1277}
38 {'TEMPSENS_1': 1278}
40 {'TEMPSENS_1': 1271}
42 {'TEMPSENS_1': 1283}
44 {'TEMPSENS_1': 1279}
46 {'TEMPSENS_1': 1273}
48 {'TEMPSENS_1': 1287}
50 {'TEMPSENS_1': 1288}
52 {'TEMPSENS_1': 1284}
54 {'TEMPSENS_1': 1280}
56 {'TEMPSENS_1': 1279}
58 {'TEMPSENS_1': 1280}
60 {'TEMPSENS_1': 1281}
62 {'TEMPSENS_1': 1276}
33 {'TEMPSENS_1': 1624}
35 {'TEMPSENS_1': 1628}
37 {'TEMPSENS_1': 1623}
39 {'TEMPSENS_1': 1627}
41 {'TEMPSENS_1': 1628}
43 {'TEMPSENS_1': 1625}
45 {'TEMPSENS_1': 1622}
47 {'TEMPSENS_1': 1627}
49 {'TEMPSENS_1': 1631}
51 {'TEMPSENS_1': 1623}
53 {'TEMPSENS_1': 1622}
55 {'TEMPSENS_1': 1630}
57 {'TEMPSENS_1': 1624}
59 {'TEMPSENS_1': 1626}
61 {'TEMPSENS_1': 1627}
63 {'TEMPSENS_1': 1626}
32 {'TEMPSENS_2': 1267}
34 {'TEMPSENS_2': 1277}
36 {'TEMPSENS_2': 1272}
38 {'TEMPSENS_2': 1267}
40 {'TEMPSENS_2': 1275}
42 {'TEMPSENS_2': 1272}
44 {'TEMPSENS_2': 1275}
46 {'TEMPSENS_2': 1264}
48 {'TEMPSENS_2': 1263}
50 {'TEMPSENS_2': 1279}
52 {'TEMPSENS_2': 1278}
54 {'TEMPSENS_2': 1259}
56 {'TEMPSENS_2': 1276}
58 {'TEMPSENS_2': 1284}
60 {'TEMPSENS_2': 1282}
62 {'TEMPSENS_2': 1275}
33 {'TEMPSENS_2': 1617}
35 {'TEMPSENS_2': 1618}
37 {'TEMPSENS_2': 1618}
39 {'TEMPSENS_2': 1614}
41 {'TEMPSENS_2': 1618}
43 {'TEMPSENS_2': 1621}
45 {'TEMPSENS_2': 1615}
47 {'TEMPSENS_2': 1619}
49 {'TEMPSENS_2': 1620}
51 {'TEMPSENS_2': 1617}
53 {'TEMPSENS_2': 1615}
55 {'TEMPSENS_2': 1617}
57 {'TEMPSENS_2': 1619}
59 {'TEMPSENS_2': 1614}
61 {'TEMPSENS_2': 1617}
63 {'TEMPSENS_2': 1617}
32 {'TEMPSENS_3': 1270}
34 {'TEMPSENS_3': 1271}
36 {'TEMPSENS_3': 1272}
38 {'TEMPSENS_3': 1270}
40 {'TEMPSENS_3': 1266}
42 {'TEMPSENS_3': 1269}
44 {'TEMPSENS_3': 1272}
46 {'TEMPSENS_3': 1271}
48 {'TEMPSENS_3': 1276}
50 {'TEMPSENS_3': 1264}
52 {'TEMPSENS_3': 1277}
54 {'TEMPSENS_3': 1276}
56 {'TEMPSENS_3': 1278}
58 {'TEMPSENS_3': 1277}
60 {'TEMPSENS_3': 1273}
62 {'TEMPSENS_3': 1273}
33 {'TEMPSENS_3': 1615}
35 {'TEMPSENS_3': 1617}
37 {'TEMPSENS_3': 1618}
39 {'TEMPSENS_3': 1620}
41 {'TEMPSENS_3': 1618}
43 {'TEMPSENS_3': 1616}
45 {'TEMPSENS_3': 1620}
47 {'TEMPSENS_3': 1615}
49 {'TEMPSENS_3': 1620}
51 {'TEMPSENS_3': 1619}
53 {'TEMPSENS_3': 1620}
55 {'TEMPSENS_3': 1619}
57 {'TEMPSENS_3': 1620}
59 {'TEMPSENS_3': 1615}
61 {'TEMPSENS_3': 1617}
63 {'TEMPSENS_3': 1617}
32 {'TEMPSENS_4': 1265}
34 {'TEMPSENS_4': 1264}
36 {'TEMPSENS_4': 1263}
38 {'TEMPSENS_4': 1267}
40 {'TEMPSENS_4': 1276}
42 {'TEMPSENS_4': 1260}
44 {'TEMPSENS_4': 1256}
46 {'TEMPSENS_4': 1265}
48 {'TEMPSENS_4': 1267}
50 {'TEMPSENS_4': 1276}
52 {'TEMPSENS_4': 1263}
54 {'TEMPSENS_4': 1265}
56 {'TEMPSENS_4': 1270}
58 {'TEMPSENS_4': 1265}
60 {'TEMPSENS_4': 1263}
62 {'TEMPSENS_4': 1265}
33 {'TEMPSENS_4': 1610}
35 {'TEMPSENS_4': 1607}
37 {'TEMPSENS_4': 1612}
39 {'TEMPSENS_4': 1608}
41 {'TEMPSENS_4': 1615}
43 {'TEMPSENS_4': 1612}
45 {'TEMPSENS_4': 1607}
47 {'TEMPSENS_4': 1610}
49 {'TEMPSENS_4': 1612}
51 {'TEMPSENS_4': 1610}
53 {'TEMPSENS_4': 1607}
55 {'TEMPSENS_4': 1607}
57 {'TEMPSENS_4': 1607}
59 {'TEMPSENS_4': 1616}
61 {'TEMPSENS_4': 1607}
63 {'TEMPSENS_4': 1608}
32 {'RADSENS_1': 3068}
34 {'RADSENS_1': 3071}
36 {'RADSENS_1': 3077}
38 {'RADSENS_1': 3080}
40 {'RADSENS_1': 3072}
42 {'RADSENS_1': 3073}
44 {'RADSENS_1': 3076}
46 {'RADSENS_1': 3076}
48 {'RADSENS_1': 3081}
50 {'RADSENS_1': 3063}
52 {'RADSENS_1': 3061}
54 {'RADSENS_1': 3076}
56 {'RADSENS_1': 3071}
58 {'RADSENS_1': 3067}
60 {'RADSENS_1': 3071}
62 {'RADSENS_1': 3069}
33 {'RADSENS_1': 3386}
35 {'RADSENS_1': 3388}
37 {'RADSENS_1': 3385}
39 {'RADSENS_1': 3392}
41 {'RADSENS_1': 3383}
43 {'RADSENS_1': 3388}
45 {'RADSENS_1': 3392}
47 {'RADSENS_1': 3387}
49 {'RADSENS_1': 3381}
51 {'RADSENS_1': 3390}
53 {'RADSENS_1': 3392}
55 {'RADSENS_1': 3395}
57 {'RADSENS_1': 3388}
59 {'RADSENS_1': 3383}
61 {'RADSENS_1': 3388}
63 {'RADSENS_1': 3386}
32 {'RADSENS_2': 2976}
34 {'RADSENS_2': 2976}
36 {'RADSENS_2': 2951}
38 {'RADSENS_2': 2971}
40 {'RADSENS_2': 2963}
42 {'RADSENS_2': 2973}
44 {'RADSENS_2': 2969}
46 {'RADSENS_2': 2958}
48 {'RADSENS_2': 2964}
50 {'RADSENS_2': 2961}
52 {'RADSENS_2': 2972}
54 {'RADSENS_2': 2957}
56 {'RADSENS_2': 2982}
58 {'RADSENS_2': 2967}
60 {'RADSENS_2': 2955}
62 {'RADSENS_2': 2978}
33 {'RADSENS_2': 3345}
35 {'RADSENS_2': 3343}
37 {'RADSENS_2': 3342}
39 {'RADSENS_2': 3345}
41 {'RADSENS_2': 3339}
43 {'RADSENS_2': 3343}
45 {'RADSENS_2': 3343}
47 {'RADSENS_2': 3352}
49 {'RADSENS_2': 3347}
51 {'RADSENS_2': 3346}
53 {'RADSENS_2': 3343}
55 {'RADSENS_2': 3347}
57 {'RADSENS_2': 3341}
59 {'RADSENS_2': 3345}
61 {'RADSENS_2': 3344}
63 {'RADSENS_2': 3343}
32 {'RADSENS_3': 2934}
34 {'RADSENS_3': 2935}
36 {'RADSENS_3': 2948}
38 {'RADSENS_3': 2936}
40 {'RADSENS_3': 2935}
42 {'RADSENS_3': 2940}
44 {'RADSENS_3': 2932}
46 {'RADSENS_3': 2932}
48 {'RADSENS_3': 2927}
50 {'RADSENS_3': 2927}
52 {'RADSENS_3': 2933}
54 {'RADSENS_3': 2927}
56 {'RADSENS_3': 2941}
58 {'RADSENS_3': 2939}
60 {'RADSENS_3': 2943}
62 {'RADSENS_3': 2931}
33 {'RADSENS_3': 3330}
35 {'RADSENS_3': 3329}
37 {'RADSENS_3': 3328}
39 {'RADSENS_3': 3331}
41 {'RADSENS_3': 3328}
43 {'RADSENS_3': 3337}
45 {'RADSENS_3': 3328}
47 {'RADSENS_3': 3336}
49 {'RADSENS_3': 3326}
51 {'RADSENS_3': 3335}
53 {'RADSENS_3': 3329}
55 {'RADSENS_3': 3326}
57 {'RADSENS_3': 3327}
59 {'RADSENS_3': 3327}
61 {'RADSENS_3': 3327}
63 {'RADSENS_3': 3323}
32 {'RADSENS_4': 2941}
34 {'RADSENS_4': 2937}
36 {'RADSENS_4': 2944}
38 {'RADSENS_4': 2939}
40 {'RADSENS_4': 2944}
42 {'RADSENS_4': 2936}
44 {'RADSENS_4': 2943}
46 {'RADSENS_4': 2952}
48 {'RADSENS_4': 2936}
50 {'RADSENS_4': 2921}
52 {'RADSENS_4': 2935}
54 {'RADSENS_4': 2937}
56 {'RADSENS_4': 2932}
58 {'RADSENS_4': 2932}
60 {'RADSENS_4': 2938}
62 {'RADSENS_4': 2936}
33 {'RADSENS_4': 3336}
35 {'RADSENS_4': 3332}
37 {'RADSENS_4': 3327}
39 {'RADSENS_4': 3332}
41 {'RADSENS_4': 3333}
43 {'RADSENS_4': 3334}
45 {'RADSENS_4': 3344}
47 {'RADSENS_4': 3324}
49 {'RADSENS_4': 3334}
51 {'RADSENS_4': 3332}
53 {'RADSENS_4': 3330}
55 {'RADSENS_4': 3329}
57 {'RADSENS_4': 3329}
59 {'RADSENS_4': 3340}
61 {'RADSENS_4': 3330}
63 {'RADSENS_4': 3328}
