Timestamp GlobalTime(s) Dose(Mrad)
01_10_2018_22_00_25 1538146232.06 150
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2155}
34 {'ADCbandgap': 2158}
36 {'ADCbandgap': 2156}
38 {'ADCbandgap': 2155}
40 {'ADCbandgap': 2160}
42 {'ADCbandgap': 2155}
44 {'ADCbandgap': 2155}
46 {'ADCbandgap': 2156}
48 {'ADCbandgap': 2156}
50 {'ADCbandgap': 2159}
52 {'ADCbandgap': 2156}
54 {'ADCbandgap': 2156}
56 {'ADCbandgap': 2156}
58 {'ADCbandgap': 2155}
60 {'ADCbandgap': 2158}
62 {'ADCbandgap': 2150}
33 {'ADCbandgap': 2158}
35 {'ADCbandgap': 2158}
37 {'ADCbandgap': 2155}
39 {'ADCbandgap': 2153}
41 {'ADCbandgap': 2160}
43 {'ADCbandgap': 2155}
45 {'ADCbandgap': 2160}
47 {'ADCbandgap': 2158}
49 {'ADCbandgap': 2154}
51 {'ADCbandgap': 2158}
53 {'ADCbandgap': 2160}
55 {'ADCbandgap': 2156}
57 {'ADCbandgap': 2157}
59 {'ADCbandgap': 2158}
61 {'ADCbandgap': 2155}
63 {'ADCbandgap': 2158}
32 {'TEMPSENS_1': 1282}
34 {'TEMPSENS_1': 1276}
36 {'TEMPSENS_1': 1275}
38 {'TEMPSENS_1': 1277}
40 {'TEMPSENS_1': 1266}
42 {'TEMPSENS_1': 1279}
44 {'TEMPSENS_1': 1273}
46 {'TEMPSENS_1': 1269}
48 {'TEMPSENS_1': 1282}
50 {'TEMPSENS_1': 1279}
52 {'TEMPSENS_1': 1279}
54 {'TEMPSENS_1': 1277}
56 {'TEMPSENS_1': 1278}
58 {'TEMPSENS_1': 1273}
60 {'TEMPSENS_1': 1276}
62 {'TEMPSENS_1': 1273}
33 {'TEMPSENS_1': 1617}
35 {'TEMPSENS_1': 1618}
37 {'TEMPSENS_1': 1622}
39 {'TEMPSENS_1': 1618}
41 {'TEMPSENS_1': 1619}
43 {'TEMPSENS_1': 1618}
45 {'TEMPSENS_1': 1621}
47 {'TEMPSENS_1': 1618}
49 {'TEMPSENS_1': 1620}
51 {'TEMPSENS_1': 1619}
53 {'TEMPSENS_1': 1615}
55 {'TEMPSENS_1': 1624}
57 {'TEMPSENS_1': 1615}
59 {'TEMPSENS_1': 1617}
61 {'TEMPSENS_1': 1619}
63 {'TEMPSENS_1': 1616}
32 {'TEMPSENS_2': 1263}
34 {'TEMPSENS_2': 1276}
36 {'TEMPSENS_2': 1268}
38 {'TEMPSENS_2': 1268}
40 {'TEMPSENS_2': 1267}
42 {'TEMPSENS_2': 1266}
44 {'TEMPSENS_2': 1272}
46 {'TEMPSENS_2': 1264}
48 {'TEMPSENS_2': 1268}
50 {'TEMPSENS_2': 1276}
52 {'TEMPSENS_2': 1272}
54 {'TEMPSENS_2': 1260}
56 {'TEMPSENS_2': 1276}
58 {'TEMPSENS_2': 1275}
60 {'TEMPSENS_2': 1271}
62 {'TEMPSENS_2': 1274}
33 {'TEMPSENS_2': 1617}
35 {'TEMPSENS_2': 1617}
37 {'TEMPSENS_2': 1616}
39 {'TEMPSENS_2': 1611}
41 {'TEMPSENS_2': 1614}
43 {'TEMPSENS_2': 1615}
45 {'TEMPSENS_2': 1617}
47 {'TEMPSENS_2': 1611}
49 {'TEMPSENS_2': 1612}
51 {'TEMPSENS_2': 1614}
53 {'TEMPSENS_2': 1615}
55 {'TEMPSENS_2': 1617}
57 {'TEMPSENS_2': 1613}
59 {'TEMPSENS_2': 1611}
61 {'TEMPSENS_2': 1618}
63 {'TEMPSENS_2': 1616}
32 {'TEMPSENS_3': 1268}
34 {'TEMPSENS_3': 1276}
36 {'TEMPSENS_3': 1267}
38 {'TEMPSENS_3': 1260}
40 {'TEMPSENS_3': 1268}
42 {'TEMPSENS_3': 1273}
44 {'TEMPSENS_3': 1265}
46 {'TEMPSENS_3': 1263}
48 {'TEMPSENS_3': 1271}
50 {'TEMPSENS_3': 1263}
52 {'TEMPSENS_3': 1279}
54 {'TEMPSENS_3': 1270}
56 {'TEMPSENS_3': 1279}
58 {'TEMPSENS_3': 1274}
60 {'TEMPSENS_3': 1270}
62 {'TEMPSENS_3': 1269}
33 {'TEMPSENS_3': 1611}
35 {'TEMPSENS_3': 1615}
37 {'TEMPSENS_3': 1611}
39 {'TEMPSENS_3': 1614}
41 {'TEMPSENS_3': 1618}
43 {'TEMPSENS_3': 1616}
45 {'TEMPSENS_3': 1616}
47 {'TEMPSENS_3': 1616}
49 {'TEMPSENS_3': 1613}
51 {'TEMPSENS_3': 1616}
53 {'TEMPSENS_3': 1614}
55 {'TEMPSENS_3': 1616}
57 {'TEMPSENS_3': 1614}
59 {'TEMPSENS_3': 1617}
61 {'TEMPSENS_3': 1615}
63 {'TEMPSENS_3': 1617}
32 {'TEMPSENS_4': 1262}
34 {'TEMPSENS_4': 1260}
36 {'TEMPSENS_4': 1260}
38 {'TEMPSENS_4': 1262}
40 {'TEMPSENS_4': 1264}
42 {'TEMPSENS_4': 1249}
44 {'TEMPSENS_4': 1264}
46 {'TEMPSENS_4': 1264}
48 {'TEMPSENS_4': 1264}
50 {'TEMPSENS_4': 1267}
52 {'TEMPSENS_4': 1251}
54 {'TEMPSENS_4': 1266}
56 {'TEMPSENS_4': 1270}
58 {'TEMPSENS_4': 1251}
60 {'TEMPSENS_4': 1266}
62 {'TEMPSENS_4': 1259}
33 {'TEMPSENS_4': 1606}
35 {'TEMPSENS_4': 1608}
37 {'TEMPSENS_4': 1605}
39 {'TEMPSENS_4': 1608}
41 {'TEMPSENS_4': 1606}
43 {'TEMPSENS_4': 1606}
45 {'TEMPSENS_4': 1607}
47 {'TEMPSENS_4': 1603}
49 {'TEMPSENS_4': 1605}
51 {'TEMPSENS_4': 1608}
53 {'TEMPSENS_4': 1603}
55 {'TEMPSENS_4': 1599}
57 {'TEMPSENS_4': 1604}
59 {'TEMPSENS_4': 1606}
61 {'TEMPSENS_4': 1602}
63 {'TEMPSENS_4': 1607}
32 {'RADSENS_1': 3046}
34 {'RADSENS_1': 3046}
36 {'RADSENS_1': 3048}
38 {'RADSENS_1': 3056}
40 {'RADSENS_1': 3039}
42 {'RADSENS_1': 3048}
44 {'RADSENS_1': 3045}
46 {'RADSENS_1': 3054}
48 {'RADSENS_1': 3056}
50 {'RADSENS_1': 3031}
52 {'RADSENS_1': 3046}
54 {'RADSENS_1': 3039}
56 {'RADSENS_1': 3047}
58 {'RADSENS_1': 3046}
60 {'RADSENS_1': 3046}
62 {'RADSENS_1': 3038}
33 {'RADSENS_1': 3376}
35 {'RADSENS_1': 3370}
37 {'RADSENS_1': 3371}
39 {'RADSENS_1': 3367}
41 {'RADSENS_1': 3367}
43 {'RADSENS_1': 3374}
45 {'RADSENS_1': 3372}
47 {'RADSENS_1': 3367}
49 {'RADSENS_1': 3372}
51 {'RADSENS_1': 3367}
53 {'RADSENS_1': 3371}
55 {'RADSENS_1': 3371}
57 {'RADSENS_1': 3376}
59 {'RADSENS_1': 3367}
61 {'RADSENS_1': 3374}
63 {'RADSENS_1': 3376}
32 {'RADSENS_2': 2960}
34 {'RADSENS_2': 2968}
36 {'RADSENS_2': 2946}
38 {'RADSENS_2': 2958}
40 {'RADSENS_2': 2946}
42 {'RADSENS_2': 2960}
44 {'RADSENS_2': 2946}
46 {'RADSENS_2': 2945}
48 {'RADSENS_2': 2935}
50 {'RADSENS_2': 2952}
52 {'RADSENS_2': 2948}
54 {'RADSENS_2': 2942}
56 {'RADSENS_2': 2960}
58 {'RADSENS_2': 2948}
60 {'RADSENS_2': 2951}
62 {'RADSENS_2': 2968}
33 {'RADSENS_2': 3330}
35 {'RADSENS_2': 3333}
37 {'RADSENS_2': 3335}
39 {'RADSENS_2': 3335}
41 {'RADSENS_2': 3335}
43 {'RADSENS_2': 3337}
45 {'RADSENS_2': 3335}
47 {'RADSENS_2': 3327}
49 {'RADSENS_2': 3327}
51 {'RADSENS_2': 3336}
53 {'RADSENS_2': 3331}
55 {'RADSENS_2': 3331}
57 {'RADSENS_2': 3336}
59 {'RADSENS_2': 3335}
61 {'RADSENS_2': 3327}
63 {'RADSENS_2': 3327}
32 {'RADSENS_3': 2921}
34 {'RADSENS_3': 2920}
36 {'RADSENS_3': 2935}
38 {'RADSENS_3': 2929}
40 {'RADSENS_3': 2926}
42 {'RADSENS_3': 2936}
44 {'RADSENS_3': 2919}
46 {'RADSENS_3': 2926}
48 {'RADSENS_3': 2915}
50 {'RADSENS_3': 2914}
52 {'RADSENS_3': 2936}
54 {'RADSENS_3': 2919}
56 {'RADSENS_3': 2929}
58 {'RADSENS_3': 2919}
60 {'RADSENS_3': 2940}
62 {'RADSENS_3': 2919}
33 {'RADSENS_3': 3319}
35 {'RADSENS_3': 3319}
37 {'RADSENS_3': 3316}
39 {'RADSENS_3': 3321}
41 {'RADSENS_3': 3317}
43 {'RADSENS_3': 3311}
45 {'RADSENS_3': 3328}
47 {'RADSENS_3': 3315}
49 {'RADSENS_3': 3317}
51 {'RADSENS_3': 3324}
53 {'RADSENS_3': 3326}
55 {'RADSENS_3': 3321}
57 {'RADSENS_3': 3320}
59 {'RADSENS_3': 3324}
61 {'RADSENS_3': 3320}
63 {'RADSENS_3': 3317}
32 {'RADSENS_4': 2926}
34 {'RADSENS_4': 2938}
36 {'RADSENS_4': 2934}
38 {'RADSENS_4': 2923}
40 {'RADSENS_4': 2934}
42 {'RADSENS_4': 2921}
44 {'RADSENS_4': 2936}
46 {'RADSENS_4': 2933}
48 {'RADSENS_4': 2911}
50 {'RADSENS_4': 2919}
52 {'RADSENS_4': 2929}
54 {'RADSENS_4': 2907}
56 {'RADSENS_4': 2924}
58 {'RADSENS_4': 2917}
60 {'RADSENS_4': 2929}
62 {'RADSENS_4': 2927}
33 {'RADSENS_4': 3313}
35 {'RADSENS_4': 3322}
37 {'RADSENS_4': 3316}
39 {'RADSENS_4': 3315}
41 {'RADSENS_4': 3321}
43 {'RADSENS_4': 3311}
45 {'RADSENS_4': 3311}
47 {'RADSENS_4': 3320}
49 {'RADSENS_4': 3324}
51 {'RADSENS_4': 3321}
53 {'RADSENS_4': 3314}
55 {'RADSENS_4': 3317}
57 {'RADSENS_4': 3315}
59 {'RADSENS_4': 3315}
61 {'RADSENS_4': 3320}
63 {'RADSENS_4': 3313}
