Timestamp GlobalTime(s) Dose(Mrad)
01_10_2018_07_36_33 1538146232.06 80
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2157}
34 {'ADCbandgap': 2156}
36 {'ADCbandgap': 2155}
38 {'ADCbandgap': 2158}
40 {'ADCbandgap': 2153}
42 {'ADCbandgap': 2157}
44 {'ADCbandgap': 2158}
46 {'ADCbandgap': 2160}
48 {'ADCbandgap': 2158}
50 {'ADCbandgap': 2156}
52 {'ADCbandgap': 2155}
54 {'ADCbandgap': 2155}
56 {'ADCbandgap': 2155}
58 {'ADCbandgap': 2157}
60 {'ADCbandgap': 2151}
62 {'ADCbandgap': 2157}
33 {'ADCbandgap': 2157}
35 {'ADCbandgap': 2157}
37 {'ADCbandgap': 2155}
39 {'ADCbandgap': 2155}
41 {'ADCbandgap': 2160}
43 {'ADCbandgap': 2160}
45 {'ADCbandgap': 2156}
47 {'ADCbandgap': 2156}
49 {'ADCbandgap': 2155}
51 {'ADCbandgap': 2156}
53 {'ADCbandgap': 2160}
55 {'ADCbandgap': 2156}
57 {'ADCbandgap': 2156}
59 {'ADCbandgap': 2161}
61 {'ADCbandgap': 2158}
63 {'ADCbandgap': 2156}
32 {'TEMPSENS_1': 1278}
34 {'TEMPSENS_1': 1279}
36 {'TEMPSENS_1': 1274}
38 {'TEMPSENS_1': 1276}
40 {'TEMPSENS_1': 1265}
42 {'TEMPSENS_1': 1275}
44 {'TEMPSENS_1': 1267}
46 {'TEMPSENS_1': 1271}
48 {'TEMPSENS_1': 1281}
50 {'TEMPSENS_1': 1279}
52 {'TEMPSENS_1': 1278}
54 {'TEMPSENS_1': 1275}
56 {'TEMPSENS_1': 1275}
58 {'TEMPSENS_1': 1277}
60 {'TEMPSENS_1': 1275}
62 {'TEMPSENS_1': 1270}
33 {'TEMPSENS_1': 1620}
35 {'TEMPSENS_1': 1617}
37 {'TEMPSENS_1': 1617}
39 {'TEMPSENS_1': 1615}
41 {'TEMPSENS_1': 1620}
43 {'TEMPSENS_1': 1624}
45 {'TEMPSENS_1': 1624}
47 {'TEMPSENS_1': 1617}
49 {'TEMPSENS_1': 1620}
51 {'TEMPSENS_1': 1619}
53 {'TEMPSENS_1': 1621}
55 {'TEMPSENS_1': 1615}
57 {'TEMPSENS_1': 1615}
59 {'TEMPSENS_1': 1617}
61 {'TEMPSENS_1': 1624}
63 {'TEMPSENS_1': 1624}
32 {'TEMPSENS_2': 1255}
34 {'TEMPSENS_2': 1271}
36 {'TEMPSENS_2': 1266}
38 {'TEMPSENS_2': 1262}
40 {'TEMPSENS_2': 1268}
42 {'TEMPSENS_2': 1264}
44 {'TEMPSENS_2': 1271}
46 {'TEMPSENS_2': 1262}
48 {'TEMPSENS_2': 1263}
50 {'TEMPSENS_2': 1268}
52 {'TEMPSENS_2': 1272}
54 {'TEMPSENS_2': 1264}
56 {'TEMPSENS_2': 1272}
58 {'TEMPSENS_2': 1271}
60 {'TEMPSENS_2': 1272}
62 {'TEMPSENS_2': 1272}
33 {'TEMPSENS_2': 1617}
35 {'TEMPSENS_2': 1614}
37 {'TEMPSENS_2': 1613}
39 {'TEMPSENS_2': 1611}
41 {'TEMPSENS_2': 1614}
43 {'TEMPSENS_2': 1612}
45 {'TEMPSENS_2': 1611}
47 {'TEMPSENS_2': 1612}
49 {'TEMPSENS_2': 1614}
51 {'TEMPSENS_2': 1610}
53 {'TEMPSENS_2': 1609}
55 {'TEMPSENS_2': 1611}
57 {'TEMPSENS_2': 1612}
59 {'TEMPSENS_2': 1611}
61 {'TEMPSENS_2': 1614}
63 {'TEMPSENS_2': 1616}
32 {'TEMPSENS_3': 1264}
34 {'TEMPSENS_3': 1275}
36 {'TEMPSENS_3': 1261}
38 {'TEMPSENS_3': 1262}
40 {'TEMPSENS_3': 1267}
42 {'TEMPSENS_3': 1269}
44 {'TEMPSENS_3': 1265}
46 {'TEMPSENS_3': 1260}
48 {'TEMPSENS_3': 1271}
50 {'TEMPSENS_3': 1263}
52 {'TEMPSENS_3': 1269}
54 {'TEMPSENS_3': 1262}
56 {'TEMPSENS_3': 1276}
58 {'TEMPSENS_3': 1270}
60 {'TEMPSENS_3': 1268}
62 {'TEMPSENS_3': 1267}
33 {'TEMPSENS_3': 1611}
35 {'TEMPSENS_3': 1611}
37 {'TEMPSENS_3': 1611}
39 {'TEMPSENS_3': 1615}
41 {'TEMPSENS_3': 1612}
43 {'TEMPSENS_3': 1611}
45 {'TEMPSENS_3': 1613}
47 {'TEMPSENS_3': 1611}
49 {'TEMPSENS_3': 1616}
51 {'TEMPSENS_3': 1611}
53 {'TEMPSENS_3': 1613}
55 {'TEMPSENS_3': 1612}
57 {'TEMPSENS_3': 1612}
59 {'TEMPSENS_3': 1612}
61 {'TEMPSENS_3': 1609}
63 {'TEMPSENS_3': 1612}
32 {'TEMPSENS_4': 1264}
34 {'TEMPSENS_4': 1260}
36 {'TEMPSENS_4': 1256}
38 {'TEMPSENS_4': 1260}
40 {'TEMPSENS_4': 1259}
42 {'TEMPSENS_4': 1255}
44 {'TEMPSENS_4': 1250}
46 {'TEMPSENS_4': 1257}
48 {'TEMPSENS_4': 1264}
50 {'TEMPSENS_4': 1268}
52 {'TEMPSENS_4': 1255}
54 {'TEMPSENS_4': 1256}
56 {'TEMPSENS_4': 1269}
58 {'TEMPSENS_4': 1256}
60 {'TEMPSENS_4': 1257}
62 {'TEMPSENS_4': 1259}
33 {'TEMPSENS_4': 1604}
35 {'TEMPSENS_4': 1602}
37 {'TEMPSENS_4': 1608}
39 {'TEMPSENS_4': 1602}
41 {'TEMPSENS_4': 1605}
43 {'TEMPSENS_4': 1602}
45 {'TEMPSENS_4': 1604}
47 {'TEMPSENS_4': 1607}
49 {'TEMPSENS_4': 1601}
51 {'TEMPSENS_4': 1599}
53 {'TEMPSENS_4': 1606}
55 {'TEMPSENS_4': 1603}
57 {'TEMPSENS_4': 1604}
59 {'TEMPSENS_4': 1603}
61 {'TEMPSENS_4': 1606}
63 {'TEMPSENS_4': 1605}
32 {'RADSENS_1': 3056}
34 {'RADSENS_1': 3054}
36 {'RADSENS_1': 3066}
38 {'RADSENS_1': 3055}
40 {'RADSENS_1': 3056}
42 {'RADSENS_1': 3050}
44 {'RADSENS_1': 3049}
46 {'RADSENS_1': 3062}
48 {'RADSENS_1': 3051}
50 {'RADSENS_1': 3052}
52 {'RADSENS_1': 3052}
54 {'RADSENS_1': 3049}
56 {'RADSENS_1': 3057}
58 {'RADSENS_1': 3064}
60 {'RADSENS_1': 3064}
62 {'RADSENS_1': 3039}
33 {'RADSENS_1': 3376}
35 {'RADSENS_1': 3376}
37 {'RADSENS_1': 3376}
39 {'RADSENS_1': 3376}
41 {'RADSENS_1': 3375}
43 {'RADSENS_1': 3375}
45 {'RADSENS_1': 3375}
47 {'RADSENS_1': 3370}
49 {'RADSENS_1': 3374}
51 {'RADSENS_1': 3377}
53 {'RADSENS_1': 3372}
55 {'RADSENS_1': 3376}
57 {'RADSENS_1': 3370}
59 {'RADSENS_1': 3367}
61 {'RADSENS_1': 3373}
63 {'RADSENS_1': 3374}
32 {'RADSENS_2': 2952}
34 {'RADSENS_2': 2964}
36 {'RADSENS_2': 2939}
38 {'RADSENS_2': 2960}
40 {'RADSENS_2': 2945}
42 {'RADSENS_2': 2968}
44 {'RADSENS_2': 2952}
46 {'RADSENS_2': 2952}
48 {'RADSENS_2': 2943}
50 {'RADSENS_2': 2942}
52 {'RADSENS_2': 2956}
54 {'RADSENS_2': 2937}
56 {'RADSENS_2': 2960}
58 {'RADSENS_2': 2951}
60 {'RADSENS_2': 2949}
62 {'RADSENS_2': 2950}
33 {'RADSENS_2': 3334}
35 {'RADSENS_2': 3331}
37 {'RADSENS_2': 3330}
39 {'RADSENS_2': 3330}
41 {'RADSENS_2': 3331}
43 {'RADSENS_2': 3334}
45 {'RADSENS_2': 3329}
47 {'RADSENS_2': 3331}
49 {'RADSENS_2': 3330}
51 {'RADSENS_2': 3336}
53 {'RADSENS_2': 3335}
55 {'RADSENS_2': 3331}
57 {'RADSENS_2': 3338}
59 {'RADSENS_2': 3340}
61 {'RADSENS_2': 3331}
63 {'RADSENS_2': 3331}
32 {'RADSENS_3': 2915}
34 {'RADSENS_3': 2919}
36 {'RADSENS_3': 2936}
38 {'RADSENS_3': 2928}
40 {'RADSENS_3': 2928}
42 {'RADSENS_3': 2926}
44 {'RADSENS_3': 2919}
46 {'RADSENS_3': 2928}
48 {'RADSENS_3': 2923}
50 {'RADSENS_3': 2926}
52 {'RADSENS_3': 2919}
54 {'RADSENS_3': 2924}
56 {'RADSENS_3': 2926}
58 {'RADSENS_3': 2915}
60 {'RADSENS_3': 2926}
62 {'RADSENS_3': 2924}
33 {'RADSENS_3': 3320}
35 {'RADSENS_3': 3328}
37 {'RADSENS_3': 3311}
39 {'RADSENS_3': 3328}
41 {'RADSENS_3': 3318}
43 {'RADSENS_3': 3320}
45 {'RADSENS_3': 3314}
47 {'RADSENS_3': 3315}
49 {'RADSENS_3': 3311}
51 {'RADSENS_3': 3311}
53 {'RADSENS_3': 3315}
55 {'RADSENS_3': 3328}
57 {'RADSENS_3': 3320}
59 {'RADSENS_3': 3315}
61 {'RADSENS_3': 3320}
63 {'RADSENS_3': 3311}
32 {'RADSENS_4': 2936}
34 {'RADSENS_4': 2936}
36 {'RADSENS_4': 2919}
38 {'RADSENS_4': 2924}
40 {'RADSENS_4': 2934}
42 {'RADSENS_4': 2917}
44 {'RADSENS_4': 2934}
46 {'RADSENS_4': 2936}
48 {'RADSENS_4': 2911}
50 {'RADSENS_4': 2911}
52 {'RADSENS_4': 2929}
54 {'RADSENS_4': 2929}
56 {'RADSENS_4': 2918}
58 {'RADSENS_4': 2911}
60 {'RADSENS_4': 2923}
62 {'RADSENS_4': 2918}
33 {'RADSENS_4': 3320}
35 {'RADSENS_4': 3315}
37 {'RADSENS_4': 3320}
39 {'RADSENS_4': 3321}
41 {'RADSENS_4': 3317}
43 {'RADSENS_4': 3315}
45 {'RADSENS_4': 3315}
47 {'RADSENS_4': 3315}
49 {'RADSENS_4': 3315}
51 {'RADSENS_4': 3318}
53 {'RADSENS_4': 3311}
55 {'RADSENS_4': 3324}
57 {'RADSENS_4': 3328}
59 {'RADSENS_4': 3317}
61 {'RADSENS_4': 3320}
63 {'RADSENS_4': 3320}
