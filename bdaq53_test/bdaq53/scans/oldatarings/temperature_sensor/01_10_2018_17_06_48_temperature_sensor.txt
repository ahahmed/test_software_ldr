Timestamp GlobalTime(s) Dose(Mrad)
01_10_2018_17_06_48 1538146232.06 100
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2160}
34 {'ADCbandgap': 2159}
36 {'ADCbandgap': 2153}
38 {'ADCbandgap': 2153}
40 {'ADCbandgap': 2158}
42 {'ADCbandgap': 2155}
44 {'ADCbandgap': 2158}
46 {'ADCbandgap': 2156}
48 {'ADCbandgap': 2158}
50 {'ADCbandgap': 2155}
52 {'ADCbandgap': 2155}
54 {'ADCbandgap': 2155}
56 {'ADCbandgap': 2158}
58 {'ADCbandgap': 2157}
60 {'ADCbandgap': 2153}
62 {'ADCbandgap': 2157}
33 {'ADCbandgap': 2155}
35 {'ADCbandgap': 2156}
37 {'ADCbandgap': 2156}
39 {'ADCbandgap': 2155}
41 {'ADCbandgap': 2155}
43 {'ADCbandgap': 2160}
45 {'ADCbandgap': 2160}
47 {'ADCbandgap': 2157}
49 {'ADCbandgap': 2157}
51 {'ADCbandgap': 2160}
53 {'ADCbandgap': 2158}
55 {'ADCbandgap': 2159}
57 {'ADCbandgap': 2160}
59 {'ADCbandgap': 2156}
61 {'ADCbandgap': 2160}
63 {'ADCbandgap': 2157}
32 {'TEMPSENS_1': 1280}
34 {'TEMPSENS_1': 1271}
36 {'TEMPSENS_1': 1270}
38 {'TEMPSENS_1': 1274}
40 {'TEMPSENS_1': 1272}
42 {'TEMPSENS_1': 1280}
44 {'TEMPSENS_1': 1271}
46 {'TEMPSENS_1': 1263}
48 {'TEMPSENS_1': 1280}
50 {'TEMPSENS_1': 1279}
52 {'TEMPSENS_1': 1279}
54 {'TEMPSENS_1': 1271}
56 {'TEMPSENS_1': 1272}
58 {'TEMPSENS_1': 1272}
60 {'TEMPSENS_1': 1278}
62 {'TEMPSENS_1': 1267}
33 {'TEMPSENS_1': 1620}
35 {'TEMPSENS_1': 1616}
37 {'TEMPSENS_1': 1619}
39 {'TEMPSENS_1': 1622}
41 {'TEMPSENS_1': 1619}
43 {'TEMPSENS_1': 1622}
45 {'TEMPSENS_1': 1620}
47 {'TEMPSENS_1': 1624}
49 {'TEMPSENS_1': 1619}
51 {'TEMPSENS_1': 1619}
53 {'TEMPSENS_1': 1615}
55 {'TEMPSENS_1': 1622}
57 {'TEMPSENS_1': 1619}
59 {'TEMPSENS_1': 1619}
61 {'TEMPSENS_1': 1617}
63 {'TEMPSENS_1': 1615}
32 {'TEMPSENS_2': 1255}
34 {'TEMPSENS_2': 1268}
36 {'TEMPSENS_2': 1271}
38 {'TEMPSENS_2': 1263}
40 {'TEMPSENS_2': 1265}
42 {'TEMPSENS_2': 1266}
44 {'TEMPSENS_2': 1266}
46 {'TEMPSENS_2': 1264}
48 {'TEMPSENS_2': 1263}
50 {'TEMPSENS_2': 1270}
52 {'TEMPSENS_2': 1271}
54 {'TEMPSENS_2': 1258}
56 {'TEMPSENS_2': 1267}
58 {'TEMPSENS_2': 1278}
60 {'TEMPSENS_2': 1274}
62 {'TEMPSENS_2': 1265}
33 {'TEMPSENS_2': 1614}
35 {'TEMPSENS_2': 1611}
37 {'TEMPSENS_2': 1614}
39 {'TEMPSENS_2': 1615}
41 {'TEMPSENS_2': 1614}
43 {'TEMPSENS_2': 1615}
45 {'TEMPSENS_2': 1615}
47 {'TEMPSENS_2': 1616}
49 {'TEMPSENS_2': 1609}
51 {'TEMPSENS_2': 1612}
53 {'TEMPSENS_2': 1614}
55 {'TEMPSENS_2': 1612}
57 {'TEMPSENS_2': 1614}
59 {'TEMPSENS_2': 1613}
61 {'TEMPSENS_2': 1609}
63 {'TEMPSENS_2': 1614}
32 {'TEMPSENS_3': 1267}
34 {'TEMPSENS_3': 1267}
36 {'TEMPSENS_3': 1266}
38 {'TEMPSENS_3': 1263}
40 {'TEMPSENS_3': 1265}
42 {'TEMPSENS_3': 1267}
44 {'TEMPSENS_3': 1261}
46 {'TEMPSENS_3': 1259}
48 {'TEMPSENS_3': 1271}
50 {'TEMPSENS_3': 1262}
52 {'TEMPSENS_3': 1273}
54 {'TEMPSENS_3': 1272}
56 {'TEMPSENS_3': 1271}
58 {'TEMPSENS_3': 1267}
60 {'TEMPSENS_3': 1273}
62 {'TEMPSENS_3': 1270}
33 {'TEMPSENS_3': 1613}
35 {'TEMPSENS_3': 1614}
37 {'TEMPSENS_3': 1616}
39 {'TEMPSENS_3': 1612}
41 {'TEMPSENS_3': 1616}
43 {'TEMPSENS_3': 1615}
45 {'TEMPSENS_3': 1614}
47 {'TEMPSENS_3': 1611}
49 {'TEMPSENS_3': 1616}
51 {'TEMPSENS_3': 1611}
53 {'TEMPSENS_3': 1615}
55 {'TEMPSENS_3': 1611}
57 {'TEMPSENS_3': 1611}
59 {'TEMPSENS_3': 1611}
61 {'TEMPSENS_3': 1614}
63 {'TEMPSENS_3': 1611}
32 {'TEMPSENS_4': 1260}
34 {'TEMPSENS_4': 1254}
36 {'TEMPSENS_4': 1256}
38 {'TEMPSENS_4': 1264}
40 {'TEMPSENS_4': 1261}
42 {'TEMPSENS_4': 1250}
44 {'TEMPSENS_4': 1256}
46 {'TEMPSENS_4': 1255}
48 {'TEMPSENS_4': 1256}
50 {'TEMPSENS_4': 1263}
52 {'TEMPSENS_4': 1252}
54 {'TEMPSENS_4': 1260}
56 {'TEMPSENS_4': 1269}
58 {'TEMPSENS_4': 1249}
60 {'TEMPSENS_4': 1251}
62 {'TEMPSENS_4': 1262}
33 {'TEMPSENS_4': 1603}
35 {'TEMPSENS_4': 1601}
37 {'TEMPSENS_4': 1603}
39 {'TEMPSENS_4': 1608}
41 {'TEMPSENS_4': 1601}
43 {'TEMPSENS_4': 1605}
45 {'TEMPSENS_4': 1601}
47 {'TEMPSENS_4': 1604}
49 {'TEMPSENS_4': 1599}
51 {'TEMPSENS_4': 1604}
53 {'TEMPSENS_4': 1601}
55 {'TEMPSENS_4': 1605}
57 {'TEMPSENS_4': 1606}
59 {'TEMPSENS_4': 1604}
61 {'TEMPSENS_4': 1604}
63 {'TEMPSENS_4': 1602}
32 {'RADSENS_1': 3040}
34 {'RADSENS_1': 3035}
36 {'RADSENS_1': 3051}
38 {'RADSENS_1': 3052}
40 {'RADSENS_1': 3039}
42 {'RADSENS_1': 3049}
44 {'RADSENS_1': 3040}
46 {'RADSENS_1': 3055}
48 {'RADSENS_1': 3056}
50 {'RADSENS_1': 3042}
52 {'RADSENS_1': 3056}
54 {'RADSENS_1': 3039}
56 {'RADSENS_1': 3047}
58 {'RADSENS_1': 3051}
60 {'RADSENS_1': 3050}
62 {'RADSENS_1': 3048}
33 {'RADSENS_1': 3376}
35 {'RADSENS_1': 3366}
37 {'RADSENS_1': 3363}
39 {'RADSENS_1': 3374}
41 {'RADSENS_1': 3369}
43 {'RADSENS_1': 3376}
45 {'RADSENS_1': 3374}
47 {'RADSENS_1': 3380}
49 {'RADSENS_1': 3368}
51 {'RADSENS_1': 3367}
53 {'RADSENS_1': 3376}
55 {'RADSENS_1': 3376}
57 {'RADSENS_1': 3371}
59 {'RADSENS_1': 3367}
61 {'RADSENS_1': 3367}
63 {'RADSENS_1': 3371}
32 {'RADSENS_2': 2943}
34 {'RADSENS_2': 2944}
36 {'RADSENS_2': 2947}
38 {'RADSENS_2': 2951}
40 {'RADSENS_2': 2936}
42 {'RADSENS_2': 2958}
44 {'RADSENS_2': 2939}
46 {'RADSENS_2': 2935}
48 {'RADSENS_2': 2941}
50 {'RADSENS_2': 2946}
52 {'RADSENS_2': 2952}
54 {'RADSENS_2': 2935}
56 {'RADSENS_2': 2960}
58 {'RADSENS_2': 2944}
60 {'RADSENS_2': 2945}
62 {'RADSENS_2': 2952}
33 {'RADSENS_2': 3334}
35 {'RADSENS_2': 3331}
37 {'RADSENS_2': 3336}
39 {'RADSENS_2': 3327}
41 {'RADSENS_2': 3337}
43 {'RADSENS_2': 3332}
45 {'RADSENS_2': 3333}
47 {'RADSENS_2': 3331}
49 {'RADSENS_2': 3344}
51 {'RADSENS_2': 3334}
53 {'RADSENS_2': 3340}
55 {'RADSENS_2': 3336}
57 {'RADSENS_2': 3329}
59 {'RADSENS_2': 3327}
61 {'RADSENS_2': 3334}
63 {'RADSENS_2': 3329}
32 {'RADSENS_3': 2920}
34 {'RADSENS_3': 2921}
36 {'RADSENS_3': 2936}
38 {'RADSENS_3': 2917}
40 {'RADSENS_3': 2931}
42 {'RADSENS_3': 2923}
44 {'RADSENS_3': 2924}
46 {'RADSENS_3': 2930}
48 {'RADSENS_3': 2925}
50 {'RADSENS_3': 2909}
52 {'RADSENS_3': 2932}
54 {'RADSENS_3': 2910}
56 {'RADSENS_3': 2920}
58 {'RADSENS_3': 2922}
60 {'RADSENS_3': 2923}
62 {'RADSENS_3': 2922}
33 {'RADSENS_3': 3324}
35 {'RADSENS_3': 3319}
37 {'RADSENS_3': 3313}
39 {'RADSENS_3': 3323}
41 {'RADSENS_3': 3320}
43 {'RADSENS_3': 3321}
45 {'RADSENS_3': 3322}
47 {'RADSENS_3': 3311}
49 {'RADSENS_3': 3316}
51 {'RADSENS_3': 3321}
53 {'RADSENS_3': 3320}
55 {'RADSENS_3': 3315}
57 {'RADSENS_3': 3318}
59 {'RADSENS_3': 3318}
61 {'RADSENS_3': 3324}
63 {'RADSENS_3': 3313}
32 {'RADSENS_4': 2920}
34 {'RADSENS_4': 2927}
36 {'RADSENS_4': 2915}
38 {'RADSENS_4': 2918}
40 {'RADSENS_4': 2932}
42 {'RADSENS_4': 2924}
44 {'RADSENS_4': 2925}
46 {'RADSENS_4': 2936}
48 {'RADSENS_4': 2910}
50 {'RADSENS_4': 2911}
52 {'RADSENS_4': 2927}
54 {'RADSENS_4': 2911}
56 {'RADSENS_4': 2919}
58 {'RADSENS_4': 2911}
60 {'RADSENS_4': 2913}
62 {'RADSENS_4': 2917}
33 {'RADSENS_4': 3311}
35 {'RADSENS_4': 3311}
37 {'RADSENS_4': 3318}
39 {'RADSENS_4': 3315}
41 {'RADSENS_4': 3312}
43 {'RADSENS_4': 3311}
45 {'RADSENS_4': 3311}
47 {'RADSENS_4': 3311}
49 {'RADSENS_4': 3318}
51 {'RADSENS_4': 3326}
53 {'RADSENS_4': 3311}
55 {'RADSENS_4': 3319}
57 {'RADSENS_4': 3313}
59 {'RADSENS_4': 3318}
61 {'RADSENS_4': 3315}
63 {'RADSENS_4': 3311}
