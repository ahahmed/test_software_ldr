Timestamp GlobalTime(s) Dose(Mrad)
02_10_2018_03_25_09 1538146232.06 150
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2160}
34 {'ADCbandgap': 2160}
36 {'ADCbandgap': 2153}
38 {'ADCbandgap': 2157}
40 {'ADCbandgap': 2156}
42 {'ADCbandgap': 2151}
44 {'ADCbandgap': 2160}
46 {'ADCbandgap': 2155}
48 {'ADCbandgap': 2158}
50 {'ADCbandgap': 2157}
52 {'ADCbandgap': 2158}
54 {'ADCbandgap': 2155}
56 {'ADCbandgap': 2160}
58 {'ADCbandgap': 2157}
60 {'ADCbandgap': 2155}
62 {'ADCbandgap': 2154}
33 {'ADCbandgap': 2156}
35 {'ADCbandgap': 2155}
37 {'ADCbandgap': 2156}
39 {'ADCbandgap': 2155}
41 {'ADCbandgap': 2158}
43 {'ADCbandgap': 2154}
45 {'ADCbandgap': 2155}
47 {'ADCbandgap': 2161}
49 {'ADCbandgap': 2155}
51 {'ADCbandgap': 2157}
53 {'ADCbandgap': 2155}
55 {'ADCbandgap': 2157}
57 {'ADCbandgap': 2157}
59 {'ADCbandgap': 2158}
61 {'ADCbandgap': 2160}
63 {'ADCbandgap': 2158}
32 {'TEMPSENS_1': 1274}
34 {'TEMPSENS_1': 1274}
36 {'TEMPSENS_1': 1272}
38 {'TEMPSENS_1': 1271}
40 {'TEMPSENS_1': 1263}
42 {'TEMPSENS_1': 1280}
44 {'TEMPSENS_1': 1270}
46 {'TEMPSENS_1': 1272}
48 {'TEMPSENS_1': 1279}
50 {'TEMPSENS_1': 1275}
52 {'TEMPSENS_1': 1281}
54 {'TEMPSENS_1': 1272}
56 {'TEMPSENS_1': 1270}
58 {'TEMPSENS_1': 1274}
60 {'TEMPSENS_1': 1276}
62 {'TEMPSENS_1': 1272}
33 {'TEMPSENS_1': 1622}
35 {'TEMPSENS_1': 1615}
37 {'TEMPSENS_1': 1617}
39 {'TEMPSENS_1': 1621}
41 {'TEMPSENS_1': 1620}
43 {'TEMPSENS_1': 1617}
45 {'TEMPSENS_1': 1619}
47 {'TEMPSENS_1': 1615}
49 {'TEMPSENS_1': 1617}
51 {'TEMPSENS_1': 1614}
53 {'TEMPSENS_1': 1620}
55 {'TEMPSENS_1': 1615}
57 {'TEMPSENS_1': 1619}
59 {'TEMPSENS_1': 1619}
61 {'TEMPSENS_1': 1620}
63 {'TEMPSENS_1': 1618}
32 {'TEMPSENS_2': 1259}
34 {'TEMPSENS_2': 1271}
36 {'TEMPSENS_2': 1266}
38 {'TEMPSENS_2': 1272}
40 {'TEMPSENS_2': 1272}
42 {'TEMPSENS_2': 1263}
44 {'TEMPSENS_2': 1271}
46 {'TEMPSENS_2': 1262}
48 {'TEMPSENS_2': 1266}
50 {'TEMPSENS_2': 1271}
52 {'TEMPSENS_2': 1270}
54 {'TEMPSENS_2': 1263}
56 {'TEMPSENS_2': 1273}
58 {'TEMPSENS_2': 1276}
60 {'TEMPSENS_2': 1276}
62 {'TEMPSENS_2': 1276}
33 {'TEMPSENS_2': 1612}
35 {'TEMPSENS_2': 1613}
37 {'TEMPSENS_2': 1614}
39 {'TEMPSENS_2': 1612}
41 {'TEMPSENS_2': 1616}
43 {'TEMPSENS_2': 1613}
45 {'TEMPSENS_2': 1613}
47 {'TEMPSENS_2': 1615}
49 {'TEMPSENS_2': 1616}
51 {'TEMPSENS_2': 1612}
53 {'TEMPSENS_2': 1612}
55 {'TEMPSENS_2': 1616}
57 {'TEMPSENS_2': 1611}
59 {'TEMPSENS_2': 1611}
61 {'TEMPSENS_2': 1616}
63 {'TEMPSENS_2': 1617}
32 {'TEMPSENS_3': 1271}
34 {'TEMPSENS_3': 1271}
36 {'TEMPSENS_3': 1270}
38 {'TEMPSENS_3': 1262}
40 {'TEMPSENS_3': 1268}
42 {'TEMPSENS_3': 1272}
44 {'TEMPSENS_3': 1267}
46 {'TEMPSENS_3': 1265}
48 {'TEMPSENS_3': 1270}
50 {'TEMPSENS_3': 1264}
52 {'TEMPSENS_3': 1280}
54 {'TEMPSENS_3': 1272}
56 {'TEMPSENS_3': 1273}
58 {'TEMPSENS_3': 1278}
60 {'TEMPSENS_3': 1270}
62 {'TEMPSENS_3': 1270}
33 {'TEMPSENS_3': 1617}
35 {'TEMPSENS_3': 1613}
37 {'TEMPSENS_3': 1611}
39 {'TEMPSENS_3': 1615}
41 {'TEMPSENS_3': 1613}
43 {'TEMPSENS_3': 1616}
45 {'TEMPSENS_3': 1614}
47 {'TEMPSENS_3': 1614}
49 {'TEMPSENS_3': 1612}
51 {'TEMPSENS_3': 1614}
53 {'TEMPSENS_3': 1615}
55 {'TEMPSENS_3': 1616}
57 {'TEMPSENS_3': 1617}
59 {'TEMPSENS_3': 1614}
61 {'TEMPSENS_3': 1616}
63 {'TEMPSENS_3': 1612}
32 {'TEMPSENS_4': 1259}
34 {'TEMPSENS_4': 1257}
36 {'TEMPSENS_4': 1262}
38 {'TEMPSENS_4': 1264}
40 {'TEMPSENS_4': 1260}
42 {'TEMPSENS_4': 1255}
44 {'TEMPSENS_4': 1251}
46 {'TEMPSENS_4': 1251}
48 {'TEMPSENS_4': 1261}
50 {'TEMPSENS_4': 1262}
52 {'TEMPSENS_4': 1255}
54 {'TEMPSENS_4': 1264}
56 {'TEMPSENS_4': 1269}
58 {'TEMPSENS_4': 1256}
60 {'TEMPSENS_4': 1258}
62 {'TEMPSENS_4': 1265}
33 {'TEMPSENS_4': 1602}
35 {'TEMPSENS_4': 1598}
37 {'TEMPSENS_4': 1603}
39 {'TEMPSENS_4': 1604}
41 {'TEMPSENS_4': 1604}
43 {'TEMPSENS_4': 1604}
45 {'TEMPSENS_4': 1603}
47 {'TEMPSENS_4': 1603}
49 {'TEMPSENS_4': 1599}
51 {'TEMPSENS_4': 1608}
53 {'TEMPSENS_4': 1599}
55 {'TEMPSENS_4': 1603}
57 {'TEMPSENS_4': 1601}
59 {'TEMPSENS_4': 1603}
61 {'TEMPSENS_4': 1599}
63 {'TEMPSENS_4': 1608}
32 {'RADSENS_1': 3022}
34 {'RADSENS_1': 3036}
36 {'RADSENS_1': 3035}
38 {'RADSENS_1': 3039}
40 {'RADSENS_1': 3025}
42 {'RADSENS_1': 3028}
44 {'RADSENS_1': 3027}
46 {'RADSENS_1': 3041}
48 {'RADSENS_1': 3033}
50 {'RADSENS_1': 3024}
52 {'RADSENS_1': 3027}
54 {'RADSENS_1': 3032}
56 {'RADSENS_1': 3037}
58 {'RADSENS_1': 3034}
60 {'RADSENS_1': 3030}
62 {'RADSENS_1': 3027}
33 {'RADSENS_1': 3368}
35 {'RADSENS_1': 3359}
37 {'RADSENS_1': 3363}
39 {'RADSENS_1': 3363}
41 {'RADSENS_1': 3363}
43 {'RADSENS_1': 3361}
45 {'RADSENS_1': 3363}
47 {'RADSENS_1': 3358}
49 {'RADSENS_1': 3361}
51 {'RADSENS_1': 3359}
53 {'RADSENS_1': 3361}
55 {'RADSENS_1': 3360}
57 {'RADSENS_1': 3365}
59 {'RADSENS_1': 3359}
61 {'RADSENS_1': 3363}
63 {'RADSENS_1': 3364}
32 {'RADSENS_2': 2940}
34 {'RADSENS_2': 2947}
36 {'RADSENS_2': 2929}
38 {'RADSENS_2': 2944}
40 {'RADSENS_2': 2932}
42 {'RADSENS_2': 2939}
44 {'RADSENS_2': 2939}
46 {'RADSENS_2': 2937}
48 {'RADSENS_2': 2930}
50 {'RADSENS_2': 2927}
52 {'RADSENS_2': 2935}
54 {'RADSENS_2': 2923}
56 {'RADSENS_2': 2948}
58 {'RADSENS_2': 2935}
60 {'RADSENS_2': 2934}
62 {'RADSENS_2': 2941}
33 {'RADSENS_2': 3328}
35 {'RADSENS_2': 3326}
37 {'RADSENS_2': 3328}
39 {'RADSENS_2': 3328}
41 {'RADSENS_2': 3327}
43 {'RADSENS_2': 3321}
45 {'RADSENS_2': 3336}
47 {'RADSENS_2': 3336}
49 {'RADSENS_2': 3327}
51 {'RADSENS_2': 3332}
53 {'RADSENS_2': 3325}
55 {'RADSENS_2': 3330}
57 {'RADSENS_2': 3328}
59 {'RADSENS_2': 3319}
61 {'RADSENS_2': 3323}
63 {'RADSENS_2': 3328}
32 {'RADSENS_3': 2916}
34 {'RADSENS_3': 2904}
36 {'RADSENS_3': 2927}
38 {'RADSENS_3': 2917}
40 {'RADSENS_3': 2915}
42 {'RADSENS_3': 2919}
44 {'RADSENS_3': 2910}
46 {'RADSENS_3': 2915}
48 {'RADSENS_3': 2918}
50 {'RADSENS_3': 2915}
52 {'RADSENS_3': 2920}
54 {'RADSENS_3': 2914}
56 {'RADSENS_3': 2906}
58 {'RADSENS_3': 2912}
60 {'RADSENS_3': 2918}
62 {'RADSENS_3': 2910}
33 {'RADSENS_3': 3312}
35 {'RADSENS_3': 3311}
37 {'RADSENS_3': 3312}
39 {'RADSENS_3': 3311}
41 {'RADSENS_3': 3303}
43 {'RADSENS_3': 3308}
45 {'RADSENS_3': 3314}
47 {'RADSENS_3': 3311}
49 {'RADSENS_3': 3311}
51 {'RADSENS_3': 3301}
53 {'RADSENS_3': 3307}
55 {'RADSENS_3': 3310}
57 {'RADSENS_3': 3303}
59 {'RADSENS_3': 3309}
61 {'RADSENS_3': 3312}
63 {'RADSENS_3': 3314}
32 {'RADSENS_4': 2908}
34 {'RADSENS_4': 2913}
36 {'RADSENS_4': 2921}
38 {'RADSENS_4': 2914}
40 {'RADSENS_4': 2911}
42 {'RADSENS_4': 2911}
44 {'RADSENS_4': 2914}
46 {'RADSENS_4': 2926}
48 {'RADSENS_4': 2908}
50 {'RADSENS_4': 2891}
52 {'RADSENS_4': 2911}
54 {'RADSENS_4': 2914}
56 {'RADSENS_4': 2905}
58 {'RADSENS_4': 2913}
60 {'RADSENS_4': 2910}
62 {'RADSENS_4': 2905}
33 {'RADSENS_4': 3312}
35 {'RADSENS_4': 3310}
37 {'RADSENS_4': 3310}
39 {'RADSENS_4': 3309}
41 {'RADSENS_4': 3305}
43 {'RADSENS_4': 3311}
45 {'RADSENS_4': 3312}
47 {'RADSENS_4': 3303}
49 {'RADSENS_4': 3299}
51 {'RADSENS_4': 3303}
53 {'RADSENS_4': 3310}
55 {'RADSENS_4': 3308}
57 {'RADSENS_4': 3316}
59 {'RADSENS_4': 3312}
61 {'RADSENS_4': 3305}
63 {'RADSENS_4': 3307}
