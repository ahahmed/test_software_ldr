Timestamp GlobalTime(s) Dose(Mrad)
01_10_2018_10_57_43 1538146232.06 90
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2160}
34 {'ADCbandgap': 2153}
36 {'ADCbandgap': 2158}
38 {'ADCbandgap': 2153}
40 {'ADCbandgap': 2160}
42 {'ADCbandgap': 2160}
44 {'ADCbandgap': 2156}
46 {'ADCbandgap': 2160}
48 {'ADCbandgap': 2159}
50 {'ADCbandgap': 2160}
52 {'ADCbandgap': 2151}
54 {'ADCbandgap': 2160}
56 {'ADCbandgap': 2155}
58 {'ADCbandgap': 2156}
60 {'ADCbandgap': 2151}
62 {'ADCbandgap': 2156}
33 {'ADCbandgap': 2155}
35 {'ADCbandgap': 2160}
37 {'ADCbandgap': 2160}
39 {'ADCbandgap': 2155}
41 {'ADCbandgap': 2155}
43 {'ADCbandgap': 2155}
45 {'ADCbandgap': 2153}
47 {'ADCbandgap': 2154}
49 {'ADCbandgap': 2158}
51 {'ADCbandgap': 2151}
53 {'ADCbandgap': 2156}
55 {'ADCbandgap': 2158}
57 {'ADCbandgap': 2156}
59 {'ADCbandgap': 2155}
61 {'ADCbandgap': 2156}
63 {'ADCbandgap': 2155}
32 {'TEMPSENS_1': 1282}
34 {'TEMPSENS_1': 1282}
36 {'TEMPSENS_1': 1276}
38 {'TEMPSENS_1': 1279}
40 {'TEMPSENS_1': 1272}
42 {'TEMPSENS_1': 1278}
44 {'TEMPSENS_1': 1271}
46 {'TEMPSENS_1': 1271}
48 {'TEMPSENS_1': 1286}
50 {'TEMPSENS_1': 1281}
52 {'TEMPSENS_1': 1279}
54 {'TEMPSENS_1': 1274}
56 {'TEMPSENS_1': 1278}
58 {'TEMPSENS_1': 1280}
60 {'TEMPSENS_1': 1275}
62 {'TEMPSENS_1': 1276}
33 {'TEMPSENS_1': 1620}
35 {'TEMPSENS_1': 1623}
37 {'TEMPSENS_1': 1619}
39 {'TEMPSENS_1': 1625}
41 {'TEMPSENS_1': 1622}
43 {'TEMPSENS_1': 1621}
45 {'TEMPSENS_1': 1623}
47 {'TEMPSENS_1': 1620}
49 {'TEMPSENS_1': 1619}
51 {'TEMPSENS_1': 1618}
53 {'TEMPSENS_1': 1619}
55 {'TEMPSENS_1': 1620}
57 {'TEMPSENS_1': 1624}
59 {'TEMPSENS_1': 1621}
61 {'TEMPSENS_1': 1623}
63 {'TEMPSENS_1': 1623}
32 {'TEMPSENS_2': 1265}
34 {'TEMPSENS_2': 1276}
36 {'TEMPSENS_2': 1271}
38 {'TEMPSENS_2': 1272}
40 {'TEMPSENS_2': 1271}
42 {'TEMPSENS_2': 1267}
44 {'TEMPSENS_2': 1274}
46 {'TEMPSENS_2': 1261}
48 {'TEMPSENS_2': 1264}
50 {'TEMPSENS_2': 1273}
52 {'TEMPSENS_2': 1274}
54 {'TEMPSENS_2': 1260}
56 {'TEMPSENS_2': 1272}
58 {'TEMPSENS_2': 1276}
60 {'TEMPSENS_2': 1271}
62 {'TEMPSENS_2': 1280}
33 {'TEMPSENS_2': 1618}
35 {'TEMPSENS_2': 1617}
37 {'TEMPSENS_2': 1616}
39 {'TEMPSENS_2': 1616}
41 {'TEMPSENS_2': 1615}
43 {'TEMPSENS_2': 1613}
45 {'TEMPSENS_2': 1611}
47 {'TEMPSENS_2': 1617}
49 {'TEMPSENS_2': 1614}
51 {'TEMPSENS_2': 1609}
53 {'TEMPSENS_2': 1614}
55 {'TEMPSENS_2': 1618}
57 {'TEMPSENS_2': 1616}
59 {'TEMPSENS_2': 1617}
61 {'TEMPSENS_2': 1617}
63 {'TEMPSENS_2': 1616}
32 {'TEMPSENS_3': 1267}
34 {'TEMPSENS_3': 1280}
36 {'TEMPSENS_3': 1269}
38 {'TEMPSENS_3': 1263}
40 {'TEMPSENS_3': 1267}
42 {'TEMPSENS_3': 1270}
44 {'TEMPSENS_3': 1266}
46 {'TEMPSENS_3': 1264}
48 {'TEMPSENS_3': 1271}
50 {'TEMPSENS_3': 1263}
52 {'TEMPSENS_3': 1280}
54 {'TEMPSENS_3': 1272}
56 {'TEMPSENS_3': 1276}
58 {'TEMPSENS_3': 1272}
60 {'TEMPSENS_3': 1267}
62 {'TEMPSENS_3': 1267}
33 {'TEMPSENS_3': 1615}
35 {'TEMPSENS_3': 1619}
37 {'TEMPSENS_3': 1616}
39 {'TEMPSENS_3': 1613}
41 {'TEMPSENS_3': 1617}
43 {'TEMPSENS_3': 1612}
45 {'TEMPSENS_3': 1615}
47 {'TEMPSENS_3': 1617}
49 {'TEMPSENS_3': 1615}
51 {'TEMPSENS_3': 1616}
53 {'TEMPSENS_3': 1614}
55 {'TEMPSENS_3': 1616}
57 {'TEMPSENS_3': 1611}
59 {'TEMPSENS_3': 1616}
61 {'TEMPSENS_3': 1617}
63 {'TEMPSENS_3': 1615}
32 {'TEMPSENS_4': 1266}
34 {'TEMPSENS_4': 1267}
36 {'TEMPSENS_4': 1264}
38 {'TEMPSENS_4': 1262}
40 {'TEMPSENS_4': 1263}
42 {'TEMPSENS_4': 1248}
44 {'TEMPSENS_4': 1255}
46 {'TEMPSENS_4': 1265}
48 {'TEMPSENS_4': 1263}
50 {'TEMPSENS_4': 1263}
52 {'TEMPSENS_4': 1256}
54 {'TEMPSENS_4': 1264}
56 {'TEMPSENS_4': 1267}
58 {'TEMPSENS_4': 1256}
60 {'TEMPSENS_4': 1263}
62 {'TEMPSENS_4': 1266}
33 {'TEMPSENS_4': 1608}
35 {'TEMPSENS_4': 1603}
37 {'TEMPSENS_4': 1603}
39 {'TEMPSENS_4': 1605}
41 {'TEMPSENS_4': 1599}
43 {'TEMPSENS_4': 1608}
45 {'TEMPSENS_4': 1607}
47 {'TEMPSENS_4': 1603}
49 {'TEMPSENS_4': 1604}
51 {'TEMPSENS_4': 1603}
53 {'TEMPSENS_4': 1608}
55 {'TEMPSENS_4': 1606}
57 {'TEMPSENS_4': 1603}
59 {'TEMPSENS_4': 1608}
61 {'TEMPSENS_4': 1604}
63 {'TEMPSENS_4': 1606}
32 {'RADSENS_1': 3055}
34 {'RADSENS_1': 3053}
36 {'RADSENS_1': 3064}
38 {'RADSENS_1': 3059}
40 {'RADSENS_1': 3052}
42 {'RADSENS_1': 3055}
44 {'RADSENS_1': 3064}
46 {'RADSENS_1': 3077}
48 {'RADSENS_1': 3063}
50 {'RADSENS_1': 3058}
52 {'RADSENS_1': 3051}
54 {'RADSENS_1': 3054}
56 {'RADSENS_1': 3072}
58 {'RADSENS_1': 3055}
60 {'RADSENS_1': 3064}
62 {'RADSENS_1': 3055}
33 {'RADSENS_1': 3375}
35 {'RADSENS_1': 3376}
37 {'RADSENS_1': 3376}
39 {'RADSENS_1': 3375}
41 {'RADSENS_1': 3380}
43 {'RADSENS_1': 3375}
45 {'RADSENS_1': 3382}
47 {'RADSENS_1': 3384}
49 {'RADSENS_1': 3377}
51 {'RADSENS_1': 3376}
53 {'RADSENS_1': 3384}
55 {'RADSENS_1': 3375}
57 {'RADSENS_1': 3377}
59 {'RADSENS_1': 3375}
61 {'RADSENS_1': 3377}
63 {'RADSENS_1': 3370}
32 {'RADSENS_2': 2953}
34 {'RADSENS_2': 2963}
36 {'RADSENS_2': 2957}
38 {'RADSENS_2': 2969}
40 {'RADSENS_2': 2958}
42 {'RADSENS_2': 2956}
44 {'RADSENS_2': 2954}
46 {'RADSENS_2': 2950}
48 {'RADSENS_2': 2960}
50 {'RADSENS_2': 2960}
52 {'RADSENS_2': 2965}
54 {'RADSENS_2': 2945}
56 {'RADSENS_2': 2965}
58 {'RADSENS_2': 2960}
60 {'RADSENS_2': 2950}
62 {'RADSENS_2': 2963}
33 {'RADSENS_2': 3344}
35 {'RADSENS_2': 3337}
37 {'RADSENS_2': 3327}
39 {'RADSENS_2': 3327}
41 {'RADSENS_2': 3331}
43 {'RADSENS_2': 3341}
45 {'RADSENS_2': 3336}
47 {'RADSENS_2': 3335}
49 {'RADSENS_2': 3339}
51 {'RADSENS_2': 3344}
53 {'RADSENS_2': 3344}
55 {'RADSENS_2': 3343}
57 {'RADSENS_2': 3334}
59 {'RADSENS_2': 3338}
61 {'RADSENS_2': 3343}
63 {'RADSENS_2': 3335}
32 {'RADSENS_3': 2927}
34 {'RADSENS_3': 2924}
36 {'RADSENS_3': 2942}
38 {'RADSENS_3': 2937}
40 {'RADSENS_3': 2943}
42 {'RADSENS_3': 2940}
44 {'RADSENS_3': 2928}
46 {'RADSENS_3': 2927}
48 {'RADSENS_3': 2919}
50 {'RADSENS_3': 2927}
52 {'RADSENS_3': 2936}
54 {'RADSENS_3': 2940}
56 {'RADSENS_3': 2919}
58 {'RADSENS_3': 2930}
60 {'RADSENS_3': 2931}
62 {'RADSENS_3': 2923}
33 {'RADSENS_3': 3328}
35 {'RADSENS_3': 3318}
37 {'RADSENS_3': 3324}
39 {'RADSENS_3': 3319}
41 {'RADSENS_3': 3324}
43 {'RADSENS_3': 3319}
45 {'RADSENS_3': 3319}
47 {'RADSENS_3': 3328}
49 {'RADSENS_3': 3328}
51 {'RADSENS_3': 3319}
53 {'RADSENS_3': 3328}
55 {'RADSENS_3': 3323}
57 {'RADSENS_3': 3325}
59 {'RADSENS_3': 3318}
61 {'RADSENS_3': 3322}
63 {'RADSENS_3': 3328}
32 {'RADSENS_4': 2936}
34 {'RADSENS_4': 2933}
36 {'RADSENS_4': 2935}
38 {'RADSENS_4': 2933}
40 {'RADSENS_4': 2929}
42 {'RADSENS_4': 2936}
44 {'RADSENS_4': 2940}
46 {'RADSENS_4': 2948}
48 {'RADSENS_4': 2927}
50 {'RADSENS_4': 2920}
52 {'RADSENS_4': 2932}
54 {'RADSENS_4': 2934}
56 {'RADSENS_4': 2928}
58 {'RADSENS_4': 2929}
60 {'RADSENS_4': 2931}
62 {'RADSENS_4': 2917}
33 {'RADSENS_4': 3318}
35 {'RADSENS_4': 3325}
37 {'RADSENS_4': 3327}
39 {'RADSENS_4': 3323}
41 {'RADSENS_4': 3324}
43 {'RADSENS_4': 3325}
45 {'RADSENS_4': 3324}
47 {'RADSENS_4': 3319}
49 {'RADSENS_4': 3319}
51 {'RADSENS_4': 3321}
53 {'RADSENS_4': 3321}
55 {'RADSENS_4': 3315}
57 {'RADSENS_4': 3328}
59 {'RADSENS_4': 3320}
61 {'RADSENS_4': 3321}
63 {'RADSENS_4': 3318}
