Timestamp GlobalTime(s) Dose(Mrad)
30_09_2018_19_38_35 1538146232.06 60
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2156}
34 {'ADCbandgap': 2158}
36 {'ADCbandgap': 2156}
38 {'ADCbandgap': 2153}
40 {'ADCbandgap': 2156}
42 {'ADCbandgap': 2151}
44 {'ADCbandgap': 2147}
46 {'ADCbandgap': 2155}
48 {'ADCbandgap': 2153}
50 {'ADCbandgap': 2156}
52 {'ADCbandgap': 2159}
54 {'ADCbandgap': 2156}
56 {'ADCbandgap': 2156}
58 {'ADCbandgap': 2151}
60 {'ADCbandgap': 2155}
62 {'ADCbandgap': 2157}
33 {'ADCbandgap': 2155}
35 {'ADCbandgap': 2158}
37 {'ADCbandgap': 2151}
39 {'ADCbandgap': 2158}
41 {'ADCbandgap': 2158}
43 {'ADCbandgap': 2153}
45 {'ADCbandgap': 2160}
47 {'ADCbandgap': 2156}
49 {'ADCbandgap': 2156}
51 {'ADCbandgap': 2156}
53 {'ADCbandgap': 2154}
55 {'ADCbandgap': 2155}
57 {'ADCbandgap': 2156}
59 {'ADCbandgap': 2156}
61 {'ADCbandgap': 2151}
63 {'ADCbandgap': 2153}
32 {'TEMPSENS_1': 1285}
34 {'TEMPSENS_1': 1288}
36 {'TEMPSENS_1': 1278}
38 {'TEMPSENS_1': 1280}
40 {'TEMPSENS_1': 1276}
42 {'TEMPSENS_1': 1288}
44 {'TEMPSENS_1': 1278}
46 {'TEMPSENS_1': 1278}
48 {'TEMPSENS_1': 1282}
50 {'TEMPSENS_1': 1287}
52 {'TEMPSENS_1': 1285}
54 {'TEMPSENS_1': 1281}
56 {'TEMPSENS_1': 1281}
58 {'TEMPSENS_1': 1286}
60 {'TEMPSENS_1': 1284}
62 {'TEMPSENS_1': 1275}
33 {'TEMPSENS_1': 1627}
35 {'TEMPSENS_1': 1632}
37 {'TEMPSENS_1': 1630}
39 {'TEMPSENS_1': 1632}
41 {'TEMPSENS_1': 1629}
43 {'TEMPSENS_1': 1625}
45 {'TEMPSENS_1': 1625}
47 {'TEMPSENS_1': 1630}
49 {'TEMPSENS_1': 1627}
51 {'TEMPSENS_1': 1626}
53 {'TEMPSENS_1': 1630}
55 {'TEMPSENS_1': 1629}
57 {'TEMPSENS_1': 1625}
59 {'TEMPSENS_1': 1622}
61 {'TEMPSENS_1': 1625}
63 {'TEMPSENS_1': 1628}
32 {'TEMPSENS_2': 1263}
34 {'TEMPSENS_2': 1273}
36 {'TEMPSENS_2': 1271}
38 {'TEMPSENS_2': 1276}
40 {'TEMPSENS_2': 1270}
42 {'TEMPSENS_2': 1265}
44 {'TEMPSENS_2': 1271}
46 {'TEMPSENS_2': 1271}
48 {'TEMPSENS_2': 1267}
50 {'TEMPSENS_2': 1280}
52 {'TEMPSENS_2': 1279}
54 {'TEMPSENS_2': 1263}
56 {'TEMPSENS_2': 1272}
58 {'TEMPSENS_2': 1275}
60 {'TEMPSENS_2': 1279}
62 {'TEMPSENS_2': 1275}
33 {'TEMPSENS_2': 1619}
35 {'TEMPSENS_2': 1617}
37 {'TEMPSENS_2': 1617}
39 {'TEMPSENS_2': 1615}
41 {'TEMPSENS_2': 1615}
43 {'TEMPSENS_2': 1620}
45 {'TEMPSENS_2': 1620}
47 {'TEMPSENS_2': 1620}
49 {'TEMPSENS_2': 1616}
51 {'TEMPSENS_2': 1619}
53 {'TEMPSENS_2': 1616}
55 {'TEMPSENS_2': 1620}
57 {'TEMPSENS_2': 1615}
59 {'TEMPSENS_2': 1620}
61 {'TEMPSENS_2': 1617}
63 {'TEMPSENS_2': 1619}
32 {'TEMPSENS_3': 1275}
34 {'TEMPSENS_3': 1280}
36 {'TEMPSENS_3': 1263}
38 {'TEMPSENS_3': 1270}
40 {'TEMPSENS_3': 1271}
42 {'TEMPSENS_3': 1272}
44 {'TEMPSENS_3': 1267}
46 {'TEMPSENS_3': 1269}
48 {'TEMPSENS_3': 1274}
50 {'TEMPSENS_3': 1267}
52 {'TEMPSENS_3': 1281}
54 {'TEMPSENS_3': 1265}
56 {'TEMPSENS_3': 1281}
58 {'TEMPSENS_3': 1273}
60 {'TEMPSENS_3': 1274}
62 {'TEMPSENS_3': 1271}
33 {'TEMPSENS_3': 1622}
35 {'TEMPSENS_3': 1624}
37 {'TEMPSENS_3': 1615}
39 {'TEMPSENS_3': 1619}
41 {'TEMPSENS_3': 1624}
43 {'TEMPSENS_3': 1624}
45 {'TEMPSENS_3': 1619}
47 {'TEMPSENS_3': 1616}
49 {'TEMPSENS_3': 1617}
51 {'TEMPSENS_3': 1615}
53 {'TEMPSENS_3': 1618}
55 {'TEMPSENS_3': 1624}
57 {'TEMPSENS_3': 1620}
59 {'TEMPSENS_3': 1618}
61 {'TEMPSENS_3': 1625}
63 {'TEMPSENS_3': 1620}
32 {'TEMPSENS_4': 1262}
34 {'TEMPSENS_4': 1270}
36 {'TEMPSENS_4': 1272}
38 {'TEMPSENS_4': 1266}
40 {'TEMPSENS_4': 1263}
42 {'TEMPSENS_4': 1257}
44 {'TEMPSENS_4': 1264}
46 {'TEMPSENS_4': 1264}
48 {'TEMPSENS_4': 1264}
50 {'TEMPSENS_4': 1268}
52 {'TEMPSENS_4': 1263}
54 {'TEMPSENS_4': 1263}
56 {'TEMPSENS_4': 1267}
58 {'TEMPSENS_4': 1256}
60 {'TEMPSENS_4': 1267}
62 {'TEMPSENS_4': 1270}
33 {'TEMPSENS_4': 1613}
35 {'TEMPSENS_4': 1612}
37 {'TEMPSENS_4': 1611}
39 {'TEMPSENS_4': 1608}
41 {'TEMPSENS_4': 1607}
43 {'TEMPSENS_4': 1614}
45 {'TEMPSENS_4': 1612}
47 {'TEMPSENS_4': 1608}
49 {'TEMPSENS_4': 1611}
51 {'TEMPSENS_4': 1616}
53 {'TEMPSENS_4': 1611}
55 {'TEMPSENS_4': 1612}
57 {'TEMPSENS_4': 1608}
59 {'TEMPSENS_4': 1614}
61 {'TEMPSENS_4': 1611}
63 {'TEMPSENS_4': 1609}
32 {'RADSENS_1': 3075}
34 {'RADSENS_1': 3081}
36 {'RADSENS_1': 3087}
38 {'RADSENS_1': 3087}
40 {'RADSENS_1': 3071}
42 {'RADSENS_1': 3080}
44 {'RADSENS_1': 3079}
46 {'RADSENS_1': 3089}
48 {'RADSENS_1': 3091}
50 {'RADSENS_1': 3078}
52 {'RADSENS_1': 3078}
54 {'RADSENS_1': 3087}
56 {'RADSENS_1': 3092}
58 {'RADSENS_1': 3086}
60 {'RADSENS_1': 3084}
62 {'RADSENS_1': 3088}
33 {'RADSENS_1': 3391}
35 {'RADSENS_1': 3385}
37 {'RADSENS_1': 3391}
39 {'RADSENS_1': 3393}
41 {'RADSENS_1': 3396}
43 {'RADSENS_1': 3394}
45 {'RADSENS_1': 3391}
47 {'RADSENS_1': 3393}
49 {'RADSENS_1': 3391}
51 {'RADSENS_1': 3390}
53 {'RADSENS_1': 3394}
55 {'RADSENS_1': 3391}
57 {'RADSENS_1': 3392}
59 {'RADSENS_1': 3392}
61 {'RADSENS_1': 3391}
63 {'RADSENS_1': 3391}
32 {'RADSENS_2': 2974}
34 {'RADSENS_2': 2975}
36 {'RADSENS_2': 2976}
38 {'RADSENS_2': 2984}
40 {'RADSENS_2': 2973}
42 {'RADSENS_2': 2983}
44 {'RADSENS_2': 2974}
46 {'RADSENS_2': 2967}
48 {'RADSENS_2': 2965}
50 {'RADSENS_2': 2974}
52 {'RADSENS_2': 2974}
54 {'RADSENS_2': 2962}
56 {'RADSENS_2': 2984}
58 {'RADSENS_2': 2972}
60 {'RADSENS_2': 2974}
62 {'RADSENS_2': 2982}
33 {'RADSENS_2': 3351}
35 {'RADSENS_2': 3350}
37 {'RADSENS_2': 3355}
39 {'RADSENS_2': 3352}
41 {'RADSENS_2': 3355}
43 {'RADSENS_2': 3354}
45 {'RADSENS_2': 3343}
47 {'RADSENS_2': 3350}
49 {'RADSENS_2': 3353}
51 {'RADSENS_2': 3356}
53 {'RADSENS_2': 3352}
55 {'RADSENS_2': 3354}
57 {'RADSENS_2': 3356}
59 {'RADSENS_2': 3352}
61 {'RADSENS_2': 3353}
63 {'RADSENS_2': 3358}
32 {'RADSENS_3': 2947}
34 {'RADSENS_3': 2941}
36 {'RADSENS_3': 2943}
38 {'RADSENS_3': 2944}
40 {'RADSENS_3': 2951}
42 {'RADSENS_3': 2947}
44 {'RADSENS_3': 2943}
46 {'RADSENS_3': 2943}
48 {'RADSENS_3': 2943}
50 {'RADSENS_3': 2942}
52 {'RADSENS_3': 2948}
54 {'RADSENS_3': 2935}
56 {'RADSENS_3': 2937}
58 {'RADSENS_3': 2947}
60 {'RADSENS_3': 2960}
62 {'RADSENS_3': 2944}
33 {'RADSENS_3': 3331}
35 {'RADSENS_3': 3331}
37 {'RADSENS_3': 3337}
39 {'RADSENS_3': 3335}
41 {'RADSENS_3': 3335}
43 {'RADSENS_3': 3334}
45 {'RADSENS_3': 3338}
47 {'RADSENS_3': 3334}
49 {'RADSENS_3': 3327}
51 {'RADSENS_3': 3334}
53 {'RADSENS_3': 3341}
55 {'RADSENS_3': 3327}
57 {'RADSENS_3': 3331}
59 {'RADSENS_3': 3337}
61 {'RADSENS_3': 3335}
63 {'RADSENS_3': 3331}
32 {'RADSENS_4': 2948}
34 {'RADSENS_4': 2950}
36 {'RADSENS_4': 2948}
38 {'RADSENS_4': 2949}
40 {'RADSENS_4': 2944}
42 {'RADSENS_4': 2947}
44 {'RADSENS_4': 2953}
46 {'RADSENS_4': 2951}
48 {'RADSENS_4': 2936}
50 {'RADSENS_4': 2935}
52 {'RADSENS_4': 2953}
54 {'RADSENS_4': 2943}
56 {'RADSENS_4': 2939}
58 {'RADSENS_4': 2944}
60 {'RADSENS_4': 2949}
62 {'RADSENS_4': 2942}
33 {'RADSENS_4': 3327}
35 {'RADSENS_4': 3335}
37 {'RADSENS_4': 3332}
39 {'RADSENS_4': 3331}
41 {'RADSENS_4': 3331}
43 {'RADSENS_4': 3335}
45 {'RADSENS_4': 3344}
47 {'RADSENS_4': 3331}
49 {'RADSENS_4': 3334}
51 {'RADSENS_4': 3333}
53 {'RADSENS_4': 3335}
55 {'RADSENS_4': 3336}
57 {'RADSENS_4': 3338}
59 {'RADSENS_4': 3338}
61 {'RADSENS_4': 3340}
63 {'RADSENS_4': 3344}
