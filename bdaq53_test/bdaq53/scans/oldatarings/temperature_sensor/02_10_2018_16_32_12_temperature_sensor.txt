Timestamp GlobalTime(s) Dose(Mrad)
02_10_2018_16_32_12 1538146232.06 200
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2156}
34 {'ADCbandgap': 2157}
36 {'ADCbandgap': 2160}
38 {'ADCbandgap': 2159}
40 {'ADCbandgap': 2160}
42 {'ADCbandgap': 2158}
44 {'ADCbandgap': 2158}
46 {'ADCbandgap': 2158}
48 {'ADCbandgap': 2156}
50 {'ADCbandgap': 2158}
52 {'ADCbandgap': 2157}
54 {'ADCbandgap': 2158}
56 {'ADCbandgap': 2158}
58 {'ADCbandgap': 2153}
60 {'ADCbandgap': 2156}
62 {'ADCbandgap': 2156}
33 {'ADCbandgap': 2156}
35 {'ADCbandgap': 2153}
37 {'ADCbandgap': 2158}
39 {'ADCbandgap': 2158}
41 {'ADCbandgap': 2159}
43 {'ADCbandgap': 2157}
45 {'ADCbandgap': 2160}
47 {'ADCbandgap': 2159}
49 {'ADCbandgap': 2155}
51 {'ADCbandgap': 2157}
53 {'ADCbandgap': 2157}
55 {'ADCbandgap': 2156}
57 {'ADCbandgap': 2154}
59 {'ADCbandgap': 2157}
61 {'ADCbandgap': 2159}
63 {'ADCbandgap': 2159}
32 {'TEMPSENS_1': 1276}
34 {'TEMPSENS_1': 1270}
36 {'TEMPSENS_1': 1259}
38 {'TEMPSENS_1': 1267}
40 {'TEMPSENS_1': 1265}
42 {'TEMPSENS_1': 1273}
44 {'TEMPSENS_1': 1266}
46 {'TEMPSENS_1': 1263}
48 {'TEMPSENS_1': 1272}
50 {'TEMPSENS_1': 1271}
52 {'TEMPSENS_1': 1280}
54 {'TEMPSENS_1': 1269}
56 {'TEMPSENS_1': 1269}
58 {'TEMPSENS_1': 1269}
60 {'TEMPSENS_1': 1271}
62 {'TEMPSENS_1': 1265}
33 {'TEMPSENS_1': 1616}
35 {'TEMPSENS_1': 1619}
37 {'TEMPSENS_1': 1615}
39 {'TEMPSENS_1': 1614}
41 {'TEMPSENS_1': 1616}
43 {'TEMPSENS_1': 1616}
45 {'TEMPSENS_1': 1617}
47 {'TEMPSENS_1': 1615}
49 {'TEMPSENS_1': 1615}
51 {'TEMPSENS_1': 1616}
53 {'TEMPSENS_1': 1618}
55 {'TEMPSENS_1': 1614}
57 {'TEMPSENS_1': 1619}
59 {'TEMPSENS_1': 1615}
61 {'TEMPSENS_1': 1618}
63 {'TEMPSENS_1': 1619}
32 {'TEMPSENS_2': 1259}
34 {'TEMPSENS_2': 1263}
36 {'TEMPSENS_2': 1265}
38 {'TEMPSENS_2': 1266}
40 {'TEMPSENS_2': 1267}
42 {'TEMPSENS_2': 1264}
44 {'TEMPSENS_2': 1263}
46 {'TEMPSENS_2': 1260}
48 {'TEMPSENS_2': 1261}
50 {'TEMPSENS_2': 1262}
52 {'TEMPSENS_2': 1271}
54 {'TEMPSENS_2': 1260}
56 {'TEMPSENS_2': 1272}
58 {'TEMPSENS_2': 1272}
60 {'TEMPSENS_2': 1271}
62 {'TEMPSENS_2': 1271}
33 {'TEMPSENS_2': 1616}
35 {'TEMPSENS_2': 1613}
37 {'TEMPSENS_2': 1607}
39 {'TEMPSENS_2': 1614}
41 {'TEMPSENS_2': 1614}
43 {'TEMPSENS_2': 1614}
45 {'TEMPSENS_2': 1609}
47 {'TEMPSENS_2': 1611}
49 {'TEMPSENS_2': 1614}
51 {'TEMPSENS_2': 1611}
53 {'TEMPSENS_2': 1616}
55 {'TEMPSENS_2': 1613}
57 {'TEMPSENS_2': 1616}
59 {'TEMPSENS_2': 1614}
61 {'TEMPSENS_2': 1609}
63 {'TEMPSENS_2': 1612}
32 {'TEMPSENS_3': 1264}
34 {'TEMPSENS_3': 1267}
36 {'TEMPSENS_3': 1263}
38 {'TEMPSENS_3': 1268}
40 {'TEMPSENS_3': 1270}
42 {'TEMPSENS_3': 1267}
44 {'TEMPSENS_3': 1268}
46 {'TEMPSENS_3': 1262}
48 {'TEMPSENS_3': 1272}
50 {'TEMPSENS_3': 1263}
52 {'TEMPSENS_3': 1279}
54 {'TEMPSENS_3': 1273}
56 {'TEMPSENS_3': 1270}
58 {'TEMPSENS_3': 1272}
60 {'TEMPSENS_3': 1272}
62 {'TEMPSENS_3': 1267}
33 {'TEMPSENS_3': 1615}
35 {'TEMPSENS_3': 1616}
37 {'TEMPSENS_3': 1617}
39 {'TEMPSENS_3': 1616}
41 {'TEMPSENS_3': 1614}
43 {'TEMPSENS_3': 1615}
45 {'TEMPSENS_3': 1616}
47 {'TEMPSENS_3': 1619}
49 {'TEMPSENS_3': 1617}
51 {'TEMPSENS_3': 1616}
53 {'TEMPSENS_3': 1613}
55 {'TEMPSENS_3': 1618}
57 {'TEMPSENS_3': 1611}
59 {'TEMPSENS_3': 1616}
61 {'TEMPSENS_3': 1616}
63 {'TEMPSENS_3': 1615}
32 {'TEMPSENS_4': 1258}
34 {'TEMPSENS_4': 1262}
36 {'TEMPSENS_4': 1257}
38 {'TEMPSENS_4': 1256}
40 {'TEMPSENS_4': 1258}
42 {'TEMPSENS_4': 1250}
44 {'TEMPSENS_4': 1253}
46 {'TEMPSENS_4': 1255}
48 {'TEMPSENS_4': 1260}
50 {'TEMPSENS_4': 1257}
52 {'TEMPSENS_4': 1249}
54 {'TEMPSENS_4': 1255}
56 {'TEMPSENS_4': 1264}
58 {'TEMPSENS_4': 1255}
60 {'TEMPSENS_4': 1259}
62 {'TEMPSENS_4': 1264}
33 {'TEMPSENS_4': 1600}
35 {'TEMPSENS_4': 1600}
37 {'TEMPSENS_4': 1601}
39 {'TEMPSENS_4': 1600}
41 {'TEMPSENS_4': 1599}
43 {'TEMPSENS_4': 1606}
45 {'TEMPSENS_4': 1603}
47 {'TEMPSENS_4': 1602}
49 {'TEMPSENS_4': 1604}
51 {'TEMPSENS_4': 1600}
53 {'TEMPSENS_4': 1599}
55 {'TEMPSENS_4': 1601}
57 {'TEMPSENS_4': 1604}
59 {'TEMPSENS_4': 1601}
61 {'TEMPSENS_4': 1601}
63 {'TEMPSENS_4': 1602}
32 {'RADSENS_1': 3009}
34 {'RADSENS_1': 3017}
36 {'RADSENS_1': 3022}
38 {'RADSENS_1': 3022}
40 {'RADSENS_1': 3007}
42 {'RADSENS_1': 3013}
44 {'RADSENS_1': 3010}
46 {'RADSENS_1': 3024}
48 {'RADSENS_1': 3024}
50 {'RADSENS_1': 3007}
52 {'RADSENS_1': 3009}
54 {'RADSENS_1': 3007}
56 {'RADSENS_1': 3017}
58 {'RADSENS_1': 3010}
60 {'RADSENS_1': 3009}
62 {'RADSENS_1': 3009}
33 {'RADSENS_1': 3355}
35 {'RADSENS_1': 3356}
37 {'RADSENS_1': 3353}
39 {'RADSENS_1': 3360}
41 {'RADSENS_1': 3356}
43 {'RADSENS_1': 3354}
45 {'RADSENS_1': 3349}
47 {'RADSENS_1': 3347}
49 {'RADSENS_1': 3355}
51 {'RADSENS_1': 3343}
53 {'RADSENS_1': 3352}
55 {'RADSENS_1': 3352}
57 {'RADSENS_1': 3353}
59 {'RADSENS_1': 3352}
61 {'RADSENS_1': 3360}
63 {'RADSENS_1': 3351}
32 {'RADSENS_2': 2931}
34 {'RADSENS_2': 2940}
36 {'RADSENS_2': 2932}
38 {'RADSENS_2': 2942}
40 {'RADSENS_2': 2927}
42 {'RADSENS_2': 2933}
44 {'RADSENS_2': 2919}
46 {'RADSENS_2': 2919}
48 {'RADSENS_2': 2923}
50 {'RADSENS_2': 2934}
52 {'RADSENS_2': 2940}
54 {'RADSENS_2': 2927}
56 {'RADSENS_2': 2948}
58 {'RADSENS_2': 2927}
60 {'RADSENS_2': 2926}
62 {'RADSENS_2': 2940}
33 {'RADSENS_2': 3324}
35 {'RADSENS_2': 3317}
37 {'RADSENS_2': 3321}
39 {'RADSENS_2': 3320}
41 {'RADSENS_2': 3311}
43 {'RADSENS_2': 3320}
45 {'RADSENS_2': 3311}
47 {'RADSENS_2': 3324}
49 {'RADSENS_2': 3319}
51 {'RADSENS_2': 3328}
53 {'RADSENS_2': 3321}
55 {'RADSENS_2': 3319}
57 {'RADSENS_2': 3315}
59 {'RADSENS_2': 3315}
61 {'RADSENS_2': 3319}
63 {'RADSENS_2': 3316}
32 {'RADSENS_3': 2896}
34 {'RADSENS_3': 2906}
36 {'RADSENS_3': 2917}
38 {'RADSENS_3': 2916}
40 {'RADSENS_3': 2914}
42 {'RADSENS_3': 2915}
44 {'RADSENS_3': 2911}
46 {'RADSENS_3': 2915}
48 {'RADSENS_3': 2895}
50 {'RADSENS_3': 2903}
52 {'RADSENS_3': 2912}
54 {'RADSENS_3': 2912}
56 {'RADSENS_3': 2910}
58 {'RADSENS_3': 2907}
60 {'RADSENS_3': 2919}
62 {'RADSENS_3': 2894}
33 {'RADSENS_3': 3312}
35 {'RADSENS_3': 3313}
37 {'RADSENS_3': 3307}
39 {'RADSENS_3': 3313}
41 {'RADSENS_3': 3312}
43 {'RADSENS_3': 3308}
45 {'RADSENS_3': 3308}
47 {'RADSENS_3': 3316}
49 {'RADSENS_3': 3303}
51 {'RADSENS_3': 3303}
53 {'RADSENS_3': 3307}
55 {'RADSENS_3': 3306}
57 {'RADSENS_3': 3307}
59 {'RADSENS_3': 3303}
61 {'RADSENS_3': 3312}
63 {'RADSENS_3': 3309}
32 {'RADSENS_4': 2910}
34 {'RADSENS_4': 2920}
36 {'RADSENS_4': 2918}
38 {'RADSENS_4': 2910}
40 {'RADSENS_4': 2909}
42 {'RADSENS_4': 2910}
44 {'RADSENS_4': 2910}
46 {'RADSENS_4': 2917}
48 {'RADSENS_4': 2902}
50 {'RADSENS_4': 2899}
52 {'RADSENS_4': 2906}
54 {'RADSENS_4': 2899}
56 {'RADSENS_4': 2898}
58 {'RADSENS_4': 2906}
60 {'RADSENS_4': 2920}
62 {'RADSENS_4': 2911}
33 {'RADSENS_4': 3312}
35 {'RADSENS_4': 3307}
37 {'RADSENS_4': 3297}
39 {'RADSENS_4': 3303}
41 {'RADSENS_4': 3295}
43 {'RADSENS_4': 3314}
45 {'RADSENS_4': 3295}
47 {'RADSENS_4': 3300}
49 {'RADSENS_4': 3320}
51 {'RADSENS_4': 3308}
53 {'RADSENS_4': 3312}
55 {'RADSENS_4': 3304}
57 {'RADSENS_4': 3310}
59 {'RADSENS_4': 3299}
61 {'RADSENS_4': 3304}
63 {'RADSENS_4': 3313}
