Timestamp GlobalTime(s) Dose(Mrad)
02_10_2018_12_42_40 1538146232.06 200
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2160}
34 {'ADCbandgap': 2157}
36 {'ADCbandgap': 2158}
38 {'ADCbandgap': 2156}
40 {'ADCbandgap': 2156}
42 {'ADCbandgap': 2158}
44 {'ADCbandgap': 2156}
46 {'ADCbandgap': 2155}
48 {'ADCbandgap': 2159}
50 {'ADCbandgap': 2158}
52 {'ADCbandgap': 2158}
54 {'ADCbandgap': 2158}
56 {'ADCbandgap': 2158}
58 {'ADCbandgap': 2155}
60 {'ADCbandgap': 2158}
62 {'ADCbandgap': 2155}
33 {'ADCbandgap': 2158}
35 {'ADCbandgap': 2161}
37 {'ADCbandgap': 2160}
39 {'ADCbandgap': 2158}
41 {'ADCbandgap': 2156}
43 {'ADCbandgap': 2158}
45 {'ADCbandgap': 2158}
47 {'ADCbandgap': 2158}
49 {'ADCbandgap': 2156}
51 {'ADCbandgap': 2160}
53 {'ADCbandgap': 2157}
55 {'ADCbandgap': 2158}
57 {'ADCbandgap': 2155}
59 {'ADCbandgap': 2155}
61 {'ADCbandgap': 2155}
63 {'ADCbandgap': 2156}
32 {'TEMPSENS_1': 1273}
34 {'TEMPSENS_1': 1267}
36 {'TEMPSENS_1': 1262}
38 {'TEMPSENS_1': 1271}
40 {'TEMPSENS_1': 1265}
42 {'TEMPSENS_1': 1276}
44 {'TEMPSENS_1': 1265}
46 {'TEMPSENS_1': 1264}
48 {'TEMPSENS_1': 1274}
50 {'TEMPSENS_1': 1276}
52 {'TEMPSENS_1': 1277}
54 {'TEMPSENS_1': 1271}
56 {'TEMPSENS_1': 1274}
58 {'TEMPSENS_1': 1268}
60 {'TEMPSENS_1': 1268}
62 {'TEMPSENS_1': 1262}
33 {'TEMPSENS_1': 1612}
35 {'TEMPSENS_1': 1614}
37 {'TEMPSENS_1': 1614}
39 {'TEMPSENS_1': 1617}
41 {'TEMPSENS_1': 1616}
43 {'TEMPSENS_1': 1616}
45 {'TEMPSENS_1': 1615}
47 {'TEMPSENS_1': 1617}
49 {'TEMPSENS_1': 1607}
51 {'TEMPSENS_1': 1610}
53 {'TEMPSENS_1': 1614}
55 {'TEMPSENS_1': 1607}
57 {'TEMPSENS_1': 1614}
59 {'TEMPSENS_1': 1615}
61 {'TEMPSENS_1': 1612}
63 {'TEMPSENS_1': 1616}
32 {'TEMPSENS_2': 1259}
34 {'TEMPSENS_2': 1268}
36 {'TEMPSENS_2': 1262}
38 {'TEMPSENS_2': 1269}
40 {'TEMPSENS_2': 1271}
42 {'TEMPSENS_2': 1265}
44 {'TEMPSENS_2': 1267}
46 {'TEMPSENS_2': 1259}
48 {'TEMPSENS_2': 1259}
50 {'TEMPSENS_2': 1268}
52 {'TEMPSENS_2': 1268}
54 {'TEMPSENS_2': 1254}
56 {'TEMPSENS_2': 1267}
58 {'TEMPSENS_2': 1275}
60 {'TEMPSENS_2': 1274}
62 {'TEMPSENS_2': 1276}
33 {'TEMPSENS_2': 1612}
35 {'TEMPSENS_2': 1612}
37 {'TEMPSENS_2': 1612}
39 {'TEMPSENS_2': 1612}
41 {'TEMPSENS_2': 1612}
43 {'TEMPSENS_2': 1612}
45 {'TEMPSENS_2': 1614}
47 {'TEMPSENS_2': 1616}
49 {'TEMPSENS_2': 1611}
51 {'TEMPSENS_2': 1610}
53 {'TEMPSENS_2': 1615}
55 {'TEMPSENS_2': 1612}
57 {'TEMPSENS_2': 1616}
59 {'TEMPSENS_2': 1616}
61 {'TEMPSENS_2': 1612}
63 {'TEMPSENS_2': 1609}
32 {'TEMPSENS_3': 1262}
34 {'TEMPSENS_3': 1267}
36 {'TEMPSENS_3': 1262}
38 {'TEMPSENS_3': 1255}
40 {'TEMPSENS_3': 1263}
42 {'TEMPSENS_3': 1268}
44 {'TEMPSENS_3': 1270}
46 {'TEMPSENS_3': 1265}
48 {'TEMPSENS_3': 1271}
50 {'TEMPSENS_3': 1262}
52 {'TEMPSENS_3': 1271}
54 {'TEMPSENS_3': 1266}
56 {'TEMPSENS_3': 1272}
58 {'TEMPSENS_3': 1271}
60 {'TEMPSENS_3': 1272}
62 {'TEMPSENS_3': 1263}
33 {'TEMPSENS_3': 1614}
35 {'TEMPSENS_3': 1609}
37 {'TEMPSENS_3': 1616}
39 {'TEMPSENS_3': 1611}
41 {'TEMPSENS_3': 1615}
43 {'TEMPSENS_3': 1614}
45 {'TEMPSENS_3': 1612}
47 {'TEMPSENS_3': 1616}
49 {'TEMPSENS_3': 1611}
51 {'TEMPSENS_3': 1614}
53 {'TEMPSENS_3': 1614}
55 {'TEMPSENS_3': 1616}
57 {'TEMPSENS_3': 1615}
59 {'TEMPSENS_3': 1611}
61 {'TEMPSENS_3': 1616}
63 {'TEMPSENS_3': 1614}
32 {'TEMPSENS_4': 1256}
34 {'TEMPSENS_4': 1254}
36 {'TEMPSENS_4': 1255}
38 {'TEMPSENS_4': 1256}
40 {'TEMPSENS_4': 1256}
42 {'TEMPSENS_4': 1246}
44 {'TEMPSENS_4': 1252}
46 {'TEMPSENS_4': 1256}
48 {'TEMPSENS_4': 1258}
50 {'TEMPSENS_4': 1259}
52 {'TEMPSENS_4': 1247}
54 {'TEMPSENS_4': 1255}
56 {'TEMPSENS_4': 1262}
58 {'TEMPSENS_4': 1246}
60 {'TEMPSENS_4': 1257}
62 {'TEMPSENS_4': 1260}
33 {'TEMPSENS_4': 1597}
35 {'TEMPSENS_4': 1600}
37 {'TEMPSENS_4': 1598}
39 {'TEMPSENS_4': 1598}
41 {'TEMPSENS_4': 1600}
43 {'TEMPSENS_4': 1604}
45 {'TEMPSENS_4': 1600}
47 {'TEMPSENS_4': 1603}
49 {'TEMPSENS_4': 1600}
51 {'TEMPSENS_4': 1595}
53 {'TEMPSENS_4': 1597}
55 {'TEMPSENS_4': 1604}
57 {'TEMPSENS_4': 1597}
59 {'TEMPSENS_4': 1599}
61 {'TEMPSENS_4': 1598}
63 {'TEMPSENS_4': 1600}
32 {'RADSENS_1': 3007}
34 {'RADSENS_1': 3009}
36 {'RADSENS_1': 3014}
38 {'RADSENS_1': 3022}
40 {'RADSENS_1': 3005}
42 {'RADSENS_1': 3024}
44 {'RADSENS_1': 3020}
46 {'RADSENS_1': 3028}
48 {'RADSENS_1': 3020}
50 {'RADSENS_1': 3008}
52 {'RADSENS_1': 3006}
54 {'RADSENS_1': 3014}
56 {'RADSENS_1': 3015}
58 {'RADSENS_1': 3024}
60 {'RADSENS_1': 3021}
62 {'RADSENS_1': 3016}
33 {'RADSENS_1': 3356}
35 {'RADSENS_1': 3350}
37 {'RADSENS_1': 3360}
39 {'RADSENS_1': 3348}
41 {'RADSENS_1': 3351}
43 {'RADSENS_1': 3358}
45 {'RADSENS_1': 3353}
47 {'RADSENS_1': 3343}
49 {'RADSENS_1': 3356}
51 {'RADSENS_1': 3356}
53 {'RADSENS_1': 3355}
55 {'RADSENS_1': 3360}
57 {'RADSENS_1': 3351}
59 {'RADSENS_1': 3347}
61 {'RADSENS_1': 3350}
63 {'RADSENS_1': 3351}
32 {'RADSENS_2': 2934}
34 {'RADSENS_2': 2942}
36 {'RADSENS_2': 2944}
38 {'RADSENS_2': 2939}
40 {'RADSENS_2': 2928}
42 {'RADSENS_2': 2936}
44 {'RADSENS_2': 2923}
46 {'RADSENS_2': 2930}
48 {'RADSENS_2': 2923}
50 {'RADSENS_2': 2931}
52 {'RADSENS_2': 2939}
54 {'RADSENS_2': 2928}
56 {'RADSENS_2': 2946}
58 {'RADSENS_2': 2942}
60 {'RADSENS_2': 2928}
62 {'RADSENS_2': 2927}
33 {'RADSENS_2': 3325}
35 {'RADSENS_2': 3328}
37 {'RADSENS_2': 3323}
39 {'RADSENS_2': 3324}
41 {'RADSENS_2': 3324}
43 {'RADSENS_2': 3320}
45 {'RADSENS_2': 3320}
47 {'RADSENS_2': 3315}
49 {'RADSENS_2': 3322}
51 {'RADSENS_2': 3328}
53 {'RADSENS_2': 3320}
55 {'RADSENS_2': 3328}
57 {'RADSENS_2': 3316}
59 {'RADSENS_2': 3319}
61 {'RADSENS_2': 3320}
63 {'RADSENS_2': 3319}
32 {'RADSENS_3': 2912}
34 {'RADSENS_3': 2902}
36 {'RADSENS_3': 2928}
38 {'RADSENS_3': 2926}
40 {'RADSENS_3': 2920}
42 {'RADSENS_3': 2911}
44 {'RADSENS_3': 2909}
46 {'RADSENS_3': 2915}
48 {'RADSENS_3': 2908}
50 {'RADSENS_3': 2900}
52 {'RADSENS_3': 2915}
54 {'RADSENS_3': 2910}
56 {'RADSENS_3': 2908}
58 {'RADSENS_3': 2910}
60 {'RADSENS_3': 2913}
62 {'RADSENS_3': 2903}
33 {'RADSENS_3': 3299}
35 {'RADSENS_3': 3308}
37 {'RADSENS_3': 3305}
39 {'RADSENS_3': 3303}
41 {'RADSENS_3': 3312}
43 {'RADSENS_3': 3295}
45 {'RADSENS_3': 3303}
47 {'RADSENS_3': 3299}
49 {'RADSENS_3': 3308}
51 {'RADSENS_3': 3304}
53 {'RADSENS_3': 3301}
55 {'RADSENS_3': 3302}
57 {'RADSENS_3': 3303}
59 {'RADSENS_3': 3301}
61 {'RADSENS_3': 3303}
63 {'RADSENS_3': 3309}
32 {'RADSENS_4': 2911}
34 {'RADSENS_4': 2918}
36 {'RADSENS_4': 2912}
38 {'RADSENS_4': 2903}
40 {'RADSENS_4': 2914}
42 {'RADSENS_4': 2912}
44 {'RADSENS_4': 2918}
46 {'RADSENS_4': 2920}
48 {'RADSENS_4': 2908}
50 {'RADSENS_4': 2902}
52 {'RADSENS_4': 2913}
54 {'RADSENS_4': 2902}
56 {'RADSENS_4': 2899}
58 {'RADSENS_4': 2894}
60 {'RADSENS_4': 2912}
62 {'RADSENS_4': 2903}
33 {'RADSENS_4': 3303}
35 {'RADSENS_4': 3303}
37 {'RADSENS_4': 3302}
39 {'RADSENS_4': 3305}
41 {'RADSENS_4': 3304}
43 {'RADSENS_4': 3299}
45 {'RADSENS_4': 3303}
47 {'RADSENS_4': 3304}
49 {'RADSENS_4': 3304}
51 {'RADSENS_4': 3307}
53 {'RADSENS_4': 3299}
55 {'RADSENS_4': 3299}
57 {'RADSENS_4': 3303}
59 {'RADSENS_4': 3302}
61 {'RADSENS_4': 3299}
63 {'RADSENS_4': 3308}
