Timestamp GlobalTime(s) Dose(Mrad)
02_10_2018_00_54_36 1538146232.06 150
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2155}
34 {'ADCbandgap': 2158}
36 {'ADCbandgap': 2156}
38 {'ADCbandgap': 2155}
40 {'ADCbandgap': 2155}
42 {'ADCbandgap': 2157}
44 {'ADCbandgap': 2156}
46 {'ADCbandgap': 2151}
48 {'ADCbandgap': 2156}
50 {'ADCbandgap': 2151}
52 {'ADCbandgap': 2156}
54 {'ADCbandgap': 2155}
56 {'ADCbandgap': 2159}
58 {'ADCbandgap': 2158}
60 {'ADCbandgap': 2157}
62 {'ADCbandgap': 2160}
33 {'ADCbandgap': 2154}
35 {'ADCbandgap': 2158}
37 {'ADCbandgap': 2157}
39 {'ADCbandgap': 2153}
41 {'ADCbandgap': 2155}
43 {'ADCbandgap': 2155}
45 {'ADCbandgap': 2154}
47 {'ADCbandgap': 2156}
49 {'ADCbandgap': 2155}
51 {'ADCbandgap': 2155}
53 {'ADCbandgap': 2158}
55 {'ADCbandgap': 2158}
57 {'ADCbandgap': 2155}
59 {'ADCbandgap': 2154}
61 {'ADCbandgap': 2151}
63 {'ADCbandgap': 2156}
32 {'TEMPSENS_1': 1279}
34 {'TEMPSENS_1': 1280}
36 {'TEMPSENS_1': 1271}
38 {'TEMPSENS_1': 1272}
40 {'TEMPSENS_1': 1270}
42 {'TEMPSENS_1': 1275}
44 {'TEMPSENS_1': 1271}
46 {'TEMPSENS_1': 1275}
48 {'TEMPSENS_1': 1275}
50 {'TEMPSENS_1': 1276}
52 {'TEMPSENS_1': 1282}
54 {'TEMPSENS_1': 1273}
56 {'TEMPSENS_1': 1275}
58 {'TEMPSENS_1': 1276}
60 {'TEMPSENS_1': 1278}
62 {'TEMPSENS_1': 1272}
33 {'TEMPSENS_1': 1620}
35 {'TEMPSENS_1': 1619}
37 {'TEMPSENS_1': 1621}
39 {'TEMPSENS_1': 1619}
41 {'TEMPSENS_1': 1615}
43 {'TEMPSENS_1': 1623}
45 {'TEMPSENS_1': 1620}
47 {'TEMPSENS_1': 1617}
49 {'TEMPSENS_1': 1619}
51 {'TEMPSENS_1': 1619}
53 {'TEMPSENS_1': 1620}
55 {'TEMPSENS_1': 1624}
57 {'TEMPSENS_1': 1617}
59 {'TEMPSENS_1': 1619}
61 {'TEMPSENS_1': 1624}
63 {'TEMPSENS_1': 1620}
32 {'TEMPSENS_2': 1265}
34 {'TEMPSENS_2': 1271}
36 {'TEMPSENS_2': 1268}
38 {'TEMPSENS_2': 1269}
40 {'TEMPSENS_2': 1272}
42 {'TEMPSENS_2': 1260}
44 {'TEMPSENS_2': 1272}
46 {'TEMPSENS_2': 1268}
48 {'TEMPSENS_2': 1264}
50 {'TEMPSENS_2': 1273}
52 {'TEMPSENS_2': 1271}
54 {'TEMPSENS_2': 1257}
56 {'TEMPSENS_2': 1275}
58 {'TEMPSENS_2': 1280}
60 {'TEMPSENS_2': 1270}
62 {'TEMPSENS_2': 1275}
33 {'TEMPSENS_2': 1617}
35 {'TEMPSENS_2': 1611}
37 {'TEMPSENS_2': 1612}
39 {'TEMPSENS_2': 1616}
41 {'TEMPSENS_2': 1616}
43 {'TEMPSENS_2': 1613}
45 {'TEMPSENS_2': 1614}
47 {'TEMPSENS_2': 1611}
49 {'TEMPSENS_2': 1614}
51 {'TEMPSENS_2': 1615}
53 {'TEMPSENS_2': 1614}
55 {'TEMPSENS_2': 1615}
57 {'TEMPSENS_2': 1613}
59 {'TEMPSENS_2': 1611}
61 {'TEMPSENS_2': 1614}
63 {'TEMPSENS_2': 1611}
32 {'TEMPSENS_3': 1273}
34 {'TEMPSENS_3': 1278}
36 {'TEMPSENS_3': 1264}
38 {'TEMPSENS_3': 1267}
40 {'TEMPSENS_3': 1269}
42 {'TEMPSENS_3': 1269}
44 {'TEMPSENS_3': 1273}
46 {'TEMPSENS_3': 1264}
48 {'TEMPSENS_3': 1273}
50 {'TEMPSENS_3': 1266}
52 {'TEMPSENS_3': 1271}
54 {'TEMPSENS_3': 1271}
56 {'TEMPSENS_3': 1273}
58 {'TEMPSENS_3': 1280}
60 {'TEMPSENS_3': 1274}
62 {'TEMPSENS_3': 1267}
33 {'TEMPSENS_3': 1616}
35 {'TEMPSENS_3': 1618}
37 {'TEMPSENS_3': 1615}
39 {'TEMPSENS_3': 1620}
41 {'TEMPSENS_3': 1615}
43 {'TEMPSENS_3': 1615}
45 {'TEMPSENS_3': 1622}
47 {'TEMPSENS_3': 1614}
49 {'TEMPSENS_3': 1618}
51 {'TEMPSENS_3': 1618}
53 {'TEMPSENS_3': 1615}
55 {'TEMPSENS_3': 1615}
57 {'TEMPSENS_3': 1614}
59 {'TEMPSENS_3': 1616}
61 {'TEMPSENS_3': 1616}
63 {'TEMPSENS_3': 1616}
32 {'TEMPSENS_4': 1262}
34 {'TEMPSENS_4': 1263}
36 {'TEMPSENS_4': 1256}
38 {'TEMPSENS_4': 1261}
40 {'TEMPSENS_4': 1259}
42 {'TEMPSENS_4': 1254}
44 {'TEMPSENS_4': 1258}
46 {'TEMPSENS_4': 1256}
48 {'TEMPSENS_4': 1261}
50 {'TEMPSENS_4': 1263}
52 {'TEMPSENS_4': 1254}
54 {'TEMPSENS_4': 1267}
56 {'TEMPSENS_4': 1266}
58 {'TEMPSENS_4': 1255}
60 {'TEMPSENS_4': 1263}
62 {'TEMPSENS_4': 1261}
33 {'TEMPSENS_4': 1608}
35 {'TEMPSENS_4': 1609}
37 {'TEMPSENS_4': 1603}
39 {'TEMPSENS_4': 1603}
41 {'TEMPSENS_4': 1604}
43 {'TEMPSENS_4': 1604}
45 {'TEMPSENS_4': 1610}
47 {'TEMPSENS_4': 1606}
49 {'TEMPSENS_4': 1604}
51 {'TEMPSENS_4': 1607}
53 {'TEMPSENS_4': 1608}
55 {'TEMPSENS_4': 1601}
57 {'TEMPSENS_4': 1608}
59 {'TEMPSENS_4': 1612}
61 {'TEMPSENS_4': 1608}
63 {'TEMPSENS_4': 1607}
32 {'RADSENS_1': 3036}
34 {'RADSENS_1': 3038}
36 {'RADSENS_1': 3047}
38 {'RADSENS_1': 3048}
40 {'RADSENS_1': 3036}
42 {'RADSENS_1': 3039}
44 {'RADSENS_1': 3042}
46 {'RADSENS_1': 3047}
48 {'RADSENS_1': 3045}
50 {'RADSENS_1': 3032}
52 {'RADSENS_1': 3041}
54 {'RADSENS_1': 3039}
56 {'RADSENS_1': 3039}
58 {'RADSENS_1': 3038}
60 {'RADSENS_1': 3048}
62 {'RADSENS_1': 3040}
33 {'RADSENS_1': 3366}
35 {'RADSENS_1': 3376}
37 {'RADSENS_1': 3368}
39 {'RADSENS_1': 3370}
41 {'RADSENS_1': 3373}
43 {'RADSENS_1': 3369}
45 {'RADSENS_1': 3359}
47 {'RADSENS_1': 3363}
49 {'RADSENS_1': 3365}
51 {'RADSENS_1': 3363}
53 {'RADSENS_1': 3367}
55 {'RADSENS_1': 3371}
57 {'RADSENS_1': 3368}
59 {'RADSENS_1': 3369}
61 {'RADSENS_1': 3376}
63 {'RADSENS_1': 3363}
32 {'RADSENS_2': 2942}
34 {'RADSENS_2': 2948}
36 {'RADSENS_2': 2937}
38 {'RADSENS_2': 2947}
40 {'RADSENS_2': 2930}
42 {'RADSENS_2': 2951}
44 {'RADSENS_2': 2943}
46 {'RADSENS_2': 2927}
48 {'RADSENS_2': 2940}
50 {'RADSENS_2': 2934}
52 {'RADSENS_2': 2941}
54 {'RADSENS_2': 2934}
56 {'RADSENS_2': 2949}
58 {'RADSENS_2': 2943}
60 {'RADSENS_2': 2939}
62 {'RADSENS_2': 2944}
33 {'RADSENS_2': 3332}
35 {'RADSENS_2': 3332}
37 {'RADSENS_2': 3328}
39 {'RADSENS_2': 3332}
41 {'RADSENS_2': 3326}
43 {'RADSENS_2': 3331}
45 {'RADSENS_2': 3327}
47 {'RADSENS_2': 3323}
49 {'RADSENS_2': 3326}
51 {'RADSENS_2': 3327}
53 {'RADSENS_2': 3327}
55 {'RADSENS_2': 3319}
57 {'RADSENS_2': 3336}
59 {'RADSENS_2': 3327}
61 {'RADSENS_2': 3325}
63 {'RADSENS_2': 3336}
32 {'RADSENS_3': 2918}
34 {'RADSENS_3': 2915}
36 {'RADSENS_3': 2937}
38 {'RADSENS_3': 2921}
40 {'RADSENS_3': 2924}
42 {'RADSENS_3': 2929}
44 {'RADSENS_3': 2919}
46 {'RADSENS_3': 2924}
48 {'RADSENS_3': 2925}
50 {'RADSENS_3': 2920}
52 {'RADSENS_3': 2918}
54 {'RADSENS_3': 2913}
56 {'RADSENS_3': 2917}
58 {'RADSENS_3': 2915}
60 {'RADSENS_3': 2936}
62 {'RADSENS_3': 2928}
33 {'RADSENS_3': 3311}
35 {'RADSENS_3': 3322}
37 {'RADSENS_3': 3320}
39 {'RADSENS_3': 3315}
41 {'RADSENS_3': 3321}
43 {'RADSENS_3': 3324}
45 {'RADSENS_3': 3317}
47 {'RADSENS_3': 3317}
49 {'RADSENS_3': 3318}
51 {'RADSENS_3': 3315}
53 {'RADSENS_3': 3324}
55 {'RADSENS_3': 3313}
57 {'RADSENS_3': 3314}
59 {'RADSENS_3': 3315}
61 {'RADSENS_3': 3311}
63 {'RADSENS_3': 3322}
32 {'RADSENS_4': 2919}
34 {'RADSENS_4': 2915}
36 {'RADSENS_4': 2927}
38 {'RADSENS_4': 2917}
40 {'RADSENS_4': 2926}
42 {'RADSENS_4': 2919}
44 {'RADSENS_4': 2911}
46 {'RADSENS_4': 2932}
48 {'RADSENS_4': 2918}
50 {'RADSENS_4': 2895}
52 {'RADSENS_4': 2921}
54 {'RADSENS_4': 2926}
56 {'RADSENS_4': 2907}
58 {'RADSENS_4': 2914}
60 {'RADSENS_4': 2928}
62 {'RADSENS_4': 2910}
33 {'RADSENS_4': 3314}
35 {'RADSENS_4': 3318}
37 {'RADSENS_4': 3311}
39 {'RADSENS_4': 3311}
41 {'RADSENS_4': 3307}
43 {'RADSENS_4': 3309}
45 {'RADSENS_4': 3316}
47 {'RADSENS_4': 3311}
49 {'RADSENS_4': 3315}
51 {'RADSENS_4': 3320}
53 {'RADSENS_4': 3311}
55 {'RADSENS_4': 3318}
57 {'RADSENS_4': 3315}
59 {'RADSENS_4': 3311}
61 {'RADSENS_4': 3310}
63 {'RADSENS_4': 3303}
