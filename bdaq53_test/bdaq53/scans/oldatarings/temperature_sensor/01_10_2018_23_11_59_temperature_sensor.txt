Timestamp GlobalTime(s) Dose(Mrad)
01_10_2018_23_11_59 1538146232.06 150
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2155}
34 {'ADCbandgap': 2156}
36 {'ADCbandgap': 2157}
38 {'ADCbandgap': 2155}
40 {'ADCbandgap': 2156}
42 {'ADCbandgap': 2160}
44 {'ADCbandgap': 2155}
46 {'ADCbandgap': 2157}
48 {'ADCbandgap': 2157}
50 {'ADCbandgap': 2158}
52 {'ADCbandgap': 2158}
54 {'ADCbandgap': 2156}
56 {'ADCbandgap': 2159}
58 {'ADCbandgap': 2156}
60 {'ADCbandgap': 2156}
62 {'ADCbandgap': 2159}
33 {'ADCbandgap': 2160}
35 {'ADCbandgap': 2160}
37 {'ADCbandgap': 2156}
39 {'ADCbandgap': 2158}
41 {'ADCbandgap': 2159}
43 {'ADCbandgap': 2157}
45 {'ADCbandgap': 2160}
47 {'ADCbandgap': 2156}
49 {'ADCbandgap': 2158}
51 {'ADCbandgap': 2155}
53 {'ADCbandgap': 2151}
55 {'ADCbandgap': 2158}
57 {'ADCbandgap': 2160}
59 {'ADCbandgap': 2156}
61 {'ADCbandgap': 2155}
63 {'ADCbandgap': 2158}
32 {'TEMPSENS_1': 1283}
34 {'TEMPSENS_1': 1279}
36 {'TEMPSENS_1': 1272}
38 {'TEMPSENS_1': 1278}
40 {'TEMPSENS_1': 1268}
42 {'TEMPSENS_1': 1273}
44 {'TEMPSENS_1': 1276}
46 {'TEMPSENS_1': 1269}
48 {'TEMPSENS_1': 1279}
50 {'TEMPSENS_1': 1280}
52 {'TEMPSENS_1': 1280}
54 {'TEMPSENS_1': 1278}
56 {'TEMPSENS_1': 1280}
58 {'TEMPSENS_1': 1275}
60 {'TEMPSENS_1': 1276}
62 {'TEMPSENS_1': 1275}
33 {'TEMPSENS_1': 1622}
35 {'TEMPSENS_1': 1624}
37 {'TEMPSENS_1': 1622}
39 {'TEMPSENS_1': 1621}
41 {'TEMPSENS_1': 1623}
43 {'TEMPSENS_1': 1621}
45 {'TEMPSENS_1': 1624}
47 {'TEMPSENS_1': 1620}
49 {'TEMPSENS_1': 1622}
51 {'TEMPSENS_1': 1617}
53 {'TEMPSENS_1': 1619}
55 {'TEMPSENS_1': 1624}
57 {'TEMPSENS_1': 1618}
59 {'TEMPSENS_1': 1617}
61 {'TEMPSENS_1': 1624}
63 {'TEMPSENS_1': 1628}
32 {'TEMPSENS_2': 1269}
34 {'TEMPSENS_2': 1269}
36 {'TEMPSENS_2': 1270}
38 {'TEMPSENS_2': 1271}
40 {'TEMPSENS_2': 1273}
42 {'TEMPSENS_2': 1261}
44 {'TEMPSENS_2': 1270}
46 {'TEMPSENS_2': 1271}
48 {'TEMPSENS_2': 1267}
50 {'TEMPSENS_2': 1271}
52 {'TEMPSENS_2': 1278}
54 {'TEMPSENS_2': 1262}
56 {'TEMPSENS_2': 1275}
58 {'TEMPSENS_2': 1279}
60 {'TEMPSENS_2': 1278}
62 {'TEMPSENS_2': 1275}
33 {'TEMPSENS_2': 1614}
35 {'TEMPSENS_2': 1614}
37 {'TEMPSENS_2': 1616}
39 {'TEMPSENS_2': 1615}
41 {'TEMPSENS_2': 1616}
43 {'TEMPSENS_2': 1615}
45 {'TEMPSENS_2': 1618}
47 {'TEMPSENS_2': 1616}
49 {'TEMPSENS_2': 1612}
51 {'TEMPSENS_2': 1617}
53 {'TEMPSENS_2': 1616}
55 {'TEMPSENS_2': 1613}
57 {'TEMPSENS_2': 1607}
59 {'TEMPSENS_2': 1615}
61 {'TEMPSENS_2': 1614}
63 {'TEMPSENS_2': 1613}
32 {'TEMPSENS_3': 1276}
34 {'TEMPSENS_3': 1276}
36 {'TEMPSENS_3': 1268}
38 {'TEMPSENS_3': 1268}
40 {'TEMPSENS_3': 1272}
42 {'TEMPSENS_3': 1275}
44 {'TEMPSENS_3': 1268}
46 {'TEMPSENS_3': 1267}
48 {'TEMPSENS_3': 1270}
50 {'TEMPSENS_3': 1261}
52 {'TEMPSENS_3': 1277}
54 {'TEMPSENS_3': 1267}
56 {'TEMPSENS_3': 1282}
58 {'TEMPSENS_3': 1271}
60 {'TEMPSENS_3': 1270}
62 {'TEMPSENS_3': 1272}
33 {'TEMPSENS_3': 1612}
35 {'TEMPSENS_3': 1615}
37 {'TEMPSENS_3': 1618}
39 {'TEMPSENS_3': 1615}
41 {'TEMPSENS_3': 1615}
43 {'TEMPSENS_3': 1616}
45 {'TEMPSENS_3': 1613}
47 {'TEMPSENS_3': 1613}
49 {'TEMPSENS_3': 1618}
51 {'TEMPSENS_3': 1615}
53 {'TEMPSENS_3': 1617}
55 {'TEMPSENS_3': 1615}
57 {'TEMPSENS_3': 1617}
59 {'TEMPSENS_3': 1616}
61 {'TEMPSENS_3': 1611}
63 {'TEMPSENS_3': 1616}
32 {'TEMPSENS_4': 1264}
34 {'TEMPSENS_4': 1259}
36 {'TEMPSENS_4': 1256}
38 {'TEMPSENS_4': 1268}
40 {'TEMPSENS_4': 1264}
42 {'TEMPSENS_4': 1252}
44 {'TEMPSENS_4': 1262}
46 {'TEMPSENS_4': 1258}
48 {'TEMPSENS_4': 1259}
50 {'TEMPSENS_4': 1267}
52 {'TEMPSENS_4': 1255}
54 {'TEMPSENS_4': 1266}
56 {'TEMPSENS_4': 1271}
58 {'TEMPSENS_4': 1251}
60 {'TEMPSENS_4': 1264}
62 {'TEMPSENS_4': 1268}
33 {'TEMPSENS_4': 1608}
35 {'TEMPSENS_4': 1605}
37 {'TEMPSENS_4': 1604}
39 {'TEMPSENS_4': 1603}
41 {'TEMPSENS_4': 1608}
43 {'TEMPSENS_4': 1605}
45 {'TEMPSENS_4': 1603}
47 {'TEMPSENS_4': 1608}
49 {'TEMPSENS_4': 1603}
51 {'TEMPSENS_4': 1604}
53 {'TEMPSENS_4': 1603}
55 {'TEMPSENS_4': 1603}
57 {'TEMPSENS_4': 1603}
59 {'TEMPSENS_4': 1607}
61 {'TEMPSENS_4': 1604}
63 {'TEMPSENS_4': 1608}
32 {'RADSENS_1': 3048}
34 {'RADSENS_1': 3039}
36 {'RADSENS_1': 3042}
38 {'RADSENS_1': 3056}
40 {'RADSENS_1': 3039}
42 {'RADSENS_1': 3042}
44 {'RADSENS_1': 3048}
46 {'RADSENS_1': 3056}
48 {'RADSENS_1': 3048}
50 {'RADSENS_1': 3039}
52 {'RADSENS_1': 3040}
54 {'RADSENS_1': 3044}
56 {'RADSENS_1': 3047}
58 {'RADSENS_1': 3038}
60 {'RADSENS_1': 3048}
62 {'RADSENS_1': 3051}
33 {'RADSENS_1': 3367}
35 {'RADSENS_1': 3376}
37 {'RADSENS_1': 3371}
39 {'RADSENS_1': 3363}
41 {'RADSENS_1': 3367}
43 {'RADSENS_1': 3372}
45 {'RADSENS_1': 3376}
47 {'RADSENS_1': 3368}
49 {'RADSENS_1': 3367}
51 {'RADSENS_1': 3376}
53 {'RADSENS_1': 3367}
55 {'RADSENS_1': 3372}
57 {'RADSENS_1': 3374}
59 {'RADSENS_1': 3367}
61 {'RADSENS_1': 3366}
63 {'RADSENS_1': 3372}
32 {'RADSENS_2': 2943}
34 {'RADSENS_2': 2952}
36 {'RADSENS_2': 2952}
38 {'RADSENS_2': 2947}
40 {'RADSENS_2': 2942}
42 {'RADSENS_2': 2960}
44 {'RADSENS_2': 2947}
46 {'RADSENS_2': 2943}
48 {'RADSENS_2': 2940}
50 {'RADSENS_2': 2939}
52 {'RADSENS_2': 2950}
54 {'RADSENS_2': 2943}
56 {'RADSENS_2': 2958}
58 {'RADSENS_2': 2944}
60 {'RADSENS_2': 2947}
62 {'RADSENS_2': 2953}
33 {'RADSENS_2': 3327}
35 {'RADSENS_2': 3344}
37 {'RADSENS_2': 3333}
39 {'RADSENS_2': 3332}
41 {'RADSENS_2': 3331}
43 {'RADSENS_2': 3332}
45 {'RADSENS_2': 3329}
47 {'RADSENS_2': 3331}
49 {'RADSENS_2': 3332}
51 {'RADSENS_2': 3329}
53 {'RADSENS_2': 3331}
55 {'RADSENS_2': 3327}
57 {'RADSENS_2': 3332}
59 {'RADSENS_2': 3331}
61 {'RADSENS_2': 3330}
63 {'RADSENS_2': 3327}
32 {'RADSENS_3': 2919}
34 {'RADSENS_3': 2913}
36 {'RADSENS_3': 2939}
38 {'RADSENS_3': 2926}
40 {'RADSENS_3': 2922}
42 {'RADSENS_3': 2928}
44 {'RADSENS_3': 2925}
46 {'RADSENS_3': 2918}
48 {'RADSENS_3': 2923}
50 {'RADSENS_3': 2915}
52 {'RADSENS_3': 2922}
54 {'RADSENS_3': 2914}
56 {'RADSENS_3': 2919}
58 {'RADSENS_3': 2919}
60 {'RADSENS_3': 2932}
62 {'RADSENS_3': 2926}
33 {'RADSENS_3': 3320}
35 {'RADSENS_3': 3316}
37 {'RADSENS_3': 3314}
39 {'RADSENS_3': 3311}
41 {'RADSENS_3': 3311}
43 {'RADSENS_3': 3315}
45 {'RADSENS_3': 3315}
47 {'RADSENS_3': 3311}
49 {'RADSENS_3': 3314}
51 {'RADSENS_3': 3320}
53 {'RADSENS_3': 3324}
55 {'RADSENS_3': 3320}
57 {'RADSENS_3': 3313}
59 {'RADSENS_3': 3316}
61 {'RADSENS_3': 3321}
63 {'RADSENS_3': 3320}
32 {'RADSENS_4': 2923}
34 {'RADSENS_4': 2924}
36 {'RADSENS_4': 2929}
38 {'RADSENS_4': 2924}
40 {'RADSENS_4': 2928}
42 {'RADSENS_4': 2928}
44 {'RADSENS_4': 2915}
46 {'RADSENS_4': 2936}
48 {'RADSENS_4': 2923}
50 {'RADSENS_4': 2915}
52 {'RADSENS_4': 2930}
54 {'RADSENS_4': 2921}
56 {'RADSENS_4': 2916}
58 {'RADSENS_4': 2917}
60 {'RADSENS_4': 2915}
62 {'RADSENS_4': 2919}
33 {'RADSENS_4': 3318}
35 {'RADSENS_4': 3313}
37 {'RADSENS_4': 3317}
39 {'RADSENS_4': 3315}
41 {'RADSENS_4': 3320}
43 {'RADSENS_4': 3313}
45 {'RADSENS_4': 3314}
47 {'RADSENS_4': 3315}
49 {'RADSENS_4': 3320}
51 {'RADSENS_4': 3320}
53 {'RADSENS_4': 3315}
55 {'RADSENS_4': 3311}
57 {'RADSENS_4': 3318}
59 {'RADSENS_4': 3311}
61 {'RADSENS_4': 3320}
63 {'RADSENS_4': 3319}
