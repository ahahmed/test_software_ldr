Timestamp GlobalTime(s) Dose(Mrad)
02_10_2018_00_47_27 1538146232.06 150
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2155}
34 {'ADCbandgap': 2155}
36 {'ADCbandgap': 2156}
38 {'ADCbandgap': 2160}
40 {'ADCbandgap': 2155}
42 {'ADCbandgap': 2158}
44 {'ADCbandgap': 2156}
46 {'ADCbandgap': 2155}
48 {'ADCbandgap': 2157}
50 {'ADCbandgap': 2156}
52 {'ADCbandgap': 2153}
54 {'ADCbandgap': 2158}
56 {'ADCbandgap': 2159}
58 {'ADCbandgap': 2155}
60 {'ADCbandgap': 2157}
62 {'ADCbandgap': 2157}
33 {'ADCbandgap': 2155}
35 {'ADCbandgap': 2157}
37 {'ADCbandgap': 2151}
39 {'ADCbandgap': 2156}
41 {'ADCbandgap': 2159}
43 {'ADCbandgap': 2155}
45 {'ADCbandgap': 2160}
47 {'ADCbandgap': 2157}
49 {'ADCbandgap': 2151}
51 {'ADCbandgap': 2160}
53 {'ADCbandgap': 2156}
55 {'ADCbandgap': 2156}
57 {'ADCbandgap': 2156}
59 {'ADCbandgap': 2155}
61 {'ADCbandgap': 2155}
63 {'ADCbandgap': 2160}
32 {'TEMPSENS_1': 1276}
34 {'TEMPSENS_1': 1274}
36 {'TEMPSENS_1': 1271}
38 {'TEMPSENS_1': 1278}
40 {'TEMPSENS_1': 1267}
42 {'TEMPSENS_1': 1277}
44 {'TEMPSENS_1': 1275}
46 {'TEMPSENS_1': 1270}
48 {'TEMPSENS_1': 1279}
50 {'TEMPSENS_1': 1280}
52 {'TEMPSENS_1': 1279}
54 {'TEMPSENS_1': 1278}
56 {'TEMPSENS_1': 1278}
58 {'TEMPSENS_1': 1276}
60 {'TEMPSENS_1': 1279}
62 {'TEMPSENS_1': 1273}
33 {'TEMPSENS_1': 1626}
35 {'TEMPSENS_1': 1623}
37 {'TEMPSENS_1': 1622}
39 {'TEMPSENS_1': 1619}
41 {'TEMPSENS_1': 1619}
43 {'TEMPSENS_1': 1619}
45 {'TEMPSENS_1': 1621}
47 {'TEMPSENS_1': 1624}
49 {'TEMPSENS_1': 1624}
51 {'TEMPSENS_1': 1619}
53 {'TEMPSENS_1': 1615}
55 {'TEMPSENS_1': 1621}
57 {'TEMPSENS_1': 1624}
59 {'TEMPSENS_1': 1622}
61 {'TEMPSENS_1': 1619}
63 {'TEMPSENS_1': 1624}
32 {'TEMPSENS_2': 1268}
34 {'TEMPSENS_2': 1271}
36 {'TEMPSENS_2': 1273}
38 {'TEMPSENS_2': 1271}
40 {'TEMPSENS_2': 1272}
42 {'TEMPSENS_2': 1261}
44 {'TEMPSENS_2': 1267}
46 {'TEMPSENS_2': 1265}
48 {'TEMPSENS_2': 1264}
50 {'TEMPSENS_2': 1271}
52 {'TEMPSENS_2': 1276}
54 {'TEMPSENS_2': 1263}
56 {'TEMPSENS_2': 1270}
58 {'TEMPSENS_2': 1278}
60 {'TEMPSENS_2': 1280}
62 {'TEMPSENS_2': 1272}
33 {'TEMPSENS_2': 1607}
35 {'TEMPSENS_2': 1611}
37 {'TEMPSENS_2': 1616}
39 {'TEMPSENS_2': 1616}
41 {'TEMPSENS_2': 1617}
43 {'TEMPSENS_2': 1615}
45 {'TEMPSENS_2': 1618}
47 {'TEMPSENS_2': 1614}
49 {'TEMPSENS_2': 1614}
51 {'TEMPSENS_2': 1616}
53 {'TEMPSENS_2': 1616}
55 {'TEMPSENS_2': 1614}
57 {'TEMPSENS_2': 1613}
59 {'TEMPSENS_2': 1612}
61 {'TEMPSENS_2': 1616}
63 {'TEMPSENS_2': 1618}
32 {'TEMPSENS_3': 1267}
34 {'TEMPSENS_3': 1271}
36 {'TEMPSENS_3': 1269}
38 {'TEMPSENS_3': 1266}
40 {'TEMPSENS_3': 1264}
42 {'TEMPSENS_3': 1276}
44 {'TEMPSENS_3': 1276}
46 {'TEMPSENS_3': 1266}
48 {'TEMPSENS_3': 1270}
50 {'TEMPSENS_3': 1265}
52 {'TEMPSENS_3': 1277}
54 {'TEMPSENS_3': 1269}
56 {'TEMPSENS_3': 1276}
58 {'TEMPSENS_3': 1271}
60 {'TEMPSENS_3': 1270}
62 {'TEMPSENS_3': 1268}
33 {'TEMPSENS_3': 1615}
35 {'TEMPSENS_3': 1615}
37 {'TEMPSENS_3': 1619}
39 {'TEMPSENS_3': 1618}
41 {'TEMPSENS_3': 1620}
43 {'TEMPSENS_3': 1616}
45 {'TEMPSENS_3': 1615}
47 {'TEMPSENS_3': 1618}
49 {'TEMPSENS_3': 1615}
51 {'TEMPSENS_3': 1614}
53 {'TEMPSENS_3': 1616}
55 {'TEMPSENS_3': 1615}
57 {'TEMPSENS_3': 1618}
59 {'TEMPSENS_3': 1613}
61 {'TEMPSENS_3': 1616}
63 {'TEMPSENS_3': 1615}
32 {'TEMPSENS_4': 1265}
34 {'TEMPSENS_4': 1262}
36 {'TEMPSENS_4': 1255}
38 {'TEMPSENS_4': 1261}
40 {'TEMPSENS_4': 1265}
42 {'TEMPSENS_4': 1254}
44 {'TEMPSENS_4': 1256}
46 {'TEMPSENS_4': 1260}
48 {'TEMPSENS_4': 1264}
50 {'TEMPSENS_4': 1264}
52 {'TEMPSENS_4': 1260}
54 {'TEMPSENS_4': 1265}
56 {'TEMPSENS_4': 1272}
58 {'TEMPSENS_4': 1250}
60 {'TEMPSENS_4': 1259}
62 {'TEMPSENS_4': 1264}
33 {'TEMPSENS_4': 1608}
35 {'TEMPSENS_4': 1601}
37 {'TEMPSENS_4': 1603}
39 {'TEMPSENS_4': 1608}
41 {'TEMPSENS_4': 1604}
43 {'TEMPSENS_4': 1606}
45 {'TEMPSENS_4': 1607}
47 {'TEMPSENS_4': 1607}
49 {'TEMPSENS_4': 1610}
51 {'TEMPSENS_4': 1608}
53 {'TEMPSENS_4': 1606}
55 {'TEMPSENS_4': 1599}
57 {'TEMPSENS_4': 1603}
59 {'TEMPSENS_4': 1608}
61 {'TEMPSENS_4': 1602}
63 {'TEMPSENS_4': 1604}
32 {'RADSENS_1': 3044}
34 {'RADSENS_1': 3033}
36 {'RADSENS_1': 3048}
38 {'RADSENS_1': 3047}
40 {'RADSENS_1': 3036}
42 {'RADSENS_1': 3034}
44 {'RADSENS_1': 3038}
46 {'RADSENS_1': 3052}
48 {'RADSENS_1': 3038}
50 {'RADSENS_1': 3030}
52 {'RADSENS_1': 3040}
54 {'RADSENS_1': 3042}
56 {'RADSENS_1': 3052}
58 {'RADSENS_1': 3037}
60 {'RADSENS_1': 3038}
62 {'RADSENS_1': 3042}
33 {'RADSENS_1': 3364}
35 {'RADSENS_1': 3359}
37 {'RADSENS_1': 3376}
39 {'RADSENS_1': 3366}
41 {'RADSENS_1': 3366}
43 {'RADSENS_1': 3374}
45 {'RADSENS_1': 3372}
47 {'RADSENS_1': 3367}
49 {'RADSENS_1': 3376}
51 {'RADSENS_1': 3370}
53 {'RADSENS_1': 3372}
55 {'RADSENS_1': 3368}
57 {'RADSENS_1': 3367}
59 {'RADSENS_1': 3374}
61 {'RADSENS_1': 3367}
63 {'RADSENS_1': 3371}
32 {'RADSENS_2': 2940}
34 {'RADSENS_2': 2942}
36 {'RADSENS_2': 2945}
38 {'RADSENS_2': 2947}
40 {'RADSENS_2': 2944}
42 {'RADSENS_2': 2950}
44 {'RADSENS_2': 2952}
46 {'RADSENS_2': 2939}
48 {'RADSENS_2': 2934}
50 {'RADSENS_2': 2934}
52 {'RADSENS_2': 2952}
54 {'RADSENS_2': 2929}
56 {'RADSENS_2': 2949}
58 {'RADSENS_2': 2950}
60 {'RADSENS_2': 2935}
62 {'RADSENS_2': 2945}
33 {'RADSENS_2': 3336}
35 {'RADSENS_2': 3327}
37 {'RADSENS_2': 3330}
39 {'RADSENS_2': 3328}
41 {'RADSENS_2': 3333}
43 {'RADSENS_2': 3325}
45 {'RADSENS_2': 3327}
47 {'RADSENS_2': 3331}
49 {'RADSENS_2': 3330}
51 {'RADSENS_2': 3324}
53 {'RADSENS_2': 3327}
55 {'RADSENS_2': 3327}
57 {'RADSENS_2': 3327}
59 {'RADSENS_2': 3330}
61 {'RADSENS_2': 3324}
63 {'RADSENS_2': 3327}
32 {'RADSENS_3': 2910}
34 {'RADSENS_3': 2920}
36 {'RADSENS_3': 2932}
38 {'RADSENS_3': 2920}
40 {'RADSENS_3': 2923}
42 {'RADSENS_3': 2919}
44 {'RADSENS_3': 2909}
46 {'RADSENS_3': 2920}
48 {'RADSENS_3': 2919}
50 {'RADSENS_3': 2913}
52 {'RADSENS_3': 2920}
54 {'RADSENS_3': 2920}
56 {'RADSENS_3': 2921}
58 {'RADSENS_3': 2912}
60 {'RADSENS_3': 2928}
62 {'RADSENS_3': 2919}
33 {'RADSENS_3': 3313}
35 {'RADSENS_3': 3318}
37 {'RADSENS_3': 3311}
39 {'RADSENS_3': 3317}
41 {'RADSENS_3': 3315}
43 {'RADSENS_3': 3313}
45 {'RADSENS_3': 3317}
47 {'RADSENS_3': 3314}
49 {'RADSENS_3': 3309}
51 {'RADSENS_3': 3312}
53 {'RADSENS_3': 3309}
55 {'RADSENS_3': 3316}
57 {'RADSENS_3': 3311}
59 {'RADSENS_3': 3320}
61 {'RADSENS_3': 3313}
63 {'RADSENS_3': 3316}
32 {'RADSENS_4': 2928}
34 {'RADSENS_4': 2930}
36 {'RADSENS_4': 2924}
38 {'RADSENS_4': 2911}
40 {'RADSENS_4': 2919}
42 {'RADSENS_4': 2928}
44 {'RADSENS_4': 2915}
46 {'RADSENS_4': 2929}
48 {'RADSENS_4': 2918}
50 {'RADSENS_4': 2904}
52 {'RADSENS_4': 2928}
54 {'RADSENS_4': 2915}
56 {'RADSENS_4': 2912}
58 {'RADSENS_4': 2901}
60 {'RADSENS_4': 2924}
62 {'RADSENS_4': 2916}
33 {'RADSENS_4': 3316}
35 {'RADSENS_4': 3316}
37 {'RADSENS_4': 3315}
39 {'RADSENS_4': 3320}
41 {'RADSENS_4': 3310}
43 {'RADSENS_4': 3314}
45 {'RADSENS_4': 3320}
47 {'RADSENS_4': 3311}
49 {'RADSENS_4': 3321}
51 {'RADSENS_4': 3317}
53 {'RADSENS_4': 3308}
55 {'RADSENS_4': 3309}
57 {'RADSENS_4': 3318}
59 {'RADSENS_4': 3322}
61 {'RADSENS_4': 3322}
63 {'RADSENS_4': 3315}
