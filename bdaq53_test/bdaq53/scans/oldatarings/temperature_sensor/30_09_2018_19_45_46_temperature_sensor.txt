Timestamp GlobalTime(s) Dose(Mrad)
30_09_2018_19_45_46 1538146232.06 60
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2153}
34 {'ADCbandgap': 2151}
36 {'ADCbandgap': 2153}
38 {'ADCbandgap': 2156}
40 {'ADCbandgap': 2156}
42 {'ADCbandgap': 2155}
44 {'ADCbandgap': 2154}
46 {'ADCbandgap': 2156}
48 {'ADCbandgap': 2155}
50 {'ADCbandgap': 2156}
52 {'ADCbandgap': 2158}
54 {'ADCbandgap': 2155}
56 {'ADCbandgap': 2151}
58 {'ADCbandgap': 2158}
60 {'ADCbandgap': 2160}
62 {'ADCbandgap': 2155}
33 {'ADCbandgap': 2160}
35 {'ADCbandgap': 2153}
37 {'ADCbandgap': 2151}
39 {'ADCbandgap': 2156}
41 {'ADCbandgap': 2158}
43 {'ADCbandgap': 2156}
45 {'ADCbandgap': 2155}
47 {'ADCbandgap': 2158}
49 {'ADCbandgap': 2154}
51 {'ADCbandgap': 2156}
53 {'ADCbandgap': 2157}
55 {'ADCbandgap': 2151}
57 {'ADCbandgap': 2153}
59 {'ADCbandgap': 2154}
61 {'ADCbandgap': 2156}
63 {'ADCbandgap': 2156}
32 {'TEMPSENS_1': 1290}
34 {'TEMPSENS_1': 1283}
36 {'TEMPSENS_1': 1277}
38 {'TEMPSENS_1': 1279}
40 {'TEMPSENS_1': 1275}
42 {'TEMPSENS_1': 1280}
44 {'TEMPSENS_1': 1274}
46 {'TEMPSENS_1': 1284}
48 {'TEMPSENS_1': 1287}
50 {'TEMPSENS_1': 1283}
52 {'TEMPSENS_1': 1284}
54 {'TEMPSENS_1': 1283}
56 {'TEMPSENS_1': 1283}
58 {'TEMPSENS_1': 1280}
60 {'TEMPSENS_1': 1277}
62 {'TEMPSENS_1': 1278}
33 {'TEMPSENS_1': 1626}
35 {'TEMPSENS_1': 1627}
37 {'TEMPSENS_1': 1626}
39 {'TEMPSENS_1': 1625}
41 {'TEMPSENS_1': 1629}
43 {'TEMPSENS_1': 1632}
45 {'TEMPSENS_1': 1627}
47 {'TEMPSENS_1': 1628}
49 {'TEMPSENS_1': 1628}
51 {'TEMPSENS_1': 1628}
53 {'TEMPSENS_1': 1625}
55 {'TEMPSENS_1': 1628}
57 {'TEMPSENS_1': 1630}
59 {'TEMPSENS_1': 1628}
61 {'TEMPSENS_1': 1628}
63 {'TEMPSENS_1': 1628}
32 {'TEMPSENS_2': 1272}
34 {'TEMPSENS_2': 1278}
36 {'TEMPSENS_2': 1268}
38 {'TEMPSENS_2': 1268}
40 {'TEMPSENS_2': 1276}
42 {'TEMPSENS_2': 1272}
44 {'TEMPSENS_2': 1271}
46 {'TEMPSENS_2': 1265}
48 {'TEMPSENS_2': 1271}
50 {'TEMPSENS_2': 1278}
52 {'TEMPSENS_2': 1274}
54 {'TEMPSENS_2': 1258}
56 {'TEMPSENS_2': 1270}
58 {'TEMPSENS_2': 1281}
60 {'TEMPSENS_2': 1276}
62 {'TEMPSENS_2': 1271}
33 {'TEMPSENS_2': 1619}
35 {'TEMPSENS_2': 1615}
37 {'TEMPSENS_2': 1622}
39 {'TEMPSENS_2': 1619}
41 {'TEMPSENS_2': 1618}
43 {'TEMPSENS_2': 1611}
45 {'TEMPSENS_2': 1620}
47 {'TEMPSENS_2': 1624}
49 {'TEMPSENS_2': 1624}
51 {'TEMPSENS_2': 1615}
53 {'TEMPSENS_2': 1624}
55 {'TEMPSENS_2': 1617}
57 {'TEMPSENS_2': 1617}
59 {'TEMPSENS_2': 1617}
61 {'TEMPSENS_2': 1616}
63 {'TEMPSENS_2': 1617}
32 {'TEMPSENS_3': 1278}
34 {'TEMPSENS_3': 1278}
36 {'TEMPSENS_3': 1268}
38 {'TEMPSENS_3': 1263}
40 {'TEMPSENS_3': 1268}
42 {'TEMPSENS_3': 1278}
44 {'TEMPSENS_3': 1275}
46 {'TEMPSENS_3': 1270}
48 {'TEMPSENS_3': 1273}
50 {'TEMPSENS_3': 1266}
52 {'TEMPSENS_3': 1283}
54 {'TEMPSENS_3': 1275}
56 {'TEMPSENS_3': 1278}
58 {'TEMPSENS_3': 1271}
60 {'TEMPSENS_3': 1276}
62 {'TEMPSENS_3': 1277}
33 {'TEMPSENS_3': 1618}
35 {'TEMPSENS_3': 1620}
37 {'TEMPSENS_3': 1619}
39 {'TEMPSENS_3': 1619}
41 {'TEMPSENS_3': 1617}
43 {'TEMPSENS_3': 1618}
45 {'TEMPSENS_3': 1614}
47 {'TEMPSENS_3': 1619}
49 {'TEMPSENS_3': 1622}
51 {'TEMPSENS_3': 1617}
53 {'TEMPSENS_3': 1618}
55 {'TEMPSENS_3': 1624}
57 {'TEMPSENS_3': 1619}
59 {'TEMPSENS_3': 1619}
61 {'TEMPSENS_3': 1620}
63 {'TEMPSENS_3': 1615}
32 {'TEMPSENS_4': 1265}
34 {'TEMPSENS_4': 1268}
36 {'TEMPSENS_4': 1263}
38 {'TEMPSENS_4': 1263}
40 {'TEMPSENS_4': 1270}
42 {'TEMPSENS_4': 1261}
44 {'TEMPSENS_4': 1259}
46 {'TEMPSENS_4': 1260}
48 {'TEMPSENS_4': 1261}
50 {'TEMPSENS_4': 1268}
52 {'TEMPSENS_4': 1264}
54 {'TEMPSENS_4': 1271}
56 {'TEMPSENS_4': 1271}
58 {'TEMPSENS_4': 1255}
60 {'TEMPSENS_4': 1266}
62 {'TEMPSENS_4': 1264}
33 {'TEMPSENS_4': 1607}
35 {'TEMPSENS_4': 1610}
37 {'TEMPSENS_4': 1610}
39 {'TEMPSENS_4': 1607}
41 {'TEMPSENS_4': 1608}
43 {'TEMPSENS_4': 1611}
45 {'TEMPSENS_4': 1608}
47 {'TEMPSENS_4': 1609}
49 {'TEMPSENS_4': 1607}
51 {'TEMPSENS_4': 1608}
53 {'TEMPSENS_4': 1607}
55 {'TEMPSENS_4': 1607}
57 {'TEMPSENS_4': 1607}
59 {'TEMPSENS_4': 1609}
61 {'TEMPSENS_4': 1610}
63 {'TEMPSENS_4': 1609}
32 {'RADSENS_1': 3086}
34 {'RADSENS_1': 3080}
36 {'RADSENS_1': 3086}
38 {'RADSENS_1': 3087}
40 {'RADSENS_1': 3088}
42 {'RADSENS_1': 3078}
44 {'RADSENS_1': 3071}
46 {'RADSENS_1': 3093}
48 {'RADSENS_1': 3084}
50 {'RADSENS_1': 3073}
52 {'RADSENS_1': 3088}
54 {'RADSENS_1': 3083}
56 {'RADSENS_1': 3087}
58 {'RADSENS_1': 3085}
60 {'RADSENS_1': 3085}
62 {'RADSENS_1': 3088}
33 {'RADSENS_1': 3395}
35 {'RADSENS_1': 3392}
37 {'RADSENS_1': 3389}
39 {'RADSENS_1': 3398}
41 {'RADSENS_1': 3391}
43 {'RADSENS_1': 3387}
45 {'RADSENS_1': 3392}
47 {'RADSENS_1': 3388}
49 {'RADSENS_1': 3393}
51 {'RADSENS_1': 3395}
53 {'RADSENS_1': 3395}
55 {'RADSENS_1': 3390}
57 {'RADSENS_1': 3400}
59 {'RADSENS_1': 3388}
61 {'RADSENS_1': 3400}
63 {'RADSENS_1': 3393}
32 {'RADSENS_2': 2975}
34 {'RADSENS_2': 2977}
36 {'RADSENS_2': 2980}
38 {'RADSENS_2': 2983}
40 {'RADSENS_2': 2978}
42 {'RADSENS_2': 2977}
44 {'RADSENS_2': 2976}
46 {'RADSENS_2': 2973}
48 {'RADSENS_2': 2969}
50 {'RADSENS_2': 2963}
52 {'RADSENS_2': 2979}
54 {'RADSENS_2': 2959}
56 {'RADSENS_2': 2992}
58 {'RADSENS_2': 2983}
60 {'RADSENS_2': 2975}
62 {'RADSENS_2': 2976}
33 {'RADSENS_2': 3360}
35 {'RADSENS_2': 3353}
37 {'RADSENS_2': 3355}
39 {'RADSENS_2': 3360}
41 {'RADSENS_2': 3352}
43 {'RADSENS_2': 3350}
45 {'RADSENS_2': 3360}
47 {'RADSENS_2': 3352}
49 {'RADSENS_2': 3350}
51 {'RADSENS_2': 3350}
53 {'RADSENS_2': 3352}
55 {'RADSENS_2': 3351}
57 {'RADSENS_2': 3360}
59 {'RADSENS_2': 3353}
61 {'RADSENS_2': 3351}
63 {'RADSENS_2': 3351}
32 {'RADSENS_3': 2942}
34 {'RADSENS_3': 2939}
36 {'RADSENS_3': 2956}
38 {'RADSENS_3': 2937}
40 {'RADSENS_3': 2943}
42 {'RADSENS_3': 2947}
44 {'RADSENS_3': 2944}
46 {'RADSENS_3': 2939}
48 {'RADSENS_3': 2934}
50 {'RADSENS_3': 2935}
52 {'RADSENS_3': 2945}
54 {'RADSENS_3': 2933}
56 {'RADSENS_3': 2947}
58 {'RADSENS_3': 2942}
60 {'RADSENS_3': 2943}
62 {'RADSENS_3': 2937}
33 {'RADSENS_3': 3332}
35 {'RADSENS_3': 3337}
37 {'RADSENS_3': 3331}
39 {'RADSENS_3': 3333}
41 {'RADSENS_3': 3327}
43 {'RADSENS_3': 3334}
45 {'RADSENS_3': 3330}
47 {'RADSENS_3': 3329}
49 {'RADSENS_3': 3327}
51 {'RADSENS_3': 3334}
53 {'RADSENS_3': 3344}
55 {'RADSENS_3': 3334}
57 {'RADSENS_3': 3336}
59 {'RADSENS_3': 3331}
61 {'RADSENS_3': 3336}
63 {'RADSENS_3': 3334}
32 {'RADSENS_4': 2943}
34 {'RADSENS_4': 2948}
36 {'RADSENS_4': 2954}
38 {'RADSENS_4': 2952}
40 {'RADSENS_4': 2946}
42 {'RADSENS_4': 2941}
44 {'RADSENS_4': 2954}
46 {'RADSENS_4': 2955}
48 {'RADSENS_4': 2942}
50 {'RADSENS_4': 2936}
52 {'RADSENS_4': 2948}
54 {'RADSENS_4': 2952}
56 {'RADSENS_4': 2939}
58 {'RADSENS_4': 2926}
60 {'RADSENS_4': 2949}
62 {'RADSENS_4': 2960}
33 {'RADSENS_4': 3336}
35 {'RADSENS_4': 3333}
37 {'RADSENS_4': 3336}
39 {'RADSENS_4': 3334}
41 {'RADSENS_4': 3334}
43 {'RADSENS_4': 3331}
45 {'RADSENS_4': 3327}
47 {'RADSENS_4': 3329}
49 {'RADSENS_4': 3336}
51 {'RADSENS_4': 3327}
53 {'RADSENS_4': 3328}
55 {'RADSENS_4': 3334}
57 {'RADSENS_4': 3327}
59 {'RADSENS_4': 3334}
61 {'RADSENS_4': 3336}
63 {'RADSENS_4': 3337}
