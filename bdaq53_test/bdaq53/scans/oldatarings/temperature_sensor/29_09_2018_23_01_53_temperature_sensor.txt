Timestamp GlobalTime(s) Dose(Mrad)
29_09_2018_23_01_53 1538146232.06 20
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2154}
34 {'ADCbandgap': 2156}
36 {'ADCbandgap': 2155}
38 {'ADCbandgap': 2153}
40 {'ADCbandgap': 2155}
42 {'ADCbandgap': 2154}
44 {'ADCbandgap': 2156}
46 {'ADCbandgap': 2153}
48 {'ADCbandgap': 2153}
50 {'ADCbandgap': 2151}
52 {'ADCbandgap': 2153}
54 {'ADCbandgap': 2153}
56 {'ADCbandgap': 2156}
58 {'ADCbandgap': 2151}
60 {'ADCbandgap': 2155}
62 {'ADCbandgap': 2154}
33 {'ADCbandgap': 2153}
35 {'ADCbandgap': 2156}
37 {'ADCbandgap': 2151}
39 {'ADCbandgap': 2153}
41 {'ADCbandgap': 2153}
43 {'ADCbandgap': 2152}
45 {'ADCbandgap': 2153}
47 {'ADCbandgap': 2155}
49 {'ADCbandgap': 2151}
51 {'ADCbandgap': 2154}
53 {'ADCbandgap': 2150}
55 {'ADCbandgap': 2151}
57 {'ADCbandgap': 2153}
59 {'ADCbandgap': 2158}
61 {'ADCbandgap': 2151}
63 {'ADCbandgap': 2152}
32 {'TEMPSENS_1': 1296}
34 {'TEMPSENS_1': 1287}
36 {'TEMPSENS_1': 1281}
38 {'TEMPSENS_1': 1288}
40 {'TEMPSENS_1': 1279}
42 {'TEMPSENS_1': 1289}
44 {'TEMPSENS_1': 1283}
46 {'TEMPSENS_1': 1288}
48 {'TEMPSENS_1': 1290}
50 {'TEMPSENS_1': 1293}
52 {'TEMPSENS_1': 1298}
54 {'TEMPSENS_1': 1283}
56 {'TEMPSENS_1': 1289}
58 {'TEMPSENS_1': 1291}
60 {'TEMPSENS_1': 1285}
62 {'TEMPSENS_1': 1282}
33 {'TEMPSENS_1': 1633}
35 {'TEMPSENS_1': 1634}
37 {'TEMPSENS_1': 1635}
39 {'TEMPSENS_1': 1633}
41 {'TEMPSENS_1': 1634}
43 {'TEMPSENS_1': 1632}
45 {'TEMPSENS_1': 1633}
47 {'TEMPSENS_1': 1631}
49 {'TEMPSENS_1': 1636}
51 {'TEMPSENS_1': 1631}
53 {'TEMPSENS_1': 1636}
55 {'TEMPSENS_1': 1633}
57 {'TEMPSENS_1': 1633}
59 {'TEMPSENS_1': 1637}
61 {'TEMPSENS_1': 1634}
63 {'TEMPSENS_1': 1636}
32 {'TEMPSENS_2': 1275}
34 {'TEMPSENS_2': 1279}
36 {'TEMPSENS_2': 1280}
38 {'TEMPSENS_2': 1273}
40 {'TEMPSENS_2': 1276}
42 {'TEMPSENS_2': 1272}
44 {'TEMPSENS_2': 1281}
46 {'TEMPSENS_2': 1272}
48 {'TEMPSENS_2': 1273}
50 {'TEMPSENS_2': 1281}
52 {'TEMPSENS_2': 1286}
54 {'TEMPSENS_2': 1267}
56 {'TEMPSENS_2': 1280}
58 {'TEMPSENS_2': 1285}
60 {'TEMPSENS_2': 1282}
62 {'TEMPSENS_2': 1279}
33 {'TEMPSENS_2': 1624}
35 {'TEMPSENS_2': 1622}
37 {'TEMPSENS_2': 1625}
39 {'TEMPSENS_2': 1624}
41 {'TEMPSENS_2': 1628}
43 {'TEMPSENS_2': 1625}
45 {'TEMPSENS_2': 1621}
47 {'TEMPSENS_2': 1625}
49 {'TEMPSENS_2': 1625}
51 {'TEMPSENS_2': 1623}
53 {'TEMPSENS_2': 1626}
55 {'TEMPSENS_2': 1625}
57 {'TEMPSENS_2': 1625}
59 {'TEMPSENS_2': 1623}
61 {'TEMPSENS_2': 1623}
63 {'TEMPSENS_2': 1624}
32 {'TEMPSENS_3': 1280}
34 {'TEMPSENS_3': 1279}
36 {'TEMPSENS_3': 1275}
38 {'TEMPSENS_3': 1277}
40 {'TEMPSENS_3': 1274}
42 {'TEMPSENS_3': 1279}
44 {'TEMPSENS_3': 1274}
46 {'TEMPSENS_3': 1267}
48 {'TEMPSENS_3': 1280}
50 {'TEMPSENS_3': 1270}
52 {'TEMPSENS_3': 1281}
54 {'TEMPSENS_3': 1280}
56 {'TEMPSENS_3': 1286}
58 {'TEMPSENS_3': 1276}
60 {'TEMPSENS_3': 1276}
62 {'TEMPSENS_3': 1281}
33 {'TEMPSENS_3': 1619}
35 {'TEMPSENS_3': 1628}
37 {'TEMPSENS_3': 1628}
39 {'TEMPSENS_3': 1623}
41 {'TEMPSENS_3': 1622}
43 {'TEMPSENS_3': 1623}
45 {'TEMPSENS_3': 1625}
47 {'TEMPSENS_3': 1621}
49 {'TEMPSENS_3': 1623}
51 {'TEMPSENS_3': 1628}
53 {'TEMPSENS_3': 1623}
55 {'TEMPSENS_3': 1622}
57 {'TEMPSENS_3': 1624}
59 {'TEMPSENS_3': 1623}
61 {'TEMPSENS_3': 1625}
63 {'TEMPSENS_3': 1622}
32 {'TEMPSENS_4': 1270}
34 {'TEMPSENS_4': 1275}
36 {'TEMPSENS_4': 1272}
38 {'TEMPSENS_4': 1269}
40 {'TEMPSENS_4': 1279}
42 {'TEMPSENS_4': 1266}
44 {'TEMPSENS_4': 1267}
46 {'TEMPSENS_4': 1271}
48 {'TEMPSENS_4': 1276}
50 {'TEMPSENS_4': 1269}
52 {'TEMPSENS_4': 1267}
54 {'TEMPSENS_4': 1273}
56 {'TEMPSENS_4': 1274}
58 {'TEMPSENS_4': 1265}
60 {'TEMPSENS_4': 1271}
62 {'TEMPSENS_4': 1271}
33 {'TEMPSENS_4': 1617}
35 {'TEMPSENS_4': 1620}
37 {'TEMPSENS_4': 1618}
39 {'TEMPSENS_4': 1615}
41 {'TEMPSENS_4': 1619}
43 {'TEMPSENS_4': 1618}
45 {'TEMPSENS_4': 1617}
47 {'TEMPSENS_4': 1615}
49 {'TEMPSENS_4': 1615}
51 {'TEMPSENS_4': 1616}
53 {'TEMPSENS_4': 1615}
55 {'TEMPSENS_4': 1617}
57 {'TEMPSENS_4': 1616}
59 {'TEMPSENS_4': 1614}
61 {'TEMPSENS_4': 1617}
63 {'TEMPSENS_4': 1620}
32 {'RADSENS_1': 3128}
34 {'RADSENS_1': 3128}
36 {'RADSENS_1': 3142}
38 {'RADSENS_1': 3139}
40 {'RADSENS_1': 3126}
42 {'RADSENS_1': 3136}
44 {'RADSENS_1': 3133}
46 {'RADSENS_1': 3142}
48 {'RADSENS_1': 3142}
50 {'RADSENS_1': 3123}
52 {'RADSENS_1': 3133}
54 {'RADSENS_1': 3135}
56 {'RADSENS_1': 3137}
58 {'RADSENS_1': 3131}
60 {'RADSENS_1': 3134}
62 {'RADSENS_1': 3131}
33 {'RADSENS_1': 3416}
35 {'RADSENS_1': 3420}
37 {'RADSENS_1': 3424}
39 {'RADSENS_1': 3420}
41 {'RADSENS_1': 3419}
43 {'RADSENS_1': 3421}
45 {'RADSENS_1': 3422}
47 {'RADSENS_1': 3416}
49 {'RADSENS_1': 3425}
51 {'RADSENS_1': 3422}
53 {'RADSENS_1': 3424}
55 {'RADSENS_1': 3415}
57 {'RADSENS_1': 3425}
59 {'RADSENS_1': 3424}
61 {'RADSENS_1': 3420}
63 {'RADSENS_1': 3421}
32 {'RADSENS_2': 3047}
34 {'RADSENS_2': 3061}
36 {'RADSENS_2': 3051}
38 {'RADSENS_2': 3055}
40 {'RADSENS_2': 3043}
42 {'RADSENS_2': 3062}
44 {'RADSENS_2': 3051}
46 {'RADSENS_2': 3047}
48 {'RADSENS_2': 3051}
50 {'RADSENS_2': 3052}
52 {'RADSENS_2': 3060}
54 {'RADSENS_2': 3039}
56 {'RADSENS_2': 3059}
58 {'RADSENS_2': 3058}
60 {'RADSENS_2': 3055}
62 {'RADSENS_2': 3051}
33 {'RADSENS_2': 3398}
35 {'RADSENS_2': 3391}
37 {'RADSENS_2': 3395}
39 {'RADSENS_2': 3388}
41 {'RADSENS_2': 3394}
43 {'RADSENS_2': 3395}
45 {'RADSENS_2': 3400}
47 {'RADSENS_2': 3395}
49 {'RADSENS_2': 3395}
51 {'RADSENS_2': 3394}
53 {'RADSENS_2': 3395}
55 {'RADSENS_2': 3389}
57 {'RADSENS_2': 3395}
59 {'RADSENS_2': 3387}
61 {'RADSENS_2': 3395}
63 {'RADSENS_2': 3391}
32 {'RADSENS_3': 3005}
34 {'RADSENS_3': 3012}
36 {'RADSENS_3': 3020}
38 {'RADSENS_3': 3017}
40 {'RADSENS_3': 3023}
42 {'RADSENS_3': 3017}
44 {'RADSENS_3': 3009}
46 {'RADSENS_3': 3025}
48 {'RADSENS_3': 3007}
50 {'RADSENS_3': 3003}
52 {'RADSENS_3': 3022}
54 {'RADSENS_3': 3008}
56 {'RADSENS_3': 3012}
58 {'RADSENS_3': 3016}
60 {'RADSENS_3': 3026}
62 {'RADSENS_3': 3020}
33 {'RADSENS_3': 3377}
35 {'RADSENS_3': 3374}
37 {'RADSENS_3': 3372}
39 {'RADSENS_3': 3378}
41 {'RADSENS_3': 3372}
43 {'RADSENS_3': 3376}
45 {'RADSENS_3': 3376}
47 {'RADSENS_3': 3371}
49 {'RADSENS_3': 3359}
51 {'RADSENS_3': 3378}
53 {'RADSENS_3': 3367}
55 {'RADSENS_3': 3374}
57 {'RADSENS_3': 3374}
59 {'RADSENS_3': 3367}
61 {'RADSENS_3': 3374}
63 {'RADSENS_3': 3384}
32 {'RADSENS_4': 3015}
34 {'RADSENS_4': 3018}
36 {'RADSENS_4': 3020}
38 {'RADSENS_4': 3020}
40 {'RADSENS_4': 3007}
42 {'RADSENS_4': 3013}
44 {'RADSENS_4': 3024}
46 {'RADSENS_4': 3026}
48 {'RADSENS_4': 3013}
50 {'RADSENS_4': 3005}
52 {'RADSENS_4': 3013}
54 {'RADSENS_4': 3014}
56 {'RADSENS_4': 3015}
58 {'RADSENS_4': 3008}
60 {'RADSENS_4': 3015}
62 {'RADSENS_4': 3017}
33 {'RADSENS_4': 3375}
35 {'RADSENS_4': 3376}
37 {'RADSENS_4': 3370}
39 {'RADSENS_4': 3376}
41 {'RADSENS_4': 3374}
43 {'RADSENS_4': 3378}
45 {'RADSENS_4': 3373}
47 {'RADSENS_4': 3373}
49 {'RADSENS_4': 3374}
51 {'RADSENS_4': 3373}
53 {'RADSENS_4': 3379}
55 {'RADSENS_4': 3379}
57 {'RADSENS_4': 3384}
59 {'RADSENS_4': 3380}
61 {'RADSENS_4': 3378}
63 {'RADSENS_4': 3372}
