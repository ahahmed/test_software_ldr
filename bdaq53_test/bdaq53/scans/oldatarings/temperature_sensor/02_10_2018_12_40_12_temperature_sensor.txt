Timestamp GlobalTime(s) Dose(Mrad)
02_10_2018_12_40_12 1538146232.06 200
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2158}
34 {'ADCbandgap': 2160}
36 {'ADCbandgap': 2160}
38 {'ADCbandgap': 2155}
40 {'ADCbandgap': 2160}
42 {'ADCbandgap': 2160}
44 {'ADCbandgap': 2160}
46 {'ADCbandgap': 2160}
48 {'ADCbandgap': 2160}
50 {'ADCbandgap': 2161}
52 {'ADCbandgap': 2156}
54 {'ADCbandgap': 2155}
56 {'ADCbandgap': 2158}
58 {'ADCbandgap': 2155}
60 {'ADCbandgap': 2157}
62 {'ADCbandgap': 2157}
33 {'ADCbandgap': 2158}
35 {'ADCbandgap': 2158}
37 {'ADCbandgap': 2155}
39 {'ADCbandgap': 2159}
41 {'ADCbandgap': 2157}
43 {'ADCbandgap': 2158}
45 {'ADCbandgap': 2156}
47 {'ADCbandgap': 2155}
49 {'ADCbandgap': 2160}
51 {'ADCbandgap': 2159}
53 {'ADCbandgap': 2157}
55 {'ADCbandgap': 2157}
57 {'ADCbandgap': 2158}
59 {'ADCbandgap': 2155}
61 {'ADCbandgap': 2155}
63 {'ADCbandgap': 2156}
32 {'TEMPSENS_1': 1278}
34 {'TEMPSENS_1': 1273}
36 {'TEMPSENS_1': 1262}
38 {'TEMPSENS_1': 1268}
40 {'TEMPSENS_1': 1264}
42 {'TEMPSENS_1': 1271}
44 {'TEMPSENS_1': 1267}
46 {'TEMPSENS_1': 1272}
48 {'TEMPSENS_1': 1273}
50 {'TEMPSENS_1': 1272}
52 {'TEMPSENS_1': 1271}
54 {'TEMPSENS_1': 1266}
56 {'TEMPSENS_1': 1270}
58 {'TEMPSENS_1': 1272}
60 {'TEMPSENS_1': 1275}
62 {'TEMPSENS_1': 1268}
33 {'TEMPSENS_1': 1611}
35 {'TEMPSENS_1': 1613}
37 {'TEMPSENS_1': 1607}
39 {'TEMPSENS_1': 1614}
41 {'TEMPSENS_1': 1617}
43 {'TEMPSENS_1': 1611}
45 {'TEMPSENS_1': 1616}
47 {'TEMPSENS_1': 1617}
49 {'TEMPSENS_1': 1616}
51 {'TEMPSENS_1': 1616}
53 {'TEMPSENS_1': 1616}
55 {'TEMPSENS_1': 1617}
57 {'TEMPSENS_1': 1611}
59 {'TEMPSENS_1': 1615}
61 {'TEMPSENS_1': 1611}
63 {'TEMPSENS_1': 1614}
32 {'TEMPSENS_2': 1260}
34 {'TEMPSENS_2': 1274}
36 {'TEMPSENS_2': 1264}
38 {'TEMPSENS_2': 1266}
40 {'TEMPSENS_2': 1265}
42 {'TEMPSENS_2': 1263}
44 {'TEMPSENS_2': 1270}
46 {'TEMPSENS_2': 1267}
48 {'TEMPSENS_2': 1269}
50 {'TEMPSENS_2': 1272}
52 {'TEMPSENS_2': 1267}
54 {'TEMPSENS_2': 1258}
56 {'TEMPSENS_2': 1267}
58 {'TEMPSENS_2': 1272}
60 {'TEMPSENS_2': 1276}
62 {'TEMPSENS_2': 1273}
33 {'TEMPSENS_2': 1614}
35 {'TEMPSENS_2': 1611}
37 {'TEMPSENS_2': 1613}
39 {'TEMPSENS_2': 1611}
41 {'TEMPSENS_2': 1614}
43 {'TEMPSENS_2': 1611}
45 {'TEMPSENS_2': 1610}
47 {'TEMPSENS_2': 1611}
49 {'TEMPSENS_2': 1608}
51 {'TEMPSENS_2': 1609}
53 {'TEMPSENS_2': 1612}
55 {'TEMPSENS_2': 1611}
57 {'TEMPSENS_2': 1612}
59 {'TEMPSENS_2': 1608}
61 {'TEMPSENS_2': 1611}
63 {'TEMPSENS_2': 1608}
32 {'TEMPSENS_3': 1268}
34 {'TEMPSENS_3': 1272}
36 {'TEMPSENS_3': 1267}
38 {'TEMPSENS_3': 1264}
40 {'TEMPSENS_3': 1272}
42 {'TEMPSENS_3': 1268}
44 {'TEMPSENS_3': 1265}
46 {'TEMPSENS_3': 1263}
48 {'TEMPSENS_3': 1263}
50 {'TEMPSENS_3': 1263}
52 {'TEMPSENS_3': 1279}
54 {'TEMPSENS_3': 1273}
56 {'TEMPSENS_3': 1278}
58 {'TEMPSENS_3': 1271}
60 {'TEMPSENS_3': 1269}
62 {'TEMPSENS_3': 1265}
33 {'TEMPSENS_3': 1613}
35 {'TEMPSENS_3': 1613}
37 {'TEMPSENS_3': 1611}
39 {'TEMPSENS_3': 1607}
41 {'TEMPSENS_3': 1617}
43 {'TEMPSENS_3': 1616}
45 {'TEMPSENS_3': 1615}
47 {'TEMPSENS_3': 1615}
49 {'TEMPSENS_3': 1615}
51 {'TEMPSENS_3': 1615}
53 {'TEMPSENS_3': 1609}
55 {'TEMPSENS_3': 1615}
57 {'TEMPSENS_3': 1613}
59 {'TEMPSENS_3': 1612}
61 {'TEMPSENS_3': 1612}
63 {'TEMPSENS_3': 1618}
32 {'TEMPSENS_4': 1254}
34 {'TEMPSENS_4': 1255}
36 {'TEMPSENS_4': 1259}
38 {'TEMPSENS_4': 1261}
40 {'TEMPSENS_4': 1262}
42 {'TEMPSENS_4': 1248}
44 {'TEMPSENS_4': 1246}
46 {'TEMPSENS_4': 1251}
48 {'TEMPSENS_4': 1256}
50 {'TEMPSENS_4': 1259}
52 {'TEMPSENS_4': 1252}
54 {'TEMPSENS_4': 1263}
56 {'TEMPSENS_4': 1268}
58 {'TEMPSENS_4': 1252}
60 {'TEMPSENS_4': 1255}
62 {'TEMPSENS_4': 1257}
33 {'TEMPSENS_4': 1600}
35 {'TEMPSENS_4': 1599}
37 {'TEMPSENS_4': 1598}
39 {'TEMPSENS_4': 1599}
41 {'TEMPSENS_4': 1598}
43 {'TEMPSENS_4': 1599}
45 {'TEMPSENS_4': 1604}
47 {'TEMPSENS_4': 1601}
49 {'TEMPSENS_4': 1599}
51 {'TEMPSENS_4': 1602}
53 {'TEMPSENS_4': 1596}
55 {'TEMPSENS_4': 1599}
57 {'TEMPSENS_4': 1600}
59 {'TEMPSENS_4': 1600}
61 {'TEMPSENS_4': 1599}
63 {'TEMPSENS_4': 1599}
32 {'RADSENS_1': 3011}
34 {'RADSENS_1': 3012}
36 {'RADSENS_1': 3028}
38 {'RADSENS_1': 3015}
40 {'RADSENS_1': 3007}
42 {'RADSENS_1': 3017}
44 {'RADSENS_1': 3022}
46 {'RADSENS_1': 3026}
48 {'RADSENS_1': 3020}
50 {'RADSENS_1': 3012}
52 {'RADSENS_1': 3012}
54 {'RADSENS_1': 3008}
56 {'RADSENS_1': 3019}
58 {'RADSENS_1': 3016}
60 {'RADSENS_1': 3023}
62 {'RADSENS_1': 3015}
33 {'RADSENS_1': 3352}
35 {'RADSENS_1': 3352}
37 {'RADSENS_1': 3352}
39 {'RADSENS_1': 3354}
41 {'RADSENS_1': 3347}
43 {'RADSENS_1': 3354}
45 {'RADSENS_1': 3360}
47 {'RADSENS_1': 3356}
49 {'RADSENS_1': 3352}
51 {'RADSENS_1': 3352}
53 {'RADSENS_1': 3355}
55 {'RADSENS_1': 3360}
57 {'RADSENS_1': 3352}
59 {'RADSENS_1': 3351}
61 {'RADSENS_1': 3352}
63 {'RADSENS_1': 3355}
32 {'RADSENS_2': 2936}
34 {'RADSENS_2': 2948}
36 {'RADSENS_2': 2936}
38 {'RADSENS_2': 2941}
40 {'RADSENS_2': 2926}
42 {'RADSENS_2': 2931}
44 {'RADSENS_2': 2925}
46 {'RADSENS_2': 2927}
48 {'RADSENS_2': 2926}
50 {'RADSENS_2': 2928}
52 {'RADSENS_2': 2935}
54 {'RADSENS_2': 2927}
56 {'RADSENS_2': 2935}
58 {'RADSENS_2': 2940}
60 {'RADSENS_2': 2924}
62 {'RADSENS_2': 2927}
33 {'RADSENS_2': 3323}
35 {'RADSENS_2': 3320}
37 {'RADSENS_2': 3324}
39 {'RADSENS_2': 3319}
41 {'RADSENS_2': 3320}
43 {'RADSENS_2': 3328}
45 {'RADSENS_2': 3328}
47 {'RADSENS_2': 3316}
49 {'RADSENS_2': 3328}
51 {'RADSENS_2': 3320}
53 {'RADSENS_2': 3315}
55 {'RADSENS_2': 3319}
57 {'RADSENS_2': 3317}
59 {'RADSENS_2': 3324}
61 {'RADSENS_2': 3320}
63 {'RADSENS_2': 3320}
32 {'RADSENS_3': 2904}
34 {'RADSENS_3': 2901}
36 {'RADSENS_3': 2926}
38 {'RADSENS_3': 2921}
40 {'RADSENS_3': 2919}
42 {'RADSENS_3': 2920}
44 {'RADSENS_3': 2908}
46 {'RADSENS_3': 2912}
48 {'RADSENS_3': 2912}
50 {'RADSENS_3': 2912}
52 {'RADSENS_3': 2915}
54 {'RADSENS_3': 2908}
56 {'RADSENS_3': 2912}
58 {'RADSENS_3': 2904}
60 {'RADSENS_3': 2916}
62 {'RADSENS_3': 2898}
33 {'RADSENS_3': 3306}
35 {'RADSENS_3': 3300}
37 {'RADSENS_3': 3303}
39 {'RADSENS_3': 3308}
41 {'RADSENS_3': 3299}
43 {'RADSENS_3': 3305}
45 {'RADSENS_3': 3304}
47 {'RADSENS_3': 3311}
49 {'RADSENS_3': 3312}
51 {'RADSENS_3': 3303}
53 {'RADSENS_3': 3304}
55 {'RADSENS_3': 3305}
57 {'RADSENS_3': 3307}
59 {'RADSENS_3': 3310}
61 {'RADSENS_3': 3304}
63 {'RADSENS_3': 3304}
32 {'RADSENS_4': 2920}
34 {'RADSENS_4': 2920}
36 {'RADSENS_4': 2913}
38 {'RADSENS_4': 2913}
40 {'RADSENS_4': 2910}
42 {'RADSENS_4': 2910}
44 {'RADSENS_4': 2913}
46 {'RADSENS_4': 2916}
48 {'RADSENS_4': 2902}
50 {'RADSENS_4': 2910}
52 {'RADSENS_4': 2919}
54 {'RADSENS_4': 2910}
56 {'RADSENS_4': 2908}
58 {'RADSENS_4': 2904}
60 {'RADSENS_4': 2908}
62 {'RADSENS_4': 2904}
33 {'RADSENS_4': 3295}
35 {'RADSENS_4': 3302}
37 {'RADSENS_4': 3300}
39 {'RADSENS_4': 3295}
41 {'RADSENS_4': 3302}
43 {'RADSENS_4': 3307}
45 {'RADSENS_4': 3304}
47 {'RADSENS_4': 3304}
49 {'RADSENS_4': 3304}
51 {'RADSENS_4': 3304}
53 {'RADSENS_4': 3303}
55 {'RADSENS_4': 3306}
57 {'RADSENS_4': 3308}
59 {'RADSENS_4': 3303}
61 {'RADSENS_4': 3293}
63 {'RADSENS_4': 3295}
