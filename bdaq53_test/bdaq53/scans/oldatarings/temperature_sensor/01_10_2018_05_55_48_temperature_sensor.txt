Timestamp GlobalTime(s) Dose(Mrad)
01_10_2018_05_55_48 1538146232.06 80
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2156}
34 {'ADCbandgap': 2160}
36 {'ADCbandgap': 2153}
38 {'ADCbandgap': 2156}
40 {'ADCbandgap': 2160}
42 {'ADCbandgap': 2156}
44 {'ADCbandgap': 2156}
46 {'ADCbandgap': 2155}
48 {'ADCbandgap': 2158}
50 {'ADCbandgap': 2156}
52 {'ADCbandgap': 2158}
54 {'ADCbandgap': 2155}
56 {'ADCbandgap': 2158}
58 {'ADCbandgap': 2159}
60 {'ADCbandgap': 2157}
62 {'ADCbandgap': 2157}
33 {'ADCbandgap': 2156}
35 {'ADCbandgap': 2156}
37 {'ADCbandgap': 2155}
39 {'ADCbandgap': 2156}
41 {'ADCbandgap': 2160}
43 {'ADCbandgap': 2157}
45 {'ADCbandgap': 2157}
47 {'ADCbandgap': 2156}
49 {'ADCbandgap': 2157}
51 {'ADCbandgap': 2154}
53 {'ADCbandgap': 2158}
55 {'ADCbandgap': 2157}
57 {'ADCbandgap': 2154}
59 {'ADCbandgap': 2160}
61 {'ADCbandgap': 2160}
63 {'ADCbandgap': 2155}
32 {'TEMPSENS_1': 1282}
34 {'TEMPSENS_1': 1283}
36 {'TEMPSENS_1': 1267}
38 {'TEMPSENS_1': 1270}
40 {'TEMPSENS_1': 1274}
42 {'TEMPSENS_1': 1283}
44 {'TEMPSENS_1': 1275}
46 {'TEMPSENS_1': 1267}
48 {'TEMPSENS_1': 1275}
50 {'TEMPSENS_1': 1282}
52 {'TEMPSENS_1': 1284}
54 {'TEMPSENS_1': 1272}
56 {'TEMPSENS_1': 1280}
58 {'TEMPSENS_1': 1278}
60 {'TEMPSENS_1': 1276}
62 {'TEMPSENS_1': 1271}
33 {'TEMPSENS_1': 1621}
35 {'TEMPSENS_1': 1624}
37 {'TEMPSENS_1': 1622}
39 {'TEMPSENS_1': 1628}
41 {'TEMPSENS_1': 1623}
43 {'TEMPSENS_1': 1628}
45 {'TEMPSENS_1': 1624}
47 {'TEMPSENS_1': 1624}
49 {'TEMPSENS_1': 1622}
51 {'TEMPSENS_1': 1623}
53 {'TEMPSENS_1': 1622}
55 {'TEMPSENS_1': 1622}
57 {'TEMPSENS_1': 1622}
59 {'TEMPSENS_1': 1619}
61 {'TEMPSENS_1': 1619}
63 {'TEMPSENS_1': 1624}
32 {'TEMPSENS_2': 1263}
34 {'TEMPSENS_2': 1273}
36 {'TEMPSENS_2': 1265}
38 {'TEMPSENS_2': 1263}
40 {'TEMPSENS_2': 1272}
42 {'TEMPSENS_2': 1265}
44 {'TEMPSENS_2': 1265}
46 {'TEMPSENS_2': 1264}
48 {'TEMPSENS_2': 1262}
50 {'TEMPSENS_2': 1272}
52 {'TEMPSENS_2': 1275}
54 {'TEMPSENS_2': 1257}
56 {'TEMPSENS_2': 1268}
58 {'TEMPSENS_2': 1275}
60 {'TEMPSENS_2': 1273}
62 {'TEMPSENS_2': 1271}
33 {'TEMPSENS_2': 1611}
35 {'TEMPSENS_2': 1614}
37 {'TEMPSENS_2': 1612}
39 {'TEMPSENS_2': 1615}
41 {'TEMPSENS_2': 1616}
43 {'TEMPSENS_2': 1615}
45 {'TEMPSENS_2': 1615}
47 {'TEMPSENS_2': 1614}
49 {'TEMPSENS_2': 1617}
51 {'TEMPSENS_2': 1607}
53 {'TEMPSENS_2': 1611}
55 {'TEMPSENS_2': 1616}
57 {'TEMPSENS_2': 1615}
59 {'TEMPSENS_2': 1611}
61 {'TEMPSENS_2': 1616}
63 {'TEMPSENS_2': 1615}
32 {'TEMPSENS_3': 1267}
34 {'TEMPSENS_3': 1270}
36 {'TEMPSENS_3': 1259}
38 {'TEMPSENS_3': 1265}
40 {'TEMPSENS_3': 1267}
42 {'TEMPSENS_3': 1267}
44 {'TEMPSENS_3': 1266}
46 {'TEMPSENS_3': 1263}
48 {'TEMPSENS_3': 1263}
50 {'TEMPSENS_3': 1260}
52 {'TEMPSENS_3': 1272}
54 {'TEMPSENS_3': 1269}
56 {'TEMPSENS_3': 1276}
58 {'TEMPSENS_3': 1270}
60 {'TEMPSENS_3': 1264}
62 {'TEMPSENS_3': 1267}
33 {'TEMPSENS_3': 1616}
35 {'TEMPSENS_3': 1613}
37 {'TEMPSENS_3': 1614}
39 {'TEMPSENS_3': 1612}
41 {'TEMPSENS_3': 1616}
43 {'TEMPSENS_3': 1614}
45 {'TEMPSENS_3': 1613}
47 {'TEMPSENS_3': 1612}
49 {'TEMPSENS_3': 1612}
51 {'TEMPSENS_3': 1612}
53 {'TEMPSENS_3': 1615}
55 {'TEMPSENS_3': 1620}
57 {'TEMPSENS_3': 1614}
59 {'TEMPSENS_3': 1612}
61 {'TEMPSENS_3': 1615}
63 {'TEMPSENS_3': 1616}
32 {'TEMPSENS_4': 1259}
34 {'TEMPSENS_4': 1259}
36 {'TEMPSENS_4': 1259}
38 {'TEMPSENS_4': 1258}
40 {'TEMPSENS_4': 1264}
42 {'TEMPSENS_4': 1254}
44 {'TEMPSENS_4': 1255}
46 {'TEMPSENS_4': 1256}
48 {'TEMPSENS_4': 1255}
50 {'TEMPSENS_4': 1265}
52 {'TEMPSENS_4': 1260}
54 {'TEMPSENS_4': 1262}
56 {'TEMPSENS_4': 1265}
58 {'TEMPSENS_4': 1251}
60 {'TEMPSENS_4': 1265}
62 {'TEMPSENS_4': 1264}
33 {'TEMPSENS_4': 1602}
35 {'TEMPSENS_4': 1604}
37 {'TEMPSENS_4': 1604}
39 {'TEMPSENS_4': 1608}
41 {'TEMPSENS_4': 1607}
43 {'TEMPSENS_4': 1605}
45 {'TEMPSENS_4': 1601}
47 {'TEMPSENS_4': 1603}
49 {'TEMPSENS_4': 1608}
51 {'TEMPSENS_4': 1601}
53 {'TEMPSENS_4': 1604}
55 {'TEMPSENS_4': 1604}
57 {'TEMPSENS_4': 1603}
59 {'TEMPSENS_4': 1605}
61 {'TEMPSENS_4': 1607}
63 {'TEMPSENS_4': 1603}
32 {'RADSENS_1': 3055}
34 {'RADSENS_1': 3051}
36 {'RADSENS_1': 3063}
38 {'RADSENS_1': 3067}
40 {'RADSENS_1': 3060}
42 {'RADSENS_1': 3055}
44 {'RADSENS_1': 3060}
46 {'RADSENS_1': 3071}
48 {'RADSENS_1': 3059}
50 {'RADSENS_1': 3053}
52 {'RADSENS_1': 3072}
54 {'RADSENS_1': 3058}
56 {'RADSENS_1': 3072}
58 {'RADSENS_1': 3063}
60 {'RADSENS_1': 3057}
62 {'RADSENS_1': 3059}
33 {'RADSENS_1': 3380}
35 {'RADSENS_1': 3385}
37 {'RADSENS_1': 3374}
39 {'RADSENS_1': 3380}
41 {'RADSENS_1': 3382}
43 {'RADSENS_1': 3383}
45 {'RADSENS_1': 3383}
47 {'RADSENS_1': 3388}
49 {'RADSENS_1': 3380}
51 {'RADSENS_1': 3377}
53 {'RADSENS_1': 3377}
55 {'RADSENS_1': 3375}
57 {'RADSENS_1': 3375}
59 {'RADSENS_1': 3379}
61 {'RADSENS_1': 3381}
63 {'RADSENS_1': 3380}
32 {'RADSENS_2': 2954}
34 {'RADSENS_2': 2959}
36 {'RADSENS_2': 2958}
38 {'RADSENS_2': 2965}
40 {'RADSENS_2': 2953}
42 {'RADSENS_2': 2964}
44 {'RADSENS_2': 2951}
46 {'RADSENS_2': 2955}
48 {'RADSENS_2': 2952}
50 {'RADSENS_2': 2947}
52 {'RADSENS_2': 2959}
54 {'RADSENS_2': 2951}
56 {'RADSENS_2': 2968}
58 {'RADSENS_2': 2951}
60 {'RADSENS_2': 2939}
62 {'RADSENS_2': 2959}
33 {'RADSENS_2': 3340}
35 {'RADSENS_2': 3335}
37 {'RADSENS_2': 3342}
39 {'RADSENS_2': 3344}
41 {'RADSENS_2': 3343}
43 {'RADSENS_2': 3336}
45 {'RADSENS_2': 3336}
47 {'RADSENS_2': 3337}
49 {'RADSENS_2': 3339}
51 {'RADSENS_2': 3340}
53 {'RADSENS_2': 3344}
55 {'RADSENS_2': 3343}
57 {'RADSENS_2': 3341}
59 {'RADSENS_2': 3335}
61 {'RADSENS_2': 3336}
63 {'RADSENS_2': 3331}
32 {'RADSENS_3': 2925}
34 {'RADSENS_3': 2927}
36 {'RADSENS_3': 2932}
38 {'RADSENS_3': 2931}
40 {'RADSENS_3': 2936}
42 {'RADSENS_3': 2940}
44 {'RADSENS_3': 2926}
46 {'RADSENS_3': 2944}
48 {'RADSENS_3': 2927}
50 {'RADSENS_3': 2915}
52 {'RADSENS_3': 2926}
54 {'RADSENS_3': 2926}
56 {'RADSENS_3': 2933}
58 {'RADSENS_3': 2923}
60 {'RADSENS_3': 2940}
62 {'RADSENS_3': 2924}
33 {'RADSENS_3': 3319}
35 {'RADSENS_3': 3326}
37 {'RADSENS_3': 3318}
39 {'RADSENS_3': 3321}
41 {'RADSENS_3': 3322}
43 {'RADSENS_3': 3323}
45 {'RADSENS_3': 3315}
47 {'RADSENS_3': 3322}
49 {'RADSENS_3': 3320}
51 {'RADSENS_3': 3324}
53 {'RADSENS_3': 3319}
55 {'RADSENS_3': 3318}
57 {'RADSENS_3': 3328}
59 {'RADSENS_3': 3327}
61 {'RADSENS_3': 3320}
63 {'RADSENS_3': 3319}
32 {'RADSENS_4': 2936}
34 {'RADSENS_4': 2927}
36 {'RADSENS_4': 2936}
38 {'RADSENS_4': 2934}
40 {'RADSENS_4': 2937}
42 {'RADSENS_4': 2934}
44 {'RADSENS_4': 2928}
46 {'RADSENS_4': 2937}
48 {'RADSENS_4': 2919}
50 {'RADSENS_4': 2920}
52 {'RADSENS_4': 2936}
54 {'RADSENS_4': 2926}
56 {'RADSENS_4': 2920}
58 {'RADSENS_4': 2921}
60 {'RADSENS_4': 2932}
62 {'RADSENS_4': 2928}
33 {'RADSENS_4': 3324}
35 {'RADSENS_4': 3321}
37 {'RADSENS_4': 3320}
39 {'RADSENS_4': 3320}
41 {'RADSENS_4': 3319}
43 {'RADSENS_4': 3324}
45 {'RADSENS_4': 3315}
47 {'RADSENS_4': 3324}
49 {'RADSENS_4': 3323}
51 {'RADSENS_4': 3328}
53 {'RADSENS_4': 3311}
55 {'RADSENS_4': 3319}
57 {'RADSENS_4': 3323}
59 {'RADSENS_4': 3336}
61 {'RADSENS_4': 3318}
63 {'RADSENS_4': 3321}
