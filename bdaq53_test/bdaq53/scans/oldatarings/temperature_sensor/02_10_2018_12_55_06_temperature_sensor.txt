Timestamp GlobalTime(s) Dose(Mrad)
02_10_2018_12_55_06 1538146232.06 200
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2158}
34 {'ADCbandgap': 2156}
36 {'ADCbandgap': 2158}
38 {'ADCbandgap': 2160}
40 {'ADCbandgap': 2156}
42 {'ADCbandgap': 2157}
44 {'ADCbandgap': 2156}
46 {'ADCbandgap': 2156}
48 {'ADCbandgap': 2158}
50 {'ADCbandgap': 2155}
52 {'ADCbandgap': 2161}
54 {'ADCbandgap': 2158}
56 {'ADCbandgap': 2157}
58 {'ADCbandgap': 2159}
60 {'ADCbandgap': 2156}
62 {'ADCbandgap': 2157}
33 {'ADCbandgap': 2155}
35 {'ADCbandgap': 2156}
37 {'ADCbandgap': 2158}
39 {'ADCbandgap': 2159}
41 {'ADCbandgap': 2160}
43 {'ADCbandgap': 2161}
45 {'ADCbandgap': 2156}
47 {'ADCbandgap': 2158}
49 {'ADCbandgap': 2160}
51 {'ADCbandgap': 2157}
53 {'ADCbandgap': 2157}
55 {'ADCbandgap': 2157}
57 {'ADCbandgap': 2158}
59 {'ADCbandgap': 2155}
61 {'ADCbandgap': 2157}
63 {'ADCbandgap': 2159}
32 {'TEMPSENS_1': 1275}
34 {'TEMPSENS_1': 1271}
36 {'TEMPSENS_1': 1263}
38 {'TEMPSENS_1': 1267}
40 {'TEMPSENS_1': 1264}
42 {'TEMPSENS_1': 1276}
44 {'TEMPSENS_1': 1267}
46 {'TEMPSENS_1': 1267}
48 {'TEMPSENS_1': 1277}
50 {'TEMPSENS_1': 1269}
52 {'TEMPSENS_1': 1276}
54 {'TEMPSENS_1': 1263}
56 {'TEMPSENS_1': 1271}
58 {'TEMPSENS_1': 1271}
60 {'TEMPSENS_1': 1271}
62 {'TEMPSENS_1': 1267}
33 {'TEMPSENS_1': 1620}
35 {'TEMPSENS_1': 1615}
37 {'TEMPSENS_1': 1614}
39 {'TEMPSENS_1': 1620}
41 {'TEMPSENS_1': 1617}
43 {'TEMPSENS_1': 1615}
45 {'TEMPSENS_1': 1611}
47 {'TEMPSENS_1': 1615}
49 {'TEMPSENS_1': 1616}
51 {'TEMPSENS_1': 1613}
53 {'TEMPSENS_1': 1616}
55 {'TEMPSENS_1': 1615}
57 {'TEMPSENS_1': 1609}
59 {'TEMPSENS_1': 1616}
61 {'TEMPSENS_1': 1614}
63 {'TEMPSENS_1': 1611}
32 {'TEMPSENS_2': 1258}
34 {'TEMPSENS_2': 1267}
36 {'TEMPSENS_2': 1263}
38 {'TEMPSENS_2': 1264}
40 {'TEMPSENS_2': 1270}
42 {'TEMPSENS_2': 1263}
44 {'TEMPSENS_2': 1266}
46 {'TEMPSENS_2': 1261}
48 {'TEMPSENS_2': 1266}
50 {'TEMPSENS_2': 1268}
52 {'TEMPSENS_2': 1265}
54 {'TEMPSENS_2': 1251}
56 {'TEMPSENS_2': 1270}
58 {'TEMPSENS_2': 1277}
60 {'TEMPSENS_2': 1271}
62 {'TEMPSENS_2': 1271}
33 {'TEMPSENS_2': 1615}
35 {'TEMPSENS_2': 1612}
37 {'TEMPSENS_2': 1612}
39 {'TEMPSENS_2': 1616}
41 {'TEMPSENS_2': 1616}
43 {'TEMPSENS_2': 1616}
45 {'TEMPSENS_2': 1616}
47 {'TEMPSENS_2': 1611}
49 {'TEMPSENS_2': 1612}
51 {'TEMPSENS_2': 1607}
53 {'TEMPSENS_2': 1612}
55 {'TEMPSENS_2': 1614}
57 {'TEMPSENS_2': 1611}
59 {'TEMPSENS_2': 1610}
61 {'TEMPSENS_2': 1611}
63 {'TEMPSENS_2': 1610}
32 {'TEMPSENS_3': 1264}
34 {'TEMPSENS_3': 1268}
36 {'TEMPSENS_3': 1266}
38 {'TEMPSENS_3': 1257}
40 {'TEMPSENS_3': 1270}
42 {'TEMPSENS_3': 1276}
44 {'TEMPSENS_3': 1269}
46 {'TEMPSENS_3': 1260}
48 {'TEMPSENS_3': 1268}
50 {'TEMPSENS_3': 1261}
52 {'TEMPSENS_3': 1271}
54 {'TEMPSENS_3': 1269}
56 {'TEMPSENS_3': 1275}
58 {'TEMPSENS_3': 1273}
60 {'TEMPSENS_3': 1270}
62 {'TEMPSENS_3': 1263}
33 {'TEMPSENS_3': 1616}
35 {'TEMPSENS_3': 1611}
37 {'TEMPSENS_3': 1617}
39 {'TEMPSENS_3': 1612}
41 {'TEMPSENS_3': 1614}
43 {'TEMPSENS_3': 1612}
45 {'TEMPSENS_3': 1614}
47 {'TEMPSENS_3': 1612}
49 {'TEMPSENS_3': 1616}
51 {'TEMPSENS_3': 1612}
53 {'TEMPSENS_3': 1609}
55 {'TEMPSENS_3': 1611}
57 {'TEMPSENS_3': 1613}
59 {'TEMPSENS_3': 1616}
61 {'TEMPSENS_3': 1612}
63 {'TEMPSENS_3': 1612}
32 {'TEMPSENS_4': 1255}
34 {'TEMPSENS_4': 1255}
36 {'TEMPSENS_4': 1255}
38 {'TEMPSENS_4': 1255}
40 {'TEMPSENS_4': 1259}
42 {'TEMPSENS_4': 1248}
44 {'TEMPSENS_4': 1248}
46 {'TEMPSENS_4': 1248}
48 {'TEMPSENS_4': 1255}
50 {'TEMPSENS_4': 1260}
52 {'TEMPSENS_4': 1250}
54 {'TEMPSENS_4': 1262}
56 {'TEMPSENS_4': 1267}
58 {'TEMPSENS_4': 1247}
60 {'TEMPSENS_4': 1256}
62 {'TEMPSENS_4': 1254}
33 {'TEMPSENS_4': 1601}
35 {'TEMPSENS_4': 1597}
37 {'TEMPSENS_4': 1598}
39 {'TEMPSENS_4': 1598}
41 {'TEMPSENS_4': 1599}
43 {'TEMPSENS_4': 1599}
45 {'TEMPSENS_4': 1602}
47 {'TEMPSENS_4': 1597}
49 {'TEMPSENS_4': 1599}
51 {'TEMPSENS_4': 1602}
53 {'TEMPSENS_4': 1595}
55 {'TEMPSENS_4': 1602}
57 {'TEMPSENS_4': 1595}
59 {'TEMPSENS_4': 1600}
61 {'TEMPSENS_4': 1597}
63 {'TEMPSENS_4': 1600}
32 {'RADSENS_1': 3011}
34 {'RADSENS_1': 3024}
36 {'RADSENS_1': 3024}
38 {'RADSENS_1': 3022}
40 {'RADSENS_1': 3005}
42 {'RADSENS_1': 3007}
44 {'RADSENS_1': 3020}
46 {'RADSENS_1': 3024}
48 {'RADSENS_1': 3029}
50 {'RADSENS_1': 3011}
52 {'RADSENS_1': 3014}
54 {'RADSENS_1': 3011}
56 {'RADSENS_1': 3022}
58 {'RADSENS_1': 3020}
60 {'RADSENS_1': 3018}
62 {'RADSENS_1': 3014}
33 {'RADSENS_1': 3351}
35 {'RADSENS_1': 3358}
37 {'RADSENS_1': 3347}
39 {'RADSENS_1': 3356}
41 {'RADSENS_1': 3350}
43 {'RADSENS_1': 3358}
45 {'RADSENS_1': 3351}
47 {'RADSENS_1': 3343}
49 {'RADSENS_1': 3352}
51 {'RADSENS_1': 3360}
53 {'RADSENS_1': 3355}
55 {'RADSENS_1': 3352}
57 {'RADSENS_1': 3345}
59 {'RADSENS_1': 3352}
61 {'RADSENS_1': 3349}
63 {'RADSENS_1': 3355}
32 {'RADSENS_2': 2939}
34 {'RADSENS_2': 2937}
36 {'RADSENS_2': 2936}
38 {'RADSENS_2': 2946}
40 {'RADSENS_2': 2923}
42 {'RADSENS_2': 2931}
44 {'RADSENS_2': 2926}
46 {'RADSENS_2': 2928}
48 {'RADSENS_2': 2930}
50 {'RADSENS_2': 2928}
52 {'RADSENS_2': 2940}
54 {'RADSENS_2': 2926}
56 {'RADSENS_2': 2939}
58 {'RADSENS_2': 2936}
60 {'RADSENS_2': 2921}
62 {'RADSENS_2': 2930}
33 {'RADSENS_2': 3319}
35 {'RADSENS_2': 3328}
37 {'RADSENS_2': 3328}
39 {'RADSENS_2': 3311}
41 {'RADSENS_2': 3319}
43 {'RADSENS_2': 3315}
45 {'RADSENS_2': 3319}
47 {'RADSENS_2': 3324}
49 {'RADSENS_2': 3319}
51 {'RADSENS_2': 3329}
53 {'RADSENS_2': 3322}
55 {'RADSENS_2': 3311}
57 {'RADSENS_2': 3311}
59 {'RADSENS_2': 3324}
61 {'RADSENS_2': 3320}
63 {'RADSENS_2': 3323}
32 {'RADSENS_3': 2904}
34 {'RADSENS_3': 2900}
36 {'RADSENS_3': 2926}
38 {'RADSENS_3': 2917}
40 {'RADSENS_3': 2917}
42 {'RADSENS_3': 2915}
44 {'RADSENS_3': 2903}
46 {'RADSENS_3': 2907}
48 {'RADSENS_3': 2908}
50 {'RADSENS_3': 2907}
52 {'RADSENS_3': 2911}
54 {'RADSENS_3': 2911}
56 {'RADSENS_3': 2912}
58 {'RADSENS_3': 2910}
60 {'RADSENS_3': 2913}
62 {'RADSENS_3': 2902}
33 {'RADSENS_3': 3305}
35 {'RADSENS_3': 3308}
37 {'RADSENS_3': 3312}
39 {'RADSENS_3': 3307}
41 {'RADSENS_3': 3295}
43 {'RADSENS_3': 3302}
45 {'RADSENS_3': 3298}
47 {'RADSENS_3': 3299}
49 {'RADSENS_3': 3304}
51 {'RADSENS_3': 3304}
53 {'RADSENS_3': 3303}
55 {'RADSENS_3': 3306}
57 {'RADSENS_3': 3310}
59 {'RADSENS_3': 3306}
61 {'RADSENS_3': 3302}
63 {'RADSENS_3': 3303}
32 {'RADSENS_4': 2908}
34 {'RADSENS_4': 2920}
36 {'RADSENS_4': 2911}
38 {'RADSENS_4': 2903}
40 {'RADSENS_4': 2915}
42 {'RADSENS_4': 2915}
44 {'RADSENS_4': 2911}
46 {'RADSENS_4': 2920}
48 {'RADSENS_4': 2896}
50 {'RADSENS_4': 2896}
52 {'RADSENS_4': 2916}
54 {'RADSENS_4': 2908}
56 {'RADSENS_4': 2909}
58 {'RADSENS_4': 2895}
60 {'RADSENS_4': 2906}
62 {'RADSENS_4': 2895}
33 {'RADSENS_4': 3302}
35 {'RADSENS_4': 3302}
37 {'RADSENS_4': 3302}
39 {'RADSENS_4': 3304}
41 {'RADSENS_4': 3300}
43 {'RADSENS_4': 3297}
45 {'RADSENS_4': 3308}
47 {'RADSENS_4': 3304}
49 {'RADSENS_4': 3301}
51 {'RADSENS_4': 3302}
53 {'RADSENS_4': 3312}
55 {'RADSENS_4': 3302}
57 {'RADSENS_4': 3299}
59 {'RADSENS_4': 3302}
61 {'RADSENS_4': 3300}
63 {'RADSENS_4': 3297}
