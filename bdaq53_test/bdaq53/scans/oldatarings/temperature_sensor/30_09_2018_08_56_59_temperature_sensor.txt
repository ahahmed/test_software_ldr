Timestamp GlobalTime(s) Dose(Mrad)
30_09_2018_08_56_59 1538146232.06 40
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2160}
34 {'ADCbandgap': 2156}
36 {'ADCbandgap': 2155}
38 {'ADCbandgap': 2154}
40 {'ADCbandgap': 2156}
42 {'ADCbandgap': 2156}
44 {'ADCbandgap': 2153}
46 {'ADCbandgap': 2160}
48 {'ADCbandgap': 2155}
50 {'ADCbandgap': 2155}
52 {'ADCbandgap': 2154}
54 {'ADCbandgap': 2156}
56 {'ADCbandgap': 2151}
58 {'ADCbandgap': 2154}
60 {'ADCbandgap': 2156}
62 {'ADCbandgap': 2153}
33 {'ADCbandgap': 2151}
35 {'ADCbandgap': 2156}
37 {'ADCbandgap': 2157}
39 {'ADCbandgap': 2160}
41 {'ADCbandgap': 2151}
43 {'ADCbandgap': 2151}
45 {'ADCbandgap': 2154}
47 {'ADCbandgap': 2156}
49 {'ADCbandgap': 2154}
51 {'ADCbandgap': 2156}
53 {'ADCbandgap': 2151}
55 {'ADCbandgap': 2158}
57 {'ADCbandgap': 2156}
59 {'ADCbandgap': 2156}
61 {'ADCbandgap': 2154}
63 {'ADCbandgap': 2155}
32 {'TEMPSENS_1': 1284}
34 {'TEMPSENS_1': 1286}
36 {'TEMPSENS_1': 1284}
38 {'TEMPSENS_1': 1280}
40 {'TEMPSENS_1': 1280}
42 {'TEMPSENS_1': 1287}
44 {'TEMPSENS_1': 1284}
46 {'TEMPSENS_1': 1278}
48 {'TEMPSENS_1': 1290}
50 {'TEMPSENS_1': 1291}
52 {'TEMPSENS_1': 1290}
54 {'TEMPSENS_1': 1281}
56 {'TEMPSENS_1': 1283}
58 {'TEMPSENS_1': 1286}
60 {'TEMPSENS_1': 1281}
62 {'TEMPSENS_1': 1279}
33 {'TEMPSENS_1': 1623}
35 {'TEMPSENS_1': 1627}
37 {'TEMPSENS_1': 1631}
39 {'TEMPSENS_1': 1627}
41 {'TEMPSENS_1': 1628}
43 {'TEMPSENS_1': 1627}
45 {'TEMPSENS_1': 1629}
47 {'TEMPSENS_1': 1632}
49 {'TEMPSENS_1': 1627}
51 {'TEMPSENS_1': 1628}
53 {'TEMPSENS_1': 1632}
55 {'TEMPSENS_1': 1628}
57 {'TEMPSENS_1': 1629}
59 {'TEMPSENS_1': 1629}
61 {'TEMPSENS_1': 1627}
63 {'TEMPSENS_1': 1630}
32 {'TEMPSENS_2': 1263}
34 {'TEMPSENS_2': 1278}
36 {'TEMPSENS_2': 1269}
38 {'TEMPSENS_2': 1272}
40 {'TEMPSENS_2': 1277}
42 {'TEMPSENS_2': 1269}
44 {'TEMPSENS_2': 1270}
46 {'TEMPSENS_2': 1272}
48 {'TEMPSENS_2': 1275}
50 {'TEMPSENS_2': 1277}
52 {'TEMPSENS_2': 1276}
54 {'TEMPSENS_2': 1268}
56 {'TEMPSENS_2': 1275}
58 {'TEMPSENS_2': 1283}
60 {'TEMPSENS_2': 1280}
62 {'TEMPSENS_2': 1277}
33 {'TEMPSENS_2': 1615}
35 {'TEMPSENS_2': 1618}
37 {'TEMPSENS_2': 1619}
39 {'TEMPSENS_2': 1619}
41 {'TEMPSENS_2': 1620}
43 {'TEMPSENS_2': 1619}
45 {'TEMPSENS_2': 1622}
47 {'TEMPSENS_2': 1622}
49 {'TEMPSENS_2': 1615}
51 {'TEMPSENS_2': 1621}
53 {'TEMPSENS_2': 1616}
55 {'TEMPSENS_2': 1620}
57 {'TEMPSENS_2': 1617}
59 {'TEMPSENS_2': 1617}
61 {'TEMPSENS_2': 1619}
63 {'TEMPSENS_2': 1615}
32 {'TEMPSENS_3': 1269}
34 {'TEMPSENS_3': 1279}
36 {'TEMPSENS_3': 1272}
38 {'TEMPSENS_3': 1263}
40 {'TEMPSENS_3': 1270}
42 {'TEMPSENS_3': 1275}
44 {'TEMPSENS_3': 1273}
46 {'TEMPSENS_3': 1272}
48 {'TEMPSENS_3': 1275}
50 {'TEMPSENS_3': 1264}
52 {'TEMPSENS_3': 1279}
54 {'TEMPSENS_3': 1275}
56 {'TEMPSENS_3': 1280}
58 {'TEMPSENS_3': 1273}
60 {'TEMPSENS_3': 1276}
62 {'TEMPSENS_3': 1273}
33 {'TEMPSENS_3': 1620}
35 {'TEMPSENS_3': 1614}
37 {'TEMPSENS_3': 1620}
39 {'TEMPSENS_3': 1619}
41 {'TEMPSENS_3': 1621}
43 {'TEMPSENS_3': 1615}
45 {'TEMPSENS_3': 1620}
47 {'TEMPSENS_3': 1619}
49 {'TEMPSENS_3': 1615}
51 {'TEMPSENS_3': 1619}
53 {'TEMPSENS_3': 1617}
55 {'TEMPSENS_3': 1619}
57 {'TEMPSENS_3': 1619}
59 {'TEMPSENS_3': 1622}
61 {'TEMPSENS_3': 1616}
63 {'TEMPSENS_3': 1616}
32 {'TEMPSENS_4': 1269}
34 {'TEMPSENS_4': 1265}
36 {'TEMPSENS_4': 1265}
38 {'TEMPSENS_4': 1267}
40 {'TEMPSENS_4': 1271}
42 {'TEMPSENS_4': 1259}
44 {'TEMPSENS_4': 1260}
46 {'TEMPSENS_4': 1263}
48 {'TEMPSENS_4': 1269}
50 {'TEMPSENS_4': 1267}
52 {'TEMPSENS_4': 1262}
54 {'TEMPSENS_4': 1270}
56 {'TEMPSENS_4': 1267}
58 {'TEMPSENS_4': 1262}
60 {'TEMPSENS_4': 1264}
62 {'TEMPSENS_4': 1265}
33 {'TEMPSENS_4': 1612}
35 {'TEMPSENS_4': 1614}
37 {'TEMPSENS_4': 1614}
39 {'TEMPSENS_4': 1613}
41 {'TEMPSENS_4': 1610}
43 {'TEMPSENS_4': 1614}
45 {'TEMPSENS_4': 1612}
47 {'TEMPSENS_4': 1611}
49 {'TEMPSENS_4': 1612}
51 {'TEMPSENS_4': 1607}
53 {'TEMPSENS_4': 1609}
55 {'TEMPSENS_4': 1610}
57 {'TEMPSENS_4': 1612}
59 {'TEMPSENS_4': 1607}
61 {'TEMPSENS_4': 1608}
63 {'TEMPSENS_4': 1612}
32 {'RADSENS_1': 3095}
34 {'RADSENS_1': 3095}
36 {'RADSENS_1': 3120}
38 {'RADSENS_1': 3110}
40 {'RADSENS_1': 3097}
42 {'RADSENS_1': 3104}
44 {'RADSENS_1': 3104}
46 {'RADSENS_1': 3117}
48 {'RADSENS_1': 3113}
50 {'RADSENS_1': 3098}
52 {'RADSENS_1': 3095}
54 {'RADSENS_1': 3110}
56 {'RADSENS_1': 3112}
58 {'RADSENS_1': 3108}
60 {'RADSENS_1': 3109}
62 {'RADSENS_1': 3108}
33 {'RADSENS_1': 3409}
35 {'RADSENS_1': 3406}
37 {'RADSENS_1': 3399}
39 {'RADSENS_1': 3406}
41 {'RADSENS_1': 3407}
43 {'RADSENS_1': 3399}
45 {'RADSENS_1': 3406}
47 {'RADSENS_1': 3402}
49 {'RADSENS_1': 3397}
51 {'RADSENS_1': 3399}
53 {'RADSENS_1': 3402}
55 {'RADSENS_1': 3399}
57 {'RADSENS_1': 3409}
59 {'RADSENS_1': 3405}
61 {'RADSENS_1': 3409}
63 {'RADSENS_1': 3391}
32 {'RADSENS_2': 3005}
34 {'RADSENS_2': 3004}
36 {'RADSENS_2': 2991}
38 {'RADSENS_2': 3008}
40 {'RADSENS_2': 3001}
42 {'RADSENS_2': 3009}
44 {'RADSENS_2': 2999}
46 {'RADSENS_2': 3000}
48 {'RADSENS_2': 2997}
50 {'RADSENS_2': 3001}
52 {'RADSENS_2': 3004}
54 {'RADSENS_2': 2992}
56 {'RADSENS_2': 3017}
58 {'RADSENS_2': 3010}
60 {'RADSENS_2': 2996}
62 {'RADSENS_2': 3008}
33 {'RADSENS_2': 3370}
35 {'RADSENS_2': 3363}
37 {'RADSENS_2': 3370}
39 {'RADSENS_2': 3365}
41 {'RADSENS_2': 3365}
43 {'RADSENS_2': 3363}
45 {'RADSENS_2': 3376}
47 {'RADSENS_2': 3368}
49 {'RADSENS_2': 3371}
51 {'RADSENS_2': 3364}
53 {'RADSENS_2': 3363}
55 {'RADSENS_2': 3368}
57 {'RADSENS_2': 3370}
59 {'RADSENS_2': 3366}
61 {'RADSENS_2': 3372}
63 {'RADSENS_2': 3376}
32 {'RADSENS_3': 2970}
34 {'RADSENS_3': 2976}
36 {'RADSENS_3': 2971}
38 {'RADSENS_3': 2969}
40 {'RADSENS_3': 2967}
42 {'RADSENS_3': 2972}
44 {'RADSENS_3': 2962}
46 {'RADSENS_3': 2959}
48 {'RADSENS_3': 2968}
50 {'RADSENS_3': 2968}
52 {'RADSENS_3': 2978}
54 {'RADSENS_3': 2958}
56 {'RADSENS_3': 2958}
58 {'RADSENS_3': 2963}
60 {'RADSENS_3': 2984}
62 {'RADSENS_3': 2977}
33 {'RADSENS_3': 3347}
35 {'RADSENS_3': 3352}
37 {'RADSENS_3': 3351}
39 {'RADSENS_3': 3350}
41 {'RADSENS_3': 3350}
43 {'RADSENS_3': 3344}
45 {'RADSENS_3': 3343}
47 {'RADSENS_3': 3353}
49 {'RADSENS_3': 3343}
51 {'RADSENS_3': 3348}
53 {'RADSENS_3': 3352}
55 {'RADSENS_3': 3348}
57 {'RADSENS_3': 3353}
59 {'RADSENS_3': 3348}
61 {'RADSENS_3': 3352}
63 {'RADSENS_3': 3345}
32 {'RADSENS_4': 2963}
34 {'RADSENS_4': 2972}
36 {'RADSENS_4': 2974}
38 {'RADSENS_4': 2963}
40 {'RADSENS_4': 2965}
42 {'RADSENS_4': 2963}
44 {'RADSENS_4': 2976}
46 {'RADSENS_4': 2971}
48 {'RADSENS_4': 2961}
50 {'RADSENS_4': 2966}
52 {'RADSENS_4': 2977}
54 {'RADSENS_4': 2959}
56 {'RADSENS_4': 2963}
58 {'RADSENS_4': 2959}
60 {'RADSENS_4': 2973}
62 {'RADSENS_4': 2968}
33 {'RADSENS_4': 3352}
35 {'RADSENS_4': 3347}
37 {'RADSENS_4': 3345}
39 {'RADSENS_4': 3349}
41 {'RADSENS_4': 3354}
43 {'RADSENS_4': 3350}
45 {'RADSENS_4': 3351}
47 {'RADSENS_4': 3354}
49 {'RADSENS_4': 3343}
51 {'RADSENS_4': 3347}
53 {'RADSENS_4': 3350}
55 {'RADSENS_4': 3356}
57 {'RADSENS_4': 3353}
59 {'RADSENS_4': 3352}
61 {'RADSENS_4': 3346}
63 {'RADSENS_4': 3343}
