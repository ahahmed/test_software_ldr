Timestamp GlobalTime(s) Dose(Mrad)
01_10_2018_23_59_42 1538146232.06 150
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2153}
34 {'ADCbandgap': 2157}
36 {'ADCbandgap': 2154}
38 {'ADCbandgap': 2160}
40 {'ADCbandgap': 2156}
42 {'ADCbandgap': 2156}
44 {'ADCbandgap': 2156}
46 {'ADCbandgap': 2158}
48 {'ADCbandgap': 2157}
50 {'ADCbandgap': 2160}
52 {'ADCbandgap': 2158}
54 {'ADCbandgap': 2157}
56 {'ADCbandgap': 2157}
58 {'ADCbandgap': 2151}
60 {'ADCbandgap': 2155}
62 {'ADCbandgap': 2151}
33 {'ADCbandgap': 2156}
35 {'ADCbandgap': 2156}
37 {'ADCbandgap': 2157}
39 {'ADCbandgap': 2151}
41 {'ADCbandgap': 2156}
43 {'ADCbandgap': 2156}
45 {'ADCbandgap': 2158}
47 {'ADCbandgap': 2158}
49 {'ADCbandgap': 2154}
51 {'ADCbandgap': 2160}
53 {'ADCbandgap': 2160}
55 {'ADCbandgap': 2156}
57 {'ADCbandgap': 2156}
59 {'ADCbandgap': 2156}
61 {'ADCbandgap': 2154}
63 {'ADCbandgap': 2155}
32 {'TEMPSENS_1': 1279}
34 {'TEMPSENS_1': 1275}
36 {'TEMPSENS_1': 1267}
38 {'TEMPSENS_1': 1276}
40 {'TEMPSENS_1': 1272}
42 {'TEMPSENS_1': 1276}
44 {'TEMPSENS_1': 1274}
46 {'TEMPSENS_1': 1272}
48 {'TEMPSENS_1': 1279}
50 {'TEMPSENS_1': 1280}
52 {'TEMPSENS_1': 1277}
54 {'TEMPSENS_1': 1273}
56 {'TEMPSENS_1': 1278}
58 {'TEMPSENS_1': 1271}
60 {'TEMPSENS_1': 1280}
62 {'TEMPSENS_1': 1271}
33 {'TEMPSENS_1': 1617}
35 {'TEMPSENS_1': 1620}
37 {'TEMPSENS_1': 1617}
39 {'TEMPSENS_1': 1620}
41 {'TEMPSENS_1': 1619}
43 {'TEMPSENS_1': 1622}
45 {'TEMPSENS_1': 1624}
47 {'TEMPSENS_1': 1624}
49 {'TEMPSENS_1': 1619}
51 {'TEMPSENS_1': 1616}
53 {'TEMPSENS_1': 1620}
55 {'TEMPSENS_1': 1620}
57 {'TEMPSENS_1': 1617}
59 {'TEMPSENS_1': 1628}
61 {'TEMPSENS_1': 1622}
63 {'TEMPSENS_1': 1624}
32 {'TEMPSENS_2': 1268}
34 {'TEMPSENS_2': 1273}
36 {'TEMPSENS_2': 1268}
38 {'TEMPSENS_2': 1272}
40 {'TEMPSENS_2': 1265}
42 {'TEMPSENS_2': 1259}
44 {'TEMPSENS_2': 1269}
46 {'TEMPSENS_2': 1264}
48 {'TEMPSENS_2': 1268}
50 {'TEMPSENS_2': 1271}
52 {'TEMPSENS_2': 1273}
54 {'TEMPSENS_2': 1260}
56 {'TEMPSENS_2': 1272}
58 {'TEMPSENS_2': 1275}
60 {'TEMPSENS_2': 1280}
62 {'TEMPSENS_2': 1269}
33 {'TEMPSENS_2': 1616}
35 {'TEMPSENS_2': 1614}
37 {'TEMPSENS_2': 1616}
39 {'TEMPSENS_2': 1614}
41 {'TEMPSENS_2': 1616}
43 {'TEMPSENS_2': 1616}
45 {'TEMPSENS_2': 1615}
47 {'TEMPSENS_2': 1611}
49 {'TEMPSENS_2': 1618}
51 {'TEMPSENS_2': 1616}
53 {'TEMPSENS_2': 1615}
55 {'TEMPSENS_2': 1614}
57 {'TEMPSENS_2': 1616}
59 {'TEMPSENS_2': 1614}
61 {'TEMPSENS_2': 1615}
63 {'TEMPSENS_2': 1620}
32 {'TEMPSENS_3': 1265}
34 {'TEMPSENS_3': 1271}
36 {'TEMPSENS_3': 1267}
38 {'TEMPSENS_3': 1268}
40 {'TEMPSENS_3': 1263}
42 {'TEMPSENS_3': 1274}
44 {'TEMPSENS_3': 1268}
46 {'TEMPSENS_3': 1263}
48 {'TEMPSENS_3': 1272}
50 {'TEMPSENS_3': 1263}
52 {'TEMPSENS_3': 1275}
54 {'TEMPSENS_3': 1272}
56 {'TEMPSENS_3': 1275}
58 {'TEMPSENS_3': 1274}
60 {'TEMPSENS_3': 1271}
62 {'TEMPSENS_3': 1272}
33 {'TEMPSENS_3': 1614}
35 {'TEMPSENS_3': 1613}
37 {'TEMPSENS_3': 1615}
39 {'TEMPSENS_3': 1613}
41 {'TEMPSENS_3': 1615}
43 {'TEMPSENS_3': 1614}
45 {'TEMPSENS_3': 1620}
47 {'TEMPSENS_3': 1615}
49 {'TEMPSENS_3': 1615}
51 {'TEMPSENS_3': 1617}
53 {'TEMPSENS_3': 1618}
55 {'TEMPSENS_3': 1613}
57 {'TEMPSENS_3': 1613}
59 {'TEMPSENS_3': 1617}
61 {'TEMPSENS_3': 1614}
63 {'TEMPSENS_3': 1613}
32 {'TEMPSENS_4': 1260}
34 {'TEMPSENS_4': 1261}
36 {'TEMPSENS_4': 1255}
38 {'TEMPSENS_4': 1261}
40 {'TEMPSENS_4': 1266}
42 {'TEMPSENS_4': 1249}
44 {'TEMPSENS_4': 1257}
46 {'TEMPSENS_4': 1262}
48 {'TEMPSENS_4': 1255}
50 {'TEMPSENS_4': 1263}
52 {'TEMPSENS_4': 1257}
54 {'TEMPSENS_4': 1261}
56 {'TEMPSENS_4': 1271}
58 {'TEMPSENS_4': 1256}
60 {'TEMPSENS_4': 1255}
62 {'TEMPSENS_4': 1264}
33 {'TEMPSENS_4': 1608}
35 {'TEMPSENS_4': 1606}
37 {'TEMPSENS_4': 1603}
39 {'TEMPSENS_4': 1604}
41 {'TEMPSENS_4': 1604}
43 {'TEMPSENS_4': 1604}
45 {'TEMPSENS_4': 1606}
47 {'TEMPSENS_4': 1604}
49 {'TEMPSENS_4': 1604}
51 {'TEMPSENS_4': 1604}
53 {'TEMPSENS_4': 1606}
55 {'TEMPSENS_4': 1599}
57 {'TEMPSENS_4': 1603}
59 {'TEMPSENS_4': 1602}
61 {'TEMPSENS_4': 1604}
63 {'TEMPSENS_4': 1605}
32 {'RADSENS_1': 3048}
34 {'RADSENS_1': 3039}
36 {'RADSENS_1': 3055}
38 {'RADSENS_1': 3037}
40 {'RADSENS_1': 3033}
42 {'RADSENS_1': 3043}
44 {'RADSENS_1': 3043}
46 {'RADSENS_1': 3056}
48 {'RADSENS_1': 3035}
50 {'RADSENS_1': 3024}
52 {'RADSENS_1': 3034}
54 {'RADSENS_1': 3044}
56 {'RADSENS_1': 3035}
58 {'RADSENS_1': 3039}
60 {'RADSENS_1': 3040}
62 {'RADSENS_1': 3023}
33 {'RADSENS_1': 3366}
35 {'RADSENS_1': 3367}
37 {'RADSENS_1': 3372}
39 {'RADSENS_1': 3368}
41 {'RADSENS_1': 3366}
43 {'RADSENS_1': 3367}
45 {'RADSENS_1': 3378}
47 {'RADSENS_1': 3370}
49 {'RADSENS_1': 3374}
51 {'RADSENS_1': 3376}
53 {'RADSENS_1': 3363}
55 {'RADSENS_1': 3369}
57 {'RADSENS_1': 3368}
59 {'RADSENS_1': 3364}
61 {'RADSENS_1': 3367}
63 {'RADSENS_1': 3373}
32 {'RADSENS_2': 2942}
34 {'RADSENS_2': 2955}
36 {'RADSENS_2': 2935}
38 {'RADSENS_2': 2943}
40 {'RADSENS_2': 2937}
42 {'RADSENS_2': 2960}
44 {'RADSENS_2': 2935}
46 {'RADSENS_2': 2944}
48 {'RADSENS_2': 2940}
50 {'RADSENS_2': 2940}
52 {'RADSENS_2': 2951}
54 {'RADSENS_2': 2932}
56 {'RADSENS_2': 2947}
58 {'RADSENS_2': 2946}
60 {'RADSENS_2': 2940}
62 {'RADSENS_2': 2952}
33 {'RADSENS_2': 3329}
35 {'RADSENS_2': 3329}
37 {'RADSENS_2': 3334}
39 {'RADSENS_2': 3336}
41 {'RADSENS_2': 3326}
43 {'RADSENS_2': 3336}
45 {'RADSENS_2': 3327}
47 {'RADSENS_2': 3336}
49 {'RADSENS_2': 3335}
51 {'RADSENS_2': 3336}
53 {'RADSENS_2': 3323}
55 {'RADSENS_2': 3332}
57 {'RADSENS_2': 3327}
59 {'RADSENS_2': 3328}
61 {'RADSENS_2': 3327}
63 {'RADSENS_2': 3327}
32 {'RADSENS_3': 2912}
34 {'RADSENS_3': 2915}
36 {'RADSENS_3': 2933}
38 {'RADSENS_3': 2927}
40 {'RADSENS_3': 2930}
42 {'RADSENS_3': 2928}
44 {'RADSENS_3': 2913}
46 {'RADSENS_3': 2919}
48 {'RADSENS_3': 2919}
50 {'RADSENS_3': 2913}
52 {'RADSENS_3': 2918}
54 {'RADSENS_3': 2915}
56 {'RADSENS_3': 2920}
58 {'RADSENS_3': 2914}
60 {'RADSENS_3': 2940}
62 {'RADSENS_3': 2920}
33 {'RADSENS_3': 3316}
35 {'RADSENS_3': 3320}
37 {'RADSENS_3': 3311}
39 {'RADSENS_3': 3321}
41 {'RADSENS_3': 3315}
43 {'RADSENS_3': 3313}
45 {'RADSENS_3': 3315}
47 {'RADSENS_3': 3321}
49 {'RADSENS_3': 3320}
51 {'RADSENS_3': 3315}
53 {'RADSENS_3': 3317}
55 {'RADSENS_3': 3311}
57 {'RADSENS_3': 3320}
59 {'RADSENS_3': 3311}
61 {'RADSENS_3': 3316}
63 {'RADSENS_3': 3317}
32 {'RADSENS_4': 2928}
34 {'RADSENS_4': 2929}
36 {'RADSENS_4': 2928}
38 {'RADSENS_4': 2924}
40 {'RADSENS_4': 2924}
42 {'RADSENS_4': 2911}
44 {'RADSENS_4': 2924}
46 {'RADSENS_4': 2926}
48 {'RADSENS_4': 2912}
50 {'RADSENS_4': 2910}
52 {'RADSENS_4': 2927}
54 {'RADSENS_4': 2909}
56 {'RADSENS_4': 2919}
58 {'RADSENS_4': 2907}
60 {'RADSENS_4': 2919}
62 {'RADSENS_4': 2915}
33 {'RADSENS_4': 3311}
35 {'RADSENS_4': 3315}
37 {'RADSENS_4': 3314}
39 {'RADSENS_4': 3320}
41 {'RADSENS_4': 3314}
43 {'RADSENS_4': 3315}
45 {'RADSENS_4': 3315}
47 {'RADSENS_4': 3313}
49 {'RADSENS_4': 3316}
51 {'RADSENS_4': 3319}
53 {'RADSENS_4': 3311}
55 {'RADSENS_4': 3311}
57 {'RADSENS_4': 3317}
59 {'RADSENS_4': 3320}
61 {'RADSENS_4': 3318}
63 {'RADSENS_4': 3315}
