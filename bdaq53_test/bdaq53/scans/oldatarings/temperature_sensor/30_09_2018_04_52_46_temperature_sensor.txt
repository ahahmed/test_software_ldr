Timestamp GlobalTime(s) Dose(Mrad)
30_09_2018_04_52_46 1538146232.06 30
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2156}
34 {'ADCbandgap': 2155}
36 {'ADCbandgap': 2156}
38 {'ADCbandgap': 2151}
40 {'ADCbandgap': 2153}
42 {'ADCbandgap': 2155}
44 {'ADCbandgap': 2160}
46 {'ADCbandgap': 2157}
48 {'ADCbandgap': 2151}
50 {'ADCbandgap': 2160}
52 {'ADCbandgap': 2156}
54 {'ADCbandgap': 2155}
56 {'ADCbandgap': 2151}
58 {'ADCbandgap': 2155}
60 {'ADCbandgap': 2154}
62 {'ADCbandgap': 2153}
33 {'ADCbandgap': 2155}
35 {'ADCbandgap': 2156}
37 {'ADCbandgap': 2151}
39 {'ADCbandgap': 2156}
41 {'ADCbandgap': 2155}
43 {'ADCbandgap': 2153}
45 {'ADCbandgap': 2156}
47 {'ADCbandgap': 2155}
49 {'ADCbandgap': 2156}
51 {'ADCbandgap': 2158}
53 {'ADCbandgap': 2157}
55 {'ADCbandgap': 2155}
57 {'ADCbandgap': 2156}
59 {'ADCbandgap': 2156}
61 {'ADCbandgap': 2155}
63 {'ADCbandgap': 2154}
32 {'TEMPSENS_1': 1294}
34 {'TEMPSENS_1': 1288}
36 {'TEMPSENS_1': 1280}
38 {'TEMPSENS_1': 1283}
40 {'TEMPSENS_1': 1275}
42 {'TEMPSENS_1': 1291}
44 {'TEMPSENS_1': 1283}
46 {'TEMPSENS_1': 1284}
48 {'TEMPSENS_1': 1295}
50 {'TEMPSENS_1': 1288}
52 {'TEMPSENS_1': 1292}
54 {'TEMPSENS_1': 1286}
56 {'TEMPSENS_1': 1286}
58 {'TEMPSENS_1': 1283}
60 {'TEMPSENS_1': 1289}
62 {'TEMPSENS_1': 1280}
33 {'TEMPSENS_1': 1630}
35 {'TEMPSENS_1': 1631}
37 {'TEMPSENS_1': 1630}
39 {'TEMPSENS_1': 1633}
41 {'TEMPSENS_1': 1631}
43 {'TEMPSENS_1': 1636}
45 {'TEMPSENS_1': 1632}
47 {'TEMPSENS_1': 1631}
49 {'TEMPSENS_1': 1629}
51 {'TEMPSENS_1': 1629}
53 {'TEMPSENS_1': 1631}
55 {'TEMPSENS_1': 1635}
57 {'TEMPSENS_1': 1634}
59 {'TEMPSENS_1': 1627}
61 {'TEMPSENS_1': 1629}
63 {'TEMPSENS_1': 1629}
32 {'TEMPSENS_2': 1271}
34 {'TEMPSENS_2': 1278}
36 {'TEMPSENS_2': 1276}
38 {'TEMPSENS_2': 1275}
40 {'TEMPSENS_2': 1274}
42 {'TEMPSENS_2': 1267}
44 {'TEMPSENS_2': 1281}
46 {'TEMPSENS_2': 1267}
48 {'TEMPSENS_2': 1277}
50 {'TEMPSENS_2': 1281}
52 {'TEMPSENS_2': 1278}
54 {'TEMPSENS_2': 1270}
56 {'TEMPSENS_2': 1271}
58 {'TEMPSENS_2': 1287}
60 {'TEMPSENS_2': 1284}
62 {'TEMPSENS_2': 1278}
33 {'TEMPSENS_2': 1624}
35 {'TEMPSENS_2': 1621}
37 {'TEMPSENS_2': 1622}
39 {'TEMPSENS_2': 1622}
41 {'TEMPSENS_2': 1623}
43 {'TEMPSENS_2': 1622}
45 {'TEMPSENS_2': 1621}
47 {'TEMPSENS_2': 1624}
49 {'TEMPSENS_2': 1623}
51 {'TEMPSENS_2': 1622}
53 {'TEMPSENS_2': 1619}
55 {'TEMPSENS_2': 1623}
57 {'TEMPSENS_2': 1617}
59 {'TEMPSENS_2': 1621}
61 {'TEMPSENS_2': 1620}
63 {'TEMPSENS_2': 1624}
32 {'TEMPSENS_3': 1276}
34 {'TEMPSENS_3': 1284}
36 {'TEMPSENS_3': 1273}
38 {'TEMPSENS_3': 1267}
40 {'TEMPSENS_3': 1272}
42 {'TEMPSENS_3': 1267}
44 {'TEMPSENS_3': 1276}
46 {'TEMPSENS_3': 1270}
48 {'TEMPSENS_3': 1273}
50 {'TEMPSENS_3': 1271}
52 {'TEMPSENS_3': 1278}
54 {'TEMPSENS_3': 1274}
56 {'TEMPSENS_3': 1282}
58 {'TEMPSENS_3': 1278}
60 {'TEMPSENS_3': 1275}
62 {'TEMPSENS_3': 1279}
33 {'TEMPSENS_3': 1620}
35 {'TEMPSENS_3': 1623}
37 {'TEMPSENS_3': 1617}
39 {'TEMPSENS_3': 1624}
41 {'TEMPSENS_3': 1622}
43 {'TEMPSENS_3': 1620}
45 {'TEMPSENS_3': 1619}
47 {'TEMPSENS_3': 1618}
49 {'TEMPSENS_3': 1622}
51 {'TEMPSENS_3': 1624}
53 {'TEMPSENS_3': 1619}
55 {'TEMPSENS_3': 1623}
57 {'TEMPSENS_3': 1624}
59 {'TEMPSENS_3': 1622}
61 {'TEMPSENS_3': 1618}
63 {'TEMPSENS_3': 1618}
32 {'TEMPSENS_4': 1267}
34 {'TEMPSENS_4': 1271}
36 {'TEMPSENS_4': 1266}
38 {'TEMPSENS_4': 1268}
40 {'TEMPSENS_4': 1270}
42 {'TEMPSENS_4': 1267}
44 {'TEMPSENS_4': 1260}
46 {'TEMPSENS_4': 1272}
48 {'TEMPSENS_4': 1266}
50 {'TEMPSENS_4': 1267}
52 {'TEMPSENS_4': 1265}
54 {'TEMPSENS_4': 1271}
56 {'TEMPSENS_4': 1272}
58 {'TEMPSENS_4': 1262}
60 {'TEMPSENS_4': 1267}
62 {'TEMPSENS_4': 1263}
33 {'TEMPSENS_4': 1616}
35 {'TEMPSENS_4': 1612}
37 {'TEMPSENS_4': 1614}
39 {'TEMPSENS_4': 1612}
41 {'TEMPSENS_4': 1615}
43 {'TEMPSENS_4': 1614}
45 {'TEMPSENS_4': 1611}
47 {'TEMPSENS_4': 1611}
49 {'TEMPSENS_4': 1615}
51 {'TEMPSENS_4': 1611}
53 {'TEMPSENS_4': 1613}
55 {'TEMPSENS_4': 1607}
57 {'TEMPSENS_4': 1616}
59 {'TEMPSENS_4': 1613}
61 {'TEMPSENS_4': 1607}
63 {'TEMPSENS_4': 1613}
32 {'RADSENS_1': 3111}
34 {'RADSENS_1': 3112}
36 {'RADSENS_1': 3119}
38 {'RADSENS_1': 3115}
40 {'RADSENS_1': 3110}
42 {'RADSENS_1': 3108}
44 {'RADSENS_1': 3118}
46 {'RADSENS_1': 3121}
48 {'RADSENS_1': 3119}
50 {'RADSENS_1': 3105}
52 {'RADSENS_1': 3103}
54 {'RADSENS_1': 3111}
56 {'RADSENS_1': 3124}
58 {'RADSENS_1': 3107}
60 {'RADSENS_1': 3117}
62 {'RADSENS_1': 3115}
33 {'RADSENS_1': 3416}
35 {'RADSENS_1': 3406}
37 {'RADSENS_1': 3409}
39 {'RADSENS_1': 3410}
41 {'RADSENS_1': 3407}
43 {'RADSENS_1': 3409}
45 {'RADSENS_1': 3409}
47 {'RADSENS_1': 3407}
49 {'RADSENS_1': 3399}
51 {'RADSENS_1': 3416}
53 {'RADSENS_1': 3407}
55 {'RADSENS_1': 3416}
57 {'RADSENS_1': 3420}
59 {'RADSENS_1': 3406}
61 {'RADSENS_1': 3407}
63 {'RADSENS_1': 3407}
32 {'RADSENS_2': 3017}
34 {'RADSENS_2': 3024}
36 {'RADSENS_2': 3012}
38 {'RADSENS_2': 3015}
40 {'RADSENS_2': 3016}
42 {'RADSENS_2': 3024}
44 {'RADSENS_2': 3010}
46 {'RADSENS_2': 3024}
48 {'RADSENS_2': 3007}
50 {'RADSENS_2': 3010}
52 {'RADSENS_2': 3020}
54 {'RADSENS_2': 2999}
56 {'RADSENS_2': 3027}
58 {'RADSENS_2': 3011}
60 {'RADSENS_2': 3011}
62 {'RADSENS_2': 3025}
33 {'RADSENS_2': 3380}
35 {'RADSENS_2': 3374}
37 {'RADSENS_2': 3374}
39 {'RADSENS_2': 3368}
41 {'RADSENS_2': 3373}
43 {'RADSENS_2': 3373}
45 {'RADSENS_2': 3380}
47 {'RADSENS_2': 3371}
49 {'RADSENS_2': 3376}
51 {'RADSENS_2': 3367}
53 {'RADSENS_2': 3375}
55 {'RADSENS_2': 3374}
57 {'RADSENS_2': 3376}
59 {'RADSENS_2': 3377}
61 {'RADSENS_2': 3371}
63 {'RADSENS_2': 3376}
32 {'RADSENS_3': 2976}
34 {'RADSENS_3': 2968}
36 {'RADSENS_3': 2975}
38 {'RADSENS_3': 2988}
40 {'RADSENS_3': 2971}
42 {'RADSENS_3': 2971}
44 {'RADSENS_3': 2971}
46 {'RADSENS_3': 2972}
48 {'RADSENS_3': 2966}
50 {'RADSENS_3': 2976}
52 {'RADSENS_3': 2969}
54 {'RADSENS_3': 2968}
56 {'RADSENS_3': 2974}
58 {'RADSENS_3': 2963}
60 {'RADSENS_3': 2975}
62 {'RADSENS_3': 2983}
33 {'RADSENS_3': 3352}
35 {'RADSENS_3': 3352}
37 {'RADSENS_3': 3354}
39 {'RADSENS_3': 3351}
41 {'RADSENS_3': 3354}
43 {'RADSENS_3': 3355}
45 {'RADSENS_3': 3357}
47 {'RADSENS_3': 3355}
49 {'RADSENS_3': 3353}
51 {'RADSENS_3': 3353}
53 {'RADSENS_3': 3355}
55 {'RADSENS_3': 3354}
57 {'RADSENS_3': 3356}
59 {'RADSENS_3': 3366}
61 {'RADSENS_3': 3356}
63 {'RADSENS_3': 3346}
32 {'RADSENS_4': 2984}
34 {'RADSENS_4': 2969}
36 {'RADSENS_4': 2980}
38 {'RADSENS_4': 2980}
40 {'RADSENS_4': 2974}
42 {'RADSENS_4': 2973}
44 {'RADSENS_4': 2985}
46 {'RADSENS_4': 2984}
48 {'RADSENS_4': 2976}
50 {'RADSENS_4': 2970}
52 {'RADSENS_4': 2984}
54 {'RADSENS_4': 2976}
56 {'RADSENS_4': 2976}
58 {'RADSENS_4': 2976}
60 {'RADSENS_4': 2976}
62 {'RADSENS_4': 2977}
33 {'RADSENS_4': 3350}
35 {'RADSENS_4': 3351}
37 {'RADSENS_4': 3356}
39 {'RADSENS_4': 3356}
41 {'RADSENS_4': 3360}
43 {'RADSENS_4': 3361}
45 {'RADSENS_4': 3354}
47 {'RADSENS_4': 3357}
49 {'RADSENS_4': 3359}
51 {'RADSENS_4': 3355}
53 {'RADSENS_4': 3351}
55 {'RADSENS_4': 3355}
57 {'RADSENS_4': 3350}
59 {'RADSENS_4': 3360}
61 {'RADSENS_4': 3356}
63 {'RADSENS_4': 3366}
