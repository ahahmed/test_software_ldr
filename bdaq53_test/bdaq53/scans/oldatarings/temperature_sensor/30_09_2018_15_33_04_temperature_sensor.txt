Timestamp GlobalTime(s) Dose(Mrad)
30_09_2018_15_33_04 1538146232.06 50
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'ADCbandgap': 2156}
34 {'ADCbandgap': 2153}
36 {'ADCbandgap': 2151}
38 {'ADCbandgap': 2155}
40 {'ADCbandgap': 2154}
42 {'ADCbandgap': 2160}
44 {'ADCbandgap': 2156}
46 {'ADCbandgap': 2154}
48 {'ADCbandgap': 2151}
50 {'ADCbandgap': 2157}
52 {'ADCbandgap': 2155}
54 {'ADCbandgap': 2154}
56 {'ADCbandgap': 2158}
58 {'ADCbandgap': 2156}
60 {'ADCbandgap': 2156}
62 {'ADCbandgap': 2151}
33 {'ADCbandgap': 2153}
35 {'ADCbandgap': 2155}
37 {'ADCbandgap': 2153}
39 {'ADCbandgap': 2158}
41 {'ADCbandgap': 2151}
43 {'ADCbandgap': 2157}
45 {'ADCbandgap': 2154}
47 {'ADCbandgap': 2155}
49 {'ADCbandgap': 2155}
51 {'ADCbandgap': 2153}
53 {'ADCbandgap': 2151}
55 {'ADCbandgap': 2151}
57 {'ADCbandgap': 2157}
59 {'ADCbandgap': 2155}
61 {'ADCbandgap': 2160}
63 {'ADCbandgap': 2151}
32 {'TEMPSENS_1': 1286}
34 {'TEMPSENS_1': 1284}
36 {'TEMPSENS_1': 1272}
38 {'TEMPSENS_1': 1280}
40 {'TEMPSENS_1': 1279}
42 {'TEMPSENS_1': 1284}
44 {'TEMPSENS_1': 1273}
46 {'TEMPSENS_1': 1271}
48 {'TEMPSENS_1': 1288}
50 {'TEMPSENS_1': 1280}
52 {'TEMPSENS_1': 1283}
54 {'TEMPSENS_1': 1279}
56 {'TEMPSENS_1': 1282}
58 {'TEMPSENS_1': 1280}
60 {'TEMPSENS_1': 1279}
62 {'TEMPSENS_1': 1277}
33 {'TEMPSENS_1': 1627}
35 {'TEMPSENS_1': 1628}
37 {'TEMPSENS_1': 1626}
39 {'TEMPSENS_1': 1623}
41 {'TEMPSENS_1': 1628}
43 {'TEMPSENS_1': 1625}
45 {'TEMPSENS_1': 1626}
47 {'TEMPSENS_1': 1625}
49 {'TEMPSENS_1': 1623}
51 {'TEMPSENS_1': 1626}
53 {'TEMPSENS_1': 1623}
55 {'TEMPSENS_1': 1629}
57 {'TEMPSENS_1': 1625}
59 {'TEMPSENS_1': 1626}
61 {'TEMPSENS_1': 1628}
63 {'TEMPSENS_1': 1623}
32 {'TEMPSENS_2': 1266}
34 {'TEMPSENS_2': 1271}
36 {'TEMPSENS_2': 1272}
38 {'TEMPSENS_2': 1271}
40 {'TEMPSENS_2': 1268}
42 {'TEMPSENS_2': 1260}
44 {'TEMPSENS_2': 1272}
46 {'TEMPSENS_2': 1267}
48 {'TEMPSENS_2': 1263}
50 {'TEMPSENS_2': 1269}
52 {'TEMPSENS_2': 1276}
54 {'TEMPSENS_2': 1262}
56 {'TEMPSENS_2': 1274}
58 {'TEMPSENS_2': 1280}
60 {'TEMPSENS_2': 1281}
62 {'TEMPSENS_2': 1271}
33 {'TEMPSENS_2': 1615}
35 {'TEMPSENS_2': 1615}
37 {'TEMPSENS_2': 1617}
39 {'TEMPSENS_2': 1615}
41 {'TEMPSENS_2': 1616}
43 {'TEMPSENS_2': 1612}
45 {'TEMPSENS_2': 1615}
47 {'TEMPSENS_2': 1618}
49 {'TEMPSENS_2': 1618}
51 {'TEMPSENS_2': 1615}
53 {'TEMPSENS_2': 1620}
55 {'TEMPSENS_2': 1615}
57 {'TEMPSENS_2': 1615}
59 {'TEMPSENS_2': 1614}
61 {'TEMPSENS_2': 1613}
63 {'TEMPSENS_2': 1615}
32 {'TEMPSENS_3': 1272}
34 {'TEMPSENS_3': 1274}
36 {'TEMPSENS_3': 1268}
38 {'TEMPSENS_3': 1269}
40 {'TEMPSENS_3': 1270}
42 {'TEMPSENS_3': 1267}
44 {'TEMPSENS_3': 1268}
46 {'TEMPSENS_3': 1270}
48 {'TEMPSENS_3': 1276}
50 {'TEMPSENS_3': 1257}
52 {'TEMPSENS_3': 1276}
54 {'TEMPSENS_3': 1271}
56 {'TEMPSENS_3': 1275}
58 {'TEMPSENS_3': 1271}
60 {'TEMPSENS_3': 1274}
62 {'TEMPSENS_3': 1269}
33 {'TEMPSENS_3': 1616}
35 {'TEMPSENS_3': 1618}
37 {'TEMPSENS_3': 1617}
39 {'TEMPSENS_3': 1617}
41 {'TEMPSENS_3': 1616}
43 {'TEMPSENS_3': 1616}
45 {'TEMPSENS_3': 1620}
47 {'TEMPSENS_3': 1618}
49 {'TEMPSENS_3': 1615}
51 {'TEMPSENS_3': 1624}
53 {'TEMPSENS_3': 1616}
55 {'TEMPSENS_3': 1618}
57 {'TEMPSENS_3': 1617}
59 {'TEMPSENS_3': 1616}
61 {'TEMPSENS_3': 1617}
63 {'TEMPSENS_3': 1613}
32 {'TEMPSENS_4': 1262}
34 {'TEMPSENS_4': 1260}
36 {'TEMPSENS_4': 1259}
38 {'TEMPSENS_4': 1268}
40 {'TEMPSENS_4': 1268}
42 {'TEMPSENS_4': 1255}
44 {'TEMPSENS_4': 1260}
46 {'TEMPSENS_4': 1263}
48 {'TEMPSENS_4': 1268}
50 {'TEMPSENS_4': 1266}
52 {'TEMPSENS_4': 1259}
54 {'TEMPSENS_4': 1267}
56 {'TEMPSENS_4': 1272}
58 {'TEMPSENS_4': 1260}
60 {'TEMPSENS_4': 1263}
62 {'TEMPSENS_4': 1264}
33 {'TEMPSENS_4': 1607}
35 {'TEMPSENS_4': 1609}
37 {'TEMPSENS_4': 1610}
39 {'TEMPSENS_4': 1607}
41 {'TEMPSENS_4': 1608}
43 {'TEMPSENS_4': 1608}
45 {'TEMPSENS_4': 1611}
47 {'TEMPSENS_4': 1612}
49 {'TEMPSENS_4': 1616}
51 {'TEMPSENS_4': 1607}
53 {'TEMPSENS_4': 1609}
55 {'TEMPSENS_4': 1612}
57 {'TEMPSENS_4': 1609}
59 {'TEMPSENS_4': 1611}
61 {'TEMPSENS_4': 1608}
63 {'TEMPSENS_4': 1610}
32 {'RADSENS_1': 3078}
34 {'RADSENS_1': 3079}
36 {'RADSENS_1': 3089}
38 {'RADSENS_1': 3096}
40 {'RADSENS_1': 3071}
42 {'RADSENS_1': 3084}
44 {'RADSENS_1': 3088}
46 {'RADSENS_1': 3094}
48 {'RADSENS_1': 3083}
50 {'RADSENS_1': 3071}
52 {'RADSENS_1': 3090}
54 {'RADSENS_1': 3079}
56 {'RADSENS_1': 3087}
58 {'RADSENS_1': 3079}
60 {'RADSENS_1': 3075}
62 {'RADSENS_1': 3086}
33 {'RADSENS_1': 3396}
35 {'RADSENS_1': 3388}
37 {'RADSENS_1': 3388}
39 {'RADSENS_1': 3400}
41 {'RADSENS_1': 3390}
43 {'RADSENS_1': 3391}
45 {'RADSENS_1': 3387}
47 {'RADSENS_1': 3390}
49 {'RADSENS_1': 3393}
51 {'RADSENS_1': 3386}
53 {'RADSENS_1': 3396}
55 {'RADSENS_1': 3396}
57 {'RADSENS_1': 3386}
59 {'RADSENS_1': 3388}
61 {'RADSENS_1': 3393}
63 {'RADSENS_1': 3389}
32 {'RADSENS_2': 2976}
34 {'RADSENS_2': 2984}
36 {'RADSENS_2': 2975}
38 {'RADSENS_2': 2983}
40 {'RADSENS_2': 2975}
42 {'RADSENS_2': 2982}
44 {'RADSENS_2': 2976}
46 {'RADSENS_2': 2975}
48 {'RADSENS_2': 2979}
50 {'RADSENS_2': 2968}
52 {'RADSENS_2': 2975}
54 {'RADSENS_2': 2963}
56 {'RADSENS_2': 2987}
58 {'RADSENS_2': 2974}
60 {'RADSENS_2': 2976}
62 {'RADSENS_2': 2986}
33 {'RADSENS_2': 3346}
35 {'RADSENS_2': 3352}
37 {'RADSENS_2': 3347}
39 {'RADSENS_2': 3350}
41 {'RADSENS_2': 3352}
43 {'RADSENS_2': 3347}
45 {'RADSENS_2': 3347}
47 {'RADSENS_2': 3343}
49 {'RADSENS_2': 3343}
51 {'RADSENS_2': 3360}
53 {'RADSENS_2': 3349}
55 {'RADSENS_2': 3352}
57 {'RADSENS_2': 3351}
59 {'RADSENS_2': 3352}
61 {'RADSENS_2': 3345}
63 {'RADSENS_2': 3349}
32 {'RADSENS_3': 2948}
34 {'RADSENS_3': 2945}
36 {'RADSENS_3': 2950}
38 {'RADSENS_3': 2946}
40 {'RADSENS_3': 2954}
42 {'RADSENS_3': 2945}
44 {'RADSENS_3': 2941}
46 {'RADSENS_3': 2944}
48 {'RADSENS_3': 2935}
50 {'RADSENS_3': 2939}
52 {'RADSENS_3': 2934}
54 {'RADSENS_3': 2944}
56 {'RADSENS_3': 2944}
58 {'RADSENS_3': 2941}
60 {'RADSENS_3': 2953}
62 {'RADSENS_3': 2945}
33 {'RADSENS_3': 3327}
35 {'RADSENS_3': 3336}
37 {'RADSENS_3': 3336}
39 {'RADSENS_3': 3334}
41 {'RADSENS_3': 3332}
43 {'RADSENS_3': 3333}
45 {'RADSENS_3': 3323}
47 {'RADSENS_3': 3336}
49 {'RADSENS_3': 3327}
51 {'RADSENS_3': 3327}
53 {'RADSENS_3': 3334}
55 {'RADSENS_3': 3331}
57 {'RADSENS_3': 3328}
59 {'RADSENS_3': 3334}
61 {'RADSENS_3': 3333}
63 {'RADSENS_3': 3344}
32 {'RADSENS_4': 2940}
34 {'RADSENS_4': 2952}
36 {'RADSENS_4': 2953}
38 {'RADSENS_4': 2945}
40 {'RADSENS_4': 2942}
42 {'RADSENS_4': 2945}
44 {'RADSENS_4': 2953}
46 {'RADSENS_4': 2950}
48 {'RADSENS_4': 2931}
50 {'RADSENS_4': 2937}
52 {'RADSENS_4': 2943}
54 {'RADSENS_4': 2937}
56 {'RADSENS_4': 2937}
58 {'RADSENS_4': 2937}
60 {'RADSENS_4': 2944}
62 {'RADSENS_4': 2938}
33 {'RADSENS_4': 3327}
35 {'RADSENS_4': 3337}
37 {'RADSENS_4': 3331}
39 {'RADSENS_4': 3336}
41 {'RADSENS_4': 3332}
43 {'RADSENS_4': 3336}
45 {'RADSENS_4': 3331}
47 {'RADSENS_4': 3327}
49 {'RADSENS_4': 3334}
51 {'RADSENS_4': 3334}
53 {'RADSENS_4': 3329}
55 {'RADSENS_4': 3344}
57 {'RADSENS_4': 3335}
59 {'RADSENS_4': 3325}
61 {'RADSENS_4': 3335}
63 {'RADSENS_4': 3327}
