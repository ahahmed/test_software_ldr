import time
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import os
import glob
import datetime
#from mon1 import monitor_datalogger_power_supply
from matplotlib.pyplot import figure
import basil
import visa
from operator import itemgetter
import ast
import sys, termios, tty, os, time
import csv
import tables as tb
from bitarray import bitarray
from datetime import datetime
from bdaq53.fifo_readout import FifoReadout
import logging
import matplotlib.pyplot as plt



def get_fe_data():
	directory_plots = 'plotsFE'
	dir_textfile_sync = 'sync.txt'
	dir_textfile_lin = 'lin.txt'
	dir_textfile_diff = 'diff.txt'
	sync = open(dir_textfile_sync, 'r')
	lin = open(dir_textfile_lin, 'r')
	diff = open(dir_textfile_diff, 'r')
	
	#sync:
	vth_mean_400ns = []
	vth_disp_400ns = []
	noise_mean_400ns = []
	noise_disp_400ns = []

	vth_mean_1600ns = []
	vth_disp_1600ns = []
	noise_mean_1600ns = []
	noise_disp_1600ns = []

	vth_mean_3200ns = []
	vth_disp_3200ns = []
	noise_mean_3200ns = []
	noise_disp_3200ns = []

	dose = []
	dose1 = []
	dose2 = []
	
	lines = sync.readlines()
	for i,x in enumerate(lines):
		if i > 1:
			dose.append((float(x.split('	')[0])))
			
			test = 	float(x.split('	')[1])
			if test>0:
				vth_mean_400ns.append((float(x.split('	')[1])))
				dose1.append((float(x.split('	')[0])))
				vth_disp_400ns.append((float(x.split('	')[2])))
			noise_mean_400ns.append((float(x.split('	')[3])))
			noise_disp_400ns.append((float(x.split('	')[4])))

			vth_mean_1600ns.append((float(x.split('	')[5])))
			vth_disp_1600ns.append((float(x.split('	')[6])))

			test = 	float(x.split('	')[7])
			if test>0:
				noise_mean_1600ns.append((float(x.split('	')[7])))
				noise_disp_1600ns.append((float(x.split('	')[8])))
				dose2.append((float(x.split('	')[0])))
			vth_mean_3200ns.append((float(x.split('	')[9])))
			vth_disp_3200ns.append((float(x.split('	')[10])))
			noise_mean_3200ns.append((float(x.split('	')[11])))
			noise_disp_3200ns.append((float(x.split('	')[12])))


	line_color = ['#ffcc00','#ff6633','#cc33ff', '#3333ff', '#0099ff', '#33cc66', '#99cc33','#11cc55']
	legend = ['AZ pulse duration = 400ns','AZ pulse duration = 1.6us','AZ pulse duration = 3.2us']

	figure(figsize=(10, 8))
	lables_size = 20	
	vth_disp_400ns_err = [x / 2 for x in vth_disp_400ns]
	vth_disp_1600ns_err = [x / 2 for x in vth_disp_1600ns]
	vth_disp_3200ns_err = [x / 2 for x in vth_disp_3200ns]

	plt.errorbar(dose1,vth_mean_400ns, yerr = vth_disp_400ns_err,  fmt='o',  label = legend[0], color = line_color[0])
	plt.errorbar(dose,vth_mean_1600ns, yerr = vth_disp_1600ns_err,  fmt='o',label = legend[1], color = line_color[1])
	plt.errorbar(dose,vth_mean_3200ns, yerr = vth_disp_3200ns_err, fmt='o', label = legend[2], color = line_color[2])
	


	lgnd = plt.legend( loc=3,prop={'size': 13})
	#legend(loc=7 , prop={'size': 13})


	#for handle in lgnd.legendHandles:
    		#3handle.set_sizes([9.0])
		
	plt.title('Sync FE, mean threshold and dispersion', fontsize=lables_size)
	plt.xlabel('Dose (Mrad)', fontsize=20)
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0.8)
	axes.set_xscale('log')
	
	plt.ylabel('Mean Vth (e-)', fontsize=20)
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")	
	
	plt.savefig(directory_plots+'/'+timestamp+'_sync_Vth_dose.png', dpi=1000)



	figure(figsize=(10, 8))
	lables_size = 20	
	vth_disp_400ns_err = [x / 2 for x in vth_disp_400ns]
	vth_disp_1600ns_err = [x / 2 for x in vth_disp_1600ns]
	vth_disp_3200ns_err = [x / 2 for x in vth_disp_3200ns]

	plt.scatter(dose1, vth_disp_400ns,    label = legend[0], color = line_color[0])
	plt.scatter(dose,vth_disp_1600ns,  label = legend[1], color = line_color[1])
	plt.scatter(dose,vth_disp_3200ns,  label = legend[2], color = line_color[2])
	


	lgnd = plt.legend( loc=3,prop={'size': 13})
	#legend(loc=7 , prop={'size': 13})


	#for handle in lgnd.legendHandles:
    		#3handle.set_sizes([9.0])
		
	plt.title('Sync FE,  threshold dispersion', fontsize=lables_size)
	plt.xlabel('Dose (Mrad)', fontsize=20)
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0.8)
	axes.set_xscale('log')
	
	plt.ylabel('Vth dispersion (e-)', fontsize=20)
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")	
	
	plt.savefig(directory_plots+'/'+timestamp+'_sync_Vth_dispersion_dose.png', dpi=1000)

	



	figure(figsize=(10, 8))
	lables_size = 20	
	noise_disp_400ns_err = [x / 2 for x in noise_disp_400ns]
	noise_disp_1600ns_err = [x / 2 for x in noise_disp_1600ns]
	noise_disp_3200ns_err = [x / 2 for x in noise_disp_3200ns]

	plt.subplot(3,1,1)
	plt.title('Sync FE, mean noise and dispersion', fontsize=lables_size)
	plt.errorbar(dose,noise_mean_400ns, yerr = noise_disp_400ns_err,  fmt='o',  label = legend[0], color = line_color[0])
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0.8)
	axes.set_xscale('log')	
	plt.ylabel('Mean noise (e-)', fontsize=10)
	lgnd = plt.legend( loc=3,prop={'size': 13})





	plt.subplot(3,1,2)
	plt.errorbar(dose2,noise_mean_1600ns, yerr = noise_disp_1600ns_err,  fmt='o',label = legend[1], color = line_color[1])
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0.8)
	axes.set_xscale('log')	
	plt.ylabel('Mean noise (e-)', fontsize=10)
	lgnd = plt.legend( loc=3,prop={'size': 13})


	plt.subplot(3,1,3)
	plt.errorbar(dose,noise_mean_3200ns, yerr = noise_disp_3200ns_err, fmt='o', label = legend[2], color = line_color[2])
	plt.xlabel('Dose (Mrad)', fontsize=20)
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0.8)
	axes.set_xscale('log')	
	plt.ylabel('Mean noise (e-)', fontsize=10)

	lgnd = plt.legend( loc=3,prop={'size': 13})

	
	
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0.8)
	axes.set_xscale('log')	
	plt.ylabel('Mean noise (e-)', fontsize=10)



	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")	
	
	plt.savefig(directory_plots+'/'+timestamp+'_sync_noise_dose.png', dpi=1000)

	'''
	#lin:
	vth_mean_400ns = []
	vth_disp_400ns = []
	noise_mean_400ns = []
	noise_disp_400ns = []

	vth_mean_1600ns = []
	vth_disp_1600ns = []
	noise_mean_1600ns = []
	noise_disp_1600ns = []

	vth_mean_3200ns = []
	vth_disp_3200ns = []
	noise_mean_3200ns = []
	noise_disp_3200ns = []

	dose = []
	
	lines = sync.readlines()
	for i,x in enumerate(lines):
		if i > 1:
			dose.append((float(x.split('	')[0])))
							
			.append((float(x.split('	')[1])))
			.append((float(x.split('	')[2])))
			.append((float(x.split('	')[3])))
			.append((float(x.split('	')[4])))

			.append((float(x.split('	')[5])))
			.append((float(x.split('	')[6])))
			.append((float(x.split('	')[7])))
			.append((float(x.split('	')[8])))

			.append((float(x.split('')[9])))
			.append((float(x.split('')[10])))
			.append((float(x.split('	')[11])))
			.append((float(x.split('	')[12])))		

		
	sync.close()

	dose,dose_after_charact,id_measurement_after_charact = adjust_doses(t, dose_ref, doserate_Mrad)
		

	ring = [r0,r1,r2,r3,r4,r5,r6,r7]
	return ring, t, temperature, idcount,dose
	'''









def plot_ro(ring, t, temperature,directory_ringoscillators,directory_plots,idcount,dose):
	line_color = ['#ffcc00','#ff6633','#ff0066','#cc33ff', '#3333ff', '#0099ff', '#33cc66', '#99cc33','#11cc55']
	legend = ['110-CKND0','111-CKND4','112-INV0','113-INV4','114-NAND0','115-NAND4','116-NOR0','117-NOR4']

	figure(figsize=(18, 8))

	lables_size = 20
	#####################################
	
	
	#Frequencies, all rings
	for i in range(len(ring)):
			plt.scatter(dose,ring[i], label = legend[i],s=0.5, color = line_color[i])	
	lgnd = plt.legend(bbox_to_anchor=(1.005, 1), loc=2,prop={'size': 13}, borderaxespad=0.)#legend(loc=7 , prop={'size': 13})


	for handle in lgnd.legendHandles:
    		handle.set_sizes([9.0])
		
	plt.title('Ring Oscillators Frequency', fontsize=lables_size)
	plt.xlabel('Dose (Mrad)', fontsize=20)
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0)
	plt.ylabel('Frequency (Hz)', fontsize=20)
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")	
	#plt.show()
	plt.savefig(directory_plots+'/'+timestamp+'_ringOscillators_freq_dose.png', dpi=1000)
	##############################################3

	#################################################
	#plotting relative decrease of frequencies in %
	figure(figsize=(15, 8))
	ring_percentage_matrix = []	
	for i in range(len(ring)):
		ring_percentage_input = []
		for r in ring[i]:
			ring_percentage_input.append(r*100/ring[i][0])
		#print ring_percentage_input
		#raw_input('lolo')
 		ring_percentage_matrix.append(ring_percentage_input)		
		
	for i in range(len(ring_percentage_matrix)):
			plt.scatter(dose,ring_percentage_matrix[i], label = legend[i],s=0.5, color = line_color[i])	
	lgnd = plt.legend(loc=1  , prop={'size': 15} )	
	for handle in lgnd.legendHandles:
    		handle.set_sizes([9.0])
	plt.title('Ring Oscillators Frequency in % respect pre-irrad', fontsize=lables_size)
	plt.xlabel('Dose (Mrad)', fontsize=lables_size)
	plt.ylabel('Frequency respect before irradiation (%)', fontsize=lables_size)	
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0)
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")	
	plt.savefig(directory_plots+'/'+timestamp+'_ringOscillators_freq_percentage_decrease_dose.png', dpi=1000)

	figure(figsize=(15, 8))
	ring_percentage_matrix = []	
	for i in range(len(ring)):
		ring_percentage_input = []
		for r in ring[i]:
			ring_percentage_input.append(r*100/ring[i][0])
		#print ring_percentage_input
		#raw_input('lolo')
 		ring_percentage_matrix.append(ring_percentage_input)		
		
	for i in range(len(ring_percentage_matrix)):
			plt.scatter(dose,ring_percentage_matrix[i], label = legend[i],s=0.5, color = line_color[i])	
	lgnd = plt.legend(loc=1  , prop={'size': 15} )	
	for handle in lgnd.legendHandles:
    		handle.set_sizes([9.0])
	plt.title('Ring Oscillators Frequency in % respect pre-irrad', fontsize=lables_size)
	plt.xlabel('Dose (Mrad)', fontsize=lables_size)
	plt.ylabel('Frequency respect before irradiation (%)', fontsize=lables_size)	
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0, right = 500)
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")	
	plt.savefig(directory_plots+'/'+timestamp+'_ringOscillators_freq_percentage_decrease_dose_justTo500.png', dpi=1000)
	#########################################################3


	#Plotting only rings with gates with driving strength 1
	figure(figsize=(8, 8))
	for i in range(len(ring)):
			if ((i==0) or (i==2) or (i==4) or (i==6)):
				plt.scatter(dose,ring[i], label = legend[i],s=0.5, color = line_color[i])	
	lgnd = plt.legend(loc=1, prop={'size': 15})
	for handle in lgnd.legendHandles:
    		handle.set_sizes([9.0])	
	plt.title('Ring Oscillators frequency', fontsize=lables_size)
	plt.xlabel('Dose(Mrad)', fontsize=lables_size)
	plt.ylabel('Frequency (Hz)', fontsize=lables_size)	
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0)
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")	
	plt.savefig(directory_plots+'/'+timestamp+'_ringOscillators_freq_dose_drivingstrenght0.png', dpi=1000)

	#######################################################
	#Plotting only rings with gates with driving strength 4
	figure(figsize=(8, 8))
	for i in range(len(ring)):
			if ((i==1) or (i==3) or (i==5) or (i==7)):
				plt.scatter(dose,ring[i], label = legend[i],s=0.5, color = line_color[i])	
	lgnd = plt.legend(loc=1, prop={'size': 15})
	for handle in lgnd.legendHandles:
    		handle.set_sizes([9.0])	
	plt.title('Ring Oscillators frequency', fontsize=lables_size)
	plt.xlabel('Dose (Mrad)', fontsize=lables_size)
	plt.ylabel('Frequency (Hz)', fontsize=lables_size)	
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0)
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")	
	plt.savefig(directory_plots+'/'+timestamp+'_ringOscillators_freq_dose_drivingstrenght4.png', dpi=1000)
	#######################################################

	#Plotting temperature
	figure(figsize=(8, 4))
	plt.scatter(dose, temperature, label = 'Temperature', s=0.5,color = line_color[8])	
	plt.legend(loc=1)	
	plt.title('Temperature', fontsize=lables_size)
	plt.xlabel('Dose (Mrad)', fontsize=lables_size)
	plt.grid(True)

	lgnd = plt.legend(loc=1  , prop={'size': 15} )	
	for handle in lgnd.legendHandles:
    		handle.set_sizes([9.0])

	axes = plt.gca()
	axes.set_xlim(left=0)
	plt.ylabel('T(C)', fontsize=lables_size)
	plt.savefig(directory_plots+'/'+timestamp+'_ringOscillators_Temp_dose.png')

	#Plotting time stamps

	figure(figsize=(15, 8))
	plt.scatter(dose,t, label = 'time', s=0.5,color = line_color[8])	
	plt.legend(loc=1)	
	plt.title('Time', fontsize=lables_size)
	plt.ylabel('time (s)', fontsize=lables_size)
	plt.xlabel('idmeas', fontsize=lables_size)
	axes = plt.gca()
	axes.set_xlim(left=0)
	plt.grid(True)
	plt.savefig(directory_plots+'/'+timestamp+'_ringOscillators_time.png')
	

if __name__ == "__main__":
	get_fe_data()
 





