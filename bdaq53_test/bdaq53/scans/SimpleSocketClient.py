'''
Created on 14 September 2018

@author: Dima Maneuski
'''

import socket
import time

socket_ = socket.socket()
host = socket.gethostname()
port = 9091
delay = 0.1

server_address = (host, port)
socket_.connect(server_address)

time.sleep(delay)
out =  socket_.sendall(time.asctime() + " :: CLIENT :: Hello Server");
time.sleep(delay)
print socket_.recv(1024)
time.sleep(delay)

## Here we do some stuff....

out = socket_.sendall(time.asctime() + " :: CLIENT :: I did my job");
time.sleep(delay)
print socket_.recv(1024)
time.sleep(delay)

out = socket_.sendall("CMD::SHUTDOWN");

socket_.close # Close the socket when done
