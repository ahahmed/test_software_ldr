import time
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import os
import glob
import datetime
from bdaq53.fifo_readout import FifoReadout
import logging
import sys, select




def ChipCharact_lin():

	directory = 'Chip_Characterization'
	################# LOADING CONFIG FILE FOR CHARACT ##################################
	
	os.system("python scan_noise_occupancy_lin.py")
	os.system("python scan_threshold_lin.py")
	os.system("python scan_analog_lin.py")	
	#os.system("python meta_tune_local_threshold_lin.py")
	os.system("python calibrate_tot_lin.py")
	os.system("mv output_data "+ directory + '/LIN') 
	os.system("python scan_threshold_lin.py")
	os.system("mv output_data "+ directory + '/LIN_notunedthscan') 

	#os.system("python ChipFullCharacterizationSync.py 0 0")

	'''

	os.system('cp 2.yaml default_chip.yaml')
	os.system("python scan_noise_occupancy_lin.py")
	os.system("python scan_threshold_lin.py")
	os.system("mv output_data "+ directory + '/LIN_th')

	os.system('cp 1.yaml default_chip.yaml')
	os.system("python scan_noise_occupancy_diff.py")
	os.system("python meta_tune_local_threshold_diff_ini.py")
	os.system('cp 2.yaml default_chip.yaml')
	os.system("python scan_noise_occupancy_diff.py")
	os.system("python meta_tune_local_threshold_diff.py")
	os.system('cp 3.yaml default_chip.yaml')
	os.system("python scan_noise_occupancy_diff.py")
	os.system("python meta_tune_local_threshold_diff.py")
	os.system("python calibrate_tot_lin.py")
	os.system("mv output_data "+ directory + '/DIFF') 

	os.system('cp 3.yaml default_chip.yaml')
	os.system("python scan_noise_occupancy_diff.py")
	os.system("python scan_threshold_diff.py")
	os.system("mv output_data "+ directory + '/DIFF_th')
	'''
	

	'''
	os.system('cp 3.yaml default_chip.yaml')
	os.system("python scan_noise_occupancy_lin.py")
	os.system("python meta_tune_local_threshold_lin_ini.py")
	os.system('cp 4.yaml default_chip.yaml')
	os.system("python scan_noise_occupancy_lin.py")
	os.system("python meta_tune_local_threshold_lin.py")
	os.system("mv output_data "+ directory + '/config2LIN') 

	os.system('cp 5.yaml default_chip.yaml')
	os.system("python scan_noise_occupancy_lin.py")
	os.system("python meta_tune_local_threshold_lin_ini.py")
	os.system('cp 6.yaml default_chip.yaml')
	os.system("python scan_noise_occupancy_lin.py")
	os.system("python meta_tune_local_threshold_lin.py")
	os.system("mv output_data "+ directory + '/config3LIN') 

	os.system('mv last_diff output_data')
	os.system('cp 7.yaml default_chip.yaml')
	os.system("python scan_noise_occupancy_diff.py")
	os.system("python meta_tune_local_threshold_diff.py")
	os.system("mv output_data "+ directory + '/config4DIFF') 
	'''



	#os.system("python masterIrrad.py")




if __name__ == '__main__':
	 ChipCharact_lin()
