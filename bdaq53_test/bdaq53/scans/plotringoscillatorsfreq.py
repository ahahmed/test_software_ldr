import time
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import os
import glob
import datetime
#from mon1 import monitor_datalogger_power_supply
from matplotlib.pyplot import figure
import basil
import visa
from operator import itemgetter
import ast
import sys, termios, tty, os, time
import csv
import tables as tb
from bitarray import bitarray
from datetime import datetime
from bdaq53.fifo_readout import FifoReadout
import logging
import matplotlib.pyplot as plt


def list_folders(directory_ringoscillators):
	dir_array = sorted(os.listdir(directory_ringoscillators))
	#print dir_array
	#raw_input('Enter ')
	return dir_array

def get_elements_line(x):
	array = []
	for i in range(len(x.split('	'))):
		a = x.split('	')[i]
		if a != '':
			array.append(a)
	return array

def adjust_doses(t, dose_ref, doserate_Mrad):
	dose = []
	t_adjust = 0
	dose_after_charact = []
	id_measurement_after_charact = []
	for i, dose_ref_value in enumerate(dose_ref):
		if i == 0:
			dose.append(0)
		elif i<len(dose_ref)-1:
			if(dose_ref_value == dose_ref[i+1]):
				dose.append((t[i]-t_adjust)*doserate_Mrad/3600)
			else:
				t_adjust = t_adjust + t[i] - t[i-1]
				dose.append((t[i]-t_adjust)*doserate_Mrad/3600)
				dose_after_charact.append((t[i]-t_adjust)*doserate_Mrad/3600)
				id_measurement_after_charact.append(i)	
		else:
			dose.append((t[i]-t_adjust)*doserate_Mrad/3600)		
	#fix for stopping at 700 Mrad and time vector restarting:
	for i, doseval in enumerate(dose):
		#print doseval, i
		if (i>4094) & (i<5511):
			dose[i] = dose[i] + 703.623005526 + 317.509806099
		elif i>5510:
			dose[i] = 1004.24899303 + 10 * (i-5510)
		
	for i, doseval in enumerate(dose):
		print doseval, i
	raw_input('Ole y Ole')
	return dose,dose_after_charact , id_measurement_after_charact

def get_ring_vector(dir_array,directory_ringoscillators):
	r0 = []
	r1 = []
	r2 = []
	r3 = []
	r4 = []
	r5 = []
	r6 = []
	r7 = []
	t = []
	idcount = []
	temperature = []
	doserate_Mrad = 3.9 #Mrad/h
	
	#TODO if global pulse is changed, change also the duration variable
	global_pulse = 12 #value for global pulse 9 equal to 409.6e-9
	global_pulse_duration = 3276.8e-9
	j = 0
	dose_ref = []
	previous_dose_ref = 0
	
	for z, directory in enumerate(dir_array):
		idcount.append(j)
		j = j+1
		dir_textfile = directory_ringoscillators + '/' + directory+'/output_data.dat'
		f = open(dir_textfile, 'r')
		lines = f.readlines()
		for i,x in enumerate(lines):
			if i == 1:				
				#t.append('%.2f'%(float(x.split(' ')[2])))
				t.append((float(x.split(' ')[2])))
				temperature.append((float(x.split(' ')[0])))
				dose_ref.append(float(x.split(' ')[3]))
				
					
			elif i == global_pulse:
				counts = get_elements_line(x)	
				
				#print counts
				#print directory
				#raw_input('press button')		
				r0.append(float(counts[1])/(global_pulse_duration))
				r1.append(float(counts[2])/(global_pulse_duration))
				r2.append(float(counts[3])/(global_pulse_duration))
				r3.append(float(counts[4])/(global_pulse_duration))
				r4.append(float(counts[5])/(global_pulse_duration))
				r5.append(float(counts[6])/(global_pulse_duration))
				r6.append(float(counts[7])/(global_pulse_duration))
				r7.append(float(counts[8])/(global_pulse_duration))
				
			else:
				a = 0

			
		f.close()

	dose,dose_after_charact,id_measurement_after_charact = adjust_doses(t, dose_ref, doserate_Mrad)
		

	ring = [r0,r1,r2,r3,r4,r5,r6,r7]
	return ring, t, temperature, idcount,dose

def plot_ro(ring, t, temperature,directory_ringoscillators,directory_plots,idcount,dose):
	line_color = ['#ffcc00','#ff6633','#ff0066','#cc33ff', '#3333ff', '#0099ff', '#33cc66', '#99cc33','#11cc55']
	legend = ['110-CKND0','111-CKND4','112-INV0','113-INV4','114-NAND0','115-NAND4','116-NOR0','117-NOR4']

	figure(figsize=(18, 8))

	'''
	for i in range(len(ring)):
			plt.scatter(idcount,ring[i], label = legend[i],s=0.5, color = line_color[i])	
	plt.legend(loc=1)	
	plt.title('Ring Oscillators frequency')
	plt.xlabel('time (s)')
	plt.ylabel('Frequency (Hz)')	
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")	
	plt.savefig(directory_plots+'/'+timestamp+'_ringOscillators_freq_idcount.png', dpi=1000)
	'''
	lables_size = 20
	#####################################
	
	
	#Frequencies, all rings
	for i in range(len(ring)):
			plt.scatter(dose,ring[i], label = legend[i],s=0.5, color = line_color[i])	
	lgnd = plt.legend(bbox_to_anchor=(1.005, 1), loc=2,prop={'size': 13}, borderaxespad=0.)#legend(loc=7 , prop={'size': 13})


	for handle in lgnd.legendHandles:
    		handle.set_sizes([9.0])
		
	plt.title('Ring Oscillators Frequency', fontsize=lables_size)
	plt.xlabel('Dose (Mrad)', fontsize=20)
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0)
	plt.ylabel('Frequency (Hz)', fontsize=20)
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")	
	#plt.show()
	plt.savefig(directory_plots+'/'+timestamp+'_ringOscillators_freq_dose.png', dpi=1000)
	##############################################3

	#################################################
	#plotting relative decrease of frequencies in %
	figure(figsize=(15, 8))
	ring_percentage_matrix = []	
	for i in range(len(ring)):
		ring_percentage_input = []
		for r in ring[i]:
			ring_percentage_input.append(r*100/ring[i][0])
		#print ring_percentage_input
		#raw_input('lolo')
 		ring_percentage_matrix.append(ring_percentage_input)		
		
	for i in range(len(ring_percentage_matrix)):
			plt.scatter(dose,ring_percentage_matrix[i], label = legend[i],s=0.5, color = line_color[i])	
	lgnd = plt.legend(loc=1  , prop={'size': 15} )	
	for handle in lgnd.legendHandles:
    		handle.set_sizes([9.0])
	plt.title('Ring Oscillators Frequency in % respect pre-irrad', fontsize=lables_size)
	plt.xlabel('Dose (Mrad)', fontsize=lables_size)
	plt.ylabel('Frequency respect before irradiation (%)', fontsize=lables_size)	
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0)
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")	
	plt.savefig(directory_plots+'/'+timestamp+'_ringOscillators_freq_percentage_decrease_dose.png', dpi=1000)

	figure(figsize=(15, 8))
	ring_percentage_matrix = []	
	for i in range(len(ring)):
		ring_percentage_input = []
		for r in ring[i]:
			ring_percentage_input.append(r*100/ring[i][0])
		#print ring_percentage_input
		#raw_input('lolo')
 		ring_percentage_matrix.append(ring_percentage_input)		
		
	for i in range(len(ring_percentage_matrix)):
			plt.scatter(dose,ring_percentage_matrix[i], label = legend[i],s=0.5, color = line_color[i])	
	lgnd = plt.legend(loc=1  , prop={'size': 15} )	
	for handle in lgnd.legendHandles:
    		handle.set_sizes([9.0])
	plt.title('Ring Oscillators Frequency in % respect pre-irrad', fontsize=lables_size)
	plt.xlabel('Dose (Mrad)', fontsize=lables_size)
	plt.ylabel('Frequency respect before irradiation (%)', fontsize=lables_size)	
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0, right = 500)
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")	
	plt.savefig(directory_plots+'/'+timestamp+'_ringOscillators_freq_percentage_decrease_dose_justTo500.png', dpi=1000)
	#########################################################3


	#Plotting only rings with gates with driving strength 1
	figure(figsize=(8, 8))
	for i in range(len(ring)):
			if ((i==0) or (i==2) or (i==4) or (i==6)):
				plt.scatter(dose,ring[i], label = legend[i],s=0.5, color = line_color[i])	
	lgnd = plt.legend(loc=1, prop={'size': 15})
	for handle in lgnd.legendHandles:
    		handle.set_sizes([9.0])	
	plt.title('Ring Oscillators frequency', fontsize=lables_size)
	plt.xlabel('Dose(Mrad)', fontsize=lables_size)
	plt.ylabel('Frequency (Hz)', fontsize=lables_size)	
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0)
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")	
	plt.savefig(directory_plots+'/'+timestamp+'_ringOscillators_freq_dose_drivingstrenght0.png', dpi=1000)

	#######################################################
	#Plotting only rings with gates with driving strength 4
	figure(figsize=(8, 8))
	for i in range(len(ring)):
			if ((i==1) or (i==3) or (i==5) or (i==7)):
				plt.scatter(dose,ring[i], label = legend[i],s=0.5, color = line_color[i])	
	lgnd = plt.legend(loc=1, prop={'size': 15})
	for handle in lgnd.legendHandles:
    		handle.set_sizes([9.0])	
	plt.title('Ring Oscillators frequency', fontsize=lables_size)
	plt.xlabel('Dose (Mrad)', fontsize=lables_size)
	plt.ylabel('Frequency (Hz)', fontsize=lables_size)	
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0)
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")	
	plt.savefig(directory_plots+'/'+timestamp+'_ringOscillators_freq_dose_drivingstrenght4.png', dpi=1000)
	#######################################################

	#Plotting temperature
	figure(figsize=(8, 4))
	plt.scatter(dose, temperature, label = 'Temperature', s=0.5,color = line_color[8])	
	plt.legend(loc=1)	
	plt.title('Temperature', fontsize=lables_size)
	plt.xlabel('Dose (Mrad)', fontsize=lables_size)
	plt.grid(True)

	lgnd = plt.legend(loc=1  , prop={'size': 15} )	
	for handle in lgnd.legendHandles:
    		handle.set_sizes([9.0])

	axes = plt.gca()
	axes.set_xlim(left=0)
	plt.ylabel('T(C)', fontsize=lables_size)
	plt.savefig(directory_plots+'/'+timestamp+'_ringOscillators_Temp_dose.png')

	#Plotting time stamps

	figure(figsize=(15, 8))
	plt.scatter(dose,t, label = 'time', s=0.5,color = line_color[8])	
	plt.legend(loc=1)	
	plt.title('Time', fontsize=lables_size)
	plt.ylabel('time (s)', fontsize=lables_size)
	plt.xlabel('idmeas', fontsize=lables_size)
	axes = plt.gca()
	axes.set_xlim(left=0)
	plt.grid(True)
	plt.savefig(directory_plots+'/'+timestamp+'_ringOscillators_time.png')
	

if __name__ == "__main__":

    directory = '00_TEST_INFO'	
    if not os.path.exists(directory):
	os.makedirs(directory)	

    directory_plots = '00_TEST_INFO/ring_oscillator_plots'		
    if not os.path.exists(directory_plots):
	os.makedirs(directory_plots)


    directory_ringoscillators = '00_TEST_INFO_rad/ring_oscillators'
    dir_array = list_folders(directory_ringoscillators)

    ring, t, temperature,idcount,dose = get_ring_vector(dir_array,directory_ringoscillators)
    plot_ro(ring, t, temperature,directory_ringoscillators,directory_plots,idcount,dose)





