'''
Created on October 26, 2016

@comment: This script records current on the pin diode as function of time.
@comment: This script is to be used with Keithley2700Monitor_v1.0_instance2.vi
@author: Dima Maneuski
'''

import socket
import sys
import time
import re
import os

##### USER INPUT =====
current_measure_command = 'km'
sleepTime = 60 # seconds
fileName = "D:\\data\\GLADD-X\\18072600-RD53A_irradiation\\Keithely2700_VDDA_mon.txt"

# Create a TCP/IP socket
sockXY = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sockC = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# connecting to CurrentMeasurer.vi

server_address = ('194.36.1.188', 6346)
print >> sys.stderr, 'connecting to %s port %s' % server_address
sockC.connect(server_address)
time.sleep(1)


# list of data
time_data = []
elapsed_data = []
c_data = []

# Open a file
if os.path.isfile(fileName):
    #fileName = fileName + "_"
    ext = os.path.splitext(fileName);
    #print ext[1]
    fileName = ('.').join(fileName.split('.')[:-1]) + time.strftime("_%Y_%m_%d_%H_%M_%S") + ext[1]
    print "File exists! Writing data to new file: " + fileName
fo = open(fileName, "w")
header = ['Time [s]\t', 'Elapsed time [s]\t', 'Voltage [V]', '\n']
fo.writelines(header)
fo.close()


try:
    start = time.time()
    while 1:
        message = current_measure_command
        sockC.sendall(message)
        msgReceivedC = sockC.recv(1024)
        #print "Receiving message: " + msgReceivedC
        end = time.time()
        #time.sleep(1)
        #print "------- TAKING DATA COMPLETE-------"
        _c = [float(x) for x in re.findall("-?\d+.?\d*(?:[Ee]-\d+)?", msgReceivedC)]
        c_data.append(_c)
        deltaT = int(end - start)
        data = [time.strftime("%d-%m-%Y %H:%M:%S"), '\t', str(deltaT), '\t', str(_c[0]), '\n']
        print time.strftime("%d-%m-%Y %H:%M:%S") + '\t' + str(deltaT) + ' s \t' + str(_c[0]) + ' V'       
        with open(fileName, 'a') as file_:
            file_.writelines(data)
        time.sleep(sleepTime)
    print "Scan complete..."

finally:
    #print >> sys.stderr, 'closing socket'
    #sockXY.close()
    print >> sys.stderr, 'closing socket'
    sockC.close()
    #print x_data
    #print y_data
    print c_data
    fo.close()
    
    
    