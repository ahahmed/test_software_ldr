import time
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import os
import glob
import datetime
from bdaq53.fifo_readout import FifoReadout
import logging
import sys, select

def waiting_input():
	print "Type 0  and enter if you want to stop. You have ten seconds to answer!"
	i, o, e = select.select( [sys.stdin], [], [], 5 )
	if (i):
		t = sys.stdin.readline().strip()
	else:
	 	print "You said nothing!, the program will keep on running"
		t = 1
	return t


def annealing_characterization():
	globalTime = str(1001)
	dose = str(1001)

	directory = 'Chip_annealing'	
	if not os.path.exists(directory):
    		os.makedirs(directory)
	

	t0 = time.time()
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	
	#testTag = 'AfterDoseOf_100Mrad_RoomT_'
	directory = directory + '/' + timestamp + '_annealing' 
	os.system('mkdir '+directory)
	
	#Temperature sensors, NTC, ring oscillators:
	
	os.system('python scan_temp_ntc_accumulative.py '+ globalTime + ' ' + dose )
	os.system('python scan_ring_oscillators.py '+ globalTime + ' ' + dose )
	#os.system('python test_dac_linearity_adc_externalmultim.py '+ globalTime + ' ' + dose)
	os.system("mv output_data "+ directory + '/dac_linearity')  
	os.system('python  scan_temperature_sensor.py '+ globalTime + ' ' + dose )
	#All FE tests:
	os.system("python scan_digital.py")
	os.system("mv output_data "+ directory + '/all_digital_scan')  
	os.system('python scan_temp_ntc_accumulative.py '+ globalTime + ' ' + dose )

	os.system("python scan_noise_occupancy.py")
	os.system("python scan_analog.py")
	os.system("mv output_data "+ directory + '/all_analog_scan') 
	os.system('python scan_temp_ntc_accumulative.py '+ globalTime + ' ' + dose )
	
	################# LOADING CONFIG FILE FOR CHARACT ##################################
	os.system('cp default_chip_whileCharacterization.yaml default_chip.yaml')
	

	#####################################################################################
	#Sync FE:
	os.system("python scan_noise_occupancy_sync.py")
	os.system("python scan_analog_sync.py")
	os.system("mv output_data "+ directory + '/sync_analog_scan') 


	os.system("python scan_noise_occupancy_sync.py")
	os.system("python scan_threshold_sync.py")
	os.system("python calibrate_tot_sync.py")
	os.system("mv output_data "+ directory + '/sync_threshold_scan_width6_400ns') 

	 
	os.system("python scan_noise_occupancy_sync.py")
	os.system("python scan_threshold_sync_width8.py")
	os.system("mv output_data "+ directory + '/sync_threshold_scan_width8_1p6us') 

	
	os.system("python scan_noise_occupancy_sync.py")
	os.system("python scan_threshold_sync_width9.py")
	os.system("mv output_data "+ directory + '/sync_threshold_scan_width9_3p2us') 
	
	os.system('python scan_temp_ntc_accumulative.py '+ globalTime + ' ' + dose )  

	os.system('cp default_chip_whileCharacterization_sync160.yaml default_chip.yaml')
	os.system("python scan_noise_occupancy_sync.py")
	os.system("python scan_threshold_sync.py")
	os.system("python calibrate_tot_sync.py")
	os.system("mv output_data "+ directory + '/sync_threshold_scan_width6_400ns_VTH160') 

	os.system("python scan_noise_occupancy_sync.py")
	os.system("python scan_threshold_sync_width8.py")
	os.system("mv output_data "+ directory + '/sync_threshold_scan_width8_1p6us_VTH160') 

	
	os.system("python scan_noise_occupancy_sync.py")
	os.system("python scan_threshold_sync_width9.py")
	os.system("mv output_data "+ directory + '/sync_threshold_scan_width9_3p2us_VTH160') 

	######################################################################################
	#Lin FE:
	os.system("python scan_noise_occupancy_lin.py")
	os.system("python scan_analog_lin.py")
	os.system("mv output_data "+ directory + '/lin_analog_scan') 
	

	os.system("python scan_noise_occupancy_lin.py")
	os.system("python scan_threshold_lin.py")
	os.system("mv output_data "+ directory + '/lin_threshold_scan_lin_untuned') 
	os.system('python scan_temp_ntc_accumulative.py '+ globalTime + ' ' + dose ) 
	
	
	#using linear triming TDACs of tune before irradiation for a threshold scan:
	#TODO: uncomment this with the initial tuning:
	os.system("mkdir output_data")
	os.system("cp lin_meta_tune_threshold_simple_tune_0Mrad/*mask.h5 output_data ") 
	os.system("python scan_noise_occupancy_lin.py")		
	os.system("python scan_threshold_lin.py")
	os.system("mv output_data "+ directory + '/lin_threshold_scan_lin_withtuneat0Mrad')
	
	
	#Tunning taking as a reference the mask of the previous step:
	os.system("mv lin_meta_tune_threshold_simple_lastTune output_data")	
	os.system("python scan_noise_occupancy_lin.py")	 	
	os.system("python meta_tune_local_threshold_lin.py")
	os.system("mkdir lin_meta_tune_threshold_simple_lastTune")
	os.system("cp output_data/*mask.h5 lin_meta_tune_threshold_simple_lastTune/")
	os.system("python calibrate_tot_lin.py")
	os.system("python scan_analog_lin.py")
	#os.system("cp -r output_data  lin_meta_tune_threshold_simple_lastTune") 
	os.system("mv output_data "+ directory + '/lin_meta_tune_threshold_simple') 

	'''
	#using linear triming TDACs of last tune for a threshold scan:
	os.system("python scan_noise_occupancy_lin.py")
	os.system("cp -r lin_meta_tune_threshold_simple_lastTune_200Mrad/* output_data ") 	
	os.system("python scan_threshold_lin.py")
	os.system("mv output_data "+ directory + '/lin_threshold_scan_lin_withlasttuneat200Mrad')
	'''
	
	##################################################################################
	#Diff FE:
	os.system("python scan_noise_occupancy_diff.py")
	os.system("python scan_analog_diff.py")
	os.system("mv output_data "+ directory + '/diff_analog_scan_diff')
	

	os.system("python scan_noise_occupancy_diff.py")
	os.system("python scan_threshold_diff.py")
	os.system("mv output_data "+ directory + '/diff_threshold_scan_untuned') 
	os.system('python scan_temp_ntc_accumulative.py '+ globalTime + ' ' + dose )


	#using linear triming TDACs of tune before irradiation for a threshold scan:
	#TODO: uncomment this with the initial tuning:
	os.system("mkdir output_data")
	os.system("cp diff_meta_tune_threshold_simple_tune_0Mrad/*mask.h5 output_data ") 
	os.system("python scan_noise_occupancy_diff.py")	
	os.system("python scan_threshold_diff.py")
	os.system("mv output_data "+ directory + '/diff_threshold_scan_lin_withtuneat0Mrad')
	
	
	#Tunning taking as a reference the mask of the previous step:
	os.system("mv diff_meta_tune_threshold_simple_lastTune output_data")	
	os.system("python scan_noise_occupancy_lin.py")	 	
	os.system("python meta_tune_local_threshold_diff.py")
	os.system("mkdir diff_meta_tune_threshold_simple_lastTune")
	os.system("cp output_data/*mask.h5 diff_meta_tune_threshold_simple_lastTune/")
	os.system("python calibrate_tot_diff.py")
	os.system("python scan_analog_diff.py")
	#os.system("cp -r output_data  lin_meta_tune_threshold_simple_lastTune") 
	os.system("mv output_data "+ directory + '/diff_meta_tune_threshold_simple') 

	'''
	#using diff triming TDACs of last tune for a threshold scan:
	os.system("python scan_noise_occupancy_diff.py")
	os.system("cp -r diff_meta_tune_threshold_simple_lastTune_200Mrad/* output_data ") 	
	os.system("python scan_threshold_diff.py")
	os.system("mv output_data "+ directory + '/diff_threshold_scan_diff_withlasttuneat200Mrad')
	'''
	


	#Noise tune for linear and diff:
	'''
	os.system("python scan_noise_occupancy_lin.py")
	os.system("python tune_local_threshold_noise_lin.py")
	os.system("mv output_data "+ directory + '/lin_meta_tune_threshold_noise') 
	os.system('python scan_temp_ntc_accumulative.py '+ globalTime + ' ' + dose ) 


	os.system("python scan_noise_occupancy_diff.py")
	os.system("python tune_local_threshold_noise_diff.py")
	os.system("mv output_data "+ directory + '/diff_meta_tune_threshold_noise') 
	os.system('python scan_temp_ntc_accumulative.py '+ globalTime + ' ' + dose ) 

	'''

	################# LOADING CONFIG FILE FOR IRRADIATION ##################################
	os.system('cp default_chip_whileIrradiation.yaml default_chip.yaml')

	#####################################################################################


	#Temperature sensors, NTC, ring oscillators:
	#os.system('python scan_temp_sensors.py')
	os.system('python scan_temp_ntc_accumulative.py '+ globalTime + ' ' + dose )
	os.system('python scan_ring_oscillators.py '+ globalTime + ' ' + dose )


	tend = time.time()
	print 'Total time needed: ', tend-t0

	directory = '00_TEST_INFO'
	if not os.path.exists(directory):
    		os.makedirs(directory)
	
	directory = directory + '/time_characterizations'
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")

	if not os.path.exists(directory):
    		os.makedirs(directory)
		out_T = open( directory + '/'+"ACCUMULATIVE_time_characterizations.txt", "a")
		tobewritten = "%Timestamp   TimeNeededForCharact(s) GlobalTime(s)  Dose(Mrad)\n"
		out_T.write(tobewritten) 
		out_T.close()

	out_T = open( directory + '/'+"ACCUMULATIVE_time_characterizations.txt", "a")
	tobewritten =  str(timestamp) + " " + str(tend-t0) + " " + str(time.time()-float(globalTime))+ ' ' + dose	
	tobewritten = tobewritten + "\n"
	out_T.write(tobewritten)
	out_T.close()

def whileIrradiation_annealing(globalTime,dose):
	#globalTime = sys.argv[1]
	#dose = sys.argv[2]
	os.system('python scan_temp_ntc_accumulative.py '+ globalTime + ' ' + dose )
	os.system('python scan_analog_no_output.py')
	#TODO Fix the next two scans	
	os.system('python  scan_temperature_sensor.py '+ globalTime + ' ' + dose )
	os.system('python scan_ring_oscillators.py '+ globalTime + ' ' + dose )
	#os.system('python scan_ADC.py '+ globalTime + ' ' + dose )


if __name__ == '__main__':
	
	t0 = time.time()
	t1 = time.time()
	t_annealing = 86400 #9h 30min
	t_loop = t1 - t0
	while((t1-t0) < t_annealing):
		t1 = time.time()
		globalTime = t1-t0
		dose = 1008
		whileIrradiation_annealing(str(globalTime),str(dose))
		print 'Time until new characterization: ', t_annealing-(t1-t0)
	annealing_characterization()








	
	 
