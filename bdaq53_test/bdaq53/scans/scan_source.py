#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Source scan with RD53A. This is equivalent to external trigger scan with HitOr (BDAQ self-trigger).
'''

import tables as tb

from bdaq53.scans.scan_ext_trigger import ExtTriggerScan
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting


local_configuration = {
    # Scan parameters
    'start_column': 128,  # start column for mask
    'stop_column': 264,  # stop column for mask
    'start_row': 0,  # start row for mask
    'stop_row': 192,  # stop row for mask
    'maskfile': 'auto',
    'scan_timeout': False,  # timeout for scan after which the scan will be stopped, in seconds; if False no limit on scan time
    'max_triggers': 80000,  # number of maximum received triggers after stopping readout, if False no limit on received trigger

    'trigger_latency': 100,  # latency of trigger in units of 25 ns (BCs)
    'trigger_delay': 65,  # trigger delay in units of 25 ns (BCs)
    'trigger_length': 32,  # length of trigger command (amount of consecutive BCs are read out)
    'veto_length': 500,  # length of TLU veto in units of 25 ns (BCs). This vetos new triggers while not all data is revieved. Should be adjusted for longer trigger length.

    # Trigger configuration
    'TRIGGER': {
        'TRIGGER_MODE': 0,  # Selecting trigger mode: Use trigger inputs/trigger select (0), TLU no handshake (1), TLU simple handshake (2), TLU data handshake (3)
        'TRIGGER_LOW_TIMEOUT': 0,  # Maximum wait cycles for TLU trigger low.
        'TRIGGER_SELECT': 1,  # Selecting trigger input: HitOR (1), disabled (0)
        'TRIGGER_INVERT': 0,  # Inverting trigger input: HitOR (1), disabled (0)
        'TRIGGER_VETO_SELECT': 0,  # Selecting trigger veto: AZ VETO (2), RX FIFO full (1), disabled (0)
        'TRIGGER_HANDSHAKE_ACCEPT_WAIT_CYCLES': 5,  # TLU trigger minimum length in TLU clock cycles
        'DATA_FORMAT': 0,  # Select trigger data format: only trigger number (0), only time stamp (1), combined, 15 bit time stamp + 16 bit trigger number (2)
        'EN_TLU_VETO': 0,  # Assert TLU veto when external veto
        'TRIGGER_COUNTER': 0
    }
}


class SourceScan(ExtTriggerScan):
    scan_id = "source_scan"

    def configure(self, **kwargs):
        super(SourceScan, self).configure(load_hitbus_mask=True, use_logo_mask=False, **kwargs)

    def analyze(self, create_pdf=True):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', cluster_hits=True) as a:
            a.analyze_data()

            with tb.open_file(a.analyzed_data_file) as in_file:
                clusters = in_file.root.Cluster[:]
                n_clusters = clusters.shape[0]

        if create_pdf:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()

        return n_clusters


if __name__ == "__main__":
    scan = SourceScan()
    scan.start(**local_configuration)
    scan.close()
