import time
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import os
import glob
import datetime
#from mon1 import monitor_datalogger_power_supply
from matplotlib.pyplot import figure
import basil
import visa
from operator import itemgetter
import ast
import sys, termios, tty, os, time
import csv
import tables as tb
from bitarray import bitarray
from datetime import datetime
from bdaq53.fifo_readout import FifoReadout
import logging
import matplotlib.pyplot as plt



def get_fe_data():
	directory_plots = 'plotsFE'
	dir_textfile_sync = 'sync.txt'
	dir_textfile_lin = 'lin.txt'
	dir_textfile_diff = 'diff.txt'
	sync = open(dir_textfile_sync, 'r')
	lin = open(dir_textfile_lin, 'r')
	diff = open(dir_textfile_diff, 'r')
	
	#lin:
	vth_mean_untuned = []
	vth_disp_untuned = []
	noise_mean_untuned = []
	noise_disp_untuned = []

	vth_mean_0MradTune = []
	vth_disp_0MradTune = []
	noise_mean_0MradTune = []
	noise_disp_0MradTune = []

	vth_mean_NewTune = []
	vth_disp_NewTune = []
	noise_mean_NewTune = []
	noise_disp_NewTune = []

	dose = []
	dose1 = []
	dose2 = []
	
	lines = lin.readlines()
	for i,x in enumerate(lines):
		if i > 1:
			dose.append((float(x.split('	')[0])))
			print dose
			test = 	float(x.split('	')[1])
			if test>0:
				vth_mean_untuned.append((float(x.split('	')[1])))
				dose1.append((float(x.split('	')[0])))
				vth_disp_untuned.append((float(x.split('	')[2])))
			noise_mean_untuned.append((float(x.split('	')[3])))
			noise_disp_untuned.append((float(x.split('	')[4])))

			vth_mean_0MradTune.append((float(x.split('	')[5])))
			vth_disp_0MradTune.append((float(x.split('	')[6])))

			test = 	float(x.split('	')[7])
			if test>0:
				noise_mean_0MradTune.append((float(x.split('	')[7])))
				noise_disp_0MradTune.append((float(x.split('	')[8])))
				dose2.append((float(x.split('	')[0])))
			vth_mean_NewTune.append((float(x.split('	')[9])))
			vth_disp_NewTune.append((float(x.split('	')[10])))
			noise_mean_NewTune.append((float(x.split('	')[11])))
			noise_disp_NewTune.append((float(x.split('	')[12])))


	line_color = ['#ffcc00','#ff6633','#cc33ff', '#3333ff', '#0099ff', '#33cc66', '#99cc33','#11cc55']
	legend = ['Untuned (all TDACs at 7) ','Using TDAC mask of pre-rad tuning','Tuned']

	figure(figsize=(10, 8))
	lables_size = 20	
	vth_disp_untuned_err = [x / 2 for x in vth_disp_untuned]
	vth_disp_0MradTune_err = [x / 2 for x in vth_disp_0MradTune]
	vth_disp_NewTune_err = [x / 2 for x in vth_disp_NewTune]

	plt.errorbar(dose1,vth_mean_untuned, yerr = vth_disp_untuned_err,  fmt='o',  label = legend[0], color = line_color[0])
	plt.errorbar(dose,vth_mean_0MradTune, yerr = vth_disp_0MradTune_err,  fmt='o',label = legend[1], color = line_color[1])
	plt.errorbar(dose,vth_mean_NewTune, yerr = vth_disp_NewTune_err, fmt='o', label = legend[2], color = line_color[2])
	


	lgnd = plt.legend( loc=2,prop={'size': 13})
	#legend(loc=7 , prop={'size': 13})


	#for handle in lgnd.legendHandles:
    		#3handle.set_sizes([9.0])
		
	plt.title('Linear FE, mean threshold and dispersion', fontsize=lables_size)
	plt.xlabel('Dose (Mrad)', fontsize=20)
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0.8)
	axes.set_xscale('log')
	
	plt.ylabel('Mean Vth (e-)', fontsize=20)
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")	
	
	plt.savefig(directory_plots+'/'+timestamp+'_lin_Vth_dose.png', dpi=1000)



	figure(figsize=(10, 8))
	lables_size = 20	
	vth_disp_untuned_err = [x / 2 for x in vth_disp_untuned]
	vth_disp_0MradTune_err = [x / 2 for x in vth_disp_0MradTune]
	vth_disp_NewTune_err = [x / 2 for x in vth_disp_NewTune]

	plt.scatter(dose1, vth_disp_untuned,    label = legend[0], color = line_color[0])
	plt.scatter(dose,vth_disp_0MradTune,  label = legend[1], color = line_color[1])
	plt.scatter(dose,vth_disp_NewTune,  label = legend[2], color = line_color[2])
	


	lgnd = plt.legend( loc=0,prop={'size': 13})
	#legend(loc=7 , prop={'size': 13})


	#for handle in lgnd.legendHandles:
    		#3handle.set_sizes([9.0])
		
	plt.title('Linear FE,  threshold dispersion', fontsize=lables_size)
	plt.xlabel('Dose (Mrad)', fontsize=20)
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0.8)
	axes.set_xscale('log')
	
	plt.ylabel('Vth dispersion (e-)', fontsize=20)
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")	
	
	plt.savefig(directory_plots+'/'+timestamp+'_lin_Vth_dispersion_dose.png', dpi=1000)

	



	figure(figsize=(10, 8))
	lables_size = 20	
	noise_disp_untuned_err = [x / 2 for x in noise_disp_untuned]
	noise_disp_0MradTune_err = [x / 2 for x in noise_disp_0MradTune]
	noise_disp_NewTune_err = [x / 2 for x in noise_disp_NewTune]

	plt.subplot(3,1,1)
	plt.title('Linear FE, mean noise and dispersion', fontsize=lables_size)
	plt.errorbar(dose,noise_mean_untuned, yerr = noise_disp_untuned_err,  fmt='o',  label = legend[0], color = line_color[0])
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0.8)
	axes.set_xscale('log')	
	plt.ylabel('Mean noise (e-)', fontsize=10)
	lgnd = plt.legend( loc=3,prop={'size': 13})





	plt.subplot(3,1,2)
	plt.errorbar(dose2,noise_mean_0MradTune, yerr = noise_disp_0MradTune_err,  fmt='o',label = legend[1], color = line_color[1])
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0.8)
	axes.set_xscale('log')	
	plt.ylabel('Mean noise (e-)', fontsize=10)
	lgnd = plt.legend( loc=3,prop={'size': 13})


	plt.subplot(3,1,3)
	plt.errorbar(dose,noise_mean_NewTune, yerr = noise_disp_NewTune_err, fmt='o', label = legend[2], color = line_color[2])
	plt.xlabel('Dose (Mrad)', fontsize=20)
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0.8)
	axes.set_xscale('log')	
	plt.ylabel('Mean noise (e-)', fontsize=10)

	lgnd = plt.legend( loc=3,prop={'size': 13})

	
	
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0.8)
	axes.set_xscale('log')	
	plt.ylabel('Mean noise (e-)', fontsize=10)



	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")	
	
	plt.savefig(directory_plots+'/'+timestamp+'_lin_noise_dose.png', dpi=1000)

	



	figure(figsize=(10, 8))
	lables_size = 20	


	plt.subplot(3,1,1)
	plt.title('Linear FE, mean threshold and dispersion', fontsize=lables_size)
	plt.errorbar(dose,vth_mean_untuned, yerr = vth_disp_untuned_err,  fmt='o',  label = legend[0], color = line_color[0])
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0.8)
	axes.set_xscale('log')	
	plt.ylabel('Mean vth (e-)', fontsize=10)
	lgnd = plt.legend( loc=2,prop={'size': 13})





	plt.subplot(3,1,2)
	plt.errorbar(dose2,vth_mean_0MradTune, yerr = vth_disp_0MradTune_err,  fmt='o',label = legend[1], color = line_color[1])
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0.8)
	axes.set_xscale('log')	
	plt.ylabel('Mean vth (e-)', fontsize=10)
	lgnd = plt.legend( loc=2,prop={'size': 13})


	plt.subplot(3,1,3)
	plt.errorbar(dose,vth_mean_NewTune, yerr = vth_disp_NewTune_err, fmt='o', label = legend[2], color = line_color[2])
	plt.xlabel('Dose (Mrad)', fontsize=20)
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0.8)
	axes.set_xscale('log')	
	plt.ylabel('Mean vth (e-)', fontsize=10)

	lgnd = plt.legend( loc=2,prop={'size': 13})

	
	
	plt.grid(True)
	axes = plt.gca()
	axes.set_xlim(left=0.8)
	axes.set_xscale('log')	
	plt.ylabel('Mean vth (e-)', fontsize=10)



	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")	
	
	plt.savefig(directory_plots+'/'+timestamp+'_lin_vth_divided_dose.png', dpi=1000)





if __name__ == "__main__":
	get_fe_data()
 





