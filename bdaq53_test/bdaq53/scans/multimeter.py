#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#
import tables as tb
from tqdm import tqdm
import sys, os, time
from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting

from bdaq53.rd53a import RD53A
from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis_utils
from bdaq53.fifo_readout import FifoReadout
#from bdaq53.serial_com import serial_com
from datetime import datetime
import logging
import yaml
import time

import socket
import sys
import time
import re
import os



voltage_mux_array=['ADCbandgap','CAL_MED_left','CAL_HI_left','TEMPSENS_1','RADSENS_1','TEMPSENS_2','RADSENS_2','TEMPSENS_4','RADSENS_4','VREF_VDAC','VOUT_BG','IMUX_out','VCAL_MED','VCAL_HIGH','RADSENS_3','TEMPSENS_3','REF_KRUM_LIN','Vthreshold_LIN','VTH_SYNC','VBL_SYNC','VREF_KRUM_SYNC','VTH_HI_DIFF','VTH_LO_DIFF','VIN_Ana_SLDO','VOUT_Ana_SLDO','VREF_Ana_SLDO','VOFF_Ana_SLDO','ground','ground1','VIN_Dig_SLDO','VOUT_Dig_SLDO','VREF_Dig_SLDO','VOFF_Dig_SLDO','ground2']

current_mux_array=['Iref','IBIASP1_SYNC','IBIASP2_SYNC','IBIAS_DISC_SYNC','IBIAS_SF_SYNC','ICTRL_SYNCT_SYNC','IBIAS_KRUM_SYNC','COMP_LIN','FC_BIAS_LIN','KRUM_CURR_LIN','LDAC_LIN','PA_IN_BIAS_LIN','COMP_DIFF','PRECOMP_DIFF','FOL_DIFF','PRMP_DIFF','LCC_DIFF','VFF_DIFF','VTH1_DIFF','VTH2_DIFF','CDR_CP_IBIAS','VCO_BUFF_BIAS','VCO_IBIAS','CML_TAP_BIAS0','CML_TAP_BIAS1','CML_TAP_BIAS2']



'''
    This basic test scans over the values of the selected DAC and
    measures the resulting analog value with the chip's internal ADC.
'''

chip_configuration = 'default_chip.yaml'
##### monitor settings
##### default sensor config to 000000
#d_config = 0

local_configuration = {'MONITOR_CONFIG'  : 1096,
                       'SENSOR_CONFIG_0' : 4095,
                       'SENSOR_CONFIG_1' : 4095}



class ADC_readout():
    scan_id = "adc_scan"

    def open_multimeter_socket(self):


	# Create a TCP/IP socket
	sockXY = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sockC = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	# connecting to CurrentMeasurer.vi
	server_address = ('194.36.1.188', 6346)
	print >> sys.stderr, 'connecting to %s port %s' % server_address
	sockC.connect(server_address)
	time.sleep(1)




    def get_multimeter_value(self):
	##### USER INPUT =====
	current_measure_command = 'km'

	# list of data
	time_data = []
	elapsed_data = []
	c_data = []
	
    	start = time.time()    
	message = current_measure_command
	sockC.sendall(message)
	msgReceivedC = sockC.recv(1024)
	end = time.time()
	_c = [float(x) for x in re.findall("-?\d+.?\d*(?:[Ee]-\d+)?", msgReceivedC)]
	c_data.append(_c)
	deltaT = int(end - start)
	data = [time.strftime("%d-%m-%Y %H:%M:%S"), '\t', str(deltaT), '\t', str(_c[0]), '\n']
	#print time.strftime("%d-%m-%Y %H:%M:%S") + '\t' + str(deltaT) + ' s \t' + str(_c[0]) + ' V'       
	print 'value: '
	print str(_c[0])
	#time.sleep(0.1)
    	print "Scan complete..."
	return str(_c[0])

    def close_multimeter_socket(self):
	print >> sys.stderr, 'closing socket'
	sockC.close()
		




    def scan(self):

	#Multimeter communication:

	# Create a TCP/IP socket
	sockXY = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sockC = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	# connecting to CurrentMeasurer.vi
	server_address = ('194.36.1.188', 6346)
	print >> sys.stderr, 'connecting to %s port %s' % server_address
	sockC.connect(server_address)
	time.sleep(1)

	##### USER INPUT =====
	current_measure_command = 'km'
	message = current_measure_command
	


	sockC.sendall(message)
	msgReceivedC = sockC.recv(1024)
	end = time.time()
	_c = [float(x) for x in re.findall("-?\d+.?\d*(?:[Ee]-\d+)?", msgReceivedC)]
	multimeter = str(_c[0])

	print multimeter
	print multimeter
	print multimeter
	print multimeter
	print >> sys.stderr, 'closing socket'
	sockC.close()



if __name__ == "__main__":
    scan = ADC_readout()
    scan.scan()
    scan.close()

