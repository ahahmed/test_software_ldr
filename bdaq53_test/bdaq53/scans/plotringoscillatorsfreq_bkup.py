import time
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import os
import glob
import datetime
#from mon1 import monitor_datalogger_power_supply
from matplotlib.pyplot import figure
import basil
import visa
from operator import itemgetter
import ast
import sys, termios, tty, os, time
import csv
import tables as tb
from bitarray import bitarray
from datetime import datetime
from bdaq53.fifo_readout import FifoReadout
import logging
import matplotlib.pyplot as plt


def list_folders(directory_ringoscillators):
	dir_array = sorted(os.listdir(directory_ringoscillators))
	#print dir_array
	#raw_input('Enter ')
	return dir_array

def get_elements_line(x):
	array = []
	for i in range(len(x.split('	'))):
		a = x.split('	')[i]
		if a != '':
			array.append(a)
	return array


def get_ring_vector(dir_array,directory_ringoscillators):
	r0 = []
	r1 = []
	r2 = []
	r3 = []
	r4 = []
	r5 = []
	r6 = []
	r7 = []
	t = []
	idcount = []
	temperature = []
	#TODO if global pulse is changed, change also the duration variable
	global_pulse = 11 #value for global pulse 9 equal to 409.6e-9
	global_pulse_duration = 1638.4e-9
	j = 0
	for directory in dir_array:
		idcount.append(j)
		j = j+1
		dir_textfile = directory_ringoscillators + '/' + directory+'/output_data.dat'
		f = open(dir_textfile, 'r')
		lines = f.readlines()
		for i,x in enumerate(lines):
			if i == 1:				
				t.append('%.2f'%(float(x.split(' ')[2])))
				temperature.append('%.1f'%(float(x.split(' ')[0])))
			elif i == global_pulse:
				counts = get_elements_line(x)	
				print counts
				#print directory
				#raw_input('press button')		
				r0.append(float(counts[1])/(global_pulse_duration))
				r1.append(float(counts[2])/(global_pulse_duration))
				r2.append(float(counts[3])/(global_pulse_duration))
				r3.append(float(counts[4])/(global_pulse_duration))
				r4.append(float(counts[5])/(global_pulse_duration))
				r5.append(float(counts[6])/(global_pulse_duration))
				r6.append(float(counts[7])/(global_pulse_duration))
				r7.append(float(counts[8])/(global_pulse_duration))
				
			else:
				a = 0
		f.close()
	print t
	#raw_input('press ent')
	ring = [r0,r1,r2,r3,r4,r5,r6,r7]
	return ring, t, temperature, idcount

def plot_ro(ring, t, temperature,directory_ringoscillators,directory_plots,idcount):
	line_color = ['#ffcc00','#ff6633','#ff0066','#cc33ff', '#3333ff', '#0099ff', '#33cc66', '#99cc33','#11cc55']
	legend = ['110-CKND0','111-CKND4','112-INV0','113-INV4','114-NAND0','115-NAND4','116-NOR0','117-NOR4']

	figure(figsize=(15, 8))
	for i in range(len(ring)):
			plt.scatter(idcount,ring[i], label = legend[i],s=0.5, color = line_color[i])	
	plt.legend(loc=1)	
	plt.title('Ring Oscillators frequency')
	plt.xlabel('time (s)')
	plt.ylabel('Frequency (GHz)')	
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")	
	plt.savefig(directory_plots+'/'+timestamp+'_ringOscillators_freq_idcount.png', dpi=1000)
	



	#Plotting only rings with gates with driving strength 1
	figure(figsize=(15, 8))
	for i in range(len(ring)):
			if ((i==0) or (i==2) or (i==4) or (i==6)):
				plt.scatter(idcount,ring[i], label = legend[i],s=0.5, color = line_color[i])	
	plt.legend(loc=1)	
	plt.title('Ring Oscillators frequency')
	plt.xlabel('time (s)')
	plt.ylabel('Frequency (GHz)')	
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")	
	plt.savefig(directory_plots+'/'+timestamp+'_ringOscillators_freq_idcount_drivingstrenght0.png', dpi=1000)


	#Plotting only rings with gates with driving strength 4
	figure(figsize=(15, 8))
	for i in range(len(ring)):
			if ((i==1) or (i==3) or (i==5) or (i==7)):
				plt.scatter(idcount,ring[i], label = legend[i],s=0.5, color = line_color[i])	
	plt.legend(loc=1)	
	plt.title('Ring Oscillators frequency')
	plt.xlabel('time (s)')
	plt.ylabel('Frequency (GHz)')	
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")	
	plt.savefig(directory_plots+'/'+timestamp+'_ringOscillators_freq_idcount_drivingstrenght4.png', dpi=1000)



	figure(figsize=(15, 8))
	plt.scatter(idcount,temperature, label = 'Temperature', s=0.5,color = line_color[8])	
	plt.legend(loc=1)	
	plt.title('Temperature')
	plt.xlabel('time (s)')
	plt.ylabel('T(C)')
	plt.savefig(directory_plots+'/'+timestamp+'_ringOscillators_Temp_idcount.png')

	figure(figsize=(15, 8))
	plt.scatter(idcount,t, label = 'time', s=0.5,color = line_color[8])	
	plt.legend(loc=1)	
	plt.title('Time')
	plt.ylabel('time (s)')
	plt.xlabel('idmeas')
	plt.savefig(directory_plots+'/'+timestamp+'_ringOscillators_time.png')
	

if __name__ == "__main__":

    directory = '00_TEST_INFO'	
    if not os.path.exists(directory):
	os.makedirs(directory)	

    directory_plots = '00_TEST_INFO/ring_oscillator_plots'		
    if not os.path.exists(directory_plots):
	os.makedirs(directory_plots)


    directory_ringoscillators = '00_TEST_INFO_rad/ring_oscillators'
    dir_array = list_folders(directory_ringoscillators)
    ring, t, temperature,idcount = get_ring_vector(dir_array,directory_ringoscillators)
    plot_ro(ring, t, temperature,directory_ringoscillators,directory_plots,idcount)





