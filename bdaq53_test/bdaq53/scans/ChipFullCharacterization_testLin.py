import time
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import os
import glob
import datetime
from bdaq53.fifo_readout import FifoReadout
import logging
import sys, select

def waiting_input():
	print "Type 0  and enter if you want to stop. You have ten seconds to answer!"
	i, o, e = select.select( [sys.stdin], [], [], 5 )
	if (i):
		t = sys.stdin.readline().strip()
	else:
	 	print "You said nothing!, the program will keep on running"
		t = 1
	return t




def ChipCharact():
	globalTime = '3'
	dose = '10'

	directory = 'Chip_Annealing_linFE'	
	if not os.path.exists(directory):
    		os.makedirs(directory)
	

	t0 = time.time()
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	
	#testTag = 'AfterDoseOf_100Mrad_RoomT_'
	directory = directory + '/' + dose + 'Mrad_' + timestamp
	os.system('mkdir '+directory)


	######################################################################################
	os.system('cp 0LIN.yaml default_chip.yaml')
	
	#Tunning taking as a reference the mask of the previous step:
		
	os.system("python scan_noise_occupancy_lin.py")	 	
	os.system("python meta_tune_local_threshold_lin_ini.py")
	os.system("mkdir lin_meta_tune_threshold_simple_lastTune")
	os.system("cp output_data/*mask.h5 lin_meta_tune_threshold_simple_lastTune/")
	os.system("python scan_analog_lin.py")
	#os.system("cp -r output_data  lin_meta_tune_threshold_simple_lastTune") 
	os.system("mv output_data "+ directory + '/lin_meta_tune_threshold_simple_0LIN') 




	os.system('cp 1LIN.yaml default_chip.yaml')
	
	#Tunning taking as a reference the mask of the previous step:
	os.system("mv lin_meta_tune_threshold_simple_lastTune output_data")	
	os.system("python scan_noise_occupancy_lin.py")	 	
	os.system("python meta_tune_local_threshold_lin.py")
	os.system("mkdir lin_meta_tune_threshold_simple_lastTune")
	os.system("cp output_data/*mask.h5 lin_meta_tune_threshold_simple_lastTune/")
	
	os.system("python scan_analog_lin.py")
	#os.system("cp -r output_data  lin_meta_tune_threshold_simple_lastTune") 
	os.system("mv output_data "+ directory + '/lin_meta_tune_threshold_simple_1LIN') 





	os.system('cp 2LIN.yaml default_chip.yaml')
	
	#Tunning taking as a reference the mask of the previous step:
	os.system("mv lin_meta_tune_threshold_simple_lastTune output_data")	
	os.system("python scan_noise_occupancy_lin.py")	 	
	os.system("python meta_tune_local_threshold_lin.py")
	os.system("mkdir lin_meta_tune_threshold_simple_lastTune")
	os.system("cp output_data/*mask.h5 lin_meta_tune_threshold_simple_lastTune/")
	
	os.system("python scan_analog_lin.py")
	#os.system("cp -r output_data  lin_meta_tune_threshold_simple_lastTune") 
	os.system("mv output_data "+ directory + '/lin_meta_tune_threshold_simple_2LIN') 




	os.system('cp 3LIN.yaml default_chip.yaml')
	
	#Tunning taking as a reference the mask of the previous step:
	os.system("mv lin_meta_tune_threshold_simple_lastTune output_data")	
	os.system("python scan_noise_occupancy_lin.py")	 	
	os.system("python meta_tune_local_threshold_lin.py")
	os.system("mkdir lin_meta_tune_threshold_simple_lastTune")
	os.system("cp output_data/*mask.h5 lin_meta_tune_threshold_simple_lastTune/")
	
	os.system("python scan_analog_lin.py")
	#os.system("cp -r output_data  lin_meta_tune_threshold_simple_lastTune") 
	os.system("mv output_data "+ directory + '/lin_meta_tune_threshold_simple_3LIN') 



	os.system('cp 4LIN.yaml default_chip.yaml')
	
	#Tunning taking as a reference the mask of the previous step:
	os.system("mv lin_meta_tune_threshold_simple_lastTune output_data")	
	os.system("python scan_noise_occupancy_lin.py")	 	
	os.system("python meta_tune_local_threshold_lin.py")
	os.system("mkdir lin_meta_tune_threshold_simple_lastTune")
	os.system("cp output_data/*mask.h5 lin_meta_tune_threshold_simple_lastTune/")
	
	os.system("python scan_analog_lin.py")
	#os.system("cp -r output_data  lin_meta_tune_threshold_simple_lastTune") 
	os.system("mv output_data "+ directory + '/lin_meta_tune_threshold_simple_4LIN')



	

	'''
	#using linear triming TDACs of last tune for a threshold scan:
	os.system("python scan_noise_occupancy_lin.py")
	os.system("cp -r lin_meta_tune_threshold_simple_lastTune_200Mrad/* output_data ") 	
	os.system("python scan_threshold_lin.py")
	os.system("mv output_data "+ directory + '/lin_threshold_scan_lin_withlasttuneat200Mrad')
	'''

if __name__ == '__main__':
	 ChipCharact()
