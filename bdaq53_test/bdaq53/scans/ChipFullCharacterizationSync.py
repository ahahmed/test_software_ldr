import time
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import os
import glob
import datetime
from bdaq53.fifo_readout import FifoReadout
import logging
import sys, select

def waiting_input():
	print "Type 0  and enter if you want to stop. You have ten seconds to answer!"
	i, o, e = select.select( [sys.stdin], [], [], 5 )
	if (i):
		t = sys.stdin.readline().strip()
	else:
	 	print "You said nothing!, the program will keep on running"
		t = 1
	return t


def ChipCharact():
	globalTime = sys.argv[1]
	dose = sys.argv[2]

	directory = 'Chip_Characterization'	
	if not os.path.exists(directory):
    		os.makedirs(directory)
	

	t0 = time.time()
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	
	#testTag = 'AfterDoseOf_100Mrad_RoomT_'
	directory = directory + '/' + dose + 'Mrad_' + timestamp
	os.system('mkdir '+directory)
	
	#Temperature sensors, NTC, ring oscillators:
	
	os.system('python scan_temp_ntc_accumulative.py '+ globalTime + ' ' + dose )
	os.system('python scan_ring_oscillators.py '+ globalTime + ' ' + dose )
	os.system('python scan_ADC.py '+ globalTime + ' ' + dose )
	os.system('python test_dac_linearity_adc_externalmultim.py'+ globalTime + ' ' + dose )
	os.system("mv output_data "+ directory + '/dac_linearity')  
	os.system('python  scan_temperature_sensor.py '+ globalTime + ' ' + dose )
	#All FE tests:
	os.system("python scan_digital.py")
	os.system("mv output_data "+ directory + '/all_digital_scan')  
	os.system('python scan_temp_ntc_accumulative.py '+ globalTime + ' ' + dose )

	os.system("python scan_noise_occupancy.py")
	os.system("python scan_analog.py")
	os.system("mv output_data "+ directory + '/all_analog_scan') 
	os.system('python scan_temp_ntc_accumulative.py '+ globalTime + ' ' + dose )
	s
	################# LOADING CONFIG FILE FOR CHARACT ##################################
	os.system('cp default_chip_whileCharacterization.yaml default_chip.yaml')
	

	#####################################################################################
	#Sync FE:
	os.system("python scan_noise_occupancy_sync.py")
	os.system("python scan_analog_sync.py")
	os.system("mv output_data "+ directory + '/sync_analog_scan') 


	os.system("python scan_noise_occupancy_sync.py")
	os.system("python scan_threshold_sync.py")
	os.system("python calibrate_tot_sync.py")
	os.system("mv output_data "+ directory + '/sync_threshold_scan_width6_400ns') 

	 
	os.system("python scan_noise_occupancy_sync.py")
	os.system("python scan_threshold_sync_width8.py")
	os.system("mv output_data "+ directory + '/sync_threshold_scan_width8_1p6us') 

	
	os.system("python scan_noise_occupancy_sync.py")
	os.system("python scan_threshold_sync_width9.py")
	os.system("mv output_data "+ directory + '/sync_threshold_scan_width9_3p2us') 
	
	os.system('python scan_temp_ntc_accumulative.py '+ globalTime + ' ' + dose )  

























	tend = time.time()
	print 'Total time needed: ', tend-t0

	directory = '00_TEST_INFO'
	if not os.path.exists(directory):
    		os.makedirs(directory)
	
	directory = directory + '/time_characterizations'
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")

	if not os.path.exists(directory):
    		os.makedirs(directory)
		out_T = open( directory + '/'+"ACCUMULATIVE_time_characterizations.txt", "a")
		tobewritten = "%Timestamp   TimeNeededForCharact(s) GlobalTime(s)  Dose(Mrad)\n"
		out_T.write(tobewritten) 
		out_T.close()

	out_T = open( directory + '/'+"ACCUMULATIVE_time_characterizations.txt", "a")
	tobewritten =  str(timestamp) + " " + str(tend-t0) + " " + str(time.time()-float(globalTime))+ ' ' + dose	
	tobewritten = tobewritten + "\n"
	out_T.write(tobewritten)
	out_T.close()

if __name__ == '__main__':
	 ChipCharact()
