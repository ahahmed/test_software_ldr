#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#
import tables as tb
from tqdm import tqdm
import sys, os, time
from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting

from bdaq53.rd53a import RD53A
from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis_utils
from bdaq53.fifo_readout import FifoReadout
#from bdaq53.serial_com import serial_com
from datetime import datetime
import logging
import yaml
import time

voltage_mux_array=['ADCbandgap','CAL_MED_left','CAL_HI_left','TEMPSENS_1','RADSENS_1','TEMPSENS_2','RADSENS_2','TEMPSENS_4','RADSENS_4','VREF_VDAC','VOUT_BG','IMUX_out','VCAL_MED','VCAL_HIGH','RADSENS_3','TEMPSENS_3','REF_KRUM_LIN','Vthreshold_LIN','VTH_SYNC','VBL_SYNC','VREF_KRUM_SYNC','VTH_HI_DIFF','VTH_LO_DIFF','VIN_Ana_SLDO','VOUT_Ana_SLDO','VREF_Ana_SLDO','VOFF_Ana_SLDO','ground','ground1','VIN_Dig_SLDO','VOUT_Dig_SLDO','VREF_Dig_SLDO','VOFF_Dig_SLDO','ground2']

current_mux_array=['Iref','IBIASP1_SYNC','IBIASP2_SYNC','IBIAS_DISC_SYNC','IBIAS_SF_SYNC','ICTRL_SYNCT_SYNC','IBIAS_KRUM_SYNC','COMP_LIN','FC_BIAS_LIN','KRUM_CURR_LIN','LDAC_LIN','PA_IN_BIAS_LIN','COMP_DIFF','PRECOMP_DIFF','FOL_DIFF','PRMP_DIFF','LCC_DIFF','VFF_DIFF','VTH1_DIFF','VTH2_DIFF','CDR_CP_IBIAS','VCO_BUFF_BIAS','VCO_IBIAS','CML_TAP_BIAS0','CML_TAP_BIAS1','CML_TAP_BIAS2']



'''
    This basic test scans over the values of the selected DAC and
    measures the resulting analog value with the chip's internal ADC.
'''

chip_configuration = 'default_chip.yaml'
##### monitor settings
##### default sensor config to 000000
#d_config = 0

local_configuration = {'MONITOR_CONFIG'  : 1096,
                       'SENSOR_CONFIG_0' : 4095,
                       'SENSOR_CONFIG_1' : 4095}



class ADC_readout():
    scan_id = "adc_scan"



    def __init__(self,**kwargs):
        self.chip=RD53A()
        #self.chip.power_on(**kwargs)
        self.chip.init()
        self.fifo_readout = FifoReadout(self.chip)
        self.chip.init_communication()
        self.chip.set_dacs(**kwargs)
        self.chip.enable_monitor_filter()
        self.chip.enable_monitor_data()
	self.chip.enable_core_col_clock(range(50))   # here we can disable clock for a part of the array


    def get_adc_value_v(self,mux_selection):
        adc_value={}
        try:
            timeout=10000
            self.chip.get_ADC(typ='U' ,address=mux_selection)
            for _ in range(timeout):
                if self.chip['FIFO'].get_FIFO_SIZE() > 0:
                    userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(self.chip['FIFO'].get_data()))
                    if len(userk_data)>0:
                        adc_value[mux_selection] = userk_data['Data'][0]
                        return temperatures
            else:
                logging.error('Timeout while waiting for ADC measurement.')
        except:
            logging.error('There was an error while receiving the chip status.')


    def scan(self,**kwargs):
	globalTime = sys.argv[1]
	dose = sys.argv[2]

	directory = '00_TEST_INFO'
	if not os.path.exists(directory):
    		os.makedirs(directory)
	
	directory = directory + '/adc_measurements'
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	if not os.path.exists(directory):
    		os.makedirs(directory)
		out_v = open( directory + '/'+"ACCUMULATIVE_adc_v.txt", "a")		
		tobewritten = "%Timestamp  T_NTC(C) GlobalTime(s)  DoseRange(Mrad) ADCbandgap CAL_MED_left CAL_HI_left TEMPSENS_1 RADSENS_1 TEMPSENS_2 RADSENS_2 TEMPSENS_4 RADSENS_4 VREF_VDAC VOUT_BG IMUX_out VCAL_MED VCAL_HIGH RADSENS_3 TEMPSENS_3 REF_KRUM_LIN Vthreshold_LIN VTH_SYNC VBL_SYNC VREF_KRUM_SYNC VTH_HI_DIFF VTH_LO_DIFF VIN_Ana_SLDO VOUT_Ana_SLDO VREF_Ana_SLDO VOFF_Ana_SLDO ground ground1 VIN_Dig_SLDO VOUT_Dig_SLDO VREF_Dig_SLDO VOFF_Dig_SLDO ground2\n"		
		out_v.write(tobewritten) 
		out_v.close()

	directory_textfile_v = directory + '/'+"ACCUMULATIVE_adc_v.txt"
	directory_notextfile = directory

	
	out_v = open( directory + '/'+"ACCUMULATIVE_adc_v.txt", "a")
	timestamp = str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
	T_FMC = self.chip._measure_temperature_ntc_CERNFMC()
	tobewritten =  str(timestamp) + " " + str("%.4f" %T_FMC) + " " + str(time.time()-float(globalTime))+ ' ' + dose + ' ' 	
        for mux_selection in voltage_mux_array:
            ADC =  self.get_adc_value_v(mux_selection)
	    tobewritten = tobewritten + str(ADC) + ' ' 

	tobewritten = tobewritten + "\n"
	out_v.write(tobewritten) 
        out_v.close()





if __name__ == "__main__":
    with open(chip_configuration, 'r') as f:
        configuration = yaml.load(f)
    configuration.update(local_configuration)    
    adc=ADC_readout()
    adc.scan()


'''
if __name__ == "__main__":
    scan = DACLinTest()
    scan.start(**local_configuration)
    scan.close()
'''
