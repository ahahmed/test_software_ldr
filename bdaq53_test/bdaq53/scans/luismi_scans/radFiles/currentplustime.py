import time
import datetime

def time_after_sec():
	loop = 1
	now_plus_time_interval = 0
	while (loop>0):
	
		now = datetime.datetime.now()
		print "Time right now is: ", now 
		print "This script calculates date after a certain amount of time introduced by the user."
		print " Select time units: "
		print "1 for SECONDS"
		print "2 for MINUTES"
		print "3 for HOURS"
		units = raw_input("write a numer 1-3 and press enter : ")
		print units 
		delay = raw_input("write delay from current date in the units selected in the previous step: " )
		print (units == '1') or (units == '2') or (units == '3')
		if ((units == '1') or (units == '2') or (units == '3')):
			if (units == '1'):	
				now_plus_time_interval = datetime.datetime.now() + datetime.timedelta(seconds = int(delay))
			elif (units == '2'):
				now_plus_time_interval = datetime.datetime.now() + datetime.timedelta(minutes = int(delay))
			elif (units == '3'):
				now_plus_time_interval = datetime.datetime.now() + datetime.timedelta(hours = int(delay))
			print "The current date plus the time interval is : ", now_plus_time_interval
			loop = raw_input( "Introduce 1 to repeat a calculation, 0 to exit")

		else:
			loop = raw_input( "Units selected wrongly, introduce 1 to try again, 0 to exit:  ")

time_after_sec()
