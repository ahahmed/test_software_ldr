import time
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import os
import glob
import datetime
from bdaq53.fifo_readout import FifoReadout
import logging
import sys, select

def times_tests():
	time_intervals=[] 
	#TODO change next 2 lines, with the starting dose and dose_steps_mrad starting with the following dose value to reach.
	starting_dose = 10
	#TODO change next list, the first value should be the next dose reached
	dose_steps_mrad = [20,30,40,50,60,70,80,90,100,150,200,250,300,350,400,450,500]#,600,700,800,900,1000]#TODO if charact is done at X, this vector has to start at X+ (x+1)
	#TODO update value of dose, next line
	dose_rate_Mrad_h = 3.956 # Dose for 11.4 cm from the source, 150KeV, 20mA.  
	dose_rate_Mrad_s = dose_rate_Mrad_h/3600
	time_for_dose_steps_s = [x*3600/dose_rate_Mrad_h for x in dose_steps_mrad] #time needed for each value of dose
	starting_time = starting_dose*3600/dose_rate_Mrad_h 

	now = datetime.datetime.now()
	time_acum = 0
	for index in range(len(time_for_dose_steps_s)):	
		if (index == 0):
			time_intervals.append(time_for_dose_steps_s[index]-starting_time)
		else: 
			time_intervals.append(time_for_dose_steps_s[index] - time_for_dose_steps_s[index-1] )
		time_acum = time_acum + time_intervals[index]	
		now_plus_time_interval = now + datetime.timedelta(minutes = time_acum/60)
		print "Dose after this time ", time_acum, " (s) is ",  time_acum/3600 * dose_rate_Mrad_h+starting_dose, " and will be reached in ", now_plus_time_interval
	return time_intervals, dose_steps_mrad



if __name__ == '__main__':
	t0 = time.time()
	time_intervals,  dose_steps_mrad = times_tests()

	print time_intervals
	while(time_intervals):
		t1 = time.time()
		t_loop = t1 - t0
		if(t_loop < float(time_intervals[0])*0.001):
			print 'trial running, now irradiation'
			time.sleep(1)


		else:
			print 'trial running, now measurements'
			tag =  dose_steps_mrad[0]
			print 'Dose reached is ' + str(tag)
			del dose_steps_mrad[0]
			t0 = time.time()
			del time_intervals[0]
			print "Last time interval vector after removal in seconds is is: ", time_intervals
			print "Next measurement in ", time_intervals[0]/60, "minutes"
			now = datetime.datetime.now()
			print "Time right now is: ", now 
			now_plus_time_interval = now + datetime.timedelta(minutes = time_intervals[0]/60)
			print "Next measurement will be done in: ", now_plus_time_interval  
			print "******************************************************************************************"
			print "__________________________________________________________________________________________"
			print " "
			print " "

	
	
'''dose = starting_dose +dose_rate_Mrad_h*(time.time()-t)/3600'''







