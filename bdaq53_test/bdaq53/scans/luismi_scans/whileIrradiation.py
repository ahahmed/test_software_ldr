import time
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import os
import glob
import datetime
from bdaq53.fifo_readout import FifoReadout
import logging
import sys, select
#to kill the script, just press Ctrl Z and then type kill %1

def waiting_input():
	print "Type 0  and enter if you want to stop. You have 5 seconds to answer!"
	i, o, e = select.select( [sys.stdin], [], [], 5 )
	if (i):
		t = sys.stdin.readline().strip()
	else:
	 	print "You said nothing!, the program will keep on running"
		t = 1
	return t

def remove_last_file():
	list_of_files = glob.glob('output_data/*') # * means all if need specific format then *.csv
	latest_file = max(list_of_files, key=os.path.getctime)
	remove = "rm "+ latest_file 
	os.system(remove)

#Functions for analog scans
def analog_scan_no_output():
	os.system("bdaq53 scan_analog_wholematrix")
	for i in range (0,5):#removes the files created by the scan_analog (5)
	    remove_last_file()

def analog_scan_to_trash():
	directory = 'toTRASH_removeTHIS'
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	if not os.path.exists(directory):
    		os.makedirs(directory)
	os.system("bdaq53 scan_analog")
	os.system("mv output_data "+ directory + '/'+timestamp)

def annealingBiased():
	keepon = 1
	while (keepon > 0):
		analog_scan_no_output()
		keepon = waiting_input()

def whileIrradiation():
	keepon = 1
	while (keepon > 0):
		os.system('python scan_temp_ntc_accumulative.py')
		analog_scan_to_trash()
		os.system('python scan_temp_ntc_accumulative.py')
		os.system('python scan_temp_sensors.py')
		os.system('python scan_temp_ntc_accumulative.py')
		os.system('python scan_ring_oscillators.py')
		os.system('python scan_temp_ntc_accumulative.py')
		keepon = waiting_input()

if __name__ == '__main__':
	 whileIrradiation()






