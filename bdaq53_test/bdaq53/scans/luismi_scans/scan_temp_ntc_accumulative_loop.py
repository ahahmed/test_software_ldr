import time
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import os
import glob
import datetime
from mon1 import monitor_datalogger_power_supply
import basil
import visa
from operator import itemgetter
import ast
import sys, termios, tty, os, time
import csv
import tables as tb
from bitarray import bitarray
from datetime import datetime

from bdaq53.fifo_readout import FifoReadout

import logging

#fmc_HPC = False #set true for FMC_HPC or false if FMC_LPC
#class needed to measure the temeprature with the sensors inside the rd53a chip
class Temperature_readout():
    scan_id = "temperature_scan"

    def __init__(self,**kwargs):
        self.chip=RD53A()

        self.chip.power_on(**kwargs)
        self.chip.init()

        self.fifo_readout = FifoReadout(self.chip)

        self.chip.init_communication()
        self.chip.enable_monitor_filter()
        self.chip.enable_monitor_data()

    def get_temperature(self,sensor):
        temperatures={}
        try:
            timeout=1000
            self.chip.get_ADC(typ='U' ,address=sensor)
            for _ in range(timeout):
                if self.chip['FIFO'].get_FIFO_SIZE() > 0:
                    userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(self.chip['FIFO'].get_data()))
                    if len(userk_data)>0:
                        temperatures[sensor] = userk_data['Data'][0]
                        return temperatures
            else:
                logging.error('Timeout while waiting for chip Temperature.')
		temperatures={}
		return temperatures
        except:
            logging.error('There was an error while receiving the chip status.')


    def scan(self,**kwargs):
        sensor_array=['TEMPSENS_1','TEMPSENS_2','TEMPSENS_3','TEMPSENS_4']
	index = 0
	temperature = [0,0,0,0]
        for sensor in sensor_array:
            temperature[index] = self.get_temperature(sensor)
	    index = index + 1 
	self.chip.close()
	return temperature



#This measures the temperature with the sensors inside the rd53a chip
def temp_rd53a():
	T_chip = Temperature_readout()	
	T_RD53A = T_chip.scan()
	T_chip.chip.close()
	return T_RD53A

#This measures the temperature with the FMC card
def meas_temperature():
	rd53a_chip = RD53A()  
	rd53a_chip.init()                                                                # Initialize chip
	T = rd53a_chip._measure_temperature_ntc_CERNFMC()	
	rd53a_chip.close()
        print T
        return T


def scan_temperature():
	#time_between_samples = 1 #time between measurements, in seconds
	timestamp = time.strftime("%d_%m_%Y_%H:%M:%S")

	directory = 'temperature'
	if not os.path.exists(directory):
    		os.makedirs(directory)
	
	directory = directory + '/temperature_ntc'
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	if not os.path.exists(directory):
    		os.makedirs(directory)
		out_T = open( directory + '/'+"ACCUMULATIVE_temperature_ntc.txt", "a")
		tobewritten = "%Timestamp   T_NTC(C) VDDA-TO VDDD-TO VDDA-TO-WC VDDD-TO-WC VDDA-BGPV VDDD-BGPV VDDA-BGPV-WC VDDD-BGPV-WC VDDD-LBNL VDDA-LBNL VDDD-LBNL-WC VDDA-LBNL-WC Idig Iana   \n"
		out_T.write(tobewritten) 
		out_T.close()


	out_T = open( directory + '/'+"ACCUMULATIVE_temperature_ntc.txt", "a")
	
	#TODO if rd53a sensors used, use the second tobewritten for the first row of the text file. Otherwise, leave it commented
	#tobewritten = "%Timestamp  T_FMC(C)  \n"
	#tobewritten = "%Timestamp  T_FMC(C)  T_RD53A_1(C)  T_RD53A_2(C)  T_RD53A_3(C)  T_RD53A_4(C)"+"\n"


	rd53a_chip = RD53A()  
	rd53a_chip.init()                                                                # Initialize chip
	
	for i in range(1):
		#out_T.write(tobewritten) 
		#timestamp = datetime.datetime.now()
		timestamp = str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

		#TEMPERATURE measurement
		print '******************************************************'
		print '******************************************************'
		T_FMC = rd53a_chip._measure_temperature_ntc_CERNFMC()
		print '******************************************************'
		print '******************************************************'
		tobewritten =  str(timestamp) + " " + str("%.3f" %T_FMC) 		
		#POWER measurement
		
		powerMeasurements = monitor_datalogger_power_supply()
		for item in powerMeasurements[0]:
			tobewritten = tobewritten + ' ' + str("%.3f" %abs(item))
		tobewritten = tobewritten + "\n"
		out_T.write(tobewritten) 
		
		#time.sleep(time_between_samples)

		#TODO uncomment next lines if internal sensors are also used (to be checked if this works fine). Also lines 93 and 94 must be commented.
		#rd53a_chip.close()
		#T_RD53A = temp_rd53a()
		#tobewritten = str(timestamp) + " " + str(T_FMC) + " " + str(T_RD53A[0]['TEMPSENS_1']) + " " + str(T_RD53A[1]['TEMPSENS_2']) + " " + str(T_RD53A[2]['TEMPSENS_3']) + " " + str(T_RD53A[3]['TEMPSENS_4'])+"\n"
		#rd53a_chip = RD53A()  
		#rd53a_chip.init() 
		
	rd53a_chip.close()
	out_T.close()



if __name__ == '__main__':
    while(True):
    	scan_temperature()


