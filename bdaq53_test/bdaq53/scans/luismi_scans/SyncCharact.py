import time
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import os
import glob
import datetime
from bdaq53.fifo_readout import FifoReadout
import logging
import sys, select

def waiting_input():
	print "Type 0  and enter if you want to stop. You have ten seconds to answer!"
	i, o, e = select.select( [sys.stdin], [], [], 5 )
	if (i):
		t = sys.stdin.readline().strip()
	else:
	 	print "You said nothing!, the program will keep on running"
		t = 1
	return t

def remove_last_file():
	list_of_files = glob.glob('output_data/*') # * means all if need specific format then *.csv
	latest_file = max(list_of_files, key=os.path.getctime)
	remove = "rm "+ latest_file 
	os.system(remove)

#Functions for analog scans
def analog_scan_no_output():
	os.system("bdaq53 scan_analog_wholematrix")
	for i in range (0,5):#removes the files created by the scan_analog (5)
	    remove_last_file()



#It goes through all the defaul_chip files generated
def ChipCharact():
	directory = 'Chip_Characterization'	
	if not os.path.exists(directory):
    		os.makedirs(directory)
	

	t0 = time.time()
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	testTag = raw_input('Introduce the tag for the test, WITHOUT SPACES:  ')
	#testTag = 'irradiation_'
	
	#testTag = 'AfterDoseOf_100Mrad_RoomT_'
	directory = directory + '/' + testTag + '_' + timestamp
	os.system('mkdir '+directory)
	


	#Sync FE:
	os.system("bdaq53 scan_noise_occupancy_sync")
	os.system("bdaq53 scan_analog_sync")
	os.system("mv output_data "+ directory + '/sync_analog_scan') 
	os.system('python scan_temp_ntc_accumulative.py')

	os.system("bdaq53 scan_noise_occupancy_sync")
	os.system("bdaq53 scan_threshold_sync")
	os.system("mv output_data "+ directory + '/sync_threshold_scan') 
	os.system('python scan_temp_ntc_accumulative.py')  



	tend = time.time()
	print 'Total time needed: ', tend-t0

if __name__ == '__main__':
	 ChipCharact()
