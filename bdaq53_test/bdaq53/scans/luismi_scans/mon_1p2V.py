import visa
#import usbtmc
import time
from operator import itemgetter
import ast
import sys, termios, tty, os, time
import csv
from datetime import date
import datetime
import numpy as np
import datetime

import time

moment=time.strftime("%Y-%b-%d-%H-%M",time.localtime())

##### Header #####

header = [
["time stamp", "VDDA-TO","VDDD-TO ","VDDA-TO-WC","VDDD-TO-WC",
"VDDA-BGPV", "VDDD-BGPV","VDDA-BGPV-WC","VDDD-BGPV-WC",
"VDDD-LBNL", "VDDA-LBNL", "VDDD-LBNL-WC", "VDDA-LBNL-WC",
 "Current-digital", "Current-analog"]
]

'''
# creating a directory "directory" to save data
filename = "measurements.csv"
#path = "/home/rd53a-testing4/scratch/bdaq53_06_2/mon1/data" #<---- change this to the respective computers directory path
path = 
if not os.path.isdir(path):
    os.makedirs(path)
filepath = os.path.join(path, filename)
## uncomment ths part to save a header line
file = open(filepath, 'a')
writer = csv.writer(file)
writer.writerows(header)
file.close()
'''


# Setting up the connection with pyvisa pyhton lib.
rm = visa.ResourceManager('@py')
print(rm.list_resources())

#raw_input('press enter')

inst = rm.open_resource('ASRL/dev/ttyUSB0::INSTR')
inst2 = rm.open_resource('ASRL/dev/ttyACM0::INSTR')


# reset the devices
'''
inst2.write("*RST")
time.sleep(2)
'''


inst2.write("OPALL 0")
print 'power cycling...'
time.sleep(3)
print 'Power cycle done'
inst2.write("OPALL 1")



# User interface for setting the current and voltage limit
'''
set_volt_VDDD = raw_input("choose VDDD [V]:  ")
set_volt_VDDA = raw_input("choose VDDA [V]:  ")
set_Current_VDDD = raw_input("choose currentlimit digital [A]:  ")
set_Current_VDDA = raw_input("choose currentlimit analog [A]: ")
'''
set_volt_VDDA = "1.2"
set_volt_VDDD = "1.2"
set_Current_VDDD = "0.9"
set_Current_VDDA = "0.9"

print set_volt_VDDD
print set_volt_VDDA
print set_Current_VDDD
print set_Current_VDDA


print ('Point 1 debugging reached' )

#Set voltage limit and current limit
inst2.write("V1 "+set_volt_VDDA)
inst2.write("V2 "+set_volt_VDDD)

print ('Point 3 debugging reached' )

inst2.write("I1 "+set_Current_VDDD)
inst2.write("I2 "+set_Current_VDDA)
inst2.write("OP1 1")
inst2.write("OP2 1")
print ('Point 3 debugging reached' )



# function for extracting digits from string (current values form powersupply)
def get_num(x):
    return float(''.join(ele for ele in x if ele.isdigit() or ele == '.'))



#### function for reading data from the data logger logger and saving to an array [Mdata] #####

# def monitor_ps():
#     current1 = inst2.query("I1O?")
#     current2 = inst2.query("I2O?")
#     currentarr = []
#     currentarr.extend((current1,current2))
#     print currentarr
#     return currentarr
def monitor_datalogger_power_supply():
    
    inst.write(":CONFigure:VOLTage:DC 2,(@301:312)")
    inst.write(":SENSe:VOLTage:DC:NPLC 1,(@301:312)")
    inst.write(":SENSe:VOLTage:DC:NPLC 1,(@301:312)")
    inst.write(":ROUTe:SCAN (@301:312)")
    inst.write(":ROUTe:CHANnel:DELay 0.01,(@301:312)")
################ processing data ##########3
    


    time.sleep(0.1)
    
    (inst.write(":INITiate"))
    
    time.sleep(2.1)
    print(inst.query(":DATA:POINts? "))
    
    dataarr = (inst.query(":DATA:REMove? 12"))

        # print(dataarr)

    
    # saving data array to 12 different int variables using the split() function
    ch1, ch2, ch3, ch4, ch5, ch6, ch7, ch8, ch9, ch10, ch11, ch12 = dataarr.split(',')
    # time.sleep(0.1)

    ch1  =  (float(ch1))
    ch2  =  (float(ch2))
    ch3  =  (float(ch3))
    ch4  =  (float(ch4))
    ch5  =  (float(ch5))
    ch6  =  abs(float(ch6))
    ch7  =  (float(ch7))
    ch8  =  (float(ch8))
    ch9  =  (float(ch9))
    ch10 =  (float(ch10))
    ch11 =  (float(ch11))
    ch12 =  (float(ch12))
    #
    # current1 = inst2.query("I1O?")

    # print "\n"
    # print ch1,"\n" ,ch2,"\n" ,ch3,"\n" , ch4,"\n" , ch5,"\n" , ch6,"\n" , ch7,"\n" , ch8, "\n" , ch9,"\n" , ch10,"\n" , ch11,"\n" , ch12
    # print "\n"

    # Asking the power supply for the current vaules of both ports
    current1 = get_num(inst2.query("I1O?"))
    current2 = get_num(inst2.query("I2O?"))


    
# saving the variables to a new array called Mdata. # float values and 4 dicimals
    Mdata =[
          [float("{0:.4f}".format(ch1)),
          float("{0:.4f}".format(ch2)), float("{0:.4f}".format(ch3)),
          float("{0:.4f}".format(ch4)), float("{0:.4f}".format(ch5)),
          abs(float("{0:.4f}".format(ch6))), float("{0:.4f}".format(ch7)),
          float("{0:.4f}".format(ch8)), float("{0:.4f}".format(ch9)),
          float("{0:.4f}".format(ch10)),float("{0:.4f}".format(ch11)),
          float("{0:.4f}".format(ch12)), current1, current2],
    ]

    return Mdata

### The main part of this module

if __name__ == "__main__":
	while True:

	    file = open(filepath, 'a')
	    with file:
		writer = csv.writer(file)
		
		writer.writerows(monitor_datalogger_power_supply())
	    file.close()
