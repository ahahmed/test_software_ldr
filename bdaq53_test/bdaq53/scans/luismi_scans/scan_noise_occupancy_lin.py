#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic scan sends triggers without injection
    into enabled pixels to identify noisy pixels.
'''

import zlib # workaround

import tables as tb
import numpy as np

from tqdm import tqdm

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting


local_configuration = {
    # Scan parameters
    'start_column' : 128,
    'stop_column'  : 264,
    'start_row'    : 0,
    'stop_row'     : 192,
    'maskfile'     : 'auto',
    'mask_diff'    : False,

    'n_triggers'   : 1e6
    }


class NoiseOccScan(ScanBase):
    scan_id = "noise_occupancy_scan"

    def scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192,
             n_triggers=1e6, wait_cycles=40, **kwargs):
        '''
        Noise occupancy scan main loop

        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.

        n_triggers : int
            Number of triggers to send.
        wait_cycles : int
            Time to wait in between trigger packages in units of sync commands.
        '''
        
        self.n_pixels = (stop_column-start_column)*(stop_row-start_row)
        n_triggers = int(n_triggers)
        if n_triggers > 50000:
            steps = range(0, n_triggers, 50000)
        else:
            steps = [n_triggers]
            
        #disable injection
        self.chip.enable_macro_col_cal(macro_cols=None) 
        self.chip.injection_mask[:,:] = False
        self.chip.write_masks()
        
        
        #TODO: long therm solution may be to use inject_analog_single for all FE
        #TODO: make CONF_LATENCY less

        trigger_data = self.chip.send_trigger(trigger=0b1111, write=False)*8 #effectively we send x32 triggers
        trigger_data += self.chip.write_sync_01(write=False)*wait_cycles
        
        #if sync enabled
        if any(x in range(0, 8*16) for x in range(start_column, stop_column)):
            self.logger.info('Sunc AFE enabled: Using analog injection based commnad.')
            trigger_data = self.chip.inject_analog_single(send_ecr=True, wait_cycles=20, write=False)
    
        self.logger.info('Starting scan...')     
        pbar = tqdm(total=len(steps)*50000)        
        with self.readout():
            for _ in steps:
                self.chip.write_command(trigger_data, repetitions=50000)
                pbar.update(50000)
                    
        pbar.close()
        self.logger.info('Scan finished')


    def analyze(self, create_pdf=True, create_mask_file=True, update_mask=True):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', store_hits=True) as a:
            a.analyze_data()
            self.disable_mask = np.ones((400,192), dtype=bool)
            with tb.open_file(a.analyzed_data_file) as in_file:
                hits = in_file.root.Hits[:]
                for hit in hits:
                    self.disable_mask[hit['col'], hit['row']] = False
            n_hit_pixels = np.count_nonzero(np.concatenate(np.invert(self.disable_mask)))
            
        if create_mask_file: self.save_disable_mask(update=update_mask)
            
        if create_pdf:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()
        
        return n_hit_pixels, round(float(n_hit_pixels)/float(self.n_pixels)*100.,2), len(hits), self.disable_mask


if __name__ == "__main__":
    scan = NoiseOccScan()
    scan.start(**local_configuration)
    print scan.analyze()[:2]
