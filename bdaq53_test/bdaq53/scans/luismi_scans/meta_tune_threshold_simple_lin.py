#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This meta script simply performs a threshold scan to obtain
    the optimal value per pixel to get as close to the target threshold as possible.
    The result is a TDAC mask file.
'''

import zlib  # Workaround
import os
import yaml

import numpy as np
import tables as tb

from bdaq53.meta_scan_base import MetaScanBase
from bdaq53.scans.scan_threshold import ThresholdScan
from bdaq53.scans.scan_noise_occupancy import NoiseOccScan

chip_configuration = 'default_chip.yaml'

local_configuration = { 


    # Scan parameters
    'start_column'    : 128,
    'stop_column'     : 264,
    'start_row'       : 0,
    'stop_row'        : 192,
    'maskfile'        : None,

    # Threshold scan parameters
    'VCAL_MED'        : 500,
    'VCAL_HIGH_start' : 500,
    'VCAL_HIGH_stop'  : 1200,
    'VCAL_HIGH_step'  : 10
}


class MetaTDACTuning(MetaScanBase):
    scan_id = 'meta_tdac_tuning'

    def start(self, **kwargs):
        self.scan(**kwargs)
        self.clean_up()

    def scan(self, **kwargs):
        VCAL_MED = kwargs.get('VCAL_MED', 500)
        tdac = np.zeros((400, 192), dtype=int)
        start_column = kwargs.get('start_column', 0)
        stop_column = kwargs.get('stop_column', 400)
        start_row = kwargs.get('start_row', 0)
        stop_row = kwargs.get('stop_row', 192)
        n_injections = kwargs.get('n_injections', 120)
        
        scan_range_mask = np.zeros((400, 192), dtype=bool)
        scan_range_mask[start_column:stop_column, start_row:stop_row] = True

        min_tdac, max_tdac, scan_len = self.get_tdac_range(start_column, stop_column)
       
        if min_tdac == 0:  # LIN
            tdac[start_column:stop_column, start_row:stop_row] = 7
            tdac[start_column:stop_column, start_row:stop_row:2] = 8
        
        self.n_pixels = (stop_column - start_column) * (stop_row - start_row)
        
        self.maskfile = kwargs.get('maskfile', None)
        
        if self.maskfile:
            if self.maskfile == 'auto':
                self.get_latest_maskfile()
                self.maskfile = self.maskfile

            with tb.open_file(self.maskfile) as in_file:
                self.logger.info('Loading TDAC from file: %s ' % (self.maskfile))
                tdac = in_file.root.TDAC_mask[:]
            
        kwargs['TDAC'] = tdac
        
        np.set_printoptions(linewidth=200)
        
        def correct(inc, tdac):
            kwargs['TDAC'] = tdac
            
            scan = ThresholdScan(record_chip_status=False)
            scan.logger.addHandler(self.fh)
            scan.start(**kwargs)
            scan.analyze(create_pdf=True)
            scan.close()
            self.scans.append(scan)
            
            with tb.open_file(scan.output_filename + '_interpreted.h5') as in_file:
                th = in_file.root.ThresholdMap[:]
                scurve = in_file.root.HistSCurve[:]
                
                th_range = th[start_column:stop_column, start_row:stop_row]
                mean = np.mean(th_range[th_range > 1])
                th_max = np.max(th_range[th_range < (kwargs.get('VCAL_HIGH_stop') - kwargs.get('VCAL_HIGH_start'))]) + 25
                kwargs['VCAL_HIGH_stop'] = kwargs.get('VCAL_HIGH_start') + int(th_max)
                
                tdac_b = tdac.copy()
                for c in range(start_column, stop_column):
                    for r in range(start_row, stop_row):
                        if min_tdac == 0:
                            if th[c, r] > mean :
                                tdac[c, r] = min(tdac[c, r] + inc, 15)
                            else: 
                                tdac[c, r] = max(tdac[c, r] - inc, 0)
                        else:
                            if th[c, r] > mean :
                                tdac[c, r] = max(tdac[c, r] - inc, -15)
                            else: 
                                tdac[c, r] = min(tdac[c, r] + inc, 15)
                            
            if min_tdac == 0:
                bc = np.bincount(tdac[scan_range_mask].flatten(), minlength=scan_len)
            else:
                bc = np.bincount(tdac[scan_range_mask].flatten() + 15, minlength=scan_len)
            self.logger.info('Mean hreshold = %f, Maximum threshold = %f ,TDAC dsitibiution %s' % (mean, th_max, str(bc)))

            return th, scurve, mean, tdac_b
        
        th_scan = [8, 4, 2, 1, 1]  # For DIFF
        if min_tdac == 0:
            th_scan = [4, 2, 1, 1]  # For LIN
            
        if self.maskfile:
            th_scan = [1, 1, 1, 1]  # 4 adjustem steps by 1 TDAC in case mask provided 
            
        for i in th_scan:
            th, scurve, mean, _ = correct(i, tdac)
        
        self.logger.info('Final adjustments.')
        kwargs['VCAL_HIGH_step'] = 5

        th, scurve, mean, tdac0 = correct(1, tdac)
        th1, scurve, mean, tdac1 = correct(1, tdac)
        
        corr = np.abs(th1 - mean) > np.abs(th - mean) 
        for c in range(start_column, stop_column):
                for r in range(start_row, stop_row):
                    if corr[c, r]:
                        tdac[c, r] = tdac0[c, r]
                    else:
                        tdac[c, r] = tdac1[c, r]
        
        kwargs['TDAC'] = tdac
        
        self.logger.info('Masking noisy pixels.')
        kwargs['n_triggers'] = 1e6  # *16
        nos = NoiseOccScan(record_chip_status=False)
        nos.logger.addHandler(self.fh)
        nos.start(**kwargs)
        _, _, n_total_hits, hit_pixels = nos.analyze(create_pdf=False, create_mask_file=False)
        nos.close()
        self.scans.append(nos)
        
        self.disable_mask = np.ones((400, 192), dtype=bool)
        for col in range(start_column, stop_column):
            for row in range(kwargs.get('start_row', 0), kwargs.get('stop_row', 192)):
                if not hit_pixels[col, row]:
                    self.disable_mask[col, row] = False
                            
        kwargs['disable'] = self.disable_mask
        
        self.TDAC_mask = tdac
        
        n_disabled_pixels = (self.disable_mask == False).sum()
        n_disabled_pixels_proc = (float(n_disabled_pixels) / self.n_pixels) * 100
        self.logger.info('%d pixels are disabled. This corresponds to %1.2f%% of %d total pixels.' % (n_disabled_pixels, n_disabled_pixels_proc, self.n_pixels))
        
        kwargs['VCAL_HIGH_step'] = 2
        self.logger.info('Running finall threshold scan')
        scan = ThresholdScan(record_chip_status=False)
        scan.logger.addHandler(self.fh)
        scan.start(**kwargs)
        scan.analyze(create_pdf=True)
        scan.close()
        
        self.logger.info('Tuning finished!')

        self.save_disable_mask(update=False)
        self.save_tdac_mask()
        
    def analyze(self, create_mask_file=True):
        pass


if __name__ == "__main__":
    with open(chip_configuration, 'r') as f:
        configuration = yaml.load(f)
    configuration.update(local_configuration)

    mtt = MetaTDACTuning()
    mtt.start(**configuration)
    mtt.analyze()
