import time
import os
import datetime
import sys
import calendar


#TODO update values of next 3 lines:
starting_dose = 700
#dose_steps_mrad = [400,425,450,475,500]
#dose_steps_mrad = [1,2,3,4,5,6,7,8,9,10, 20, 30, 40, 50, 60,70,80,90,100,200,300,400,500]
#TODO The first value should be the next dose step to reach
dose_steps_mrad= [750,800,850,900,950,1000]
#TODO change the dose with respect the machine
dose_rate_Mrad_h = 3.9
#dose_rate_Mrad_h = 600
#dose_rate_Mrad_h = 3.956 #Dose for 10.8 cm from source, 150KeV, 20mA
#dose_rate_Mrad_h = 3.829174 # Dose for 11.1 cm from the source, 150KeV, 20mA.  
#dose_rate_Mrad_h = 3.587574 # Dose for 11.4 cm from the source, 150KeV, 20mA.

dose_rate_Mrad_s = dose_rate_Mrad_h/3600
time_for_dose_steps_s = [x*3600/dose_rate_Mrad_h for x in dose_steps_mrad]
starting_time = starting_dose*3600/dose_rate_Mrad_h 



yes = {'yes','y', 'ye', ''}
no = {'no','n'}


class Tee(object):
    def __init__(self, *files):
        self.files = files
    def write(self, obj):
        for f in self.files:
            f.write(obj)
            f.flush() # If you want the output to be visible immediately
    def flush(self) :
        for f in self.files:
            f.flush()


def times_tests():
	#TODO Change this when stopping software for corrections and change it with correct starting dose

	directory = '00_TEST_INFO'	
	if not os.path.exists(directory):
    		os.makedirs(directory)	

	directory = '00_TEST_INFO/radiation_doses'		
	if not os.path.exists(directory):
    		os.makedirs(directory)
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	now = datetime.datetime.now()
	time_intervals=[] 
	time_acum = 0

	choice = raw_input('Is the starting date of the irradiation now? [yes/no]').lower()
	if choice in yes:
		startingDate = datetime.datetime.now()
	elif choice in no:
		print("Introduce starting date of the campaign: ")
		year = int(raw_input("Year: "))
		month = int(raw_input("Month: "))
		day = int(raw_input("Day: "))
		hour = int(raw_input("Hour: "))
		minutes = int(raw_input("Minutes: ")) 
		seconds = int(raw_input("Seconds: "))	
		startingDate = datetime.datetime(year, month, day, hour, minutes, seconds)	
	
	orig_stdout = sys.stdout
	out = open (directory + '/' + timestamp +"_radSteps_ESTIMATE.txt", "w")
	sys.stdout = out
	print 'Starting date of the campaign: '+ str(startingDate)
	print 'Starting dose: '+ str(starting_dose)
	print 'Dose rate: '+ str(dose_rate_Mrad_h)+ 'Mrad/h' +"\n"
	

	for index in range(len(time_for_dose_steps_s)):
	
		if (index == 0):
			time_intervals.append(time_for_dose_steps_s[index]-starting_time)
		else: 
			time_intervals.append(time_for_dose_steps_s[index] - time_for_dose_steps_s[index-1] )
		time_acum = time_acum + time_intervals[index]	
		startingDatePlusDelay = startingDate + datetime.timedelta(minutes = time_acum/60) 
		#print "Dose reached after irradiating the chip during this time :%.2f" % (time_acum/60)+"(min), %.2f" % + (time_acum/3600)+"(h) is ",  time_acum/3600 * dose_rate_Mrad_h+starting_dose, "Mrad and will be reached in (taking into account starting date introduced) ", str(startingDatePlusDelay) 
		print "Dose reached after:%.2f" % (time_acum/60)+"(min), %.2f" % + (time_acum/3600)+"(h) is",  time_acum/3600 * dose_rate_Mrad_h+starting_dose, "Mrad and will be reached on", calendar.day_name[startingDatePlusDelay.weekday()], ",", str(startingDatePlusDelay)
		
	
	sys.stdout = orig_stdout
	out.close()
	return time_intervals, dose_steps_mrad


def time_after_sec(delay):	
	now_plus_time_interval = 0	
	now = datetime.datetime.now()	
	now_plus_time_interval = datetime.datetime.now() + datetime.timedelta(seconds = int(delay))
	return now_plus_time_interval


def time_after_time():
	loop = 1
	now_plus_time_interval = 0
	while (loop>0):
	
		now = datetime.datetime.now()
		print "Time right now is: ", now 
		print "This script calculates date after a certain amount of time introduced by the user."
		print " Select time units: "
		print "1 for SECONDS"
		print "2 for MINUTES"
		print "3 for HOURS"
		units = raw_input("write a numer 1-3 and press enter : ")
		print units 
		delay = raw_input("write delay from current date in the units selected in the previous step: " )
		print (units == '1') or (units == '2') or (units == '3')
		if ((units == '1') or (units == '2') or (units == '3')):
			if (units == '1'):	
				now_plus_time_interval = datetime.datetime.now() + datetime.timedelta(seconds = int(delay))
			elif (units == '2'):
				now_plus_time_interval = datetime.datetime.now() + datetime.timedelta(minutes = int(delay))
			elif (units == '3'):
				now_plus_time_interval = datetime.datetime.now() + datetime.timedelta(hours = int(delay))
			print "The current date plus the time interval is : ", now_plus_time_interval
			loop = raw_input( "Introduce 1 to repeat a calculation, 0 to exit")

		else:
			loop = raw_input( "Units selected wrongly, introduce 1 to try again, 0 to exit:  ")






