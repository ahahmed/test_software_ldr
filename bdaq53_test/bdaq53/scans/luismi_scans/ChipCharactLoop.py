import time
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import os
import glob
import datetime
from bdaq53.fifo_readout import FifoReadout
import logging
import sys, select

def waiting_input():
	print "Type 0  and enter if you want to stop. You have ten seconds to answer!"
	i, o, e = select.select( [sys.stdin], [], [], 5 )
	if (i):
		t = sys.stdin.readline().strip()
	else:
	 	print "You said nothing!, the program will keep on running"
		t = 1
	return t

def remove_last_file():
	list_of_files = glob.glob('output_data/*') # * means all if need specific format then *.csv
	latest_file = max(list_of_files, key=os.path.getctime)
	remove = "rm "+ latest_file 
	os.system(remove)

#Functions for analog scans
def analog_scan_no_output():
	os.system("bdaq53 scan_analog_wholematrix")
	for i in range (0,5):#removes the files created by the scan_analog (5)
	    remove_last_file()



#It goes through all the defaul_chip files generated
def ChipCharact():
	directory = 'Chip_Characterization'	
	if not os.path.exists(directory):
    		os.makedirs(directory)
	

	t0 = time.time()
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	#testTag = raw_input('Introduce the tag for the test, WITHOUT SPACES:  ')
	testTag = 'irradiation_'
	
	#testTag = 'AfterDoseOf_100Mrad_RoomT_'
	directory = directory + '/' + testTag + '_' + timestamp
	os.system('mkdir '+directory)
	
	#Temperature sensors, NTC, ring oscillators:
	os.system('python scan_temp_sensors.py')
	os.system('python scan_temp_ntc_accumulative.py')
	os.system('python scan_ring_oscillators.py')


	#All FE tests:
	os.system("bdaq53 scan_digital")
	os.system("mv output_data "+ directory + '/all_digital_scan')  
	os.system('python scan_temp_ntc_accumulative.py')

	os.system("bdaq53 scan_noise_occupancy")
	os.system("bdaq53 scan_analog")
	os.system("mv output_data "+ directory + '/all_analog_scan') 
	os.system('python scan_temp_ntc_accumulative.py')

	#####################################################################################
	#Sync FE:
	os.system("bdaq53 scan_noise_occupancy_sync")
	os.system("bdaq53 scan_analog_sync")
	os.system("mv output_data "+ directory + '/sync_analog_scan') 
	os.system('python scan_temp_ntc_accumulative.py')

	#Temperature sensors, NTC, ring oscillators:
	os.system('python scan_temp_sensors.py')
	os.system('python scan_temp_ntc_accumulative.py')
	os.system('python scan_ring_oscillators.py')

	os.system("bdaq53 scan_noise_occupancy_sync")
	os.system("bdaq53 scan_threshold_sync")
	os.system("mv output_data "+ directory + '/sync_threshold_scan') 
	os.system('python scan_temp_ntc_accumulative.py') 

 
	#Temperature sensors, NTC, ring oscillators:
	os.system('python scan_temp_sensors.py')
	os.system('python scan_temp_ntc_accumulative.py')
	os.system('python scan_ring_oscillators.py')
	

	######################################################################################
	#Lin FE:
	os.system("bdaq53 scan_noise_occupancy_lin")
	os.system("bdaq53 scan_analog_lin")
	os.system("mv output_data "+ directory + '/lin_analog_scan') 
	os.system('python scan_temp_ntc_accumulative.py')

	#Temperature sensors, NTC, ring oscillators:
	os.system('python scan_temp_sensors.py')
	os.system('python scan_temp_ntc_accumulative.py')
	os.system('python scan_ring_oscillators.py')

	os.system("bdaq53 scan_noise_occupancy_lin")
	os.system("bdaq53 scan_threshold_lin")
	os.system("mv output_data "+ directory + '/lin_threshold_scan_lin_untuned') 
	os.system('python scan_temp_ntc_accumulative.py') 
	
	
	#using linear triming TDACs of last tune for a threshold scan:
	os.system("bdaq53 scan_noise_occupancy_lin")
	os.system("cp -r lin_meta_tune_threshold_simple_lastTune_370Mrad/* output_data ") 	
	os.system("bdaq53 scan_threshold_lin")
	os.system("mv output_data "+ directory + '/lin_threshold_scan_lin_withlasttuneat370Mrad')
	'''
	os.system("bdaq53 scan_noise_occupancy_lin")
	os.system("bdaq53 meta_tune_threshold_simple_lin")
	#os.system("cp -r output_data  lin_meta_tune_threshold_simple_lastTune") 
	os.system("mv output_data "+ directory + '/lin_meta_tune_threshold_simple') 
	os.system('python scan_temp_ntc_accumulative.py') 
	'''
	#Temperature sensors, NTC, ring oscillators:
	os.system('python scan_temp_sensors.py')
	os.system('python scan_temp_ntc_accumulative.py')
	os.system('python scan_ring_oscillators.py')

	##################################################################################
	#Diff FE:
	os.system("bdaq53 scan_noise_occupancy_diff")
	os.system("bdaq53 scan_analog_diff")
	os.system("mv output_data "+ directory + '/diff_analog_scan_diff')
	os.system('python scan_temp_ntc_accumulative.py')

	#Temperature sensors, NTC, ring oscillators:
	os.system('python scan_temp_sensors.py')
	os.system('python scan_temp_ntc_accumulative.py')
	os.system('python scan_ring_oscillators.py')

	os.system("bdaq53 scan_noise_occupancy_diff")
	os.system("bdaq53 scan_threshold_diff")
	os.system("mv output_data "+ directory + '/diff_threshold_scan_untuned') 
	os.system('python scan_temp_ntc_accumulative.py')

	#Temperature sensors, NTC, ring oscillators:
	os.system('python scan_temp_sensors.py')
	os.system('python scan_temp_ntc_accumulative.py')
	os.system('python scan_ring_oscillators.py')

	#using diff triming TDACs of last tune for a threshold scan:
	os.system("bdaq53 scan_noise_occupancy_diff")
	os.system("cp -r diff_meta_tune_threshold_simple_lastTune_370Mrad/* output_data ") 	
	os.system("bdaq53 scan_threshold_diff")
	os.system("mv output_data "+ directory + '/diff_threshold_scan_diff_withlasttuneat370Mrad')
	
	'''
	os.system("bdaq53 scan_noise_occupancy_diff")
	os.system("bdaq53 meta_tune_threshold_simple_diff")
	#os.system("cp -r output_data  diff_meta_tune_threshold_simple_lastTune") 
	os.system("mv output_data "+ directory + '/diff_meta_tune_threshold_simple') 
	os.system('python scan_temp_ntc_accumulative.py') 
	'''


	#Noise tune for linear and diff:
	'''
	os.system("bdaq53 scan_noise_occupancy_lin")
	os.system("bdaq53 meta_tune_threshold_noise_lin")
	os.system("mv output_data "+ directory + '/lin_meta_tune_threshold_noise') 
	os.system('python scan_temp_ntc_accumulative.py') 


	os.system("bdaq53 scan_noise_occupancy_diff")
	os.system("bdaq53 meta_tune_threshold_noise_diff")
	os.system("mv output_data "+ directory + '/diff_meta_tune_threshold_noise') 
	os.system('python scan_temp_ntc_accumulative.py') 
	'''
	#Temperature sensors, NTC, ring oscillators:
	os.system('python scan_temp_sensors.py')
	os.system('python scan_temp_ntc_accumulative.py')
	os.system('python scan_ring_oscillators.py')


	tend = time.time()
	print 'Total time needed: ', tend-t0

if __name__ == '__main__':
	while(True):
		ChipCharact()
