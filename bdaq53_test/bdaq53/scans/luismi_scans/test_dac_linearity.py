#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic test scans over the values of the selected DAC and
    measures the resulting analog value with the chip's internal ADC.
'''

import tables as tb
from tqdm import tqdm

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting


local_configuration = {
    # Scan parameters
    'DAC'          : 'VCAL_HIGH',
    'type'         : 'U',
    'value_start'  : 0,
    'value_stop'   : 4096,
    'value_step'   : 100}

class DacTable(tb.IsDescription):
    scan_param_id = tb.UInt32Col(pos=0)
    dac = tb.UInt32Col(pos=1)
    voltage = tb.Float32Col(pos=2)

class LinearityTest(ScanBase):
    scan_id = "dac_linearity_test"

    def configure(self, **kwargs):
        self.chip.set_dacs(**kwargs)
        self.chip.enable_monitor_filter()
        self.chip.enable_monitor_data()


    def scan(self, **kwargs):
        values = range(kwargs.get('value_start'), kwargs.get('value_stop'), kwargs.get('value_step'))
        address = kwargs.get('DAC')
        typ = kwargs.get('type')

        self.logger.info('Starting scan...')

        self.dac_data_table = self.h5_file.create_table(self.h5_file.root, name='dac_data',
                                                        title='dac_data', description=DacTable)
        row = self.dac_data_table.row

        for scan_param_id, value in tqdm(enumerate(values), total=len(values)):
            with self.readout(scan_param_id=scan_param_id):
                self.chip.write_register(register=address, data=value, write=True)
                self.chip.get_ADC(typ, address)
                try:
                    row['voltage'] = self.periphery.get_multimeter_voltage()
                except:
                    pass
                row['scan_param_id'] = scan_param_id
                row['dac'] = value
                row.append()
                self.dac_data_table.flush()
        self.logger.info('Scan finished')


    def analyze(self, create_pdf=True):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5') as a:
            a.analyze_adc_data()
        
        if create_pdf:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.logger.info('Creating selected plots...')
                p.create_parameter_page()
                p.create_dac_linearity_plot()


if __name__ == "__main__":
    scan = LinearityTest()
    scan.start(**local_configuration)
    scan.analyze()