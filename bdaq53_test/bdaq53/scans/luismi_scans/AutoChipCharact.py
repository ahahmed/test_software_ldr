import time
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import os
import glob
import datetime
from bdaq53.fifo_readout import FifoReadout
import logging
import sys, select

def times_tests():
	time_intervals=[] 
	#TODO change next 2 lines, with the starting dose and dose_steps_mrad starting with the following dose value to reach.
	starting_dose = 10
	#TODO change next list, the first value should be the next dose reached
	dose_steps_mrad = [20,30,40,50,60,70,80,90,100,150,200,250,300,350,400,450,500]#,600,700,800,900,1000]#TODO if charact is done at X, this vector has to start at X+ (x+1)
	#TODO update value of dose, next line
	dose_rate_Mrad_h = 3.956 # Dose for 11.4 cm from the source, 150KeV, 20mA.  
	dose_rate_Mrad_s = dose_rate_Mrad_h/3600
	time_for_dose_steps_s = [x*3600/dose_rate_Mrad_h for x in dose_steps_mrad] #time needed for each value of dose
	starting_time = starting_dose*3600/dose_rate_Mrad_h 

	now = datetime.datetime.now()
	time_acum = 0
	for index in range(len(time_for_dose_steps_s)):	
		if (index == 0):
			time_intervals.append(time_for_dose_steps_s[index]-starting_time)
		else: 
			time_intervals.append(time_for_dose_steps_s[index] - time_for_dose_steps_s[index-1] )
		time_acum = time_acum + time_intervals[index]	
		now_plus_time_interval = now + datetime.timedelta(minutes = time_acum/60)
		print "Dose after this time ", time_acum, " (s) is ",  time_acum/3600 * dose_rate_Mrad_h+starting_dose, " and will be reached in ", now_plus_time_interval
	return time_intervals,  dose_steps_mrad


def waiting_input():
	print "Type 0  and enter if you want to stop. You have ten seconds to answer!"
	i, o, e = select.select( [sys.stdin], [], [], 5 )
	if (i):
		t = sys.stdin.readline().strip()
	else:
	 	print "You said nothing!, the program will keep on running"
		t = 1
	return t

def remove_last_file():
	list_of_files = glob.glob('output_data/*') # * means all if need specific format then *.csv
	latest_file = max(list_of_files, key=os.path.getctime)
	remove = "rm "+ latest_file 
	os.system(remove)

#Functions for analog scans
def analog_scan_no_output():
	os.system("bdaq53 scan_analog_wholematrix")
	for i in range (0,5):#removes the files created by the scan_analog (5)
	    remove_last_file()
#Functions for analog scans
def analog_scan_no_output():
	os.system("bdaq53 scan_analog_wholematrix")
	for i in range (0,5):#removes the files created by the scan_analog (5)
	    remove_last_file()

def analog_scan_to_trash():
	directory = 'toTRASH_removeTHIS'
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	if not os.path.exists(directory):
    		os.makedirs(directory)
	os.system("bdaq53 scan_analog")
	os.system("mv output_data "+ directory + '/'+timestamp)

def annealingBiased():
	keepon = 1
	while (keepon > 0):
		analog_scan_no_output()
		keepon = waiting_input()


def whileIrradiation():
	keepon = 1
	while (keepon > 0):
		os.system('python scan_temp_ntc_accumulative.py')
		analog_scan_to_trash()
		os.system('python scan_temp_ntc_accumulative.py')
		os.system('python scan_temp_sensors.py')
		os.system('python scan_temp_ntc_accumulative.py')
		os.system('python scan_ring_oscillators.py')
		os.system('python scan_temp_ntc_accumulative.py')
		keepon = waiting_input()

#It goes through all the defaul_chip files generated
def ChipCharact(tag):

	directory = 'Chip_Characterization'	
	if not os.path.exists(directory):
    		os.makedirs(directory)
	

	t0 = time.time()
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	testTag = 'irradiation_' + str(tag)
	#testTag = 'irradiation_'
	
	#testTag = 'AfterDoseOf_100Mrad_RoomT_'
	directory = directory + '/' + testTag + '_' + timestamp
	os.system('mkdir '+directory)




	#Temperature sensors, NTC, ring oscillators:
	os.system('python scan_temp_sensors.py')
	os.system('python scan_temp_ntc_accumulative.py')
	os.system('python scan_ring_oscillators.py')


	#All FE tests:
	os.system("bdaq53 scan_digital")
	os.system("mv output_data "+ directory + '/all_digital_scan')  
	os.system('python scan_temp_ntc_accumulative.py')

	os.system("bdaq53 scan_noise_occupancy")
	os.system("bdaq53 scan_analog")
	os.system("mv output_data "+ directory + '/all_analog_scan') 
	os.system('python scan_temp_ntc_accumulative.py')


	#Sync FE:
	os.system("bdaq53 scan_noise_occupancy_sync")
	os.system("bdaq53 scan_analog_sync")
	os.system("mv output_data "+ directory + '/sync_analog_scan') 
	os.system('python scan_temp_ntc_accumulative.py')

	os.system("bdaq53 scan_noise_occupancy_sync")
	os.system("bdaq53 scan_threshold_sync")
	os.system("mv output_data "+ directory + '/sync_threshold_scan') 
	os.system('python scan_temp_ntc_accumulative.py')  


	#Lin FE:
	os.system("bdaq53 scan_noise_occupancy_lin")
	os.system("bdaq53 scan_analog_lin")
	os.system("mv output_data "+ directory + '/lin_analog_scan') 
	os.system('python scan_temp_ntc_accumulative.py')

	os.system("bdaq53 scan_noise_occupancy_lin")
	os.system("bdaq53 scan_threshold_lin")
	os.system("mv output_data "+ directory + '/lin_threshold_scan_lin_untuned') 
	os.system('python scan_temp_ntc_accumulative.py') 
	
	'''
	#using linear triming TDACs of last tune for a threshold scan:
	os.system("bdaq53 scan_noise_occupancy_lin")
	os.system("mv lin_meta_tune_threshold_simple_lastTune output_data ") 	
	os.system("bdaq53 scan_threshold_lin")
	os.system("mv output_data "+ directory + '/lin_threshold_scan_lin_withlasttune')
	'''
	os.system("bdaq53 scan_noise_occupancy_lin")
	os.system("bdaq53 meta_tune_threshold_simple_lin")
	os.system("cp -r output_data  lin_meta_tune_threshold_simple_lastTune") 
	os.system("mv output_data "+ directory + '/lin_meta_tune_threshold_simple') 
	os.system('python scan_temp_ntc_accumulative.py') 
	


	
	#Diff FE:
	os.system("bdaq53 scan_noise_occupancy_diff")
	os.system("bdaq53 scan_analog_diff")
	os.system("mv output_data "+ directory + '/diff_analog_scan_diff')
	os.system('python scan_temp_ntc_accumulative.py')

	os.system("bdaq53 scan_noise_occupancy_diff")
	os.system("bdaq53 scan_threshold_diff")
	os.system("mv output_data "+ directory + '/diff_threshold_scan_untuned') 
	os.system('python scan_temp_ntc_accumulative.py')


	#using diff triming TDACs of last tune for a threshold scan:
	os.system("bdaq53 scan_noise_occupancy_diff")
	os.system("mv diff_meta_tune_threshold_simple_lastTune output_data ") 	
	os.system("bdaq53 scan_threshold_diff")
	os.system("mv output_data "+ directory + '/diff_threshold_scan_diff_withlasttune')
	
	'''
	os.system("bdaq53 scan_noise_occupancy_diff")
	os.system("bdaq53 meta_tune_threshold_simple_diff")
	os.system("cp -r output_data  diff_meta_tune_threshold_simple_lastTune") 
	os.system("mv output_data "+ directory + '/diff_meta_tune_threshold_simple') 
	os.system('python scan_temp_ntc_accumulative.py') 
	'''


	#Noise tune for linear and diff:
	'''
	os.system("bdaq53 scan_noise_occupancy_lin")
	os.system("bdaq53 meta_tune_threshold_noise_lin")
	os.system("mv output_data "+ directory + '/lin_meta_tune_threshold_noise') 
	os.system('python scan_temp_ntc_accumulative.py') 


	os.system("bdaq53 scan_noise_occupancy_diff")
	os.system("bdaq53 meta_tune_threshold_noise_diff")
	os.system("mv output_data "+ directory + '/diff_meta_tune_threshold_noise') 
	os.system('python scan_temp_ntc_accumulative.py') 
	'''
	#Temperature sensors, NTC, ring oscillators:
	os.system('python scan_temp_sensors.py')
	os.system('python scan_temp_ntc_accumulative.py')
	os.system('python scan_ring_oscillators.py')





	tend = time.time()
	print 'Total time needed: ', tend-t0

if __name__ == '__main__':
	t0 = time.time()
	time_intervals, dose_steps_mrad = times_tests()
	while(time_intervals):
		t1 = time.time()
		t_loop = t1 - t0
		if(t_loop < time_intervals[0]):
			whileIrradiation()


		else:
			
			t0 = time.time()
			tag =  dose_steps_mrad[0]
			ChipCharact(tag)
			del dose_steps_mrad[0]
			del time_intervals[0]
			print "Last time interval vector after removal in seconds is is: ", time_intervals
			print "Next measurement in ", time_intervals[0]/60, "minutes"
			now = datetime.datetime.now()
			print "Time right now is: ", now 
			now_plus_time_interval = now + datetime.timedelta(minutes = time_intervals[0]/60)
			print "Next measurement will be done in: ", now_plus_time_interval  
			print "******************************************************************************************"
			print "__________________________________________________________________________________________"
			print " "
			print " "

	
	
'''dose = starting_dose +dose_rate_Mrad_h*(time.time()-t)/3600'''







