import time
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import os
import glob
import datetime
from bdaq53.fifo_readout import FifoReadout
import logging
import sys, select

def waiting_input():
	print "Type 0  and enter if you want to stop. You have ten seconds to answer!"
	i, o, e = select.select( [sys.stdin], [], [], 5 )
	if (i):
		t = sys.stdin.readline().strip()
	else:
	 	print "You said nothing!, the program will keep on running"
		t = 1
	return t




def ChipCharact():
	globalTime = sys.argv[1]
	dose = sys.argv[2]
	t0 = time.time()
	time.sleep(2)


	tend = time.time()
	print 'Total time needed: ', tend-t0



	directory = '00_TEST_INFO'
	if not os.path.exists(directory):
    		os.makedirs(directory)
	
	directory = directory + '/time_characterizations'
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	if not os.path.exists(directory):
    		os.makedirs(directory)
		out_T = open( directory + '/'+"ACCUMULATIVE_time_characterizations.txt", "a")
		tobewritten = "%Timestamp   TimeNeededForCharact(s) GlobalTime(s)  Dose(Mrad)\n"
		out_T.write(tobewritten) 
		out_T.close()
	out_T = open( directory + '/'+"ACCUMULATIVE_time_characterizations.txt", "a")
	tobewritten =  str(timestamp) + " " + str(tend-t0) + " " + str(time.time()-float(globalTime))+ ' ' + dose	
	tobewritten = tobewritten + "\n"
	out_T.write(tobewritten)
	out.close()

if __name__ == '__main__':
	 ChipCharact()
