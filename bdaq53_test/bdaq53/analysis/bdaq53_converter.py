'''
This script converts raw data taken with BDAQ53 and RD53 into data file which can be used for testbeam analysis.
'''
import logging
import numpy as np
import tables as tb

from numba import njit
from tqdm import tqdm

from bdaq53.analysis import analysis
import bdaq53.analysis.analysis_utils as au

logger = logging.getLogger('TBA converter')


def analyze_raw_data(raw_data_file, chunk_size):
    '''Analyze raw data with BDAQ53 analysis class to create hit table
    '''
    with analysis.Analysis(raw_data_file=raw_data_file, align_method=1, force_trg_number=True, store_hits=True, chunk_size=chunk_size) as analyze_raw_data:
        analyze_raw_data.analyze_data()


@njit
def fix_event_number(hits, trg_number, offset, force_trg_number):
    ''' Correct event number by trigger number '''

    for hit in hits:
        if hit['event_status'] & au.E_EXT_TRG and (not hit['event_status'] & au.E_EXT_TRG_ERR or force_trg_number):
            if hit['ext_trg_number'] < trg_number:  # Overflow detection
                offset += 2**31
                print 'WARNING: Trigger number overflow detected in event %d', hit['event_number']

            trg_number = hit['ext_trg_number']
            if hit['event_number'] != offset + trg_number:
                print 'WARNING: Incorrect event number in event %i', hit['event_number']
                hit['event_number'] = offset + trg_number
    return hits, trg_number, offset


def align_hit_table(in_file, out_file, force_trg_number, chunk_size):
    ''' Checks the distance between event number and trigger number for each hit.
    If the FE data allowed a successful event recognition the distance is always constant (besides the fact that the trigger number can overflow).
    Otherwise the event number is corrected by the trigger number. How often an inconsistency occurs is counted.

    Note
    ----
    Only one event analyzed wrong shifts all event numbers leading to no correlation! But usually data does not have to be corrected.

    Parameters
    ----------
    input_file : pytables file
    output_file : pytables file
    force_trg_number : boolean
        Force trigger number event alignment, even if number of event headers
        exceeds the number of sub-triggers. Default is False.
    chunk_size :  integer
        Number of hits to read into RAM at once.
    '''

    with tb.open_file(in_file, 'r') as in_file_h5:
        hit_table_dtype = in_file_h5.root.Hits.dtype
        n_hits = in_file_h5.root.Hits.shape[0]
        first_trg_number = in_file_h5.root.Hits[0]['ext_trg_number']
        first_event_number = in_file_h5.root.Hits[0]['event_number']
        offset = 0  # Offset between trigger number and event number

        if first_event_number != first_trg_number:
            logger.warning('First hit has offset between event number (%d) and trigger number (%d)', first_event_number, first_trg_number)
            offset = first_event_number - first_trg_number  # Offset between trigger number and event number
        with tb.open_file(out_file, 'w') as out_file_h5:
            hit_table_out = out_file_h5.create_table(out_file_h5.root, name='Hits', description=hit_table_dtype,
                                                     expectedrows=chunk_size, title='Hit Table for Testbeam Analysis',
                                                     filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            trg_number = first_trg_number  # Trigger number corrected by overflow
            for i in tqdm(range(0, n_hits, chunk_size)):
                hits = in_file_h5.root.Hits[i:i + chunk_size]
                hits, trg_number, offset = fix_event_number(hits, trg_number, offset, force_trg_number)
                hit_table_out.append(hits)


def format_hits(in_file, out_file, chunk_size, transpose=False):
    ''' Format hit table to Testbeam Analysis data format.

    Parameters
    ----------
    input_file : pytables file
    output_file : pytables file
    transpose : boolean
        If True, column and row is switched. Default is False.
    chunk_size : integer
        Number of hits to read into RAM at once.
    '''

    # Define data typ needed for TBA
    description = np.dtype([('event_number', '<i8'), ('frame', 'u1'), ('column', '<u2'), ('row', '<u2'), ('charge', '<u2')])

    with tb.open_file(out_file, mode='w') as out_file_h5:
        hit_table_out = out_file_h5.create_table(out_file_h5.root, name='Hits', description=description, expectedrows=chunk_size,
                                                 title='Hit Table for Testbeam Analysis', filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
        with tb.open_file(in_file, mode='r') as in_file_h5:
            n_hits = in_file_h5.root.Hits.shape[0]
            for i in tqdm(range(0, n_hits, chunk_size)):
                hits = in_file_h5.root.Hits[i:i + chunk_size]
                result = np.zeros(shape=hits.shape[0], dtype=description)
                result['event_number'] = hits['event_number']
                result['frame'] = hits['rel_bcid']
                if transpose:
                    result['column'] = hits['row'] + 1
                    result['row'] = hits['col'] + 1
                else:
                    result['column'] = hits['col'] + 1
                    result['row'] = hits['row'] + 1
                result['charge'] = hits['tot']
                if not np.all(np.diff(result['event_number']) >= 0):
                    raise RuntimeError('The event number does not always increase. This data cannot be used like this!')
                hit_table_out.append(result)


def process_raw_data(raw_data_file, n_trg_num_bits=16, force_trg_number=True, transpose=False, chunk_size=1000000):
    ''' Creates Testbeam Analysis compatible hit tables

        https://github.com/SiLab-Bonn/testbeam_analysis

    Parameters
    ----------
    raw_data_file : pytables file
        with bdaq53 raw data from TLU and RD53A
    n_trg_num_bits : integer
        Number of bits use for trigger number
    force_trg_number : boolean
        Force trigger number event alignment, even if number of event headers
        exceeds the number of sub-triggers. Default is False.
    transpose : boolean
        If True, column and row is switched. Default is False.
    chunk_size : integer
        Number much data is read into RAM at once.
    '''

    if n_trg_num_bits != 16:
        raise NotImplemented('This in not implemented yet, sorry. Leaving it at 16 has no bad consequences')

    analyze_raw_data(raw_data_file, chunk_size)   # Convert raw data to hit table
    # Correct event numbers by external trigger numbers from TLU
    align_hit_table(in_file=raw_data_file[:-3] + '_interpreted.h5',
                    out_file=raw_data_file[:-3] + '_event_aligned.h5',
                    force_trg_number=force_trg_number,
                    chunk_size=chunk_size)
    # Format interpreted data
    format_hits(in_file=raw_data_file[:-3] + '_event_aligned.h5',
                out_file=raw_data_file[:-3] + '_aligned.h5',
                chunk_size=chunk_size,
                transpose=transpose)


if __name__ == "__main__":
    process_raw_data(raw_data_file=r'20180417_181841_ext_trigger_scan.h5')
