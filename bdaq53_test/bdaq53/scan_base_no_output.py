#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import time
import os
import yaml
import logging
import pkg_resources
import inspect

import basil
import git
import zmq
import tables as tb
import numpy as np
from slackclient import SlackClient
from tables.exceptions import NoSuchNodeError
from contextlib import contextmanager

from rd53a import RD53A
from periphery import BDAQ53Periphery
from fifo_readout import FifoReadout
from analysis import analysis_utils as au


VERSION = pkg_resources.get_distribution("bdaq53").version
loglevel = logging.getLogger('RD53A').getEffectiveLevel()


def get_software_version():
    ''' Extract software version from git checkout

        Fallback to package version if no git repo detected.
    '''
    try:
        if os.getenv('CI'):  # gitpython hangs in CI for unknown reasons
            raise git.InvalidGitRepositoryError
        repo = git.Repo(search_parent_directories=True)
        active_branch = repo.active_branch
        rev = active_branch.object.name_rev[:7]
        branch = active_branch.name
        changed_files = [item.a_path for item in repo.index.diff(None)]
        version_string = branch + '@' + rev
        if changed_files:
            version_string += '\nChanged files:' + ', '.join(changed_files)
        return version_string
    except git.InvalidGitRepositoryError:
        return VERSION


class MetaTable(tb.IsDescription):
    index_start = tb.UInt32Col(pos=0)
    index_stop = tb.UInt32Col(pos=1)
    data_length = tb.UInt32Col(pos=2)
    timestamp_start = tb.Float64Col(pos=3)
    timestamp_stop = tb.Float64Col(pos=4)
    scan_param_id = tb.UInt32Col(pos=5)
    error = tb.UInt32Col(pos=6)
    trigger = tb.Float64Col(pos=7)


class RunConfigTable(tb.IsDescription):
    attribute = tb.StringCol(64)
    value = tb.StringCol(128)


class DacTable(tb.IsDescription):
    DAC = tb.StringCol(64)
    value = tb.UInt16Col()


class PowersupplyTable(tb.IsDescription):
    powersupply = tb.StringCol(128, pos=0)
    voltage = tb.Float64Col(pos=1)
    current = tb.Float64Col(pos=2)


def send_data(socket, data, scan_par_id, name='ReadoutData'):
    '''Sends the data of every read out (raw data and meta data)

        via ZeroMQ to a specified socket
    '''

    data_meta_data = dict(
        name=name,
        dtype=str(data[0].dtype),
        shape=data[0].shape,
        timestamp_start=data[1],  # float
        timestamp_stop=data[2],  # float
        error=data[3],  # int
        scan_par_id=scan_par_id
    )
    try:
        socket.send_json(data_meta_data, flags=zmq.SNDMORE | zmq.NOBLOCK)
        # PyZMQ supports sending numpy arrays without copying any data
        socket.send(data[0], flags=zmq.NOBLOCK)
    except zmq.Again:
        pass


class ScanBaseNoOutput(object):
    '''
        Basic run meta class.
        Base class for scan- / tune- / analyze-class.
    '''

    def __init__(self, dut_conf=None, bench_config=None, record_chip_status=True):
        self.configuration = au.ConfigDict()
        self.record_chip_status = record_chip_status
        self.proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        if bench_config is None:
            bench_config = os.path.join(self.proj_dir, 'bdaq53' + os.sep + 'testbench.yaml')

        with open(bench_config) as f:
            self.configuration['bench'] = yaml.load(f)

        if self.configuration['bench']['output_directory'] is not None:
            #TODO
	    self.working_dir = self.configuration['bench']['output_directory']
	    
        else:
            self.working_dir = os.path.join(os.getcwd(), "whileIrradiation")
        if not os.path.exists(self.working_dir):
            os.makedirs(self.working_dir)

        if not os.path.isfile(self.configuration['bench']['chip_configuration']):
            self.configuration['bench']['chip_configuration'] = os.path.join(self.proj_dir, 'bdaq53' + os.sep + 'scans' + os.sep + self.configuration['bench']['chip_configuration'])
        with open(self.configuration['bench']['chip_configuration'], 'r') as f:
            self.configuration['chip'] = yaml.load(f)

        self.timestamp = time.strftime("%Y%m%d_%H%M%S")
        self.run_name = 'toTRASH' + '_' + self.scan_id
	#TODO
        self.output_filename = os.path.join(self.working_dir, self.run_name)
	#self.output_filename = os.path.join(self.working_dir, )
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(loglevel)
        self.logger.success = lambda msg, *args, **kwargs: self.logger.log(logging.SUCCESS, msg, *args, **kwargs)
        self.setup_logfile()

        if self.configuration['chip']['chip_id'] == '0x0000':
            self.logger.warning('You are using the default chip configuration file. Please use a chip specific configuration file!')

        if self.configuration['bench']['slack_token'] != '':
            if os.path.isfile(os.path.expanduser(self.configuration['bench']['slack_token'])):
                token = open(os.path.expanduser(self.configuration['bench']['slack_token']), 'r').read().strip()
            else:
                token = self.configuration['bench']['slack_token']
            self.slack = SlackClient(token)
            if len(self.configuration['bench']['notify_users']) == 1 and self.configuration['bench']['notify_users'] == 'AAAAAA123':
                self.slack = None
        else:
            self.slack = None

        self.logger.info('Initializing %s...', self.__class__.__name__)
        self.periphery = BDAQ53Periphery()
        self.chip = RD53A(dut_conf)

        self.initialized = False

    def get_basil_dir(self):
        return str(os.path.dirname(os.path.dirname(basil.__file__)))

    def get_chip(self):
        return self.chip

    def notify(self, message):
        if self.slack is not None:
            for user in self.configuration['bench']['notify_users']:
                self.slack.api_call('chat.postMessage', channel=user, text=message, username='BDAQ53 Bot', icon_emoji=':robot_face:')

    def load_disable_mask(self, **kwargs):
        mask = np.ones((400, 192), dtype=np.bool)
        disable = kwargs.get('disable', None)

        if self.maskfile:
            try:
                with tb.open_file(self.maskfile, 'r') as infile:
                    mask = infile.root.disable_mask[:]
            except NoSuchNodeError:
                self.logger.debug('Specified maskfile does not include a disable_mask!')
                pass

        if disable is not None:
            if isinstance(disable, list):
                for tup in disable:
                    mask[tup[0], tup[1]] = False
            elif isinstance(disable, np.ndarray):
                mask = disable

        for col in range(400):
            for row in range(192):
                if not mask[col, row]:
                    self.chip.enable_mask[col, row] = False

    def load_enable_mask(self, **kwargs):
        self.chip.enable_mask[kwargs.get('start_column', 0):kwargs.get('stop_column', 400),
                              kwargs.get('start_row', 0):kwargs.get('stop_row', 192)] = True
        if kwargs.get('mask_diff', False):
            self.chip.good_pixel_mask_diff()

        # Enable clock when is necessary (less link issues)
        start_core_column = kwargs.get('start_column', 0) / 8
        stop_core_column = (kwargs.get('stop_column', 400) - 1) / 8 + 1
        self.chip.enable_core_col_clock(range(start_core_column, stop_core_column))

    def load_hitbus_mask(self, **kwargs):
        self.chip.hitbus_mask[kwargs.get('start_column', 0):kwargs.get('stop_column', 400),
                              kwargs.get('start_row', 0):kwargs.get('stop_row', 192)] = True

    def prepare_injection_masks(self, start_column, stop_column, start_row, stop_row, mask_step):

        # Can be possible faster by limit sending row/col address if same row/col to be set

        mask_data = []

        cmd_mem_size = self.chip['cmd'].MEM_BYTES

        flavor_cols = []
        if start_column < 128:
            flavor_cols.append([start_column, min(stop_column, 128), 0])
        if start_column < 264 and stop_column > 128:
            flavor_cols.append([max(start_column, 128), min(stop_column, 264), 1])
        if stop_column > 264:
            flavor_cols.append([max(start_column, 264), stop_column, 2])

        for (start_column_flavor, stop_column_flavor, flavor_id) in flavor_cols:

            cols = (stop_column_flavor - start_column_flavor)
            rows = (stop_row - start_row)

            inj_mask = np.zeros((cols * rows), dtype=bool)
            inj_mask[::mask_step] = True

            # disable flavour that is not scaned (other way sync is doing something bad)
            indata = self.chip.enable_core_col_clock(core_cols=range(start_column_flavor / 8, (stop_column_flavor - 1) / 8 + 1), write=False)

            for step in range(mask_step + 1):
                mask = []

                if(step == mask_step):
                    inj_mask[:] = False

                inj_mask_2d = np.reshape(inj_mask, (cols, rows))

                injection_mask_prev = self.chip.injection_mask.copy()
                self.chip.injection_mask[start_column_flavor:stop_column_flavor, start_row:stop_row] = inj_mask_2d

                # clauclulate the mask diffrence (xor) witin pixel pair (double column)
                injection_mask_xor = np.logical_xor(injection_mask_prev, self.chip.injection_mask)
                injection_mask_xor_dcol = np.logical_or(injection_mask_xor[::2, :], injection_mask_xor[1::2, :])
                to_change = np.where(injection_mask_xor_dcol > 0)

                if step == 0:
                    indata += self.chip.write_register(register='PIX_DEFAULT_CONFIG', data=0, write=False)
                else:
                    indata = self.chip.write_register(register='PIX_DEFAULT_CONFIG', data=0, write=False)

                for i in range(len(to_change[0])):
                    next_cmd = self.chip.write_double_pixel(to_change[0][i], to_change[1][i])

                    if len(indata) + len(next_cmd) + 1 > cmd_mem_size:  # +1 to overcome issue #145
                        mask.append(indata)
                        indata = self.chip.write_register(register='PIX_DEFAULT_CONFIG', data=0, write=False)

                    indata += next_cmd

                mask.append(indata)
                mask_data.append({'flavor': flavor_id, 'command': mask})

                inj_mask[1:] = inj_mask[: -1]  # shift mask
                inj_mask[0] = 0

        return mask_data

    def prepare_logo_mask(self, image='../logo.bmp'):
        from pylab import imread
        img = np.invert(imread(image).astype('bool'))
        img_mask = np.zeros((400, 192), dtype=bool)

        img_mask[105:295, 1:191] = np.transpose(img[:, :, 0])
        img_mask[:, 0] = False
        img_mask[:, -1] = False
        img_mask[0:105, :] = False
        img_mask[295:400, :] = False

        self.chip.enable_mask[:, :] = False
        self.chip.enable_mask[105:295, 1:191] = True

        mask_data = []

        start_row, stop_row = 0, 192
        start_column, stop_column, column_step = 100, 300, 8
        crange = column_step if column_step < (stop_column - start_column) else (stop_column - start_column)
        for start in range(start_column, stop_column, column_step)[::8]:
            stop = start + column_step * 8
            if stop > stop_column:
                stop = stop_column

            for c in range(crange):
                for m in range(4):
                    self.chip.injection_mask[:, :] = False
                    self.chip.injection_mask[start + c:stop:column_step, start_row + m:stop_row:4] = img_mask[start + c:stop:column_step, start_row + m:stop_row:4]

                    write_range = []
                    for i in range(start + c, stop, column_step):
                        write_range.append(i)
                        if not i == 0:
                            write_range.append(i - 1)

                    mask_data.append(self.chip.write_masks(write_range, write=False))

            self.chip.injection_mask[:, :] = False
            mask_data.append(self.chip.write_masks(range(start, stop), write=False))

        return mask_data

    def save_disable_mask(self, update=True):
        self.logger.info('Writing disable mask to file...')
        if not self.maskfile:
            self.maskfile = os.path.join(self.working_dir, self.timestamp + '_mask.h5')

        new_mask = self.disable_mask
        with tb.open_file(self.maskfile, 'a') as out_file:
            try:
                if update:
                    new_mask = out_file.root.disable_mask[:]
                    self.logger.debug('Updating existing mask...')
                    for col in range(400):
                        for row in range(192):
                            if not self.disable_mask[col, row]:
                                new_mask[col, row] = False
                out_file.remove_node(out_file.root.disable_mask)
            except NoSuchNodeError:
                self.logger.debug('Specified maskfile does not include a disable_mask yet!')

            out_file.create_carray(out_file.root,
                                   name='disable_mask',
                                   title='Disable mask',
                                   obj=new_mask,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            self.logger.info('Closing disable mask file: %s' % (self.maskfile))

    def save_tdac_mask(self):
        self.logger.info('Writing TDAC mask to file...')
        if not self.maskfile:
            self.maskfile = os.path.join(self.working_dir, self.timestamp + '_mask.h5')

        with tb.open_file(self.maskfile, 'a') as out_file:
            try:
                out_file.remove_node(out_file.root.TDAC_mask)
            except NoSuchNodeError:
                self.logger.debug('Specified maskfile does not include a TDAC_mask yet!')

            out_file.create_carray(out_file.root,
                                   name='TDAC_mask',
                                   title='TDAC mask',
                                   obj=self.TDAC_mask,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            self.logger.info('Closing TDAC mask file: %s' % (self.maskfile))

    def get_latest_maskfile(self):
        candidates = []
        for _, _, files in os.walk(self.working_dir):
            for f in files:
                if 'mask' in f and f.split('.')[-1] == 'h5':
                    candidates.append({'name': f, 'modified': os.path.getmtime(os.path.join(self.working_dir, f))})

        if len(candidates) == 0:
            self.maskfile = None
            return

        maskfile = candidates[0]
        for candidate in candidates:
            if candidate['modified'] > maskfile['modified']:
                maskfile = candidate

        self.maskfile = os.path.join(self.working_dir, maskfile['name'])

    def dump_configuration(self, **kwargs):
        defaults = inspect.getargspec(self.scan)
        for ix, key in enumerate(defaults.args[1:]):
            if key not in kwargs:
                kwargs[key] = defaults.defaults[ix]

        self.h5_file.create_group(self.h5_file.root, 'configuration', 'Configuration')

        run_config_table = self.h5_file.create_table(self.h5_file.root.configuration, name='run_config', title='Run config', description=RunConfigTable)
        row = run_config_table.row
        row['attribute'] = 'scan_id'
        row['value'] = self.scan_id
        row.append()
        row = run_config_table.row
        row['attribute'] = 'run_name'
        row['value'] = self.run_name
        row.append()
        row = run_config_table.row
        row['attribute'] = 'software_version'
        row['value'] = get_software_version()
        row.append()
        row = run_config_table.row
        row['attribute'] = 'chip_id'
        row['value'] = kwargs.get('chip_id', '0x0000')
        row.append()

        # Set default values for some attributes
        kwargs['start_column'] = kwargs.get('start_column', 0)
        kwargs['stop_column'] = kwargs.get('stop_column', 400)
        kwargs['start_row'] = kwargs.get('start_row', 0)
        kwargs['stop_row'] = kwargs.get('stop_row', 192)
        kwargs['mask_step'] = kwargs.get('mask_step', 192 + 24)

        # attributes which are saved in run_config
        run_config_attributes = ['start_column', 'stop_column', 'start_row', 'stop_row', 'mask_step', 'maskfile',
                                 'scan_timeout', 'max_triggers', 'trigger_latency', 'trigger_delay', 'trigger_length', 'veto_length',
                                 'disable', 'n_injections', 'n_triggers', 'limit', 'VCAL_MED', 'VCAL_HIGH_start', 'VCAL_HIGH_stop',
                                 'VCAL_HIGH_step', 'VTH_start', 'VTH_stop', 'VTH_step', 'VTH_name', 'vth_offset', 'DAC', 'type',
                                 'value_start', 'value_stop', 'value_step', 'addresses', 'scan_interval', 'correct_errors',
                                 'TRIGGER', 'trigger_data_delay']
        for kw, value in kwargs.iteritems():
            if kw not in run_config_attributes:
                continue
            # need to treat TRIGGER configuration differently, since it is a dictionary
            if kw is 'TRIGGER':
                # value is now dict
                for key, val in value.iteritems():
                    row = run_config_table.row
                    row['attribute'] = key
                    row['value'] = val if isinstance(val, str) else str(val)
                    row.append()
            else:
                row = run_config_table.row
                row['attribute'] = kw
                row['value'] = value if isinstance(value, str) else str(value)
                row.append()
        run_config_table.flush()

        if self.chip is not None:
            dac_table = self.h5_file.create_table(self.h5_file.root.configuration, name='dacs', title='DACs', description=DacTable)
            for dac, value in self.chip.dacs.iteritems():
                row = dac_table.row
                row['DAC'] = dac
                row['value'] = value
                row.append()
            dac_table.flush()

            self.h5_file.create_carray(self.h5_file.root.configuration,
                                       name='TDAC_mask',
                                       title='TDAC mask',
                                       obj=self.chip.tdac_mask,
                                       filters=tb.Filters(complib='blosc',
                                                          complevel=5,
                                                          fletcher32=False))

            self.h5_file.create_carray(self.h5_file.root.configuration,
                                       name='enable_mask',
                                       title='Enable mask',
                                       obj=self.chip.enable_mask,
                                       filters=tb.Filters(complib='blosc',
                                                          complevel=5,
                                                          fletcher32=False))

    def init(self, force=False, **kwargs):
        '''
            Initialize hardware and set one time settings (e.g. chip link config)
        '''

        if not self.initialized or force:
            self.logger.debug('Initialize hardware')
            self.periphery.init()

            self.periphery.power_on_BDAQ()
            self.periphery.power_on_SCC(**kwargs)

            self.chip.init(**kwargs)
            self.chip.print_powered_dp_connectors()
            self.chip.init_communication()

            self.initialized = True
        else:
            self.logger.info('Hardware already initialized, skip initialization!')

    def _configure(self, load_hitbus_mask=False, **kwargs):
        '''
            Always needed configuring steps after chip reset and before scan configure
        '''

        self.maskfile = kwargs.get('maskfile', None)
        if self.maskfile == 'auto':
            self.get_latest_maskfile()
            kwargs['maskfile'] = self.maskfile

        self.logger.info('Configuring chip...')
        self.chip.set_dacs(**kwargs)
        self.chip.set_tdac(**kwargs)
        self.load_enable_mask(**kwargs)
        self.load_disable_mask(**kwargs)
        if load_hitbus_mask:
            self.load_hitbus_mask(**kwargs)
        if self.chip.board_version != 'SIMULATION':
            self.chip.write_masks()

        # FIXME: Hotfix for injection based scans after external trigger based scans
        self.chip.write_register(register='LATENCY_CONFIG', data=0b111110100)

    def configure(self, **kwarg):
        self.logger.debug('configure() method not implemented; take std. configuration')

    def _configure_readout(self, **kwargs):
        self._first_read = False
        self.scan_param_id = 0

        self.chip.print_powered_dp_connectors()
        self.fifo_readout = FifoReadout(self.chip)
        self.chip['rx'].reset_counters()

        self.maskfile = kwargs.get('maskfile', None)
        if self.maskfile == 'auto':
            self.get_latest_maskfile()
            kwargs['maskfile'] = self.maskfile

        filename = self.output_filename + '.h5'
        filter_raw_data = tb.Filters(complib='blosc', complevel=5, fletcher32=False)
        self.filter_tables = tb.Filters(complib='zlib', complevel=5, fletcher32=False)
        self.h5_file = tb.open_file(filename, mode='w', title=self.scan_id)
        self.raw_data_earray = self.h5_file.create_earray(self.h5_file.root, name='raw_data', atom=tb.UIntAtom(),
                                                          shape=(0,), title='raw_data', filters=filter_raw_data)
        self.meta_data_table = self.h5_file.create_table(self.h5_file.root, name='meta_data', description=MetaTable,
                                                         title='meta_data', filters=self.filter_tables)

        self.dump_configuration(**kwargs)

        if self.periphery.devices:
            self.h5_file.create_group(self.h5_file.root.configuration, 'powersupply', 'Powersupply values')
            self.ps_currents_before_table = self.h5_file.create_table(self.h5_file.root.configuration.powersupply, name='before_scan', title='Powersupply values before scan', description=PowersupplyTable)
            self.ps_currents_after_table = self.h5_file.create_table(self.h5_file.root.configuration.powersupply, name='after_scan', title='Powersupply values after scan', description=PowersupplyTable)

        # Setup data sending
        socket_addr = kwargs.pop('send_data', 'tcp://127.0.0.1:5500')
        if socket_addr:
            try:
                self.context = zmq.Context()
                self.socket = self.context.socket(zmq.PUB)  # publisher socket
                self.socket.bind(socket_addr)
                self.logger.debug('Sending data to server %s', socket_addr)
            except zmq.error.ZMQError:
                self.logger.exception('Cannot connect to socket for data sending.')
                self.socket = None
        else:
            self.socket = None

    def reset_chip(self):
        self.chip.reset_chip()
        self.chip['rx'].reset_counters()

    def _add_powersupply_info(self):
        if self.periphery.devices:
            ps_voltages, ps_currents = self.periphery.get_power_SCC(log=True)
            for ps, ps_current in ps_currents.iteritems():
                row = self.ps_currents_before_table.row
                row['powersupply'] = ps
                row['voltage'] = ps_voltages[ps]
                row['current'] = ps_current
                row.append()

    def _add_link_info(self):
        # Read Aurora link statistics
        discard_count, soft_error_count, hard_error_count = self.fifo_readout.print_readout_status()

        self.h5_file.create_group(self.h5_file.root.configuration, 'link_status', 'Aurora link status')
        aurora_table = self.h5_file.create_table(self.h5_file.root.configuration.link_status, name='aurora_link', title='Aurora link status', description=RunConfigTable)
        row = aurora_table.row
        row['attribute'] = 'discard_counter'
        row['value'] = discard_count[0]
        row.append()
        row['attribute'] = 'soft_error_counter'
        row['value'] = soft_error_count[0]
        row.append()
        row['attribute'] = 'hard_error_counter'
        row['value'] = hard_error_count[0]
        row.append()
        aurora_table.flush()

    def _add_chip_status(self):
        # Read all important chip values and dump to yaml
        if self.record_chip_status and self.chip.board_version != 'SIMULATION':
            voltages, currents = self.chip.get_chip_status()
            self.h5_file.create_group(self.h5_file.root.configuration, 'chip_status', 'Chip status')
            dac_currents_table = self.h5_file.create_table(self.h5_file.root.configuration.chip_status, name='dac_currents', title='DAC currents', description=RunConfigTable)
            dac_voltages_table = self.h5_file.create_table(self.h5_file.root.configuration.chip_status, name='dac_voltages', title='DAC voltages', description=RunConfigTable)

            for name, value in currents.iteritems():
                row = dac_currents_table.row
                row['attribute'] = name
                row['value'] = value
                row.append()
            dac_currents_table.flush()
            for name, value in voltages.iteritems():
                row = dac_voltages_table.row
                row['attribute'] = name
                row['value'] = value
                row.append()
            dac_voltages_table.flush()

    def start(self, **kwargs):
        '''
            All steps of a complete scan implementation:
            - Initialize hardware (communication, periphery, ...)
            - Configure chip with std. settings
            - Configure chip with scan specific settings
            - Configure raw data handlng (file writing, data sending)
            - Add power supply measurement
            - Run scan routine
            - Add power supply measurement
            - Add end of scan info (link info, chip status)
        '''

        self.configuration['scan'] = kwargs
        # FIXME: Better usage of configuration object instead of kwargs!
        kwargs.update(self.configuration['chip'])
        kwargs.update(self.configuration['bench'])

        self.init(**kwargs)
        self.reset_chip()
        self._configure(**kwargs)  # Masks from default config
        self.configure(**kwargs)  # Configure step of scan	
        self._configure_readout(**kwargs)  # Raw data handling	
        #self._add_powersupply_info()
        self.scan(**kwargs)
        #self._add_powersupply_info()

        #self._add_link_info()
        #self._add_chip_status()

        self.logger.info('Closing raw data file: %s', self.output_filename + '.h5')
        self.h5_file.close()

        if self.socket:
            self.logger.debug('Closing socket connection')
            self.socket.close()
            self.socket = None

        self.analyze()

    def analyze(self):
        self.logger.warning('analyze() method not implemented; do not analyze data')

    def scan(self, **kwargs):
        raise NotImplementedError('ScanBase.scan() not implemented')

    @contextmanager
    def readout(self, *args, **kwargs):
        timeout = kwargs.pop('timeout', 10.0)

        # self.fifo_readout.readout_interval = 10
        if not self._first_read:
            self._first_read = True

            self.chip['rx'].reset_logic()
            time.sleep(0.01)
            self.chip.wait_for_aurora_sync()

        self.start_readout(*args, **kwargs)
        try:
            yield
        finally:
            self.stop_readout(timeout=timeout)

    def start_readout(self, scan_param_id=0, *args, **kwargs):
        # Pop parameters for fifo_readout.start
        callback = kwargs.pop('callback', self.handle_data)
        clear_buffer = kwargs.pop('clear_buffer', False)
        fill_buffer = kwargs.pop('fill_buffer', False)
        reset_sram_fifo = kwargs.pop('reset_sram_fifo', True)
        errback = kwargs.pop('errback', self.handle_err)
        no_data_timeout = kwargs.pop('no_data_timeout', None)
        self.scan_param_id = scan_param_id
        self.fifo_readout.start(reset_sram_fifo=reset_sram_fifo, fill_buffer=fill_buffer, clear_buffer=clear_buffer,
                                callback=callback, errback=errback, no_data_timeout=no_data_timeout)

    def stop_readout(self, timeout=10.0):
        self.fifo_readout.stop(timeout=timeout)

    def handle_data(self, data_tuple):
        '''
            Handling of the data.
        '''
#         get_bin = lambda x, n: format(x, 'b').zfill(n)

        total_words = self.raw_data_earray.nrows

        self.raw_data_earray.append(data_tuple[0])
        self.raw_data_earray.flush()

        len_raw_data = data_tuple[0].shape[0]
        self.meta_data_table.row['timestamp_start'] = data_tuple[1]
        self.meta_data_table.row['timestamp_stop'] = data_tuple[2]
        self.meta_data_table.row['error'] = data_tuple[3]
        self.meta_data_table.row['data_length'] = len_raw_data
        self.meta_data_table.row['index_start'] = total_words
        total_words += len_raw_data
        self.meta_data_table.row['index_stop'] = total_words
        self.meta_data_table.row['scan_param_id'] = self.scan_param_id

        self.meta_data_table.row.append()
        self.meta_data_table.flush()

        if self.socket:
            send_data(self.socket, data=data_tuple, scan_par_id=self.scan_param_id)

    def handle_err(self, exc):
        msg = '%s' % exc[1]
        if msg:
            self.logger.error('%s Aborting run...', msg)
        else:
            self.logger.error('Aborting run...')

    def setup_logfile(self):
        self.fh = logging.FileHandler(self.output_filename + '.log')
        self.fh.setLevel(loglevel)
        self.fh.setFormatter(logging.Formatter("%(asctime)s - [%(name)-15s] - %(levelname)-7s %(message)s"))
        for lg in logging.Logger.manager.loggerDict.itervalues():
            if isinstance(lg, logging.Logger):
                lg.addHandler(self.fh)

        return self.fh

    def close_logfile(self):
        for lg in logging.Logger.manager.loggerDict.itervalues():
            if isinstance(lg, logging.Logger):
                lg.removeHandler(self.fh)

    def close(self):
        self.chip.close()
        self.periphery.close()
        self.close_logfile()
        self.initialized = False
        self.logger.success('All done!')

    def get_tdac_range(self, start_column, stop_column):
        if (start_column >= 128 and stop_column <= 264) or (start_column >= 264 and stop_column <= 400):
            min_tdac, max_tdac, scan_len = au.get_tdac_range(start_column, stop_column)
        elif start_column >= 0 and stop_column <= 128:
            raise RuntimeError('The Synchronous flavour does not require TDAC tuning')
        else:
            raise NotImplemented('Please choose only one flavour at a time to tune')

        return min_tdac, max_tdac, scan_len
