#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import zlib  # workaround
import os
import unittest

import tables as tb
import numpy as np

from bdaq53.analysis import analysis
import bdaq53.analysis.analysis_utils as au
from bdaq53.tests import utils

import bdaq53
from nose.tools import assert_list_equal
bdaq53_path = os.path.dirname(bdaq53.__file__)
data_folder = os.path.abspath(os.path.join(bdaq53_path, '..', 'data',
                                           'fixtures'))


def get_hists(rawdata, trig_pattern=0b11111111111111111111111111111111, align_method=0):
    ''' Helper function to create hists important for event building'''
    data = au.analyze_chunk(rawdata=rawdata,
                            return_hists=('rel_bcid',
                                          'event_status',
                                          'bcid_error'),
                            trig_pattern=trig_pattern,
                            align_method=align_method)
    data['rel_bcid'] = data['rel_bcid'].sum(axis=(0, 1))  # 2D -> 1D
    return data


class TestAnalysis(unittest.TestCase):

    def test_all_ok(self):
        ''' Test event building with good data '''
        raw_data_file = os.path.join(data_folder, 'digital_scan_few_hits.h5')

        n_hits = 400 * 192 * 1  # digital scan with one injection
        with tb.open_file(raw_data_file) as in_file:
            data = get_hists(in_file.root.raw_data[:])
            self.assertTrue(data['rel_bcid'][15] == n_hits)
            self.assertTrue(np.where(data['rel_bcid'] != 0)[0][0] == 15)
            self.assertFalse(np.any(data['event_status']))
            self.assertFalse(np.any(data['bcid_error']))

    def test_event_header_missing(self):
        ''' Test analysis with missing event header(s).

            Parameters:
                - 32 consecutive Trigger

            Expectation:
                - The event is flagged to have a Trigger ID error & BCID error & Event structure error
                - The following event has a Trigger ID error
                - The relative BCID histogram is unaltered
                - Other events are OK
         '''
        raw_data_file = os.path.join(data_folder, 'digital_scan_few_hits.h5')
        n_hits = 400 * 192 * 1  # digital scan with one injection

        # One event header missing
        with tb.open_file(raw_data_file) as in_file:
            # Delete event header
            raw_data = np.delete(in_file.root.raw_data[:], [2, 3])
            data = get_hists(raw_data)
            self.assertTrue(data['rel_bcid'][15] == n_hits - 768)
            self.assertTrue(np.where(data['rel_bcid'] != 0)[0][0] == 15)
            self.assertListEqual(data['event_status'].tolist(),
                                 [0, 0, 0, 1, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0])
            self.assertListEqual(data['bcid_error'].tolist(),
                                 [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0])

        # Three event header missing
        with tb.open_file(raw_data_file) as in_file:
            # Delete event header
            raw_data = np.delete(in_file.root.raw_data[:], [2, 3, 4, 5, 6, 7])
            data = get_hists(raw_data)
            self.assertTrue(data['rel_bcid'][15] == n_hits - 768)
            self.assertTrue(np.where(data['rel_bcid'] != 0)[0][0] == 15)
            self.assertListEqual(data['event_status'].tolist(),
                                 [0, 0, 0, 1, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0])
            self.assertListEqual(data['bcid_error'].tolist(),
                                 [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0])

    def test_event_header_corrupt(self):
        ''' Test analysis with event header with wrong trigger ID and BCID

            Parameters:
                - 32 consecutive Trigger

            Expectation:
                - The event is flagged to have a Trigger ID & Event structure error
                - The following event is flagged to have a Trigger ID & Event structure error
                - The wrongly splitted event adds another Trigger ID & Event structure error
                - The relative BCID histogram is only filled with hits from good events
                - Other events are OK
         '''
        raw_data_file = os.path.join(data_folder, 'digital_scan_few_hits.h5')
        n_hits = 400 * 192 * 1  # digital scan with one injection

        # One event header corrupt
        with tb.open_file(raw_data_file) as in_file:
            raw_data = in_file.root.raw_data[:]
            raw_data[2] |= 0x0020  # Trg ID 1 --> 3
            raw_data[3] |= 0xF  # BCID 9653 --> 9663
            data = get_hists(raw_data)
            self.assertTrue(data['rel_bcid'][15] == n_hits - 768)
            self.assertTrue(np.where(data['rel_bcid'] != 0)[0][0] == 15)
            self.assertListEqual(data['event_status'].tolist(),
                                 [0, 0, 0, 0, 3, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0,
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0])
            self.assertListEqual(data['bcid_error'].tolist(),
                                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0])

    def test_bcid_wrong(self):
        ''' Test analysis with wrong bcid value.

            Parameters:
                - 32 consecutive Trigger

            Expectation:
                - The event is flagged to have a BCID error
                - The relative BCID histogram is unaltered
                - One BCID error at index 1
                - Other events are OK
        '''
        raw_data_file = os.path.join(data_folder, 'digital_scan_few_hits.h5')
        n_hits = 400 * 192 * 1  # digital scan with one injection

        # One event header bcid wrong
        with tb.open_file(raw_data_file) as in_file:
            # Delete event header
            raw_data = in_file.root.raw_data[:]
            raw_data[3] |= 0x2  # BCID 9653 --> 9655
            data = get_hists(raw_data)
            self.assertTrue(data['rel_bcid'][15] == n_hits - 384)
            self.assertTrue(np.where(data['rel_bcid'] != 0)[0][0] == 15)
            self.assertListEqual(data['event_status'].tolist(),
                                 [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0])
            self.assertListEqual(data['bcid_error'].tolist(),
                                 [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0])

        # Three consecutive bcids are equal (2 are wrong)
        with tb.open_file(raw_data_file) as in_file:
            # Delete event header
            raw_data = in_file.root.raw_data[:]
            raw_data[3] |= 0x2  # BCID 9653 --> 9655
            raw_data[5] |= 0x1  # BCID 9654 --> 9655
            data = get_hists(raw_data)
            self.assertTrue(data['rel_bcid'][15] == n_hits - 384)
            self.assertTrue(np.where(data['rel_bcid'] != 0)[0][0] == 15)
            self.assertListEqual(data['event_status'].tolist(),
                                 [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0])
            self.assertListEqual(data['bcid_error'].tolist(),
                                 [0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0])

    def test_trigger_id_wrong(self):
        ''' Test analysis with wrong trigger id value

        Parameters:
            - 32 consecutive Trigger

        Expectation:
            - The event is flagged to have a triger ID error
            - The relative BCID histogram is unaltered
            - Other events are OK
        '''
        raw_data_file = os.path.join(data_folder, 'digital_scan_few_hits.h5')
        n_hits = 400 * 192 * 1  # digital scan with one injection

        # One event header trigger id wrong
        with tb.open_file(raw_data_file) as in_file:
            # Delete event header
            raw_data = in_file.root.raw_data[:]
            raw_data[2] |= 0x0020  # Trg ID 1 --> 3
            data = get_hists(raw_data)
            self.assertTrue(data['rel_bcid'][15] == n_hits - 384)
            self.assertTrue(np.where(data['rel_bcid'] != 0)[0][0] == 15)
            self.assertListEqual(data['event_status'].tolist(),
                                 [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0])
            self.assertFalse(np.count_nonzero(data['bcid_error']))

        # Two consecutive event header trigger id wrong
        with tb.open_file(raw_data_file) as in_file:
            # Delete event header
            raw_data = in_file.root.raw_data[:]
            raw_data[2] |= 0x0020  # Trg ID 1 --> 3
            raw_data[4] |= 0x00F0  # Trg ID 2 --> 15
            data = get_hists(raw_data)
            self.assertTrue(data['rel_bcid'][15] == n_hits - 384)
            self.assertTrue(np.where(data['rel_bcid'] != 0)[0][0] == 15)
            self.assertListEqual(data['event_status'].tolist(),
                                 [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0])
            self.assertFalse(np.count_nonzero(data['bcid_error']))

    def test_small_trigger_window(self):
        ''' Test analysis with small trigger window

        Parameters:
            - 4 consecutive Trigger from one trigger cmd

        Expectation:
            - Event building works --> No event errors, bcid errros
        '''
        raw_data_file = os.path.join(data_folder, 'one_trig_cmd.h5')

        with tb.open_file(raw_data_file) as in_file:
            raw_data = in_file.root.raw_data[:]
            data = get_hists(raw_data, trig_pattern=0b1111)
            self.assertFalse(np.any(data['event_status']))
            self.assertFalse(np.any(data['bcid_error']))
            self.assertTrue(data['rel_bcid'][1] == 1536)

    def test_bcid_1(self):
        ''' Test analysis with hits after second event header

        Parameters:
            - 32 consecutive Trigger
            - Latency that hits occur on second event header

        Expectation:
            - Event building works --> No event errors, bcid errros
        '''
        raw_data_file = os.path.join(data_folder, 'rel_bcid_1.h5')
        # One event header trigger id wrong
        with tb.open_file(raw_data_file) as in_file:
            raw_data = in_file.root.raw_data[:]
            data = get_hists(raw_data)
            self.assertFalse(np.any(data['event_status']))
            self.assertFalse(np.any(data['bcid_error']))
            self.assertTrue(data['rel_bcid'][1] == 1536)

    def test_trigger_pattern(self):
        ''' Test with complex trigger pattern

        Parameters:
            - 32 consecutive Trigger
            - Pattern 0TT0 0TT0 0TT0 0TT0 0TT0 0TT0 0TT0 0TT0
            - Latency that hits occur after second BCID

        Expectation:
            - Event building works --> No event errors, bcid errros
        '''

        raw_data_file = os.path.join(data_folder, 'trig_pattern_0110.h5')

        with tb.open_file(raw_data_file) as in_file:
            raw_data = in_file.root.raw_data[:]
            data = get_hists(raw_data,
                             trig_pattern=0b01100110011001100110011001100110)
            self.assertFalse(np.any(data['event_status']))
            self.assertFalse(np.any(data['bcid_error']))
            self.assertTrue(data['rel_bcid'][0] == 1536)

    def test_too_many_headers(self):
        ''' Check analysis if some events have too many data headers.

            https://gitlab.cern.ch/silab/bdaq53/issues/120

            Parameters:
            - 16 consecutive Trigger
            - Trigger command from trigger pulse
            - No hits, but external TLU trigger
            - 40 Events with 17 consecutive trigger

            Expectation:
            - 40 events with:
              E_STRUCT_WRONG, E_BCID_INC_ERROR, E_TRG_ID_INC_ERROR
              E_EXT_TRG_ERR (since these events are build before the
              trigger number word)
            - The longer events create BCID error at the 2nd
              event header in the BCID error histogram
        '''
        raw_data_file = os.path.join(data_folder, 'ext_trigger_scan.h5')

        with tb.open_file(raw_data_file) as in_file:
            raw_data = in_file.root.raw_data[:]
            data = get_hists(raw_data,
                             trig_pattern=0b1111111111111111)
            self.assertListEqual(data['event_status'].tolist(),
                                 [0, 7958, 0, 40, 40, 0, 0, 0, 40, 40, 0, 0, 0,
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0, 0, 0])
            self.assertFalse(np.any(data['rel_bcid']))
            self.assertEqual(data['bcid_error'][1], 40)

    def test_trigger_alignment(self):
        ''' Check analysis with event alginment at trigger words.

            Parameters:
            - 16 consecutive Trigger
            - Trigger command from trigger pulse
            - No hits, but external TLU trigger
            - 40 Events with 17 consecutive trigger
            - Event alignment at trigger word
            - in total 8000 trigger words

            Expectation:
            - No E_EXT_TRG_ERR, since all numbers are as expected
            - 41 events with: E_STRUCT_WRONG
              since 40 events with too many event headers and
              one event (first trigger) without any event headers
            - 8000 events with trigger (E_EXT_TRG)
        '''
        raw_data_file = os.path.join(data_folder, 'ext_trigger_scan.h5')

        with tb.open_file(raw_data_file) as in_file:
            raw_data = in_file.root.raw_data[:]
            data = get_hists(raw_data,
                             trig_pattern=0b1111111111111111,
                             align_method=1)
            # FIXME: event counts (7999) != # trigger words (8000)
            print data['event_status'].tolist()
            self.assertListEqual(data['event_status'].tolist(),
                                 [0, 7999, 0, 0, 0, 0, 0, 0, 41, 0, 0, 0, 0,
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0, 0, 0])
            self.assertFalse(np.any(data['rel_bcid']))
            self.assertFalse(np.any(data['bcid_error']))

    def test_broken_events(self):
        ''' Test event building with bad data from sync flavor

            Some events have dublicates of data headers and sometimes
            data headers with incorrect BCID and trigger ID.
            This is challenging for event alignment based on number of event headers.

            Expectation:
            - Event alignment recovers quickly after an event with additional event header
              (< 300 bad events)
        '''
        raw_data_file = os.path.join(data_folder, 'sync_analog_scan.h5')

        with tb.open_file(raw_data_file) as in_file:
            data = get_hists(in_file.root.raw_data[:])
            self.assertListEqual(data['event_status'].tolist(),
                                 [0, 0, 0, 281, 281, 0, 0, 0, 201, 0, 0, 0, 0,
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0, 0, 0])

            self.assertTrue(data['rel_bcid'][9] == 2426758)
            self.assertTrue(data['rel_bcid'][10] == 1)


if __name__ == '__main__':
    unittest.main()
